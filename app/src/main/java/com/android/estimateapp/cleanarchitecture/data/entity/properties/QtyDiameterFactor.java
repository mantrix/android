package com.android.estimateapp.cleanarchitecture.data.entity.properties;

public class QtyDiameterFactor extends QtyDiameterPair {

    double lengthFactor;

    public QtyDiameterFactor(int qty, int diameter,double lengthFactor){
        quantity = qty;
        this.diameter = diameter;
        this.lengthFactor = lengthFactor;
    }

    public QtyDiameterFactor(){
    }

    public double getLengthFactor() {
        return lengthFactor;
    }

    public void setLengthFactor(double lengthFactor) {
        this.lengthFactor = lengthFactor;
    }
}
