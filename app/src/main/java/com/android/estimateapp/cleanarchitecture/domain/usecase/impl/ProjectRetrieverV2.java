package com.android.estimateapp.cleanarchitecture.domain.usecase.impl;

import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.BaseUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.ProjectRetrieverUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ProjectRetrieverV2 extends BaseUseCase implements ProjectRetrieverUseCase {

    private ProjectRepository projectRepository;

    @Inject
    public ProjectRetrieverV2(ProjectRepository projectRepository,
                            ThreadExecutorProvider threadExecutorProvider,
                            PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }

    @Override
    public Observable<List<Project>> getUserProjects(boolean reload, int page, int pageSize) {
       return projectRepository.getUserProjects();
    }

    @Override
    public Observable<Project> getProject(String id) {
        return projectRepository.getProject(id);
    }
}
