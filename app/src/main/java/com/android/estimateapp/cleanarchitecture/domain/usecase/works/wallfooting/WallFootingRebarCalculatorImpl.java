package com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;

import java.util.List;

import javax.inject.Inject;

public class WallFootingRebarCalculatorImpl extends BaseCalculator implements WallFootingWorkCalculator {

    @Inject
    public WallFootingRebarCalculatorImpl() {

    }

    @Override
    public RebarMaterialSummary generateSummary(List<WallFootingRebarResult> results) {
        double totalTenMm = 0;
        double totalTwelveMm = 0;
        double totalGiTieWire = 0;

        for (WallFootingRebarResult result : results) {
            totalTenMm += result.getTenRSB();
            totalTwelveMm += result.getTwelveRSB();
            totalGiTieWire += result.getGiTieWire();
        }

        return new RebarMaterialSummary(totalTenMm, totalTwelveMm, totalGiTieWire);
    }

    @Override
    public WallFootingRebarResult calculate(int lengthOfTieWirePerKilo, WallFootingRebarInput input, List<RSBFactor> rsbFactorFactors) {
        // Total Length of Longitudinal bars  = (set/s) x (L) x (# of Bars along L)
        double totalLofLongitudinalBars = input.getSets() * input.getLength() * input.getNumOfBarsAlongL();

        // (roundDown( L / Spacing along L)) x set/s
        int numOfTransverseBars = (int) ((input.getLength() / input.getSpacingAlongL()) * input.getSets());


        double a = 0;
        double b = 0;

        RSBFactor rsbFactorLongitudinal = getRSB(rsbFactorFactors, input.getRsbFactorLongitudinalBars());
        RSBFactor rsbFactorTransverse = getRSB(rsbFactorFactors, input.getRsbFactorTransverseBars());

        if (rsbFactorLongitudinal != null && rsbFactorLongitudinal.getSize() == RsbSize.TEN_MM.getValue()) {
            a = totalLofLongitudinalBars * rsbFactorLongitudinal.getWeight();
        }

        if (rsbFactorTransverse != null && rsbFactorTransverse.getSize() == RsbSize.TEN_MM.getValue()) {
            b = numOfTransverseBars * input.getTransverseBarsUnitLength() * rsbFactorTransverse.getWeight();
        }

        double tenRSB = a + b;

        double c = 0;
        double d = 0;

        if (rsbFactorLongitudinal != null && rsbFactorLongitudinal.getSize() == RsbSize.TWELVE_MM.getValue()) {
            c = totalLofLongitudinalBars * rsbFactorLongitudinal.getWeight();
        }

        if (rsbFactorTransverse != null && rsbFactorTransverse.getSize() == RsbSize.TWELVE_MM.getValue()) {
            d = numOfTransverseBars * input.getTransverseBarsUnitLength() * rsbFactorTransverse.getWeight();
        }

        double twelveRSB = c + d;

        double tieWire = roundHalfUp((input.getNumOfBarsAlongL() * ((int) (input.getLength() / input.getSpacingAlongL())) * input.getTieWireLength() * input.getSets()) / lengthOfTieWirePerKilo);

        WallFootingRebarResult result = new WallFootingRebarResult();
        result.setGiTieWire(tieWire);
        result.setTenRSB(roundHalfUp(tenRSB));
        result.setTwelveRSB(roundHalfUp(twelveRSB));
        result.setTraverseBarCount(numOfTransverseBars);
        result.setLongitudinalBarsTotalLength(totalLofLongitudinalBars);

        return result;
    }
}
