package com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface CHBListContract {

    interface View {
        void displayList(List<CHBInput> chbInputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void reloadList();

        void deleteItem(String inputId);
    }
}
