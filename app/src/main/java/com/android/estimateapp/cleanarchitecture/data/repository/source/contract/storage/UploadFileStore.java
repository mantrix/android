package com.android.estimateapp.cleanarchitecture.data.repository.source.contract.storage;

import android.graphics.Bitmap;

import io.reactivex.Flowable;

public interface UploadFileStore {

    Flowable<Double> uploadProfilePhoto(Bitmap bitmap);
}
