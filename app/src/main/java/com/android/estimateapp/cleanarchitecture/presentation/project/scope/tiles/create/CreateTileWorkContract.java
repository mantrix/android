package com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkResult;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateTileWorkContract {

    interface View {

        void setInput(TileWorkInput input);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(TileWorkResult result);

        void inputSaved();

        void savingError();

        void requestForReCalculation();

        void setMortarClassPickerOptions(List<Grade> classes);

        void setTileTypes(List<TileType> tileTypes);

        void showSavingDialog();

        void showSavingNotAllowedError();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(TileWorkInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
