package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.mortarclass;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

import static com.android.estimateapp.utils.DisplayUtil.toDisplayFormat;

public class CHBConcreteClassFactorFragment  extends BaseFragment implements CHBFactorContract.View {

    public static CHBConcreteClassFactorFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();
        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        CHBConcreteClassFactorFragment fragment = new CHBConcreteClassFactorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.til_chb)
    TextInputLayout tilCHB;

    @BindView(R.id.et_chb)
    TextInputEditText etCHB;

    @Inject
    CHBFactorAdapter adapter;

    @Inject
    CHBFactorContract.UserActionListener presenter;

    double chb;

    NpaLinearLayoutManager npaLinearLayoutManager;

    private ProgressDialog saveProgressDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.chb_concrete_class_factor;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        initRecyclerView();

        presenter.setData(getArguments().getString(Constants.PROJECT_ID), getArguments().getString(Constants.SCOPE_KEY));
        presenter.setView(this);
        presenter.start();

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

    }


    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void displayList(double chb,List<CHBMortarClass> factors) {
        this.chb = chb;
        etCHB.setText(toDisplayFormat(chb));
        adapter.setItems(factors);
    }

    @OnClick(R.id.rl_save)
    public void onSaveClick() {


        String input = etCHB.getText().toString();
        if (input != null && !input.isEmpty()){

            chb = Double.parseDouble(input);
            tilCHB.setError(null);

            if (saveProgressDialog != null) {
                saveProgressDialog.show();
            }
            presenter.updateFactors(chb,adapter.getData());

        } else {
            // chb cannot be empty
            tilCHB.setError(getString(R.string.required_error));
        }
    }

    @Override
    public void factorSaved() {
        if (saveProgressDialog != null) {
            saveProgressDialog.dismiss();
        }
    }

}