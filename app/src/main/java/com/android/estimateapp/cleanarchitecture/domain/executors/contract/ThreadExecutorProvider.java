package com.android.estimateapp.cleanarchitecture.domain.executors.contract;


import io.reactivex.Scheduler;

public interface ThreadExecutorProvider {

    Scheduler ioScheduler();

    Scheduler computationScheduler();
}
