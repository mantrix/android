package com.android.estimateapp.cleanarchitecture.data.entity;

import com.android.estimateapp.cleanarchitecture.data.entity.contract.Subscription;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;

@IgnoreExtraProperties
public class SubscriptionEntity extends Model implements Subscription {

    @PropertyName("paid")
    public boolean isPaid;

    @PropertyName("trial")
    public boolean isTrial;

    @PropertyName("subscribed")
    public boolean isSubscribed;

    @PropertyName("plan")
    public String planName;

    @PropertyName("trialEndAt")
    public String trialEndAt;

    @PropertyName("start")
    public Long start;

    @PropertyName("end")
    public Long end;

    public SubscriptionEntity() {

    }

    public SubscriptionEntity(@Model.InstanceType int type) {
        super(type);
    }


    @Override
    public boolean isPaid() {
        return isPaid;
    }

    @Override
    public String getPlan() {
        return planName;
    }

    @Override
    public boolean isTrial() {
        return isTrial;
    }

    @Override
    public String getTrialEndAt() {
        return trialEndAt;
    }

    @Override
    public boolean isSubscribed() {
        return isSubscribed;
    }

    @Override
    public long getSubscriptionStartTime() {
        return start != null ? start : 0;
    }

    @Override
    public long getSubscriptionEndTime() {
        return end != null ? end : 0;
    }
}
