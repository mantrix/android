package com.android.estimateapp.cleanarchitecture.presentation.summary.beamrebar;

import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarMaterial;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarSummary;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.Observable;

public class BeamRebarSummaryFragment extends BaseFragment implements BeamSummaryContract.View {

    @Inject
    BeamSummaryContract.UserActionListener presenter;

    @Inject
    BeamRebarSummaryAdapter beamRebarSummaryAdapter;

    @BindView(R.id.rv_rebar_summary_list)
    RecyclerView rvRebarSummaryList;
    @BindView(R.id.tv_total)
    TextView tvTotal;


    private String projectId;
    private String scopeKey;

    NpaLinearLayoutManager npaLinearLayoutManager;

    public static BeamRebarSummaryFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        BeamRebarSummaryFragment fragment = new BeamRebarSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.beam_rebar_summary;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {

        extractExtras();
        initRecyclerView();
        presenter.setData(projectId, scopeKey);
        presenter.setView(this);
        presenter.start();
    }

    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
    }

    @Override
    public void displaySummary(BeamRebarSummary beamRebarSummary) {
        Map<Integer, BeamRebarMaterial> materialMap = beamRebarSummary.getBeamRebarMaterialMap();

        double total = 0;
        List<BeamRebarMaterial> list = new ArrayList<>();
        for (Integer key : materialMap.keySet()) {
            BeamRebarMaterial material = materialMap.get(key);
            total += material.getRebarTotal();
            list.add(material);
        }

        list = Observable.fromIterable(list)
                .toSortedList((o1, o2) -> Integer.compare(o1.rsbSize.getValue(), o2.rsbSize.getValue())).blockingGet();

        beamRebarSummaryAdapter.setItems(list);
        displayTotal(total);
    }

    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        rvRebarSummaryList.setLayoutManager(npaLinearLayoutManager);
        rvRebarSummaryList.setAdapter(beamRebarSummaryAdapter);
    }

    @Override
    public void displayTotal(double total) {
        tvTotal.setText(String.format(getString(R.string.label_total), DisplayUtil.toDisplayFormat(total)));
    }
}