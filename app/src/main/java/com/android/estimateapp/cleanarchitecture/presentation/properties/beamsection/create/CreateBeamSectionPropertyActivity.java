package com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.create;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.QtyDiameterFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.QtyDiameterPair;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.ExtraBar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.VerticalBar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterLength;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Stirrups;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateBeamSectionPropertyActivity extends CreateScopeInputBaseActivity implements CreateBeamSectionPropertyContract.View {


    @Inject
    CreateBeamSectionPropertyContract.UserActionListener presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_description)
    TextInputEditText etDescription;
    @BindView(R.id.til_description)
    TextInputLayout tilDescription;
    @BindView(R.id.et_base)
    TextInputEditText etBase;
    @BindView(R.id.til_base)
    TextInputLayout tilBase;
    @BindView(R.id.et_height)
    TextInputEditText etHeight;
    @BindView(R.id.til_height)
    TextInputLayout tilHeight;
    @BindView(R.id.et_area)
    TextInputEditText etArea;
    @BindView(R.id.til_area)
    TextInputLayout tilArea;
    @BindView(R.id.cv_beam_section_info)
    CardView cvBeamSectionInfo;
    @BindView(R.id.ll_header)
    LinearLayout llHeader;
    @BindView(R.id.et_top_bar_qty1)
    TextInputEditText etTopBarQty1;
    @BindView(R.id.til_top_bar_qty1)
    TextInputLayout tilTopBarQty1;
    @BindView(R.id.et_top_bar_diameter1)
    TextInputEditText etTopBarDiameter1;
    @BindView(R.id.til_top_bar_diameter1)
    TextInputLayout tilTopBarDiameter1;
    @BindView(R.id.et_top_bar_qty2)
    TextInputEditText etTopBarQty2;
    @BindView(R.id.til_top_bar_qty2)
    TextInputLayout tilTopBarQty2;
    @BindView(R.id.et_top_bar_diameter2)
    TextInputEditText etTopBarDiameter2;
    @BindView(R.id.til_top_bar_diameter2)
    TextInputLayout tilTopBarDiameter2;
    @BindView(R.id.cv_top_bar)
    CardView cvTopBar;
    @BindView(R.id.et_bottom_bar_qty1)
    TextInputEditText etBottomBarQty1;
    @BindView(R.id.til_bottom_bar_qty1)
    TextInputLayout tilBottomBarQty1;
    @BindView(R.id.et_bottom_bar_diameter1)
    TextInputEditText etBottomBarDiameter1;
    @BindView(R.id.til_bottom_bar_diameter1)
    TextInputLayout tilBottomBarDiameter1;
    @BindView(R.id.et_bottom_bar_qty2)
    TextInputEditText etBottomBarQty2;
    @BindView(R.id.til_bottom_bar_qty2)
    TextInputLayout tilBottomBarQty2;
    @BindView(R.id.et_bottom_bar_diameter2)
    TextInputEditText etBottomBarDiameter2;
    @BindView(R.id.til_bottom_bar_diameter2)
    TextInputLayout tilBottomBarDiameter2;
    @BindView(R.id.cv_bottom_bar)
    CardView cvBottomBar;
    @BindView(R.id.et_left_bar_qty1)
    TextInputEditText etLeftBarQty1;
    @BindView(R.id.til_left_bar_qty1)
    TextInputLayout tilLeftBarQty1;
    @BindView(R.id.et_left_bar_diameter1)
    TextInputEditText etLeftBarDiameter1;
    @BindView(R.id.til_left_bar_diameter1)
    TextInputLayout tilLeftBarDiameter1;
    @BindView(R.id.et_left_bar_f1)
    TextInputEditText etLeftBarF1;
    @BindView(R.id.til_left_bar_f1)
    TextInputLayout tilLeftBarF1;
    @BindView(R.id.et_left_bar_qty2)
    TextInputEditText etLeftBarQty2;
    @BindView(R.id.til_left_bar_qty2)
    TextInputLayout tilLeftBarQty2;
    @BindView(R.id.et_left_bar_diameter2)
    TextInputEditText etLeftBarDiameter2;
    @BindView(R.id.til_left_bar_diameter2)
    TextInputLayout tilLeftBarDiameter2;
    @BindView(R.id.et_left_bar_f2)
    TextInputEditText etLeftBarF2;
    @BindView(R.id.til_left_bar_f2)
    TextInputLayout tilLeftBarF2;
    @BindView(R.id.cv_left_bar)
    CardView cvLeftBar;
    @BindView(R.id.et_mid_bar_qty1)
    TextInputEditText etMidBarQty1;
    @BindView(R.id.til_mid_bar_qty1)
    TextInputLayout tilMidBarQty1;
    @BindView(R.id.et_mid_bar_diameter1)
    TextInputEditText etMidBarDiameter1;
    @BindView(R.id.til_mid_bar_diameter1)
    TextInputLayout tilMidBarDiameter1;
    @BindView(R.id.et_mid_bar_f1)
    TextInputEditText etMidBarF1;
    @BindView(R.id.til_mid_bar_f1)
    TextInputLayout tilMidBarF1;
    @BindView(R.id.et_mid_bar_qty2)
    TextInputEditText etMidBarQty2;
    @BindView(R.id.til_mid_bar_qty2)
    TextInputLayout tilMidBarQty2;
    @BindView(R.id.et_mid_bar_diameter2)
    TextInputEditText etMidBarDiameter2;
    @BindView(R.id.til_mid_bar_diameter2)
    TextInputLayout tilMidBarDiameter2;
    @BindView(R.id.et_mid_bar_f2)
    TextInputEditText etMidBarF2;
    @BindView(R.id.til_mid_bar_f2)
    TextInputLayout tilMidBarF2;
    @BindView(R.id.cv_mid_bar)
    CardView cvMidBar;
    @BindView(R.id.et_right_bar_qty1)
    TextInputEditText etRightBarQty1;
    @BindView(R.id.til_right_bar_qty1)
    TextInputLayout tilRightBarQty1;
    @BindView(R.id.et_right_bar_diameter1)
    TextInputEditText etRightBarDiameter1;
    @BindView(R.id.til_right_bar_diameter1)
    TextInputLayout tilRightBarDiameter1;
    @BindView(R.id.et_right_bar_f1)
    TextInputEditText etRightBarF1;
    @BindView(R.id.til_right_bar_f1)
    TextInputLayout tilRightBarF1;
    @BindView(R.id.et_right_bar_qty2)
    TextInputEditText etRightBarQty2;
    @BindView(R.id.til_right_bar_qty2)
    TextInputLayout tilRightBarQty2;
    @BindView(R.id.et_right_bar_diameter2)
    TextInputEditText etRightBarDiameter2;
    @BindView(R.id.til_right_bar_diameter2)
    TextInputLayout tilRightBarDiameter2;
    @BindView(R.id.et_right_bar_f2)
    TextInputEditText etRightBarF2;
    @BindView(R.id.til_right_bar_f2)
    TextInputLayout tilRightBarF2;
    @BindView(R.id.cv_right_bar)
    CardView cvRightBar;
    @BindView(R.id.et_a_qty)
    TextInputEditText etAQty;
    @BindView(R.id.til_a_qty)
    TextInputLayout tilAQty;
    @BindView(R.id.et_a_spacing)
    TextInputEditText etASpacing;
    @BindView(R.id.til_a_spacing)
    TextInputLayout tilASpacing;
    @BindView(R.id.et_b_qty)
    TextInputEditText etBQty;
    @BindView(R.id.til_b_qty)
    TextInputLayout tilBQty;
    @BindView(R.id.et_b_spacing)
    TextInputEditText etBSpacing;
    @BindView(R.id.til_b_spacing)
    TextInputLayout tilBSpacing;
    @BindView(R.id.et_c_qty)
    TextInputEditText etCQty;
    @BindView(R.id.til_c_qty)
    TextInputLayout tilCQty;
    @BindView(R.id.et_c_spacing)
    TextInputEditText etCSpacing;
    @BindView(R.id.til_c_spacing)
    TextInputLayout tilCSpacing;
    @BindView(R.id.et_the_rest_spacing)
    TextInputEditText etTheRestSpacing;
    @BindView(R.id.til_the_rest_spacing)
    TextInputLayout tilTheRestSpacing;
    @BindView(R.id.et_d_qty)
    TextInputEditText etDQty;
    @BindView(R.id.til_d_qty)
    TextInputLayout tilDQty;
    @BindView(R.id.et_d_spacing)
    TextInputEditText etDSpacing;
    @BindView(R.id.til_d_spacing)
    TextInputLayout tilDSpacing;
    @BindView(R.id.et_e_qty)
    TextInputEditText etEQty;
    @BindView(R.id.til_e_qty)
    TextInputLayout tilEQty;
    @BindView(R.id.et_e_spacing)
    TextInputEditText etESpacing;
    @BindView(R.id.til_e_spacing)
    TextInputLayout tilESpacing;
    @BindView(R.id.et_f_qty)
    TextInputEditText etFQty;
    @BindView(R.id.til_f_qty)
    TextInputLayout tilFQty;
    @BindView(R.id.et_f_spacing)
    TextInputEditText etFSpacing;
    @BindView(R.id.til_f_spacing)
    TextInputLayout tilFSpacing;
    @BindView(R.id.et_g_diameter)
    TextInputEditText etGDiameter;
    @BindView(R.id.til_g_diameter)
    TextInputLayout tilGDiameter;
    @BindView(R.id.et_g_length)
    TextInputEditText etGLength;
    @BindView(R.id.til_g_length)
    TextInputLayout tilGLength;
    @BindView(R.id.cv_stirrups)
    CardView cvStirrups;
    @BindView(R.id.et_splicing_qty1)
    TextInputEditText etSplicingQty1;
    @BindView(R.id.til_splicing_qty1)
    TextInputLayout tilSplicingQty1;
    @BindView(R.id.et_splicing_diameter1)
    TextInputEditText etSplicingDiameter1;
    @BindView(R.id.til_splicing_diameter1)
    TextInputLayout tilSplicingDiameter1;
    @BindView(R.id.et_splicing_length1)
    TextInputEditText etSplicingLength1;
    @BindView(R.id.til_splicing_length1)
    TextInputLayout tilSplicingLength1;
    @BindView(R.id.et_splicing_qty2)
    TextInputEditText etSplicingQty2;
    @BindView(R.id.til_splicing_qty2)
    TextInputLayout tilSplicingQty2;
    @BindView(R.id.et_splicing_diameter2)
    TextInputEditText etSplicingDiameter2;
    @BindView(R.id.til_splicing_diameter2)
    TextInputLayout tilSplicingDiameter2;
    @BindView(R.id.et_splicing_length2)
    TextInputEditText etSplicingLength2;
    @BindView(R.id.til_splicing_length2)
    TextInputLayout tilSplicingLength2;
    @BindView(R.id.cv_splicing)
    CardView cvSplicing;
    @BindView(R.id.et_hookes_qty1)
    TextInputEditText etHookesQty1;
    @BindView(R.id.til_hookes_qty1)
    TextInputLayout tilHookesQty1;
    @BindView(R.id.et_hookes_diameter1)
    TextInputEditText etHookesDiameter1;
    @BindView(R.id.til_hookes_diameter1)
    TextInputLayout tilHookesDiameter1;
    @BindView(R.id.et_hookes_length1)
    TextInputEditText etHookesLength1;
    @BindView(R.id.til_hookes_length1)
    TextInputLayout tilHookesLength1;
    @BindView(R.id.et_hookes_qty2)
    TextInputEditText etHookesQty2;
    @BindView(R.id.til_hookes_qty2)
    TextInputLayout tilHookesQty2;
    @BindView(R.id.et_hookes_diameter2)
    TextInputEditText etHookesDiameter2;
    @BindView(R.id.til_hookes_diameter2)
    TextInputLayout tilHookesDiameter2;
    @BindView(R.id.et_hookes_length2)
    TextInputEditText etHookesLength2;
    @BindView(R.id.til_hookes_length2)
    TextInputLayout tilHookesLength2;
    @BindView(R.id.cv_hookes)
    CardView cvHookes;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.tv_stirrups_total_qty)
    TextView tvStirrupsTotalQty;


    private String projectId;
    private String itemKey;

    private BeamSectionProperties input;

    private ProgressDialog saveProgressDialog;

    private boolean updateContent;

    AlertDialog rsbSizePickerDialog;
    RSBInputLoc selectedRsbInputLoc;

    VerticalBar topBar;
    QtyDiameterPair topBarProperty1;
    QtyDiameterPair topBarProperty2;

    VerticalBar bottomBar;
    QtyDiameterPair bottomBarProperty1;
    QtyDiameterPair bottomBarProperty2;

    ExtraBar leftBar;
    QtyDiameterFactor leftBarProperty1;
    QtyDiameterFactor leftBarProperty2;

    ExtraBar midBar;
    QtyDiameterFactor midBarProperty1;
    QtyDiameterFactor midBarProperty2;

    ExtraBar rightBar;
    QtyDiameterFactor rightBarProperty1;
    QtyDiameterFactor rightBarProperty2;

    QtyDiameterSpacing stirrupsA;
    QtyDiameterSpacing stirrupsB;
    QtyDiameterSpacing stirrupsC;
    QtyDiameterSpacing stirrupsTheRest;
    QtyDiameterSpacing stirrupsD;
    QtyDiameterSpacing stirrupsE;
    QtyDiameterSpacing stirrupsF;
    QtyDiameterSpacing stirrupsG;

    Stirrups stirrups;
    QtyDiameterLength splicing1;
    QtyDiameterLength splicing2;
    QtyDiameterLength hookes1;
    QtyDiameterLength hookes2;


    enum RSBInputLoc {
        TOP_BAR_1,
        TOP_BAR_2,
        BOTTOM_BAR_1,
        BOTTOM_BAR_2,
        LEFT_BAR_1,
        LEFT_BAR_2,
        MID_BAR_1,
        MID_BAR_2,
        RIGHT_BAR_1,
        RIGHT_BAR_2,
        STIRRUPS_G,
        SPLICING_1,
        SPLICING_2,
        HOOKES_1,
        HOOKES_2
    }


    public static Intent createIntent(Context context, String projectId, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateBeamSectionPropertyActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.beam_section_property_input_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(getString(R.string.label_beam_section_properties));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        tvStirrupsTotalQty.setText(String.format(getString(R.string.label_total_quantity), Integer.toString(0)));

        input = new BeamSectionProperties();

        presenter.setView(this);
        presenter.setData(projectId, itemKey);
        presenter.start();

        topBar = new VerticalBar();
        topBarProperty1 = new QtyDiameterPair();
        topBarProperty2 = new QtyDiameterPair();

        bottomBar = new VerticalBar();
        bottomBarProperty1 = new QtyDiameterPair();
        bottomBarProperty2 = new QtyDiameterPair();

        leftBar = new ExtraBar();
        leftBarProperty1 = new QtyDiameterFactor();
        leftBarProperty2 = new QtyDiameterFactor();

        midBar = new ExtraBar();
        midBarProperty1 = new QtyDiameterFactor();
        midBarProperty2 = new QtyDiameterFactor();

        rightBar = new ExtraBar();
        rightBarProperty1 = new QtyDiameterFactor();
        rightBarProperty2 = new QtyDiameterFactor();

        stirrups = new Stirrups();
        stirrupsA = new QtyDiameterSpacing();
        stirrupsB = new QtyDiameterSpacing();
        stirrupsC = new QtyDiameterSpacing();
        stirrupsTheRest = new QtyDiameterSpacing();
        stirrupsD = new QtyDiameterSpacing();
        stirrupsE = new QtyDiameterSpacing();
        stirrupsF = new QtyDiameterSpacing();
        stirrupsG = new QtyDiameterSpacing();

        splicing1 = new QtyDiameterLength();
        splicing2 = new QtyDiameterLength();
        hookes1 = new QtyDiameterLength();
        hookes2 = new QtyDiameterLength();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }


    @Override
    public void setRSBPickerOptions(List<RsbSize> sizes) {
        String[] options = new String[sizes.size()];
        for (int i = 0; i < sizes.size(); i++) {
            options[i] = sizes.get(i).toString();
        }

        rsbSizePickerDialog = createChooser(getString(R.string.diameter_symbol), options, (dialog, which) -> {

            setRSBInputValue(sizes.get(which));

            dialog.dismiss();
        });
    }

    @OnClick({R.id.et_top_bar_diameter1,
            R.id.et_top_bar_diameter2,
            R.id.et_bottom_bar_diameter1,
            R.id.et_bottom_bar_diameter2,
            R.id.et_left_bar_diameter1,
            R.id.et_left_bar_diameter2,
            R.id.et_mid_bar_diameter1,
            R.id.et_mid_bar_diameter2,
            R.id.et_right_bar_diameter1,
            R.id.et_right_bar_diameter2,
            R.id.et_g_diameter,
            R.id.et_splicing_diameter1,
            R.id.et_splicing_diameter2,
            R.id.et_hookes_diameter1,
            R.id.et_hookes_diameter2})
    protected void diameterFieldClick(View view) {

        switch (view.getId()) {
            case R.id.et_top_bar_diameter1:
                selectedRsbInputLoc = RSBInputLoc.TOP_BAR_1;
                break;
            case R.id.et_top_bar_diameter2:
                selectedRsbInputLoc = RSBInputLoc.TOP_BAR_2;
                break;
            case R.id.et_bottom_bar_diameter1:
                selectedRsbInputLoc = RSBInputLoc.BOTTOM_BAR_1;
                break;
            case R.id.et_bottom_bar_diameter2:
                selectedRsbInputLoc = RSBInputLoc.BOTTOM_BAR_2;
                break;
            case R.id.et_left_bar_diameter1:
                selectedRsbInputLoc = RSBInputLoc.LEFT_BAR_1;
                break;
            case R.id.et_left_bar_diameter2:
                selectedRsbInputLoc = RSBInputLoc.LEFT_BAR_2;
                break;
            case R.id.et_mid_bar_diameter1:
                selectedRsbInputLoc = RSBInputLoc.MID_BAR_1;
                break;
            case R.id.et_mid_bar_diameter2:
                selectedRsbInputLoc = RSBInputLoc.MID_BAR_2;
                break;
            case R.id.et_right_bar_diameter1:
                selectedRsbInputLoc = RSBInputLoc.RIGHT_BAR_1;
                break;
            case R.id.et_right_bar_diameter2:
                selectedRsbInputLoc = RSBInputLoc.RIGHT_BAR_2;
                break;
            case R.id.et_g_diameter:
                selectedRsbInputLoc = RSBInputLoc.STIRRUPS_G;
                break;
            case R.id.et_splicing_diameter1:
                selectedRsbInputLoc = RSBInputLoc.SPLICING_1;
                break;
            case R.id.et_splicing_diameter2:
                selectedRsbInputLoc = RSBInputLoc.SPLICING_2;
                break;
            case R.id.et_hookes_diameter1:
                selectedRsbInputLoc = RSBInputLoc.HOOKES_1;
                break;
            case R.id.et_hookes_diameter2:
                selectedRsbInputLoc = RSBInputLoc.HOOKES_2;
                break;
        }

        rsbSizePickerDialog.show();

    }

    private void setRSBInputValue(RsbSize rsbSize) {
        TextView textView = null;
        switch (selectedRsbInputLoc) {
            case TOP_BAR_1:
                topBarProperty1.setDiameter(rsbSize.getValue());
                textView = etTopBarDiameter1;
                break;
            case TOP_BAR_2:
                topBarProperty2.setDiameter(rsbSize.getValue());
                textView = etTopBarDiameter2;
                break;
            case BOTTOM_BAR_1:
                bottomBarProperty1.setDiameter(rsbSize.getValue());
                textView = etBottomBarDiameter1;
                break;
            case BOTTOM_BAR_2:
                bottomBarProperty2.setDiameter(rsbSize.getValue());
                textView = etBottomBarDiameter2;
                break;
            case LEFT_BAR_1:
                leftBarProperty1.setDiameter(rsbSize.getValue());
                textView = etLeftBarDiameter1;
                break;
            case LEFT_BAR_2:
                leftBarProperty2.setDiameter(rsbSize.getValue());
                textView = etLeftBarDiameter2;
                break;
            case MID_BAR_1:
                midBarProperty1.setDiameter(rsbSize.getValue());
                textView = etMidBarDiameter1;
                break;
            case MID_BAR_2:
                midBarProperty2.setDiameter(rsbSize.getValue());
                textView = etMidBarDiameter2;
                break;
            case RIGHT_BAR_1:
                rightBarProperty1.setDiameter(rsbSize.getValue());
                textView = etRightBarDiameter1;
                break;
            case RIGHT_BAR_2:
                rightBarProperty2.setDiameter(rsbSize.getValue());
                textView = etRightBarDiameter2;
                break;
            case STIRRUPS_G:
                stirrupsG.setDiameter(rsbSize.getValue());
                textView = etGDiameter;
                break;
            case SPLICING_1:
                splicing1.setDiameter(rsbSize.getValue());
                textView = etSplicingDiameter1;
                break;
            case SPLICING_2:
                splicing2.setDiameter(rsbSize.getValue());
                textView = etSplicingDiameter2;
                break;
            case HOOKES_1:
                hookes1.setDiameter(rsbSize.getValue());
                textView = etHookesDiameter1;
                break;
            case HOOKES_2:
                hookes2.setDiameter(rsbSize.getValue());
                textView = etHookesDiameter2;
                break;
        }

        if (textView != null){
            textView.setText(Integer.toString(rsbSize.getValue()));
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void setInput(BeamSectionProperties input) {
        this.input = input;
        if (input.getDescription() != null) {
            etDescription.setText(input.getDescription());
        }

        if (input.getBase() != 0) {
            etBase.setText(toDisplayFormat(input.getBase()));
        }

        if (input.getHeight() != 0) {
            etHeight.setText(toDisplayFormat(input.getHeight()));
        }

        etArea.setText(toDisplayFormat(input.getArea()));


        if (input.getTopBar() != null) {

            topBar = input.getTopBar();

            if (topBar.getProperty1() != null) {
                topBarProperty1 = topBar.getProperty1();

                etTopBarQty1.setText(toDisplayFormat(topBarProperty1.getQuantity()));

                if (topBarProperty1.getDiameter() != 0)
                    etTopBarDiameter1.setText(Integer.toString(topBarProperty1.getDiameter()));
            }

            if (topBar.getProperty2() != null) {
                topBarProperty2 = topBar.getProperty2();

                etTopBarQty2.setText(toDisplayFormat(topBarProperty2.getQuantity()));

                if (topBarProperty2.getDiameter() != 0)
                    etTopBarDiameter2.setText(Integer.toString(topBarProperty2.getDiameter()));
            }
        }

        if (input.getBottomBar() != null) {
            bottomBar = input.getBottomBar();

            if (bottomBar.getProperty1() != null) {
                bottomBarProperty1 = bottomBar.getProperty1();

                etBottomBarQty1.setText(toDisplayFormat(bottomBarProperty1.getQuantity()));

                if (bottomBarProperty1.getDiameter() != 0)
                    etBottomBarDiameter1.setText(Integer.toString(bottomBarProperty1.getDiameter()));
            }

            if (bottomBar.getProperty2() != null) {
                bottomBarProperty2 = bottomBar.getProperty2();

                etBottomBarQty2.setText(toDisplayFormat(bottomBarProperty2.getQuantity()));

                if (bottomBarProperty2.getDiameter() != 0)
                    etBottomBarDiameter2.setText(Integer.toString(bottomBarProperty2.getDiameter()));
            }
        }




        if (input.getExtraBarAtLeftSupport() != null) {

            leftBar = input.getExtraBarAtLeftSupport();

            if (leftBar.getProperty1() != null) {
                leftBarProperty1 = leftBar.getProperty1();

                etLeftBarQty1.setText(toDisplayFormat(leftBarProperty1.getQuantity()));
                etLeftBarF1.setText(toDisplayFormat(leftBarProperty1.getLengthFactor()));

                if (leftBarProperty1.getDiameter() != 0)
                    etLeftBarDiameter1.setText(Integer.toString(leftBarProperty1.getDiameter()));
            }

            if (leftBar.getProperty2() != null) {
                leftBarProperty2 = leftBar.getProperty2();

                etLeftBarQty2.setText(toDisplayFormat(leftBarProperty2.getQuantity()));
                etLeftBarF2.setText(toDisplayFormat(leftBarProperty2.getLengthFactor()));

                if (leftBarProperty2.getDiameter() != 0)
                    etLeftBarDiameter2.setText(Integer.toString(leftBarProperty2.getDiameter()));
            }
        }


        if (input.getBottomExtraAtMid() != null) {
            midBar = input.getBottomExtraAtMid();

            if (midBar.getProperty1() != null) {
                midBarProperty1 = midBar.getProperty1();

                etMidBarQty1.setText(toDisplayFormat(midBarProperty1.getQuantity()));
                etMidBarF1.setText(toDisplayFormat(midBarProperty1.getLengthFactor()));

                if (midBarProperty1.getDiameter() != 0)
                    etMidBarDiameter1.setText(Integer.toString(midBarProperty1.getDiameter()));
            }

            if (midBar.getProperty2() != null) {
                midBarProperty2 = midBar.getProperty2();
                etMidBarQty2.setText(toDisplayFormat(midBarProperty2.getQuantity()));
                etMidBarF2.setText(toDisplayFormat(midBarProperty2.getLengthFactor()));

                if (midBarProperty2.getDiameter() != 0)
                    etMidBarDiameter2.setText(Integer.toString(midBarProperty2.getDiameter()));
            }
        }



        if (input.getExtraBarAtRightSupport() != null) {
            rightBar = input.getExtraBarAtRightSupport();

            if (rightBar.getProperty1() != null) {
                rightBarProperty1 = rightBar.getProperty1();

                etRightBarQty1.setText(toDisplayFormat(rightBarProperty1.getQuantity()));
                etRightBarF1.setText(toDisplayFormat(rightBarProperty1.getLengthFactor()));

                if (rightBarProperty1.getDiameter() != 0)
                    etRightBarDiameter1.setText(Integer.toString(rightBarProperty1.getDiameter()));
            }

            if (rightBar.getProperty2() != null) {
                rightBarProperty2 = rightBar.getProperty2();

                etRightBarQty2.setText(toDisplayFormat(rightBarProperty2.getQuantity()));
                etRightBarF2.setText(toDisplayFormat(rightBarProperty2.getLengthFactor()));

                if (rightBarProperty2.getDiameter() != 0)
                    etRightBarDiameter2.setText(Integer.toString(rightBarProperty2.getDiameter()));
            }
        }



        if (input.getStirrups() != null) {
            stirrups = input.getStirrups();


            if (stirrups.getColumnTieA() != null) {
                stirrupsA = stirrups.getColumnTieA();

                etAQty.setText(toDisplayFormat(stirrupsA.getQuantity()));
                etASpacing.setText(toDisplayFormat(stirrupsA.getSpacing()));
            }


            if (stirrups.getColumnTieB() != null) {
                stirrupsB = stirrups.getColumnTieB();
                etBQty.setText(toDisplayFormat(stirrupsB.getQuantity()));
                etBSpacing.setText(toDisplayFormat(stirrupsB.getSpacing()));
            }


            if (stirrups.getColumnTieC() != null) {
                stirrupsC = stirrups.getColumnTieC();
                etCQty.setText(toDisplayFormat(stirrupsC.getQuantity()));
                etCSpacing.setText(toDisplayFormat(stirrupsC.getSpacing()));
            }


            if (stirrups.getColumnTieTheRest() != null) {
                stirrupsTheRest = stirrups.getColumnTieTheRest();
                etTheRestSpacing.setText(toDisplayFormat(stirrupsTheRest.getSpacing()));
            }


            if (stirrups.getColumnTieD() != null) {
                stirrupsD = stirrups.getColumnTieD();
                etDQty.setText(toDisplayFormat(stirrupsD.getQuantity()));
                etDSpacing.setText(toDisplayFormat(stirrupsD.getSpacing()));
            }


            if (stirrups.getColumnTieE() != null) {
                stirrupsE = stirrups.getColumnTieE();
                etEQty.setText(toDisplayFormat(stirrupsE.getQuantity()));
                etESpacing.setText(toDisplayFormat(stirrupsE.getSpacing()));
            }


            if (stirrups.getColumnTieF() != null) {
                stirrupsF = stirrups.getColumnTieF();
                etFQty.setText(toDisplayFormat(stirrupsF.getQuantity()));
                etFSpacing.setText(toDisplayFormat(stirrupsF.getSpacing()));
            }


            if (stirrups.getColumnTieG() != null) {
                stirrupsG = stirrups.getColumnTieG();
                etGLength.setText(toDisplayFormat(stirrupsG.getLength()));
                if (stirrupsG.getDiameter() != 0)
                    etGDiameter.setText(toDisplayFormat(stirrupsG.getDiameter()));

            }
        }


        if (input.getSplicing() != null) {
            splicing1 = input.getSplicing();
            etSplicingQty1.setText(toDisplayFormat(splicing1.getQuantity()));
            etSplicingLength1.setText(toDisplayFormat(splicing1.getLength()));

            if (splicing1.getDiameter() != 0)
                etSplicingDiameter1.setText(toDisplayFormat(splicing1.getDiameter()));
        }


        if (input.getSplicing2() != null) {
            splicing2 = input.getSplicing2();
            etSplicingQty2.setText(toDisplayFormat(splicing2.getQuantity()));
            etSplicingLength2.setText(toDisplayFormat(splicing2.getLength()));

            if (splicing2.getDiameter() != 0)
                etSplicingDiameter2.setText(toDisplayFormat(splicing2.getDiameter()));
        }


        if (input.getHookes() != null) {
            hookes1 = input.getHookes();
            etHookesQty1.setText(toDisplayFormat(hookes1.getQuantity()));
            etHookesLength1.setText(toDisplayFormat(hookes1.getLength()));

            if (hookes1.getDiameter() != 0)
                etHookesDiameter1.setText(toDisplayFormat(hookes1.getDiameter()));
        }


        if (input.getHookes2() != null) {
            hookes2 = input.getHookes2();
            etHookesQty2.setText(toDisplayFormat(hookes2.getQuantity()));
            etHookesLength2.setText(toDisplayFormat(hookes2.getLength()));

            if (hookes2.getDiameter() != 0)
                etHookesDiameter2.setText(toDisplayFormat(hookes2.getDiameter()));

        }
    }

    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }


    @Override
    public void displayResult(int splicingTotalQty, double area) {
        tvStirrupsTotalQty.setText(String.format(getString(R.string.label_total_quantity),Integer.toString(splicingTotalQty)));
        etArea.setText(Double.toString(area));
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }


    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setDescription(etDescription.getText().toString());

        String base = etBase.getText().toString();
        if (base.isEmpty()) {
            input.setBase(0);
        } else {
            input.setBase(Double.parseDouble(base));
        }

        String height = etHeight.getText().toString();
        if (height.isEmpty()) {
            input.setHeight(0);
        } else {
            input.setHeight(Double.parseDouble(height));
        }


        // TOP
        if (!etTopBarQty1.getText().toString().isEmpty()) {
            topBarProperty1.setQuantity(Integer.parseInt(etTopBarQty1.getText().toString()));
        }

        if (!etTopBarQty2.getText().toString().isEmpty()) {
            topBarProperty2.setQuantity(Integer.parseInt(etTopBarQty2.getText().toString()));
        }


        // BOTTOM
        if (!etBottomBarQty1.getText().toString().isEmpty()) {
            bottomBarProperty1.setQuantity(Integer.parseInt(etBottomBarQty1.getText().toString()));
        }

        if (!etBottomBarQty2.getText().toString().isEmpty()) {
            bottomBarProperty2.setQuantity(Integer.parseInt(etBottomBarQty2.getText().toString()));
        }

        // LEFT
        if (!etLeftBarQty1.getText().toString().isEmpty()) {
            leftBarProperty1.setQuantity(Integer.parseInt(etLeftBarQty1.getText().toString()));
        }

        if (!etLeftBarF1.getText().toString().isEmpty()) {
            leftBarProperty1.setLengthFactor(Double.parseDouble(etLeftBarF1.getText().toString()));
        }

        if (!etLeftBarQty2.getText().toString().isEmpty()) {
            leftBarProperty2.setQuantity(Integer.parseInt(etLeftBarQty2.getText().toString()));
        }

        if (!etLeftBarF2.getText().toString().isEmpty()) {
            leftBarProperty2.setLengthFactor(Double.parseDouble(etLeftBarF2.getText().toString()));
        }

        // MID
        if (!etMidBarQty1.getText().toString().isEmpty()) {
            midBarProperty1.setQuantity(Integer.parseInt(etMidBarQty1.getText().toString()));
        }

        if (!etMidBarF1.getText().toString().isEmpty()) {
            midBarProperty1.setLengthFactor(Double.parseDouble(etMidBarF1.getText().toString()));
        }

        if (!etMidBarQty2.getText().toString().isEmpty()) {
            midBarProperty2.setQuantity(Integer.parseInt(etMidBarQty2.getText().toString()));
        }

        if (!etMidBarF2.getText().toString().isEmpty()) {
            midBarProperty2.setLengthFactor(Double.parseDouble(etMidBarF2.getText().toString()));
        }

        // RIGHT
        if (!etRightBarQty1.getText().toString().isEmpty()) {
            rightBarProperty1.setQuantity( Integer.parseInt(etRightBarQty1.getText().toString()));
        }

        if (!etRightBarF1.getText().toString().isEmpty()) {
            rightBarProperty1.setLengthFactor(Double.parseDouble(etRightBarF1.getText().toString()));
        }


        if (!etRightBarQty2.getText().toString().isEmpty()) {
            rightBarProperty2.setQuantity(Integer.parseInt(etRightBarQty2.getText().toString()));
        }

        if (!etRightBarF2.getText().toString().isEmpty()) {
            rightBarProperty2.setLengthFactor(Double.parseDouble(etRightBarF2.getText().toString()));
        }

        // STIRRUPS A
        if (!etAQty.getText().toString().isEmpty()) {
            stirrupsA.setQuantity(Integer.parseInt(etAQty.getText().toString()));
        }

        if (!etASpacing.getText().toString().isEmpty()) {
            stirrupsA.setSpacing(Double.parseDouble(etASpacing.getText().toString()));
        }

        // STIRRUPS B
        if (!etBQty.getText().toString().isEmpty()) {
            stirrupsB.setQuantity(Integer.parseInt(etBQty.getText().toString()));
        }

        if (!etBSpacing.getText().toString().isEmpty()) {
            stirrupsB.setSpacing(Double.parseDouble(etBSpacing.getText().toString()));
        }

        // STIRRUPS C
        if (!etCQty.getText().toString().isEmpty()) {
            stirrupsC.setQuantity(Integer.parseInt(etCQty.getText().toString()));

        }

        if (!etCSpacing.getText().toString().isEmpty()) {
            stirrupsC.setSpacing(Double.parseDouble(etCSpacing.getText().toString()));
        }

        // STIRRUPS THE REST
        if (!etTheRestSpacing.getText().toString().isEmpty()) {
            stirrupsTheRest.setSpacing(Double.parseDouble(etTheRestSpacing.getText().toString()));
        }


        // STIRRUPS D
        if (!etDQty.getText().toString().isEmpty()) {
            stirrupsD.setQuantity(Integer.parseInt(etDQty.getText().toString()));

        }

        if (!etDSpacing.getText().toString().isEmpty()) {
            stirrupsD.setSpacing(Double.parseDouble(etDSpacing.getText().toString()));
        }

        // STIRRUPS E
        if (!etEQty.getText().toString().isEmpty()) {
            stirrupsE.setQuantity(Integer.parseInt(etEQty.getText().toString()));
        }

        if (!etESpacing.getText().toString().isEmpty()) {
            stirrupsE.setSpacing(Double.parseDouble(etESpacing.getText().toString()));
        }

        // STIRRUPS F
        if (!etFQty.getText().toString().isEmpty()) {
            stirrupsF.setQuantity(Integer.parseInt(etFQty.getText().toString()));
        }

        if (!etFSpacing.getText().toString().isEmpty()) {
            stirrupsF.setSpacing(Double.parseDouble(etFSpacing.getText().toString()));
        }

        // STIRRUPS G
        if (!etGLength.getText().toString().isEmpty()) {
            stirrupsG.setLength(Double.parseDouble(etGLength.getText().toString()));
        }


        // SPLICING
        if (!etSplicingQty1.getText().toString().isEmpty()) {
            splicing1.setQuantity(Integer.parseInt(etSplicingQty1.getText().toString()));
        }

        if (!etSplicingLength1.getText().toString().isEmpty()) {
            splicing1.setLength(Double.parseDouble(etSplicingLength1.getText().toString()));
        }

        if (!etSplicingQty2.getText().toString().isEmpty()) {
            splicing2.setQuantity(Integer.parseInt(etSplicingQty2.getText().toString()));
        }

        if (!etSplicingLength2.getText().toString().isEmpty()) {
            splicing2.setLength(Double.parseDouble(etSplicingLength2.getText().toString()));
        }


        // HOOKES
        if (!etHookesQty1.getText().toString().isEmpty()) {
            hookes1.setQuantity(Integer.parseInt(etHookesQty1.getText().toString()));
        }

        if (!etHookesLength1.getText().toString().isEmpty()) {
            hookes1.setLength(Double.parseDouble(etHookesLength1.getText().toString()));
        }


        if (!etHookesQty2.getText().toString().isEmpty()) {
            hookes2.setQuantity(Integer.parseInt(etHookesQty2.getText().toString()));
        }

        if (!etHookesLength2.getText().toString().isEmpty()) {
            hookes2.setLength(Double.parseDouble(etHookesLength2.getText().toString()));
        }


        topBar.setProperty1(topBarProperty1);
        topBar.setProperty2(topBarProperty2);
        input.setTopBar(topBar);

        bottomBar.setProperty1(bottomBarProperty1);
        bottomBar.setProperty2(bottomBarProperty2);
        input.setBottomBar(bottomBar);

        midBar.setProperty1(midBarProperty1);
        midBar.setProperty2(midBarProperty2);
        input.setBottomExtraAtMid(midBar);

        leftBar.setProperty1(leftBarProperty1);
        leftBar.setProperty2(leftBarProperty2);
        input.setExtraBarAtLeftSupport(leftBar);

        rightBar.setProperty1(rightBarProperty1);
        rightBar.setProperty2(rightBarProperty2);
        input.setExtraBarAtRightSupport(rightBar);

        stirrups.setColumnTieA(stirrupsA);
        stirrups.setColumnTieB(stirrupsB);
        stirrups.setColumnTieC(stirrupsC);
        stirrups.setColumnTieTheRest(stirrupsTheRest);
        stirrups.setColumnTieD(stirrupsD);
        stirrups.setColumnTieE(stirrupsE);
        stirrups.setColumnTieF(stirrupsF);
        stirrups.setColumnTieG(stirrupsG);
        input.setStirrups(stirrups);

        input.setSplicing(splicing1);
        input.setSplicing2(splicing2);

        input.setHookes(hookes1);
        input.setHookes2(hookes2);

        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etDescription.getText().toString().isEmpty()) {
            etDescription.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etDescription.setError(null);
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
