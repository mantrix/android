package com.android.estimateapp.cleanarchitecture.presentation.application;

import com.android.estimateapp.cleanarchitecture.data.clients.DatabaseClient;

import javax.inject.Inject;

public class ApplicationPresenter implements ApplicationContract.UserActionListener {


    private ApplicationContract.View view;
    private DatabaseClient databaseClient;

    @Inject
    public ApplicationPresenter(DatabaseClient databaseClient) {
        this.databaseClient = databaseClient;
    }


    @Override
    public void setView(ApplicationContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        databaseClient.setup();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }
}
