package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.list.StoneCLaddingListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.list.StoneCladdingListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class StoneCladdingListModule {

    @Provides
    @PerActivity
    StoneCLaddingListContract.UserActionListener provideStoneCladdingListPresenter(StoneCladdingListPresenter presenter) {
        return presenter;
    }
}
