package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;

import java.util.List;

import io.reactivex.Observable;

public interface CloudStoneCladdingDataStore {

    Observable<StoneCladdingInput> createStoneCladdingInput(String projectId, String scopeOfWorkId, StoneCladdingInput input);

    Observable<StoneCladdingInput> updateStoneCladdingInput(String projectId, String scopeOfWorkId, String itemId, StoneCladdingInput input);

    Observable<List<StoneCladdingInput>> getStoneCladdingInputs(String projectId, String scopeOfWorkId);

    Observable<StoneCladdingInput> getStoneCladdingInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<List<StoneCladdingFactor>> createStoneCladdingFactors(String projectId, List<StoneCladdingFactor> factors);

    Observable<List<StoneCladdingFactor>> getStoneCladdingFactors(String projectId);

    Observable<List<StoneCladdingFactor>> updateStoneCladdingFactors(String projectId, List<StoneCladdingFactor> factors);

    Observable<StoneType> createStoneTypeFactor(String projectId, StoneType stoneType);

    Observable<List<StoneType>> getStoneTypeFactors(String projectId);

    Observable<List<StoneType>> updateStoneTypeFactors(String projectId,List<StoneType> factors);

    Observable<StoneCladdingResult> saveStoneCladdingInputResult(String projectId, String scopeOfWorkId, String itemId, StoneCladdingResult result);
}
