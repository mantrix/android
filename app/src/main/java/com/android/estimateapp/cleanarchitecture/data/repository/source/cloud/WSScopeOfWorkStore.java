package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import android.content.Context;

import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudScopeWorkDataStore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;

@Singleton
public class WSScopeOfWorkStore extends BaseStore implements CloudScopeWorkDataStore {

    private Context context;
    private DatabaseReference scopesRef;
    private DatabaseReference projectScopeOfWorksRef;


    @Inject
    public WSScopeOfWorkStore(Context context) {
        super();
        this.context = context;

        scopesRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);

    }

    @Override
    public Observable<List<Work>> getProjectScopes(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = scopesRef
                    .child(projectId)
                    .child(FIREBASE_DATA_REF);
            query.keepSynced(true);
            return query;

        }).flatMap(query -> queryListValue(query,Work.class));
    }

    @Override
    public Observable<Work> createProjectScope(String projectId, Work work) {
        return Observable.fromCallable(() -> {
            return scopesRef.child(projectId)
                    .child(FIREBASE_DATA_REF);

        }).flatMap(databaseReference -> push(databaseReference,work));
    }

    @Override
    public Completable deleteScopeInput(String projectId, String scopeOfWorkId, String inputId) {
        return Observable.fromCallable(() -> {
            Query query = projectScopeOfWorksRef
                    .child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(inputId);
            query.keepSynced(true);
            return query;
        }).flatMapCompletable(query -> delete(query));
    }
}
