package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.concrete.GeneralConcreteMaterialDialogFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class GeneralConcreteMaterialBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {GeneralConcreteMaterialModule.class})
    abstract GeneralConcreteMaterialDialogFragment bindGeneralConcreteMaterialDialogFragment();
}
