package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar.ColumnRebarWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar.ColumnRebarWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.create.CreateColumnRebarInputContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.create.CreateColumnRebarInputPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateColumnRebarModule {

    @Provides
    @PerActivity
    CreateColumnRebarInputContract.UserActionListener provideCreateColumnRebarPresenter(CreateColumnRebarInputPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ColumnRebarWorkCalculator provideColumnRebarWorkCalculator(ColumnRebarWorkCalculatorImpl calculator) {
        return calculator;
    }
}
