package com.android.estimateapp.configuration.injection.module.app;


import com.android.estimateapp.cleanarchitecture.data.clients.DatabaseClient;
import com.android.estimateapp.cleanarchitecture.data.clients.FirebaseDatabaseClient;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.FirebaseUploadStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSAccountStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSBeamRebarStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSCHBStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSColumnFootingRebarStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSColumnRebarStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSConcreteWorkStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSDoorAndWindowStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSFactorStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSPlasteringStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSProjectPropertiesStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSProjectStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSScopeOfWorkStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSStoneCladdingStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSTileWorkStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSWallFootingRebarStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudBeamRebarStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudCHBStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudColumnFootingRebarDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudColumnRebarDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudConcreteWorkDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudDoorAndWindowDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudFactorDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudPlasteringStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudProjectPropertiesStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudScopeWorkDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudStoneCladdingDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudTileWorkStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudWallFootingRebarDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.account.CloudAccountDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.project.CloudProjectDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.storage.UploadFileStore;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataStoreModule {

    @Provides
    @Singleton
    CloudAccountDataStore provideCloudAccountDataStore(WSAccountStore wsAccountStore) {
        return wsAccountStore;
    }

    @Provides
    @Singleton
    DatabaseClient provideDatabaseClient(FirebaseDatabaseClient firebaseDatabaseClient) {
        return firebaseDatabaseClient;
    }

    @Provides
    @Singleton
    UploadFileStore provideUploadFileStore(FirebaseUploadStore firebaseUploadStore) {
        return firebaseUploadStore;
    }

    @Provides
    @Singleton
    CloudProjectDataStore provideCloudProjectDataStore(WSProjectStore wsProjectStore) {
        return wsProjectStore;
    }

    @Provides
    @Singleton
    CloudScopeWorkDataStore provideCloudScopeWorkDataStore(WSScopeOfWorkStore scopeOfWorkStore) {
        return scopeOfWorkStore;
    }


    @Provides
    @Singleton
    CloudFactorDataStore provideCloudFactorDataStore(WSFactorStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudCHBStore provideCloudCHBFactorStore(WSCHBStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudPlasteringStore provideCloudPlasteringStore(WSPlasteringStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudConcreteWorkDataStore provideCloudConcreteWorkDataStore(WSConcreteWorkStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudTileWorkStore provideCloudTileWorkStore(WSTileWorkStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudStoneCladdingDataStore provideCloudStoneCladdingDataStore(WSStoneCladdingStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudProjectPropertiesStore provideCloudProjectPropertiesStore(WSProjectPropertiesStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudDoorAndWindowDataStore provideCloudDoorAndWindowDataStore(WSDoorAndWindowStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudWallFootingRebarDataStore provideCloudWallFootingRebarDataStore(WSWallFootingRebarStore store) {
        return store;
    }

    @Provides
    @Singleton
    CloudColumnFootingRebarDataStore provideCloudColumnFootingRebarDataStore(WSColumnFootingRebarStore store){
        return store;
    }

    @Provides
    @Singleton
    CloudBeamRebarStore provideCloudBeamRebarStore(WSBeamRebarStore store){
        return store;
    }

    @Provides
    @Singleton
    CloudColumnRebarDataStore provideCloudColumnRebarDataStore(WSColumnRebarStore store) {
        return store;
    }

}
