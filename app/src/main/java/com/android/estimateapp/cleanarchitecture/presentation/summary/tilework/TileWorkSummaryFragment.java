package com.android.estimateapp.cleanarchitecture.presentation.summary.tilework;

import android.os.Bundle;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkMaterials;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkSummary;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class TileWorkSummaryFragment extends BaseFragment implements TileWorkSummaryContract.View {


    @Inject
    TileWorkSummaryContract.UserActionListener presenter;

    @Inject
    TileWorkSummaryAdapter adapter;
    @BindView(R.id.tv_area)
    TextView tvArea;
    @BindView(R.id.tv_cement)
    TextView tvCement;
    @BindView(R.id.tv_adhesive)
    TextView tvAdhesive;
    @BindView(R.id.tv_sand)
    TextView tvSand;


    public static TileWorkSummaryFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        TileWorkSummaryFragment fragment = new TileWorkSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rv_summary)
    RecyclerView recyclerView;

    private String projectId;
    private String scopeKey;
    NpaLinearLayoutManager npaLinearLayoutManager;

    @Override
    protected int getLayoutResource() {
        return R.layout.tile_work_summary_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        initRecyclerView();
        extractExtras();
        presenter.setData(projectId, scopeKey);
        presenter.setView(this);
        presenter.start();

    }

    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
    }

    @Override
    public void displaySummary(TileWorkSummary summary) {
        List<TileWorkMaterials> materials = new ArrayList<>();
        for (String key : summary.getMaterialPerTileType().keySet()) {
            TileWorkMaterials material = summary.getMaterialPerTileType().get(key);
            materials.add(material);
        }
        adapter.setItems(materials);

        BaseActivity baseActivity = (BaseActivity) getActivity();

        TileWorkMaterials totals = summary.getTotal();

        tvArea.setText(String.format(getString(R.string.sqm_result), baseActivity.toDisplayFormat(totals.area)));
        tvCement.setText(String.format(getString(R.string.bag_result), baseActivity.toDisplayFormat(totals.cement)));
        tvSand.setText(String.format(getString(R.string.cum_result), baseActivity.toDisplayFormat(totals.sand)));
        tvAdhesive.setText(String.format(getString(R.string.bag_result), baseActivity.toDisplayFormat(totals.adhesive)));
    }
}
