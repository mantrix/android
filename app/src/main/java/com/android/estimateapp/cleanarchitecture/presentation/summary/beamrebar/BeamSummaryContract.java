package com.android.estimateapp.cleanarchitecture.presentation.summary.beamrebar;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarSummary;

public interface BeamSummaryContract {

    interface View extends SummaryView<BeamRebarSummary> {
        void displayTotal(double total);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
