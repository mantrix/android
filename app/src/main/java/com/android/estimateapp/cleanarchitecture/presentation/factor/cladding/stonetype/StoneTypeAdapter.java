package com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.stonetype;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.EdittextBaseOutputListener;
import com.android.estimateapp.utils.DisplayUtil;
import com.android.estimateapp.utils.NumberUtils;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StoneTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<StoneType>> {


    private List<StoneType> list;

    @Inject
    public StoneTypeAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.title_type_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<StoneType> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<StoneType> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    public List<StoneType> getData() {
        return list;
    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.et_length)
        EditText etLength;

        @BindView(R.id.et_width)
        EditText etWidth;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(StoneType stone, int position) {


            tvName.setText(stone.getName());
            etLength.setText(DisplayUtil.toPlainString(stone.getLength()));
            etWidth.setText(DisplayUtil.toPlainString(stone.getWidth()));


            etLength.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);
                    String input = etLength.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setLength(Double.parseDouble(etLength.getText().toString()));
                    }
                }
            });


            etWidth.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etWidth.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setWidth(Double.parseDouble(etWidth.getText().toString()));
                    }
                }
            });
        }
    }
}
