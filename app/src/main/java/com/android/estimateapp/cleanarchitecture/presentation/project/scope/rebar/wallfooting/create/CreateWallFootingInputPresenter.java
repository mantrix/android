package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting.WallFootingRebarCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class CreateWallFootingInputPresenter extends CreateScopeInputBasePresenter implements CreateWallFootingRebarContract.UserActionListener {

    private CreateWallFootingRebarContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;
    private WallFootingRebarCalculatorImpl wallFootingRebarCalculator;

    private WallFootingRebarInput input;

    private List<RSBFactor> rsbFactors;

    private WallFootingRebarResult result;


    @Inject
    public CreateWallFootingInputPresenter(ProjectRepository projectRepository,
                                           AccountSubscriptionUseCase accountSubscriptionUseCase,
                                           WallFootingRebarCalculatorImpl wallFootingRebarCalculator,
                                           ThreadExecutorProvider threadExecutorProvider,
                                           PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase, threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.wallFootingRebarCalculator = wallFootingRebarCalculator;

        rsbFactors = new ArrayList<>();
    }

    @Override
    public void setView(CreateWallFootingRebarContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        setPickerOptions();
        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {
        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateWallFootingInput(projectId, scopeKey, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createWallFootingInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<WallFootingRebarInput> inputObservable) {

        view.showSavingDialog();

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    saveResult();
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    private void saveResult(){
        if (result != null && itemKey != null) {
            projectRepository.saveWallFootingInputResult(projectId, scopeKey, itemKey,result)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(result -> {
                        this.result = result;
                    }, throwable -> {
                        throwable.printStackTrace();
                    });
        }
    }

    @Override
    public void calculate(WallFootingRebarInput input) {

        result = wallFootingRebarCalculator.calculate(53, input, rsbFactors);

        this.input = input;

        view.displayResult(result);
    }


    private void setPickerOptions() {
        view.setRSBPickerOptions(Arrays.asList(RsbSize.TEN_MM, RsbSize.TWELVE_MM));

    }

    private void loadWallFootingInput() {
        projectRepository.getWallFootingInput(projectId, scopeKey, itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {

                    this.input = input;

                    view.setInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate) {

        projectRepository.getProjectRSBFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(rsbFactors -> {
                    this.rsbFactors = rsbFactors;


                    if (!factorUpdate) {
                        view.enabledCalculateBtn(true);

                        if (itemKey != null) {
                            loadWallFootingInput();
                        }
                    } else {
                        /*
                         * Factors were updated hence input data and ui should be be updated.
                         */
                        if (itemKey != null) {

                            view.setInput(input);

                            view.requestForReCalculation();
                        }
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }

}