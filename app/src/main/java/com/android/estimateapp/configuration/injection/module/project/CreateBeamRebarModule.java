package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar.BeamRebarCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar.BeamRebarCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.create.CreateBeamRebarInputContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.create.CreateBeamRebarInputPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateBeamRebarModule {
    @Provides
    @PerActivity
    CreateBeamRebarInputContract.UserActionListener provideCreateBeamRebarPresenter(CreateBeamRebarInputPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    BeamRebarCalculator provideColumnFootingWorkCalculator(BeamRebarCalculatorImpl calculator) {
        return calculator;
    }

}
