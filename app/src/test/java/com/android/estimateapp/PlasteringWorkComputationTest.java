package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering.PlasteringWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.PlasteringSummary;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class PlasteringWorkComputationTest extends BaseTest {

    PlasteringWorkCalculatorImpl plasteringWorkCalculator;

    @Before
    public void setup() {
        plasteringWorkCalculator = new PlasteringWorkCalculatorImpl();
    }

    @Test
    public void plasteringTest() {

        PlasteringInput input = new PlasteringInput();
        input.setMortarClass(Grade.A.getId());

        Dimension dimension = new Dimension();
        dimension.setWidth(3.72);
        dimension.setHeight(2.60);
        input.setDimension(dimension);
        input.setThickness(20);
        input.setOpenings(1.68);
        input.setSides(1);
        input.setWastage(5);

        PlasteringMortarFactor factor = defaultFactorFactory.getPlasteringFactor(input.getMortarClassEnum());

        PlasteringResult result = plasteringWorkCalculator.calculate(factor,input);

        System.out.println("Area: " + result.getArea());
        System.out.println("Cement: " + result.getCement());
        System.out.println("Sand: " + result.getSand());

        assertEquals(7.99, result.getArea(), DELTA);
        assertEquals(2.84, result.getCement(), DELTA);
        assertEquals(0.16, result.getSand(), DELTA);
    }

    @Test
    public void plasteringSummaryTest(){

        PlasteringSummary report = plasteringWorkCalculator
                .generateSummary(Arrays.asList(new PlasteringResult(798,17.56,2.63),new PlasteringResult(1995,15.36,15.36)));

        assertEquals(2793, report.getTotalArea(), DELTA);
        assertEquals(32.92, report.getCement(), DELTA);
        assertEquals(17.99, report.getSand(), DELTA);
    }
}
