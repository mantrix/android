package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding.StoneCladdingCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding.StoneCladdingCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.stonecladding.StoneCladdingSummaryContract;
import com.android.estimateapp.cleanarchitecture.presentation.summary.stonecladding.StoneCladdingSummaryPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class StoneCladdingSummaryModule {

    @Provides
    @PerFragment
    StoneCladdingCalculator provideStoneCladdingCalculator(StoneCladdingCalculatorImpl calculator) {
        return calculator;
    }

    @Provides
    @PerFragment
    StoneCladdingSummaryContract.UserActionListener provideStoneCladdingSummaryPresenter(StoneCladdingSummaryPresenter presenter) {
        return presenter;
    }
}
