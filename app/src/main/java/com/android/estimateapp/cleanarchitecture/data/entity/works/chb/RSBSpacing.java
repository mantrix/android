package com.android.estimateapp.cleanarchitecture.data.entity.works.chb;

public class RSBSpacing {

    double vertical;

     int horizontal;

    public double getVertical() {
        return vertical;
    }

    public void setVertical(double vertical) {
        this.vertical = vertical;
    }

    public int getHorizontal() {
        return horizontal;
    }

    public void setHorizontal(int horizontal) {
        this.horizontal = horizontal;
    }
}
