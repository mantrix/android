package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;

import java.util.List;

import io.reactivex.Observable;

public interface CloudTileWorkStore {


    Observable<TileWorkInput> getTileWorkInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<List<TileWorkInput>> getTileWorkInputs(String projectId, String scopeOfWorkId);

    Observable<TileWorkInput> updateTileWorkInput(String projectId, String scopeOfWorkId, String itemId, TileWorkInput input);

    Observable<TileWorkInput> createTileWorkInput(String projectId, String scopeOfWorkId, TileWorkInput input);


    Observable<List<TileWorkFactor>> updateTileWorkFactors(String projectId, List<TileWorkFactor> tileWorkFactors);

    Observable<List<TileWorkFactor>> getTileWorkFactors(String projectId);

    Observable<List<TileWorkFactor>> createTileWorkFactors(String projectId, List<TileWorkFactor> factors);

    Observable<TileType> createTileTypeFactor(String projectId, TileType tileType);

    Observable<List<TileType>> getTileTypeFactors(String projectId);

    Observable<List<TileType>> updateTileTypeFactors(String projectId, List<TileType> factors);
}
