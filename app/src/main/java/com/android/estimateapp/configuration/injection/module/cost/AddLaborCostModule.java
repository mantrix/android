package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.add.AddLaborCostContract;
import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.add.AddLaborCostPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class AddLaborCostModule {

    @Provides
    @PerFragment
    AddLaborCostContract.UserActionListener provideAddLaborCostPresenter(AddLaborCostPresenter presenter) {
        return presenter;
    }

}
