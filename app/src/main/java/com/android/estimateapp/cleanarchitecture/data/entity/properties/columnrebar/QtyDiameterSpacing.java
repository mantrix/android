package com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class QtyDiameterSpacing extends Model {


    String label;

    int quantity;

    double spacing;

    int diameter;

    double length;



    public QtyDiameterSpacing(int quantity, double spacing) {
        this.quantity = quantity;
        this.spacing = spacing;
    }

    public QtyDiameterSpacing(String label, int diameter, double length) {
        this.label = label;
        this.diameter = diameter;
        this.length = length;
    }

    public QtyDiameterSpacing() {

    }



    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSpacing() {
        return spacing;
    }

    public void setSpacing(double spacing) {
        this.spacing = spacing;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
