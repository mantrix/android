package com.android.estimateapp.configuration.injection.module.properties;

import com.android.estimateapp.cleanarchitecture.presentation.properties.membersection.MemberSectionPropertiesFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ProjectPropertiesBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {MemberSectionModule.class})
    abstract MemberSectionPropertiesFragment bindMemberSectionPropertiesFragment();

}
