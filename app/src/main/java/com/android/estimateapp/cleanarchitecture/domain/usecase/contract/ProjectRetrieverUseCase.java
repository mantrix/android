package com.android.estimateapp.cleanarchitecture.domain.usecase.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;

import java.util.List;

import io.reactivex.Observable;

public interface ProjectRetrieverUseCase {

    Observable<List<Project>> getUserProjects(boolean reload, int page, int pageSize);

    Observable<Project> getProject(String id);
}
