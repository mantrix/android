package com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

import java.util.Arrays;
import java.util.List;

public class Stirrups extends Model {

    QtyDiameterSpacing columnTieA;

    QtyDiameterSpacing columnTieB;

    QtyDiameterSpacing columnTieC;

    QtyDiameterSpacing columnTieTheRest;

    QtyDiameterSpacing columnTieD;

    QtyDiameterSpacing columnTieE;

    QtyDiameterSpacing columnTieF;

    QtyDiameterSpacing columnTieG;

    public Stirrups(){

    }

    double data1;

    double data2;

    int totalQuantity;

    public QtyDiameterSpacing getColumnTieA() {
        return columnTieA;
    }

    public void setColumnTieA(QtyDiameterSpacing columnTieA) {
        this.columnTieA = columnTieA;
    }

    public QtyDiameterSpacing getColumnTieB() {
        return columnTieB;
    }

    public void setColumnTieB(QtyDiameterSpacing columnTieB) {
        this.columnTieB = columnTieB;
    }

    public QtyDiameterSpacing getColumnTieC() {
        return columnTieC;
    }

    public void setColumnTieC(QtyDiameterSpacing columnTieC) {
        this.columnTieC = columnTieC;
    }

    public QtyDiameterSpacing getColumnTieTheRest() {
        return columnTieTheRest;
    }

    public void setColumnTieTheRest(QtyDiameterSpacing columnTieTheRest) {
        this.columnTieTheRest = columnTieTheRest;
    }

    public QtyDiameterSpacing getColumnTieD() {
        return columnTieD;
    }

    public void setColumnTieD(QtyDiameterSpacing columnTieD) {
        this.columnTieD = columnTieD;
    }

    public QtyDiameterSpacing getColumnTieE() {
        return columnTieE;
    }

    public void setColumnTieE(QtyDiameterSpacing columnTieE) {
        this.columnTieE = columnTieE;
    }

    public QtyDiameterSpacing getColumnTieF() {
        return columnTieF;
    }

    public void setColumnTieF(QtyDiameterSpacing columnTieF) {
        this.columnTieF = columnTieF;
    }

    public QtyDiameterSpacing getColumnTieG() {
        return columnTieG;
    }

    public void setColumnTieG(QtyDiameterSpacing columnTieG) {
        this.columnTieG = columnTieG;
    }

    public double getData1() {
        double total = 0;
        List<QtyDiameterSpacing> spacings =  Arrays.asList(columnTieA,columnTieB,columnTieC,columnTieD,columnTieE,columnTieF);
        for (QtyDiameterSpacing  spacing: spacings){
            if (spacing != null){
                total  += (spacing.getQuantity() - 1) * spacing.getSpacing();
            }
        }
        return total;
    }

    public void setData1(double data1) {
        this.data1 = data1;
    }

    public double getData2() {
        double total = 0;
        List<QtyDiameterSpacing> spacings =  Arrays.asList(columnTieA,columnTieB,columnTieC,columnTieD,columnTieE,columnTieF);
        for (QtyDiameterSpacing  spacing: spacings){
            if (spacing != null){
                total  += spacing.getQuantity();
            }
        }
        return total;
    }

    public void setData2(double data2) {
        this.data2 = data2;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

}
