package com.android.estimateapp.cleanarchitecture.data.entity.factors.chb;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

@IgnoreExtraProperties
public class CHBMortarClass extends Model {

    int mortarClass;

    List<CHBSizeFactor> chbSizeFactors;

    public CHBMortarClass(){

    }

    public CHBMortarClass(int mortarClass){
        this.mortarClass = mortarClass;
    }

    public int getMortarClass() {
        return mortarClass;
    }


    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    @Exclude
    public Grade getMortarClassEnum() {
        return Grade.parseInt(mortarClass);
    }

    @Exclude
    public List<CHBSizeFactor> getChbSizeFactors() {
        return chbSizeFactors;
    }

    public void setChbSizeFactors(List<CHBSizeFactor> chbSizeFactors) {
        this.chbSizeFactors = chbSizeFactors;
    }
}
