package com.android.estimateapp.cleanarchitecture.presentation.summary.generalconcrete;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ConcreteWorkMaterials;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ConcreteWorkSummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<ConcreteWorkMaterials>> {


    private List<ConcreteWorkMaterials> list;

    @Inject
    public ConcreteWorkSummaryAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.concrete_work_summary_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<ConcreteWorkMaterials> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<ConcreteWorkMaterials> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }


    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;


        @BindView(R.id.tv_quantity)
        TextView tvQuantity;

        @BindView(R.id.tv_cement)
        TextView tvCement;

        @BindView(R.id.tv_sand)
        TextView tvSand;

        @BindView(R.id.tv_three_fourths)
        TextView tvThreeFourths;

        @BindView(R.id.tv_g1)
        TextView tvG1;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(ConcreteWorkMaterials materials, int position) {

            if (materials.memberSectionProperty != null){
                tvName.setText(materials.memberSectionProperty.getName());
            } else if (materials.category != null){
                tvName.setText(materials.category.toString());
            }

            tvQuantity.setText(DisplayUtil.toPlainString(materials.quantity));
            tvCement.setText(DisplayUtil.toPlainString(materials.cement));
            tvSand.setText(DisplayUtil.toPlainString(materials.sand));
            tvThreeFourths.setText(DisplayUtil.toPlainString(materials.threeFourthGravel));
            tvG1.setText(DisplayUtil.toPlainString(materials.g1Gravel));
        }
    }
}