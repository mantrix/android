package com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterLength;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyLength;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Rebar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.RebarBend;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Stirrups;
import com.android.estimateapp.cleanarchitecture.data.entity.works.StirrupsOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.RSBValuePair;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar.BeamRebarCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarMaterial;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarSummary;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ColumnRebarMaterial;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ColumnRebarSummary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class ColumnRebarWorkCalculatorImpl extends BaseCalculator implements ColumnRebarWorkCalculator {


    private List<RSBFactor> rsbFactors;

    enum Location {
        REBAR_1,
        REBAR_2,
        BEND_AT_FOOTING,
        BEND_AT_COLUMN_END,
        SPLICING
    }


    @Inject
    public ColumnRebarWorkCalculatorImpl() {

    }

    @Override
    public ColumnRebarSummary generateSummary(List<ColumnRebarOutput> results){
        Map<Integer, ColumnRebarMaterial> columnRebarMaterialMap = new HashMap<>();

        for (ColumnRebarOutput result : results){
            // REBAR 1
            applyData(columnRebarMaterialMap, result.getRebar1(), Location.REBAR_1);

            // REBAR 2
            applyData(columnRebarMaterialMap, result.getRebar2(), Location.REBAR_2);

            // BEND_AT_FOOTING
            applyData(columnRebarMaterialMap, result.getRebarBendAtFooting1(), Location.BEND_AT_FOOTING);
            applyData(columnRebarMaterialMap, result.getRebarBendAtFooting2(), Location.BEND_AT_FOOTING);

            // BEND_AT_COLUMN_END
            applyData(columnRebarMaterialMap, result.getRebarBendAtColumnEnd1(), Location.BEND_AT_COLUMN_END);
            applyData(columnRebarMaterialMap, result.getRebarBendAtColumnEnd2(), Location.BEND_AT_COLUMN_END);

            // SPLICING
            applyData(columnRebarMaterialMap, result.getPossibleSplicing1(), Location.SPLICING);
            applyData(columnRebarMaterialMap, result.getPossibleSplicing2(), Location.SPLICING);

        }

        return new ColumnRebarSummary(columnRebarMaterialMap);
    }

    private void applyData(Map<Integer, ColumnRebarMaterial> columnRebarMaterialMap, RSBValuePair rsbValuePair, Location location) {
        if (rsbValuePair != null){
            int rebarSize = rsbValuePair.getRsbFactor().getSize();
            if (columnRebarMaterialMap.containsKey(rebarSize)) {
                // get previous data and add the result to it.
                columnRebarMaterialMap.put(rebarSize, addResult(columnRebarMaterialMap.get(rebarSize), rsbValuePair, location));
            } else {
                // create a new map entry and set result to it.
                ColumnRebarMaterial material = new ColumnRebarMaterial();
                material.setRsbSize(RsbSize.parseInt(rebarSize));
                columnRebarMaterialMap.put(rebarSize, addResult(material, rsbValuePair, location));
            }
        }
    }

    private ColumnRebarMaterial addResult(ColumnRebarMaterial material, RSBValuePair rsbValuePair, Location location) {

        double value = rsbValuePair.getValue();

        switch (location){
            case REBAR_1:
                material.rebar1 += value;
                break;
            case REBAR_2:
                material.rebar2 += value;
                break;
            case BEND_AT_FOOTING:
                material.bendAtFootings += value;
                break;
            case BEND_AT_COLUMN_END:
                material.bendAtColumnEnds += value;
                break;
            case SPLICING:
                material.splicings += value;
                break;
        }
        return material;
    }

    @Override
    public ColumnRebarOutput calculate(boolean autoCompute, ColumnRebarInput input, ColumnRebarProperties properties, List<RSBFactor> rsbFactors) {
        this.rsbFactors = rsbFactors;

        double lengthCC = input.getLengthCC();
        if (autoCompute) {
            lengthCC = input.computeLengthCCBasedOnNodes();
        }


        ColumnRebarOutput output = new ColumnRebarOutput();


        if (properties != null) {

            RebarBend rebarBendFooting = properties.getRebarBendAtFooting();
            RebarBend rebarBendColumnEnd = properties.getRebarBendAtColumnEnd();

            QtyLength footingQtyLength1 = null;
            QtyLength footingQtyLength2 = null;

            QtyLength columnEndQtyLength1 = null;
            QtyLength columnEndLength2 = null;

            if (rebarBendFooting != null) {
                footingQtyLength1 = rebarBendFooting.getProperty1();
                footingQtyLength2 = rebarBendFooting.getProperty2();
            }

            if (rebarBendColumnEnd != null) {
                columnEndQtyLength1 = rebarBendColumnEnd.getProperty1();
                columnEndLength2 = rebarBendColumnEnd.getProperty2();
            }

            setRSBValuePairs(1, output, properties.getRebar1(), footingQtyLength1, columnEndQtyLength1, properties.getSplicing(), lengthCC, input.getWastage());
            setRSBValuePairs(2, output, properties.getRebar2(), footingQtyLength2, columnEndLength2, properties.getSplicing2(), lengthCC, input.getWastage());


            double lClear = roundHalfUp(lengthCC - input.getOffset1() - input.getOffset2());

            double theRest = 0;
            double data1 = 0;
            double length = 0;
            RSBFactor gRsbFactor = null;

            Stirrups stirrups = properties.getStirrups();
            if (stirrups != null) {
                data1 = stirrups.getData1();
                if (stirrups.getColumnTieTheRest() != null) {
                    theRest = stirrups.getColumnTieTheRest().getSpacing();
                }

                if (stirrups.getColumnTieG() != null) {
                    length = stirrups.getColumnTieG().getLength();
                    gRsbFactor = getRSB(rsbFactors, stirrups.getColumnTieG().getDiameter());
                }
            }

            double qty = (lClear - data1) / theRest;

            StirrupsOutput stirrupsOutput = new StirrupsOutput();


            if (gRsbFactor != null) {
                double rsbVal = qty * length * gRsbFactor.getWeight() * (1 + toDoublePercentage(input.getWastage()));

                stirrupsOutput.setSelectedRsb(gRsbFactor.getSize());
                stirrupsOutput.setTotalWeight(rsbVal);
            }

            output.setlClear(lClear);
            stirrupsOutput.setQuantity((int) Math.floor(qty));
            stirrupsOutput.setLength(length);
            stirrupsOutput.setTotal(stirrupsOutput.getRSBTotalWeight());
            output.setStirrupsOutput(stirrupsOutput);
        }

        return output;

    }

    private RSBValuePair createRSBValuePair(RSBFactor rsbFactor, double value) {
        return new RSBValuePair(rsbFactor, value);
    }

    private void setRSBValuePairs(int order,
                                  ColumnRebarOutput output,
                                  Rebar rebar,
                                  QtyLength rebarBendAtFooting,
                                  QtyLength rebarBendAtColumnEnd,
                                  QtyDiameterLength splicing,
                                  double lengthCC,
                                  double wastage) {

        RSBValuePair rebarResult = null;

        RSBValuePair bendAtFooting = null;

        RSBValuePair bendAtColumnEnd = null;

        RSBValuePair possibleSplicing = null;

        if (rebar != null && rebar.getBarSize() != 0) {
            RSBFactor rsbFactor = getRSB(rsbFactors, rebar.getBarSize());

            double rebarVal = (rebar.getQuantity() * lengthCC * rsbFactor.getWeight()) * (1 + toDoublePercentage(wastage));

            rebarResult = createRSBValuePair(rsbFactor, rebarVal);

            if (rebarBendAtFooting != null) {
                QtyLength qtyLength = rebarBendAtFooting;
                double bendAtFootingVal = (qtyLength.getQuantity() * qtyLength.getLength() * rsbFactor.getWeight()) * (1 + toDoublePercentage(wastage));

                bendAtFooting = createRSBValuePair(rsbFactor, bendAtFootingVal);
            }


            if (rebarBendAtColumnEnd != null) {
                QtyLength qtyLength = rebarBendAtColumnEnd;
                double bendAtColumnEndVal = (qtyLength.getQuantity() * qtyLength.getLength() * rsbFactor.getWeight()) * (1 + toDoublePercentage(wastage));

                bendAtColumnEnd = createRSBValuePair(rsbFactor, bendAtColumnEndVal);
            }

            if (splicing != null) {
                double splicingVal = (splicing.getQuantity() * splicing.getLength() * rsbFactor.getWeight()) * (1 + toDoublePercentage(wastage));

                possibleSplicing = createRSBValuePair(rsbFactor, splicingVal);
            }
        }

        if (order == 1) {
            output.setRebar1(rebarResult);
            output.setRebarBendAtFooting1(bendAtFooting);
            output.setRebarBendAtColumnEnd1(bendAtColumnEnd);
            output.setPossibleSplicing1(possibleSplicing);
        } else {
            output.setRebar2(rebarResult);
            output.setRebarBendAtFooting2(bendAtFooting);
            output.setRebarBendAtColumnEnd2(bendAtColumnEnd);
            output.setPossibleSplicing2(possibleSplicing);
        }
    }


}
