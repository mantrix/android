package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.create;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateWallFootingInputActivity extends CreateScopeInputBaseActivity implements CreateWallFootingRebarContract.View {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_description)
    TextInputEditText etDescription;
    @BindView(R.id.til_description)
    TextInputLayout tilDescription;
    @BindView(R.id.et_sets)
    TextInputEditText etSets;
    @BindView(R.id.til_sets)
    TextInputLayout tilSets;
    @BindView(R.id.et_length)
    TextInputEditText etLength;
    @BindView(R.id.til_length)
    TextInputLayout tilLength;
    @BindView(R.id.et_bars_along_l)
    TextInputEditText etBarsAlongL;
    @BindView(R.id.til_bars_along_l)
    TextInputLayout tilBarsAlongL;
    @BindView(R.id.et_spacing_along_l)
    TextInputEditText etSpacingAlongL;
    @BindView(R.id.til_spacing_along_l)
    TextInputLayout tilSpacingAlongL;
    @BindView(R.id.et_l_bars_diameters)
    TextInputEditText etLBarsDiameters;
    @BindView(R.id.til_l_bars_diameter)
    TextInputLayout tilLBarsDiameter;
    @BindView(R.id.et_t_bars_diameters)
    TextInputEditText etTBarsDiameters;
    @BindView(R.id.til_t_bars_diameter)
    TextInputLayout tilTBarsDiameter;
    @BindView(R.id.et_tie_wire)
    TextInputEditText etTieWire;
    @BindView(R.id.til_tie_wire)
    TextInputLayout tilTieWire;
    @BindView(R.id.et_unit_length_of_transverse_bars)
    TextInputEditText etUnitLengthOfTransverseBars;
    @BindView(R.id.til_unit_length_of_transverse_bars)
    TextInputLayout tilUnitLengthOfTransverseBars;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.tv_transverse_bars_count)
    TextView tvTransverseBarsCount;
    @BindView(R.id.tv_longitudinal_bars)
    TextView tvLongitudinalBars;
    @BindView(R.id.tv_ten_rsb_label)
    TextView tvTenRsbLabel;
    @BindView(R.id.tv_ten_rsb_result)
    TextView tvTenRsbResult;
    @BindView(R.id.tv_twelve_rsb_label)
    TextView tvTwelveRsbLabel;
    @BindView(R.id.tv_twelve_rsb_result)
    TextView tvTwelveRsbResult;
    @BindView(R.id.tv_g1)
    TextView tvG1;
    @BindView(R.id.cv_results)
    CardView cvResults;

    @Inject
    CreateWallFootingRebarContract.UserActionListener presenter;

    private String projectId;
    private String scopeKey;
    private String itemKey;

    private WallFootingRebarInput input;

    private ProgressDialog saveProgressDialog;

    private boolean updateContent;

    AlertDialog rsbSizePickerDialog;
    
    boolean isDiameterForLBars;

    public static Intent createIntent(Context context, String projectId, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateWallFootingInputActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.create_wall_footing_rebar_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.WALL_FOOTING_REBAR.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        input = new WallFootingRebarInput();

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();

        tvTenRsbLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 10));
        tvTwelveRsbLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 12));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.WALL_FOOTING_REBAR.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }

    @Override
    public void setRSBPickerOptions(List<RsbSize> sizes) {
        String[] options = new String[sizes.size()];
        for (int i = 0; i < sizes.size(); i++) {
            options[i] = sizes.get(i).toString();
        }

        rsbSizePickerDialog = createChooser(getString(R.string.rsb_label), options, (dialog, which) -> {

            if (isDiameterForLBars) {
                etLBarsDiameters.setText(options[which]);
                input.setRsbFactorLongitudinalBars(sizes.get(which).getValue());
            } else {
                etTBarsDiameters.setText(options[which]);
                input.setRsbFactorTransverseBars(sizes.get(which).getValue());
            }

            dialog.dismiss();
        });
    }

    @OnClick(R.id.et_l_bars_diameters)
    protected void lBarDiameterClick() {
        if (rsbSizePickerDialog != null) {
            isDiameterForLBars = true;
            rsbSizePickerDialog.setTitle(getString(R.string.diameter_for_L_bars_mm));
            rsbSizePickerDialog.show();
        }
    }

    @OnClick(R.id.et_t_bars_diameters)
    protected void tBarDiameterClick() {
        if (rsbSizePickerDialog != null) {
            isDiameterForLBars = false;
            rsbSizePickerDialog.setTitle(getString(R.string.diameter_for_T_bars_mm));
            rsbSizePickerDialog.show();
        }
    }




    @Override
    public void setInput(WallFootingRebarInput input) {
        this.input = input;
        if (input.getDescription() != null) {
            etDescription.setText(input.getDescription());
        }

        if (input.getSets() != 0) {
            etSets.setText(toDisplayFormat(input.getSets()));
        }

        if (input.getLength() != 0) {
            etLength.setText(toDisplayFormat(input.getLength()));
        }

        if (input.getNumOfBarsAlongL() != 0) {
            etBarsAlongL.setText(toDisplayFormat(input.getNumOfBarsAlongL()));
        }

        if (input.getSpacingAlongL() != 0) {
            etSpacingAlongL.setText(toDisplayFormat(input.getSpacingAlongL()));
        }

        if (input.getRsbFactorLongitudinalBars() != 0) {
            etLBarsDiameters.setText(toDisplayFormat(input.getRsbFactorLongitudinalBars()));
        }

        if (input.getRsbFactorTransverseBars() != 0) {
            etTBarsDiameters.setText(toDisplayFormat(input.getRsbFactorTransverseBars()));
        }

        if (input.getTieWireLength() != 0) {
            etTieWire.setText(toDisplayFormat(input.getTieWireLength()));
        }

        if (input.getTransverseBarsUnitLength() != 0) {
            etUnitLengthOfTransverseBars.setText(toDisplayFormat(input.getTransverseBarsUnitLength()));
        }
    }

    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayResult(WallFootingRebarResult result) {

        tvTransverseBarsCount.setText(String.format(getString(R.string.pcs_result), toDisplayFormat(result.getTraverseBarCount())));
        tvLongitudinalBars.setText(String.format(getString(R.string.meter_result), toDisplayFormat(result.getLongitudinalBarsTotalLength())));
        tvTenRsbResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getTenRSB())));
        tvTwelveRsbResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getTwelveRSB())));
        tvG1.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getGiTieWire())));

        cvResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }


    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setDescription(etDescription.getText().toString());


        String sets = etSets.getText().toString();
        if (sets.isEmpty()) {
            input.setSets(0);
        } else {
            input.setSets(Integer.parseInt(sets));
        }


        String length = etLength.getText().toString();
        if (length.isEmpty()) {
            input.setLength(0);
        } else {
            input.setLength(Double.parseDouble(length));
        }

        String barsAlongL = etBarsAlongL.getText().toString();
        if (barsAlongL.isEmpty()) {
            input.setNumOfBarsAlongL(0);
        } else {
            input.setNumOfBarsAlongL(Double.parseDouble(barsAlongL));
        }


        String spacingAlongL = etSpacingAlongL.getText().toString();
        if (spacingAlongL.isEmpty()) {
            input.setSpacingAlongL(0);
        } else {
            input.setSpacingAlongL(Double.parseDouble(spacingAlongL));
        }

        String tieWire = etTieWire.getText().toString();
        if (tieWire.isEmpty()) {
            input.setTieWireLength(0);
        } else {
            input.setTieWireLength(Double.parseDouble(tieWire));
        }

        String unitLengthOfTransverseBars = etUnitLengthOfTransverseBars.getText().toString();
        if (unitLengthOfTransverseBars.isEmpty()) {
            input.setTransverseBarsUnitLength(0);
        } else {
            input.setTransverseBarsUnitLength(Double.parseDouble(unitLengthOfTransverseBars));
        }


        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etDescription.getText().toString().isEmpty()) {
            etDescription.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etDescription.setError(null);
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
