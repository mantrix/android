package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.chb.CHBLayingMaterialCostDialogFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CHBLayingBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {CHBLayingMaterialCostDialogModule.class})
    abstract CHBLayingMaterialCostDialogFragment bindChbLayingMaterialCostDialogFragment();
}
