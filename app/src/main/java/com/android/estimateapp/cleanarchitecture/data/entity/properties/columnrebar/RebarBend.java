package com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class RebarBend extends Model {

    QtyLength property1;

    QtyLength property2;

    public RebarBend(){

    }

    public QtyLength getProperty1() {
        return property1;
    }

    public void setProperty1(QtyLength property1) {
        this.property1 = property1;
    }

    public QtyLength getProperty2() {
        return property2;
    }

    public void setProperty2(QtyLength property2) {
        this.property2 = property2;
    }
}
