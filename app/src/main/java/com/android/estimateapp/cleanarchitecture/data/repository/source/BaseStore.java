package com.android.estimateapp.cleanarchitecture.data.repository.source;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.domain.exception.ArCiRuntimeException;
import com.android.estimateapp.configuration.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import androidx.annotation.NonNull;
import io.reactivex.Completable;
import io.reactivex.Observable;

public class BaseStore {

    // REFERENCES
    protected static String FIREBASE_USER_REF = "users";
    protected static String FIREBASE_DATA_REF = "data";
    protected static String FIREBASE_PROJECT_REF = "projects";
    protected static String FIREBASE_USER_PROJECT_REF = "user-projects";
    protected static String FIREBASE_PROJECT_SCOPE_OF_WORKS_REF = "project-scope-of-works";
    protected static String FIREBASE_PROJECT_FACTORS_REF = "project-factors";
    protected static String FIREBASE_PROJECT_PROPERTY_REF = "project-properties";
    protected static String FIREBASE_RSB_FACTOR = "RSB Factor";
    protected static String FIREBASE_FACTORS = "factors";
    protected static String FIREBASE_INPUTS = "inputs";
    protected static String FIREBASE_LABOR = "labor-cost";
    protected static String FIREBASE_MATERIALS = "material-cost";
    protected static String FIREBASE_RESULT = "result";


    protected static String FIREBASE_CHB_LAYING_FACTOR = "CHB Laying Factor";
    protected static String FIREBASE_CHB_CONCRETE_MORTAR_CLASSES = "concreteMortarClasses";
    protected static String FIREBASE_CHB_SPACING = "spacing";
    protected static String FIREBASE_CHB_VERTICAL = "vertical";
    protected static String FIREBASE_CHB_HORIZONTAL = "horizontal";
    protected static String FIREBASE_CHB_TIE_WIRE_REBAR_SPACING = "tieWireRebarSpacing";

    protected static String FIREBASE_CHB_PLASTERING_MORTAR_FACTOR = "Plastering Mortar Factor";
    protected static String FIREBASE_CONCRETE_WORK_FACTOR = "Concrete Work Factor";
    protected static String FIREBASE_TILE_WORK_FACTOR = "Tile Work Factor";
    protected static String FIREBASE_TILE_TYPE_FACTOR = "Tile Types Factor";
    protected static String FIREBASE_STONE_CLADDING_FACTOR = "Stone Cladding Factor";
    protected static String FIREBASE_STONE_TYPE_FACTOR = "Stone Types Factor";
    protected static String FIREBASE_MEMBER_SECTION_FACTOR = "Member Section Properties";
    protected static String FIREBASE_BEAM_SECTION_FACTOR = "Beam Section Properties";
    protected static String FIREBASE_COLUMN_SECTION_FACTOR = "Column Section Properties";

    // FIELDS
    protected static String CREATED_BY_FIELD_NAME = "createdBy";


    protected DatabaseReference baseReference;
    protected DatabaseReference projectFactorReference;
    protected DatabaseReference projectPropertiesReference;

    public BaseStore() {
        baseReference = FirebaseDatabase.getInstance().getReference();
        baseReference = baseReference.child(Constants.FIREBASE_DB_VERSION);
        projectFactorReference = baseReference.child(FIREBASE_PROJECT_FACTORS_REF);
        projectPropertiesReference = baseReference.child(FIREBASE_PROJECT_PROPERTY_REF);
    }


    protected String getLoggedInUserUid() throws ArCiRuntimeException {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser != null && firebaseUser.getUid() != null) {
            return firebaseUser.getUid();
        }

        throw new ArCiRuntimeException(Constants.Code.NO_LOGIN_USER);
    }

    protected static <T> T getLastElement(final Iterable<T> elements) {
        final Iterator<T> itr = elements.iterator();
        T lastElement = itr.next();

        while (itr.hasNext()) {
            lastElement = itr.next();
        }

        return lastElement;
    }


    /**
     * Generic method for data creation
     */
    protected <T extends Model> Observable<T> setValue(DatabaseReference reference, T model) {
        return Observable.create(emitter -> {
            reference
                    .setValue(model);

            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String uniqueKey = dataSnapshot.getKey();
                    model.setKey(uniqueKey);
                    emitter.onNext(model);
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            });
        });
    }

    /**
     * Generic method for data creation
     */
    protected <T extends Model> Observable<T> push(DatabaseReference reference, T model) {
        return Observable.create(emitter -> {

            DatabaseReference pushRef = reference.push();

            pushRef.setValue(model);

            pushRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String uniqueKey = dataSnapshot.getKey();
                    model.setKey(uniqueKey);
                    emitter.onNext(model);
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            });
        });
    }

    public Completable delete(Query query) {
        return Completable.create(emitter -> {

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        dataSnapshot.getRef().removeValue();
                    }
                    emitter.onComplete();
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            });
        });
    }

    /**
     * Generic method for retrieving single value in Firebase
     */
    public <T extends Model> Observable<T> get(Query query, Class<T> type) {
        return Observable.create(emitter -> {

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        T t = dataSnapshot.getValue(type);
                        t.setKey(dataSnapshot.getKey());
                        emitter.onNext(t);
                    }
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            });
        });
    }

    /**
     * Generic method for retrieving list from Firebase
     */
    protected <T extends Model> Observable<List<T>> queryListValue(Query query, Class<T> type) {
        return Observable.create(emitter -> {
            query.keepSynced(true);

            ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<T> list = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        T t = snapshot.getValue(type);
                        t.setKey(snapshot.getKey());
                        list.add(t);
                    }
                    emitter.onNext(list);
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            };
            query.addListenerForSingleValueEvent(eventListener);
        });
    }


    protected Observable<List<String>> queryListKey(Query query) {
        return Observable.create(emitter -> {
            query.keepSynced(true);

            ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<String> list = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        list.add(snapshot.getKey());
                    }
                    emitter.onNext(list);
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            };
            query.addListenerForSingleValueEvent(eventListener);
        });
    }

}
