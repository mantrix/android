package com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.create.CreateConcreteWorkActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.ProjectPropertiesActivity;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryActivity;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class ConcreteWorkListActivity extends BaseActivity implements ConcreteWorkListContract.View, ListItemClickLister<RectangularConcreteInput> {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    ConcreteWorkListContract.UserActionListener presenter;

    @Inject
    ConcreteWorkListAdapter adapter;

    String projectId;
    String scopeKey;
    int scopeID;
    ScopeOfWork scopeOfWork;

    NpaLinearLayoutManager linearLayoutManager;


    public static Intent createIntent(Context context, String projectId, int scopeID, String scopeKey) {
        Intent intent = new Intent(context, ConcreteWorkListActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.SCOPE_OF_WORK, scopeID);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.scope_list_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(scopeOfWork.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        initRecyclerView();

        presenter.setView(this);
        presenter.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.work_list_w_property_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, scopeOfWork.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            case R.id.menu_summary:
                startActivityForResult(SummaryActivity.createIntent(this, projectId, scopeKey, scopeOfWork.getId()), Constants.REQUEST_CODE);
                break;
            case R.id.menu_property:
                startActivityForResult(ProjectPropertiesActivity.createIntent(this, projectId), Constants.REQUEST_CODE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        scopeID = intent.getIntExtra(Constants.SCOPE_OF_WORK, 0);
        scopeOfWork = ScopeOfWork.parseInt(scopeID);
        presenter.setData(projectId, scopeKey);
    }

    private void initRecyclerView() {
        linearLayoutManager = new NpaLinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setListItemClickLister(this);
    }


    @Override
    public void itemClick(RectangularConcreteInput input) {
        Intent intent = CreateConcreteWorkActivity.createIntent(this, projectId, scopeID, scopeKey, input.getKey());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onLongClick(RectangularConcreteInput input) {
        AlertDialog alertDialog = createAlertDialog(getString(R.string.delete),
                getString(R.string.delete_input_msg),
                getString(R.string.delete),
                getString(R.string.cancel), (dialog, which) -> {
                    presenter.deleteItem(input.getKey());
                    dialog.dismiss();
                }, (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    @Override
    public void removeItem(String inputId) {
        adapter.deleteItem(inputId);
    }

    @Override
    public void displayList(List<RectangularConcreteInput> concreteWorksInputs) {
        adapter.setItems(concreteWorksInputs);
    }

    @OnClick(R.id.fab)
    public void addItemClick() {
        Intent intent = CreateConcreteWorkActivity.createIntent(this, projectId, scopeID, scopeKey, null);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_CONTENT_UPDATE:
                    presenter.reloadList();
                    break;
            }
        }
    }

}
