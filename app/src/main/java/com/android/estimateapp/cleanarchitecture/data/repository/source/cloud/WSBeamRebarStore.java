package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudBeamRebarStore;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class WSBeamRebarStore extends BaseStore implements CloudBeamRebarStore {

    private DatabaseReference projectScopeOfWorksRef;

    @Inject
    public WSBeamRebarStore() {
        super();
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
    }

    @Override
    public Observable<BeamRebarInput> createBeamRebarInputInput(String projectId, String scopeOfWorkId, BeamRebarInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(databaseReference -> push(databaseReference, input));
    }

    @Override
    public Observable<BeamRebarInput> updateBeamRebarInputInput(String projectId, String scopeOfWorkId, String itemId, BeamRebarInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference, input));
    }

    @Override
    public Observable<BeamRebarOutput> saveBeamRebarInputResult(String projectId, String scopeOfWorkId, String itemId, BeamRebarOutput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId)
                    .child(FIREBASE_RESULT);
        }).flatMap(databaseReference -> setValue(databaseReference, input));
    }

    @Override
    public Observable<List<BeamRebarInput>> getBeamRebarInputInputs(String projectId, String scopeOfWorkId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(query -> queryListValue(query, BeamRebarInput.class));
    }

    @Override
    public Observable<BeamRebarInput> getBeamRebarInputInput(String projectId, String scopeOfWorkId, String itemId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(query -> get(query, BeamRebarInput.class));
    }

}
