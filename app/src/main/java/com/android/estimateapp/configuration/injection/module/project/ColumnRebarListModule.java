package com.android.estimateapp.configuration.injection.module.project;


import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.list.ColumnRebarListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.list.ColumnRebarListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ColumnRebarListModule {

    @Provides
    @PerActivity
    ColumnRebarListContract.UserActionListener provideColumnRebarListPresenter(ColumnRebarListPresenter presenter) {
        return presenter;
    }
}
