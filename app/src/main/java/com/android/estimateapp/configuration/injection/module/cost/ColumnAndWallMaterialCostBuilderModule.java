package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.columnwallfooting.ColumnAndWallFootingMaterialCostDialogFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ColumnAndWallMaterialCostBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {ColumnAndWallFootingMaterialCostModule.class})
    abstract ColumnAndWallFootingMaterialCostDialogFragment bindColumnAndWallFootingMaterialCostDialogFragment();
}
