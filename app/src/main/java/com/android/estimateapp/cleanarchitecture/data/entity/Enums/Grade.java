package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum Grade {

    AA(1,"AA"),
    A(2,"A"),
    B(3,"B"),
    C(4,"C"),
    D(5,"D"),
    SPECIAL_CASE_ONE(6,"1"),
    SPECIAL_CASE_TWO(7,"2"),
    SPECIAL_CASE_THREE(8,"3");

    int id;
    String name;

    Grade(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public static Grade parseInt(int value){
        for (Grade grade : getGrades()){
            if (value == grade.getId()){
                return grade;
            }
        }
        return null;
    }

    public static List<Grade> getGrades(){
        List<Grade> grades = new ArrayList<>();
        grades.add(AA);
        grades.add(A);
        grades.add(B);
        grades.add(C);
        grades.add(D);
        grades.add(SPECIAL_CASE_ONE);
        grades.add(SPECIAL_CASE_TWO);
        grades.add(SPECIAL_CASE_THREE);
        return grades;
    }
}