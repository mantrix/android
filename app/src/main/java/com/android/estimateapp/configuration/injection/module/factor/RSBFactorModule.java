package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.shared.rsb.RSBFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.shared.rsb.RSBFactorPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class RSBFactorModule {

    @Provides
    @PerFragment
    RSBFactorContract.UserActionListener providesRSBFactorPresenter(RSBFactorPresenter presenter) {
        return presenter;
    }
}
