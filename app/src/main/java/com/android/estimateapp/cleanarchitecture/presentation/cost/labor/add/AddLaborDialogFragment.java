package com.android.estimateapp.cleanarchitecture.presentation.cost.labor.add;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerEntity;
import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.LaborCostActivity;
import com.android.estimateapp.utils.DisplayUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatDialogFragment;

public class AddLaborDialogFragment extends DaggerAppCompatDialogFragment  implements AddLaborCostContract.View{

    private final static String PROJECT_ID = "projectId";
    private final static String SCOPE_KEY = "scopeKey";
    private final static String ITEM_KEY = "itemKey";
    public static final String TAG = AddLaborDialogFragment.class.getSimpleName();
    public static final int FAILED = 4000;
    @BindView(R.id.ll_header)
    LinearLayout llHeader;
    @BindView(R.id.et_worker_designation_1)
    TextInputEditText etWorkerDesignation1;
    @BindView(R.id.til_worker_designation_1)
    TextInputLayout tilWorkerDesignation1;
    @BindView(R.id.et_hourly_rate_1)
    TextInputEditText etHourlyRate1;
    @BindView(R.id.til_worker_hourly_rate_1)
    TextInputLayout tilWorkerHourlyRate1;
    @BindView(R.id.et_worker_designation_2)
    TextInputEditText etWorkerDesignation2;
    @BindView(R.id.til_worker_designation_2)
    TextInputLayout tilWorkerDesignation2;
    @BindView(R.id.et_hourly_rate_2)
    TextInputEditText etHourlyRate2;
    @BindView(R.id.til_worker_hourly_rate_2)
    TextInputLayout tilWorkerHourlyRate2;
    @BindView(R.id.et_worker_designation_3)
    TextInputEditText etWorkerDesignation3;
    @BindView(R.id.til_worker_designation_3)
    TextInputLayout tilWorkerDesignation3;
    @BindView(R.id.et_hourly_rate_3)
    TextInputEditText etHourlyRate3;
    @BindView(R.id.til_worker_hourly_rate_3)
    TextInputLayout tilWorkerHourlyRate3;


    private WorkerGroup input;
    private List<WorkerEntity> workers;

    private ProgressDialog saveProgressDialog;

    @Inject
    AddLaborCostContract.UserActionListener presenter;


    public static AddLaborDialogFragment newInstance(String projectId, String scopeKey, String itemKey) {

        Bundle args = new Bundle();
        args.putString(PROJECT_ID, projectId);
        args.putString(SCOPE_KEY, scopeKey);
        if (itemKey != null) {
            args.putString(ITEM_KEY, itemKey);
        }
        AddLaborDialogFragment fragment = new AddLaborDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.worker_group_layout, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        String itemKey = null;

        if (getArguments().containsKey(ITEM_KEY)){
            itemKey = getArguments().getString(ITEM_KEY);
        }

        input = new WorkerGroup();

        workers = new ArrayList<>();
        workers.add(new WorkerEntity());
        workers.add(new WorkerEntity());
        workers.add(new WorkerEntity());

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        presenter.setData(getArguments().getString(PROJECT_ID), getArguments().getString(SCOPE_KEY), itemKey);
        presenter.setView(this);
        presenter.start();

        return rootView;
    }


    public ProgressDialog createLoadingAlert(String title, String message){
        ProgressDialog dialog = new ProgressDialog(getActivity());
        if (title != null){
            dialog.setTitle(title);
        }

        dialog.setMessage(message);

        return dialog;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void setInput(WorkerGroup group) {
        this.input = group;

        if (group.getWorkers() != null) {

            for (int i = 0; i < group.getWorkers().size(); i++) {
                WorkerEntity worker = group.getWorkers().get(i);

                switch (i) {
                    case 0:
                        etWorkerDesignation1.setText(worker.getDesignation() != null ? worker.getDesignation() : "");
                        etHourlyRate1.setText(DisplayUtil.toDisplayFormat(worker.getHourlyRate()));
                        break;
                    case 1:
                        etWorkerDesignation2.setText(worker.getDesignation() != null ? worker.getDesignation() : "");
                        etHourlyRate2.setText(DisplayUtil.toDisplayFormat(worker.getHourlyRate()));
                        break;
                    case 2:
                        etWorkerDesignation3.setText(worker.getDesignation() != null ? worker.getDesignation() : "");
                        etHourlyRate3.setText(DisplayUtil.toDisplayFormat(worker.getHourlyRate()));
                        break;
                }
            }
        }
    }

    @Override
    public void inputSaved() {
        saveProgressDialog.dismiss();

        if (getActivity() != null && getActivity() instanceof LaborCostActivity){
            ((LaborCostActivity)getActivity()).reload();
        }

        dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }

    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingNotAllowedError() {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_add)
    public void onAddClick(){

        String designation1 = etWorkerDesignation1.getText().toString();
        String designation2 = etWorkerDesignation2.getText().toString();
        String designation3 = etWorkerDesignation3.getText().toString();

        String rate1 = etHourlyRate1.getText().toString();
        String rate2 = etHourlyRate2.getText().toString();
        String rate3 = etHourlyRate3.getText().toString();

        WorkerEntity worker1;
        if (input.getWorkers().size() >= 1){
            worker1 = input.getWorkers().get(0);
        } else {
            worker1 = new WorkerEntity();
        }

        if (!designation1.isEmpty()){
            worker1.setDesignation(designation1);
        } else {
            worker1.setDesignation("");
        }

        if (rate1.isEmpty()){
            worker1.setHourlyRate(0);
        } else {
            worker1.setHourlyRate(Double.parseDouble(rate1));
        }

        WorkerEntity worker2;
        if (input.getWorkers().size() >= 2){
            worker2 = input.getWorkers().get(1);
        } else {
            worker2 = new WorkerEntity();
        }

        if (!designation2.isEmpty()){
            worker2.setDesignation(designation2);
        } else {
            worker2.setDesignation("");
        }

        if (rate2.isEmpty()){
            worker2.setHourlyRate(0);
        } else {
            worker2.setHourlyRate(Double.parseDouble(rate2));
        }

        WorkerEntity worker3;
        if (input.getWorkers().size() >= 3){
            worker3 = input.getWorkers().get(2);
        } else {
            worker3 = new WorkerEntity();
        }

        if (!designation3.isEmpty()){
            worker3.setDesignation(designation3);
        }  else {
            worker3.setDesignation("");
        }

        if (rate3.isEmpty()){
            worker3.setHourlyRate(0);
        } else {
            worker3.setHourlyRate(Double.parseDouble(rate3));
        }

        workers.set(0,worker1);
        workers.set(1,worker2);
        workers.set(2,worker3);

        input.setWorkers(workers);

        presenter.save(input);
    }
}
