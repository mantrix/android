package com.android.estimateapp.cleanarchitecture.presentation.project.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.list.CHBListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.list.ConcreteWorkListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.list.DoorAndWindowListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.list.GeneralConcreteWorkListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.list.PlasteringListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.list.BeamRebarListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.list.ColumnRebarInputListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.list.ColumnFootingRebarListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.list.WallFootingRebarListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.list.StoneCladdingListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.list.TileWorkListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class ProjectDetailActivity extends BaseActivity implements ProjectDetailContract.View, ListItemClickLister<Work> {

    private static final String TAG = ProjectDetailActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.iv_add)
    ImageView ivAdd;

    NpaLinearLayoutManager linearLayoutManager;

    @Inject
    ProjectDetailContract.UserActionListener presenter;

    @Inject
    ScopesItemAdapter scopesItemAdapter;

    private String projectId;

    private AlertDialog scopeChooserDialog;


    public static Intent createIntent(Context context, String projectId) {
        Intent intent = new Intent(context, ProjectDetailActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);

        return intent;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.project_detail_activity_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(Constants.RESULT_CODE_BACK_BUTTON);

        setSupportActionBar(toolbar);
        setToolbarTitle(getString(R.string.project));
        extractExtras();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        initRecyclerView();

        presenter.setView(this);
        presenter.start();
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        presenter.setProjectId(projectId);
        setToolbarTitle(intent.getStringExtra(Constants.PROJECT_NAME));
    }


    @OnClick(R.id.iv_add)
    public void addScopeClick() {
        if (scopeChooserDialog != null) {
            scopeChooserDialog.show();
        }
    }

    @Override
    public void setProjectName(String projectName) {
        setToolbarTitle(projectName);
    }


    @Override
    public void itemClick(Work work) {
        Intent intent = createIntentBaseOnScopeOfWork(work.getScopeID(), work.getKey());

        if (intent != null) {
            startActivityForResult(intent, Constants.REQUEST_CODE);
        }
    }

    @Override
    public void onLongClick(Work work) {

    }

    @Override
    public void setAddScopeVisibility(boolean visible) {
        ivAdd.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setScopeChooserOption(List<ScopeOfWork> works) {

        String[] names = new String[works.size()];
        for (int i = 0; i < works.size(); i++) {
            names[i] = works.get(i).toString();
        }

        scopeChooserDialog = createChooser(getString(R.string.choose_scope_of_work), names, (dialog, which) -> {
            ScopeOfWork scopeOfWork = works.get(which);
            presenter.createProjectScope(scopeOfWork);
            dialog.dismiss();
        });
    }

    @Override
    public void displayProjectScopes(List<Work> works) {
        scopesItemAdapter.setItems(works);
    }

    private void initRecyclerView() {
        linearLayoutManager = new NpaLinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(scopesItemAdapter);
        scopesItemAdapter.setListItemClickLister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private Intent createIntentBaseOnScopeOfWork(int scopeId, String scopeKey) {
        Intent intent = null;
        switch (ScopeOfWork.parseInt(scopeId)) {
            case CHB_LAYING:
                intent = CHBListActivity.createIntent(this, projectId, scopeKey);
                break;
            case CHB_PLASTERING:
                intent = PlasteringListActivity.createIntent(this, projectId, scopeKey);
                break;
            case CONCRETE_WORKS:
                intent = GeneralConcreteWorkListActivity.createIntent(this, projectId, scopeKey);
                break;
            case TILE_WORKS:
                intent = TileWorkListActivity.createIntent(this, projectId, scopeKey);
                break;
            case STONE_CLADDING:
                intent = StoneCladdingListActivity.createIntent(this, projectId, scopeKey);
                break;
            case COLUMN_CONCRETE:
            case BEAM_CONCRETE:
                intent = ConcreteWorkListActivity.createIntent(this, projectId, scopeId, scopeKey);
                break;
            case DOORS:
            case WINDOWS:
                intent = DoorAndWindowListActivity.createIntent(this, projectId, scopeId, scopeKey);
                break;
            case WALL_FOOTING_REBAR:
                intent = WallFootingRebarListActivity.createIntent(this, projectId, scopeKey);
                break;
            case COLUMN_FOOTING_REBAR:
                intent = ColumnFootingRebarListActivity.createIntent(this, projectId, scopeKey);
                break;
            case BEAM_REBAR:
                intent = BeamRebarListActivity.createIntent(this,projectId, scopeKey);
                break;
            case COLUMN_REBAR:
                intent = ColumnRebarInputListActivity.createIntent(this,projectId, scopeKey);
                break;
        }
        return intent;
    }


}
