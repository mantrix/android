package com.android.estimateapp.cleanarchitecture.domain.usecase.impl;

import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.BaseUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.ProjectRetrieverUseCase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ProjectRetriever extends BaseUseCase implements ProjectRetrieverUseCase {

    private ProjectRepository projectRepository;

    private List<String> projectIds = new ArrayList<>();

    @Inject
    public ProjectRetriever(ProjectRepository projectRepository,
                            ThreadExecutorProvider threadExecutorProvider,
                            PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }

    @Override
    public Observable<List<Project>> getUserProjects(boolean reload, int page, int pageSize) {

        Observable<List<List<String>>> projectIdsObs;

        if (reload || projectIds.isEmpty()) {

            projectIds.clear();

            projectIdsObs = projectRepository.getUserProjectsIds()
                    .flatMap(ids -> Observable.fromIterable(ids))
                    .toSortedList((id1, id2) -> id2.compareTo(id1))
                    .toObservable()
                    .map(ids -> projectIds = ids)
                    .map(ids -> getPages(ids, pageSize));
        } else {
            projectIdsObs = Observable.just(projectIds)
                    .map(ids -> getPages(ids, pageSize));
        }

            return projectIdsObs.map(pages ->  {
            List<String> idPage = new ArrayList<>();

            if (!pages.isEmpty()){
                if (page < pages.size()){
                    idPage =  pages.get(page);
                }
            }
            return idPage;
        }).flatMap(ids -> Observable.fromIterable(ids))
                .flatMap(id -> projectRepository.getProject(id))
                .toList().toObservable();
    }

    @Override
    public Observable<Project> getProject(String id) {
        return projectRepository.getProject(id);
    }
}
