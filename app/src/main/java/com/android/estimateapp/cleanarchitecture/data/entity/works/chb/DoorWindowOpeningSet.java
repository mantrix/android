package com.android.estimateapp.cleanarchitecture.data.entity.works.chb;

public class DoorWindowOpeningSet {

    String key;

    int sets;


    public DoorWindowOpeningSet(){

    }

    public DoorWindowOpeningSet(String key, int sets){
        setKey(key);
        setSets(sets);
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }
}
