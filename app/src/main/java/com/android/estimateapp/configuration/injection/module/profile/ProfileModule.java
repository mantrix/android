package com.android.estimateapp.configuration.injection.module.profile;


import com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileContract;
import com.android.estimateapp.cleanarchitecture.presentation.profile.ProfilePresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ProfileModule {

    @Provides
    @PerActivity
    ProfileContract.UserActionListener provideProfilePresenter(ProfilePresenter presenter){
        return presenter;
    }
}
