package com.android.estimateapp.cleanarchitecture.presentation.summary.generalconcrete;

import android.os.Bundle;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ConcreteWorkMaterials;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.GeneralConcreteWorkSummary;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class ConcreteWorkSummaryFragment extends BaseFragment implements GeneralConcreteWorkSummaryContract.View {


    @Inject
    GeneralConcreteWorkSummaryContract.UserActionListener presenter;

    @Inject
    ConcreteWorkSummaryAdapter adapter;
    @BindView(R.id.tv_sand)
    TextView tvSand;
    @BindView(R.id.tv_cement)
    TextView tvCement;
    @BindView(R.id.tv_three_fourths)
    TextView tvThreeFourths;
    @BindView(R.id.tv_g1)
    TextView tvG1;


    public static ConcreteWorkSummaryFragment newInstance(String projectId, String scopeKey, int scopeID) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        args.putInt(Constants.SCOPE_OF_WORK, scopeID);
        ConcreteWorkSummaryFragment fragment = new ConcreteWorkSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rv_summary)
    RecyclerView recyclerView;

    @BindView(R.id.tv_summary_title)
    TextView tvSummaryTitle;

    private String projectId;
    private String scopeKey;
    private ScopeOfWork scopeOfWork;
    NpaLinearLayoutManager npaLinearLayoutManager;

    @Override
    protected int getLayoutResource() {
        return R.layout.concrete_work_summary_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        initRecyclerView();
        extractExtras();
        presenter.setData(projectId, scopeKey,scopeOfWork);
        presenter.setView(this);
        presenter.start();

    }

    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
        int scopeID = bundle.getInt(Constants.SCOPE_OF_WORK);
        scopeOfWork = ScopeOfWork.parseInt(scopeID);

        tvSummaryTitle.setText(String.format(getString(R.string.summary_of),scopeOfWork.toString()));

    }

    @Override
    public void displayGeneralSummary(GeneralConcreteWorkSummary<Category> summary) {
        List<ConcreteWorkMaterials> materials = new ArrayList<>();
        for (Category key : summary.getMaterialPerCategory().keySet()) {
            ConcreteWorkMaterials material = summary.getMaterialPerCategory().get(key);
            materials.add(material);
        }
        adapter.setItems(materials);

        BaseActivity baseActivity = (BaseActivity) getActivity();

        ConcreteWorkMaterials totals = summary.getTotalMaterials();

        tvSand.setText(String.format(getString(R.string.cum_result), baseActivity.toDisplayFormat(totals.sand)));
        tvCement.setText(String.format(getString(R.string.bag_result), baseActivity.toDisplayFormat(totals.cement)));
        tvThreeFourths.setText(String.format(getString(R.string.cum_result), baseActivity.toDisplayFormat(totals.threeFourthGravel)));
        tvG1.setText(String.format(getString(R.string.cum_result), baseActivity.toDisplayFormat(totals.g1Gravel)));
    }

    @Override
    public void displaySummary(GeneralConcreteWorkSummary<String> summary) {
        List<ConcreteWorkMaterials> materials = new ArrayList<>();
        for (String key : summary.getMaterialPerCategory().keySet()) {
            ConcreteWorkMaterials material = summary.getMaterialPerCategory().get(key);
            materials.add(material);
        }
        adapter.setItems(materials);

        BaseActivity baseActivity = (BaseActivity) getActivity();

        ConcreteWorkMaterials totals = summary.getTotalMaterials();

        tvSand.setText(String.format(getString(R.string.cum_result), baseActivity.toDisplayFormat(totals.sand)));
        tvCement.setText(String.format(getString(R.string.bag_result), baseActivity.toDisplayFormat(totals.cement)));
        tvThreeFourths.setText(String.format(getString(R.string.cum_result), baseActivity.toDisplayFormat(totals.threeFourthGravel)));
        tvG1.setText(String.format(getString(R.string.cum_result), baseActivity.toDisplayFormat(totals.g1Gravel)));
    }


}
