package com.android.estimateapp.cleanarchitecture.presentation.welcome.contract;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserContract;

public interface LoginContract {

    interface View {

        void loginProgressVisibility(boolean show);

        void googleLoginProgressVisibility(boolean show);

        void showInvalidCredentialError();

        void showInvalidDeviceError();

        void showNoConnectionError();

        void showUnhandledError();

        void showEmailRequiredError();

        void showPasswordRequiredError();

        void showInvalidEmailError();

        void removePasswordError();

        void removeEmailError();

        void loginSuccessful();

        void showCreateUserFragment(String uid, CreateUserContract.RegistrationMode registrationMode);
    }

    interface UserActionListener extends Presenter<View> {

        void login(String email, String password);

        void loginUsingGoogleSuccess(String userId);

        void loginSuccessful();
    }
}
