package com.android.estimateapp.cleanarchitecture.data.entity.factors.chb;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class CHBSizeFactor extends Model{

    int size;

    double cement;

    double sand;

    public CHBSizeFactor(){

    }

    public CHBSizeFactor(int size, double cement, double sand){
        this.size = size;
        this.cement = cement;
        this.sand = sand;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    @Exclude
    public CHBSize getChbSize() {
        return CHBSize.parseInt(size);
    }
}
