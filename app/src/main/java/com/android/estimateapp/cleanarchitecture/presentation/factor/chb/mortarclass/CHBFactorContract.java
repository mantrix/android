package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.mortarclass;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface CHBFactorContract {

    interface View {

        void displayList(double chb, List<CHBMortarClass> factors);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(double chb, List<CHBMortarClass> factors);

    }
}
