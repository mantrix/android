package com.android.estimateapp.cleanarchitecture.data.entity.project;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WorkerGroup extends Model {

    @SerializedName("workers")
    List<WorkerEntity> workers = new ArrayList<>();

    public WorkerGroup(){

    }

    public void setWorkers(List<WorkerEntity> workers) {
        this.workers = workers;
    }

    public List<WorkerEntity> getWorkers() {
        return workers;
    }
}
