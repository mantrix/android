package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.mortarclass.CHBFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.mortarclass.CHBFactorPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class CHBFactorModule {

    @Provides
    @PerFragment
    CHBFactorContract.UserActionListener providesCHBFactorPresenter(CHBFactorPresenter presenter) {
        return presenter;
    }
}
