package com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSizeFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.DoorWindowOpeningSet;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBLayingSummary;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerLocData;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerSizeData;

import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class CHBLayingCalculatorImpl extends BaseCalculator implements CHBLayingCalculator {

    @Inject
    public CHBLayingCalculatorImpl() {

    }

    @Override
    public CHBLayingSummary generateSummary(List<CHBResult> results){

        Map<String,CHBSummaryPerLocData> summaryPerStoreyMap = new HashMap<>();

        Map<CHBSize,CHBSummaryPerSizeData> summaryPerCHBSizeMap = new HashMap<>();
        summaryPerCHBSizeMap.put(CHBSize.FOUR, new CHBSummaryPerSizeData(CHBSize.FOUR));
        summaryPerCHBSizeMap.put(CHBSize.FIVE, new CHBSummaryPerSizeData(CHBSize.FIVE));
        summaryPerCHBSizeMap.put(CHBSize.SIX, new CHBSummaryPerSizeData(CHBSize.SIX));


        for (int i = 0 ; i < results.size() ; i++){
            CHBResult result = results.get(i);

            CHBSize chbSize = result.getChbSizeEnum();
            if (chbSize != null){
                // get previous data and add the result to it.
                summaryPerCHBSizeMap.put(chbSize,addResult(summaryPerCHBSizeMap.get(chbSize),result));
            }

            if (summaryPerStoreyMap.containsKey(result.getStoreyLocation())){
                // get previous data and add the result to it.
                summaryPerStoreyMap.put(result.getStoreyLocation(),addResult(summaryPerStoreyMap.get(result.getStoreyLocation()),result));
            } else {
                // create a new map entry and set result to it.
                summaryPerStoreyMap.put(result.getStoreyLocation(),addResult(new CHBSummaryPerLocData(),result));
            }
        }

        // convert to displayable results
        for (CHBSize chbSize : summaryPerCHBSizeMap.keySet()){
            CHBSummaryPerSizeData data = summaryPerCHBSizeMap.get(chbSize);
            data.cement = Math.ceil(data.cement);
            data.sand = truncateToOneDecimal(data.sand, RoundingMode.FLOOR);
            data.tenRsb = roundHalfUp(data.tenRsb);
            data.twelveRsb = roundHalfUp(data.twelveRsb);
            data.tieWire = Math.ceil(data.tieWire);
            summaryPerCHBSizeMap.put(chbSize,data);
        }

        for (String location : summaryPerStoreyMap.keySet()){
            CHBSummaryPerLocData data = summaryPerStoreyMap.get(location);

            data.location = location;
            data.cement = roundHalfUp(data.cement);
            data.sand = roundHalfUp(data.sand);
            data.tenRsb = roundHalfUp(data.tenRsb);
            data.twelveRsb = roundHalfUp(data.twelveRsb);
            data.tieWire = roundHalfUp(data.tieWire);

            summaryPerStoreyMap.put(location,data);
        }

        return new CHBLayingSummary(summaryPerCHBSizeMap,summaryPerStoreyMap);
    }

    private CHBSummaryPerSizeData addResult(CHBSummaryPerSizeData summaryData, CHBResult result){
        summaryData.chbCount += result.getChbCount();
        summaryData.cement += result.getCement();
        summaryData.sand += result.getSand();
        summaryData.tenRsb += result.getTenRsb();
        summaryData.twelveRsb += result.getTwelveRsb();
        summaryData.tieWire += result.getTieWire();
        return summaryData;
    }

    private CHBSummaryPerLocData addResult(CHBSummaryPerLocData summaryData, CHBResult result){

        switch (result.getChbSizeEnum()){
            case FOUR:
                summaryData.chbFourInch += result.getChbCount();
                break;
            case FIVE:
                summaryData.chbFiveInch += result.getChbCount();
                break;
            case SIX:
                summaryData.chbSixInch += result.getChbCount();
                break;
        }

        summaryData.cement += result.getCement();
        summaryData.sand += result.getSand();
        summaryData.tenRsb += result.getTenRsb();
        summaryData.twelveRsb += result.getTwelveRsb();
        summaryData.tieWire += result.getTieWire();
        return summaryData;
    }


    @Override
    public CHBResult calculate(CHBInput input, List<DoorWindowInputBase> doorsReference, List<DoorWindowInputBase> windowsReference, List<RSBFactor> rsbFactorFactors) {
        CHBResult output = new CHBResult();
        CHBFactor chbFactor = input.getChbFactor();
        List<CHBMortarClass> chbMortarClasses = chbFactor.getConcreteMortarClasses();


        double totalDoorsOpening = 0;
        double totalWindowsOpening = 0;
        double miscOpenings = input.getMiscellaneousOpenings();

        if (doorsReference != null && !doorsReference.isEmpty() && input.getDoors() != null){
            totalDoorsOpening = calculateOpening(input.getDoors(),doorsReference);
        }

        if (windowsReference != null && !windowsReference.isEmpty()  && input.getWindows() != null){
            totalWindowsOpening = calculateOpening(input.getWindows(),windowsReference);
        }

        double totalOpenings = totalDoorsOpening + totalWindowsOpening + miscOpenings;

        Dimension dimension = input.getDimension();
        double chbArea = dimension.getLength() * dimension.getWidth();
        double netArea = input.getSets() * (chbArea - totalOpenings);

        if (input.getChbSize() != 0){
            setCHBValue(output,CHBSize.parseInt(input.getChbSize()),netArea,chbFactor.getCHB());
        }


        double cement = 0;
        double sand = 0;
        if (input.getMortarClass() != 0){
            CHBMortarClass chbMortarClass = getCHBMortarClass(chbMortarClasses, Grade.parseInt(input.getMortarClass()));
            CHBSizeFactor chbSizeFactor = getCHBSizeFactor(chbMortarClass.getChbSizeFactors(),input.getChbSize());
            cement = netArea * chbSizeFactor.getCement();
            sand = netArea * chbSizeFactor.getSand();
        }


        // Compute 10mm and 12 mm RSBFactor
        CHBSpacing chbSpacing = chbFactor.getChbSpacing();
        HorizontalSpacing horizontalSpacing = getEquivalentHorizontalSpacing(chbSpacing.getHorizontalSpacings(), input.getRsbSpacing().getHorizontal());
        RSBFactor horizontalRSBFactor = getRSB(rsbFactorFactors,input.getDiameterOfRSBHorizontalBar());

        double horizontal = 0;
        if (horizontalSpacing != null && horizontalRSBFactor != null){
            horizontal = netArea * horizontalSpacing.getLength() * horizontalRSBFactor.getWeight();
        }


        VerticalSpacing verticalSpacing = getEquivalentVerticalSpacing(chbSpacing.getVerticalSpacings(), input.getRsbSpacing().getVertical());
        RSBFactor verticalRSBFactor = getRSB(rsbFactorFactors,input.getDiameterOfRSBVerticalBar());

        double vertical = 0;
        if (verticalSpacing != null && verticalRSBFactor != null){
            vertical = netArea * verticalSpacing.getLength() * verticalRSBFactor.getWeight();
        }


        double tieWire = 0;
        if (verticalSpacing != null && horizontalSpacing != null){
            tieWire  = computeTieWire(netArea, input.getTieWireLength(),chbSpacing,verticalSpacing,horizontalSpacing);
        }

        double tenRSB = 0;
        double twelveRSB = 0;

        if (input.getDiameterOfRSBVerticalBar() == RsbSize.TEN_MM.getValue()) {
            tenRSB = tenRSB + vertical;
        } else if (input.getDiameterOfRSBVerticalBar() == RsbSize.TWELVE_MM.getValue()) {
            twelveRSB = twelveRSB + vertical;
        }

        if (input.getDiameterOfRSBHorizontalBar() == RsbSize.TEN_MM.getValue()) {
            tenRSB = tenRSB + horizontal;
        } else if (input.getDiameterOfRSBHorizontalBar() == RsbSize.TWELVE_MM.getValue()) {
            twelveRSB = twelveRSB + horizontal;
        }

        output.setStoreyLocation(input.getStoreyLocation());
        output.setOpenings(roundHalfUp(totalOpenings));
        output.setArea(truncateToTwoDecimal(netArea, RoundingMode.FLOOR));
        output.setCement(roundHalfUp(cement));
        output.setSand(roundHalfUp(sand));
        output.setTenRsb(roundHalfUp(tenRSB));
        output.setTwelveRsb(roundHalfUp(twelveRSB));
        output.setTieWire(roundHalfUp(tieWire));

        return output;
    }

    private double calculateOpening(List<DoorWindowOpeningSet> sets,List<DoorWindowInputBase> doorWindowInputBases){

        double openings = 0;
        for (DoorWindowOpeningSet set : sets){

            DoorWindowInputBase doorWindowInputBase = getDoorWindowInputBase(doorWindowInputBases, set.getKey());

            if (doorWindowInputBase != null){
                openings += set.getSets() * (doorWindowInputBase.getLength() * doorWindowInputBase.getWidth());
            }
        }
        return openings;
    }

    private DoorWindowInputBase getDoorWindowInputBase(List<DoorWindowInputBase> doorWindowInputBases, String key){
        for (DoorWindowInputBase base : doorWindowInputBases){
            if (key.equalsIgnoreCase(base.getKey())){
                return base;
            }
        }
        return null;
    }

    /**
     * Formula: AREA x (CHB Pcs Per Sqr Mtr)
     */
    private void setCHBValue(CHBResult output, CHBSize chbSize, double area, double chb){
        double chbOutput = Math.round(chb * area);
        output.setChbCount(chbOutput);
        output.setChbSizeEnum(chbSize);
    }

    /**
     * Formula: AREA x (tie wire factor) x length of tie wire
     *
     * @return Tie Wire
     */
    private double computeTieWire(double area, double tieWireLength, CHBSpacing chbSpacing, VerticalSpacing verticalSpacing, HorizontalSpacing horizontalSpacing){
        TieWireRebarSpacingFactor tieWireRebarSpacingFactor = getTieWireSpacingFactor(chbSpacing.getTieWireRebarSpacingFactors(),verticalSpacing.getMeter(), horizontalSpacing.getLayer());
        return (area * tieWireRebarSpacingFactor.getTieWirePerSqm() * tieWireLength);
    }


    private CHBMortarClass getCHBMortarClass(List<CHBMortarClass> chbMortarClasses, Grade mortarClass) {
        for (CHBMortarClass chbMortarClass : chbMortarClasses) {
            if (mortarClass == chbMortarClass.getMortarClassEnum()) {
                return chbMortarClass;
            }
        }
        return null;
    }

    private HorizontalSpacing getEquivalentHorizontalSpacing(List<HorizontalSpacing> horizontalSpacings, double layer) {
        for (HorizontalSpacing spacing : horizontalSpacings) {
            if (layer == spacing.getLayer()) {
                return spacing;
            }
        }
        return null;
    }

    private VerticalSpacing getEquivalentVerticalSpacing(List<VerticalSpacing> verticalSpacings, double meter) {
        for (VerticalSpacing spacing : verticalSpacings) {
            if (meter == spacing.getMeter()) {
                return spacing;
            }
        }
        return null;
    }




    private CHBSizeFactor getCHBSizeFactor(List<CHBSizeFactor> chbSizeFactors, int size){
        for (CHBSizeFactor factor : chbSizeFactors) {
            if (size == factor.getSize()) {
                return factor;
            }
        }
        return null;
    }

    private TieWireRebarSpacingFactor getTieWireSpacingFactor(List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors, double meter, int layer){
        for (TieWireRebarSpacingFactor factor : tieWireRebarSpacingFactors) {
            if (meter == factor.getMeter() && layer == factor.getLayer()) {
                return factor;
            }
        }
        return null;
    }


}
