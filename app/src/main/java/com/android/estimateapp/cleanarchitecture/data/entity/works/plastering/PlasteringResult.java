package com.android.estimateapp.cleanarchitecture.data.entity.works.plastering;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class PlasteringResult extends Model {

    double area;

    double cement;

    double sand;

    public PlasteringResult(){

    }

    public PlasteringResult(double area, double cement, double sand){
        setArea(area);
        setCement(cement);
        setSand(sand);
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }
}
