package com.android.estimateapp.configuration.injection.module.app;

import com.android.estimateapp.cleanarchitecture.data.repository.AccountDataRepository;
import com.android.estimateapp.cleanarchitecture.data.repository.ProjectDataRepository;
import com.android.estimateapp.cleanarchitecture.data.repository.ProjectPropertiesDataRepository;
import com.android.estimateapp.cleanarchitecture.data.repository.StorageDataRepository;
import com.android.estimateapp.cleanarchitecture.data.repository.UserSessionDataRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.AccountRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.StorageRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.UserSessionRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {


    @Provides
    @Singleton
    AccountRepository provideAccountRepository(AccountDataRepository accountDataRepository){
        return accountDataRepository;
    }

    @Provides
    @Singleton
    UserSessionRepository provideUserSessionRepository(UserSessionDataRepository userSessionDataRepository){
        return userSessionDataRepository;
    }

    @Provides
    @Singleton
    StorageRepository provideStorageRepository(StorageDataRepository repository){
        return repository;
    }

    @Provides
    @Singleton
    ProjectRepository provideProjectRepository(ProjectDataRepository projectDataRepository){
        return projectDataRepository;
    }

    @Provides
    @Singleton
    ProjectPropertiesRepository provideProjectPropertiesRepository(ProjectPropertiesDataRepository repository){
        return repository;
    }
}
