package com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.create;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterLength;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyLength;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Rebar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.RebarBend;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Stirrups;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateColumnSectionPropertyActivity extends CreateScopeInputBaseActivity implements CreateColumnSecionPropertyContract.View {


    @Inject
    CreateColumnSecionPropertyContract.UserActionListener presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_description)
    TextInputEditText etDescription;
    @BindView(R.id.til_description)
    TextInputLayout tilDescription;
    @BindView(R.id.et_base)
    TextInputEditText etBase;
    @BindView(R.id.til_base)
    TextInputLayout tilBase;
    @BindView(R.id.et_height)
    TextInputEditText etHeight;
    @BindView(R.id.til_height)
    TextInputLayout tilHeight;
    @BindView(R.id.et_area)
    TextInputEditText etArea;
    @BindView(R.id.til_area)
    TextInputLayout tilArea;
    @BindView(R.id.cv_column_section_info)
    CardView cvColumnSectionInfo;
    @BindView(R.id.ll_header)
    LinearLayout llHeader;
    @BindView(R.id.et_rebar_1_qty)
    TextInputEditText etRebar1Qty;
    @BindView(R.id.til_rebar_1_qty)
    TextInputLayout tilRebar1Qty;
    @BindView(R.id.et_rebar_1_diameter)
    TextInputEditText etRebar1Diameter;
    @BindView(R.id.til_rebar_1_diameter)
    TextInputLayout tilRebar1Diameter;
    @BindView(R.id.cv_rebar_1_bar)
    CardView cvRebar1Bar;
    @BindView(R.id.et_rebar_2_qty)
    TextInputEditText etRebar2Qty;
    @BindView(R.id.til_rebar_2_qty)
    TextInputLayout tilRebar2Qty;
    @BindView(R.id.et_rebar_2_diameter)
    TextInputEditText etRebar2Diameter;
    @BindView(R.id.til_rebar_2_diameter)
    TextInputLayout tilRebar2Diameter;
    @BindView(R.id.cv_rebar_2_bar)
    CardView cvRebar2Bar;
    @BindView(R.id.et_rebar_bend_footing_qty1)
    TextInputEditText etRebarBendFootingQty1;
    @BindView(R.id.til_rebar_bend_footing_qty1)
    TextInputLayout tilRebarBendFootingQty1;
    @BindView(R.id.et_rebar_bend_footing_length1)
    TextInputEditText etRebarBendFootingLength1;
    @BindView(R.id.til_rebar_bend_footing_length1)
    TextInputLayout tilRebarBendFootingLength1;
    @BindView(R.id.et_rebar_bend_footing_qty2)
    TextInputEditText etRebarBendFootingQty2;
    @BindView(R.id.til_rebar_bend_footing_qty2)
    TextInputLayout tilRebarBendFootingQty2;
    @BindView(R.id.et_rebar_bend_footing_length2)
    TextInputEditText etRebarBendFootingLength2;
    @BindView(R.id.til_rebar_bend_footing_length2)
    TextInputLayout tilRebarBendFootingLength2;
    @BindView(R.id.cv_rebar_bend_at_footing)
    CardView cvRebarBendAtFooting;
    @BindView(R.id.et_rebar_bend_column_qty1)
    TextInputEditText etRebarBendColumnQty1;
    @BindView(R.id.til_rebar_bend_column_qty1)
    TextInputLayout tilRebarBendColumnQty1;
    @BindView(R.id.et_rebar_bend_column_length1)
    TextInputEditText etRebarBendColumnLength1;
    @BindView(R.id.til_rebar_bend_column_length1)
    TextInputLayout tilRebarBendColumnLength1;
    @BindView(R.id.et_rebar_bend_column_qty2)
    TextInputEditText etRebarBendColumnQty2;
    @BindView(R.id.til_rebar_bend_column_qty2)
    TextInputLayout tilRebarBendColumnQty2;
    @BindView(R.id.et_rebar_bend_column_length2)
    TextInputEditText etRebarBendColumnLength2;
    @BindView(R.id.til_rebar_bend_column_length2)
    TextInputLayout tilRebarBendColumnLength2;
    @BindView(R.id.cv_rebar_bend_at_column)
    CardView cvRebarBendAtColumn;
    @BindView(R.id.et_a_qty)
    TextInputEditText etAQty;
    @BindView(R.id.til_a_qty)
    TextInputLayout tilAQty;
    @BindView(R.id.et_a_spacing)
    TextInputEditText etASpacing;
    @BindView(R.id.til_a_spacing)
    TextInputLayout tilASpacing;
    @BindView(R.id.et_b_qty)
    TextInputEditText etBQty;
    @BindView(R.id.til_b_qty)
    TextInputLayout tilBQty;
    @BindView(R.id.et_b_spacing)
    TextInputEditText etBSpacing;
    @BindView(R.id.til_b_spacing)
    TextInputLayout tilBSpacing;
    @BindView(R.id.et_c_qty)
    TextInputEditText etCQty;
    @BindView(R.id.til_c_qty)
    TextInputLayout tilCQty;
    @BindView(R.id.et_c_spacing)
    TextInputEditText etCSpacing;
    @BindView(R.id.til_c_spacing)
    TextInputLayout tilCSpacing;
    @BindView(R.id.et_the_rest_spacing)
    TextInputEditText etTheRestSpacing;
    @BindView(R.id.til_the_rest_spacing)
    TextInputLayout tilTheRestSpacing;
    @BindView(R.id.et_d_qty)
    TextInputEditText etDQty;
    @BindView(R.id.til_d_qty)
    TextInputLayout tilDQty;
    @BindView(R.id.et_d_spacing)
    TextInputEditText etDSpacing;
    @BindView(R.id.til_d_spacing)
    TextInputLayout tilDSpacing;
    @BindView(R.id.et_e_qty)
    TextInputEditText etEQty;
    @BindView(R.id.til_e_qty)
    TextInputLayout tilEQty;
    @BindView(R.id.et_e_spacing)
    TextInputEditText etESpacing;
    @BindView(R.id.til_e_spacing)
    TextInputLayout tilESpacing;
    @BindView(R.id.et_f_qty)
    TextInputEditText etFQty;
    @BindView(R.id.til_f_qty)
    TextInputLayout tilFQty;
    @BindView(R.id.et_f_spacing)
    TextInputEditText etFSpacing;
    @BindView(R.id.til_f_spacing)
    TextInputLayout tilFSpacing;
    @BindView(R.id.et_g_diameter)
    TextInputEditText etGDiameter;
    @BindView(R.id.til_g_diameter)
    TextInputLayout tilGDiameter;
    @BindView(R.id.et_g_length)
    TextInputEditText etGLength;
    @BindView(R.id.til_g_length)
    TextInputLayout tilGLength;
    @BindView(R.id.tv_stirrups_total_qty)
    TextView tvStirrupsTotalQty;
    @BindView(R.id.cv_stirrups)
    CardView cvStirrups;
    @BindView(R.id.et_splicing_qty1)
    TextInputEditText etSplicingQty1;
    @BindView(R.id.til_splicing_qty1)
    TextInputLayout tilSplicingQty1;
    @BindView(R.id.et_splicing_diameter1)
    TextInputEditText etSplicingDiameter1;
    @BindView(R.id.til_splicing_diameter1)
    TextInputLayout tilSplicingDiameter1;
    @BindView(R.id.et_splicing_length1)
    TextInputEditText etSplicingLength1;
    @BindView(R.id.til_splicing_length1)
    TextInputLayout tilSplicingLength1;
    @BindView(R.id.et_splicing_qty2)
    TextInputEditText etSplicingQty2;
    @BindView(R.id.til_splicing_qty2)
    TextInputLayout tilSplicingQty2;
    @BindView(R.id.et_splicing_diameter2)
    TextInputEditText etSplicingDiameter2;
    @BindView(R.id.til_splicing_diameter2)
    TextInputLayout tilSplicingDiameter2;
    @BindView(R.id.et_splicing_length2)
    TextInputEditText etSplicingLength2;
    @BindView(R.id.til_splicing_length2)
    TextInputLayout tilSplicingLength2;
    @BindView(R.id.cv_splicing)
    CardView cvSplicing;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;


    private String projectId;
    private String itemKey;

    private ColumnRebarProperties input;

    private ProgressDialog saveProgressDialog;

    private boolean updateContent;

    AlertDialog rsbSizePickerDialog;
    RSBInputLoc selectedRsbInputLoc;

    Rebar rebar1;
    Rebar rebar2;

    RebarBend rebarBendAtFooting;
    QtyLength footingProperty1;
    QtyLength footingProperty2;

    RebarBend rebarBendAtColumnEnd;
    QtyLength columnProperty1;
    QtyLength columnProperty2;

    QtyDiameterSpacing stirrupsA;
    QtyDiameterSpacing stirrupsB;
    QtyDiameterSpacing stirrupsC;
    QtyDiameterSpacing stirrupsTheRest;
    QtyDiameterSpacing stirrupsD;
    QtyDiameterSpacing stirrupsE;
    QtyDiameterSpacing stirrupsF;
    QtyDiameterSpacing stirrupsG;

    Stirrups stirrups;
    QtyDiameterLength splicing1;
    QtyDiameterLength splicing2;


    enum RSBInputLoc {
        REBAR_1,
        REBAR_2,
        STIRRUPS_G,
        SPLICING_1,
        SPLICING_2
    }


    public static Intent createIntent(Context context, String projectId, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateColumnSectionPropertyActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.create_column_section_property_input_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(getString(R.string.label_column_section_properties));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        tvStirrupsTotalQty.setText(String.format(getString(R.string.label_total_quantity), Integer.toString(0)));

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        input = new ColumnRebarProperties();

        presenter.setView(this);
        presenter.setData(projectId, itemKey);
        presenter.start();

        rebar1 = new Rebar();
        rebar2 = new Rebar();


        rebarBendAtFooting = new RebarBend();
        footingProperty1 = new QtyLength();
        footingProperty2 = new QtyLength();

        rebarBendAtColumnEnd = new RebarBend();
        columnProperty1 = new QtyLength();
        columnProperty2 = new QtyLength();

        stirrups = new Stirrups();
        stirrupsA = new QtyDiameterSpacing();
        stirrupsB = new QtyDiameterSpacing();
        stirrupsC = new QtyDiameterSpacing();
        stirrupsTheRest = new QtyDiameterSpacing();
        stirrupsD = new QtyDiameterSpacing();
        stirrupsE = new QtyDiameterSpacing();
        stirrupsF = new QtyDiameterSpacing();
        stirrupsG = new QtyDiameterSpacing();

        splicing1 = new QtyDiameterLength();
        splicing2 = new QtyDiameterLength();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }


    @Override
    public void setRSBPickerOptions(List<RsbSize> sizes) {
        String[] options = new String[sizes.size()];
        for (int i = 0; i < sizes.size(); i++) {
            options[i] = sizes.get(i).toString();
        }

        rsbSizePickerDialog = createChooser(getString(R.string.diameter_symbol), options, (dialog, which) -> {

            setRSBInputValue(sizes.get(which));

            dialog.dismiss();
        });
    }

    @OnClick({R.id.et_rebar_1_diameter,
            R.id.et_rebar_2_diameter,
            R.id.et_g_diameter,
            R.id.et_splicing_diameter1,
            R.id.et_splicing_diameter2})
    protected void diameterFieldClick(View view) {

        switch (view.getId()) {
            case R.id.et_rebar_1_diameter:
                selectedRsbInputLoc = RSBInputLoc.REBAR_1;
                break;
            case R.id.et_rebar_2_diameter:
                selectedRsbInputLoc = RSBInputLoc.REBAR_2;
                break;
            case R.id.et_g_diameter:
                selectedRsbInputLoc = RSBInputLoc.STIRRUPS_G;
                break;
            case R.id.et_splicing_diameter1:
                selectedRsbInputLoc = RSBInputLoc.SPLICING_1;
                break;
            case R.id.et_splicing_diameter2:
                selectedRsbInputLoc = RSBInputLoc.SPLICING_2;
                break;
        }

        rsbSizePickerDialog.show();

    }

    private void setRSBInputValue(RsbSize rsbSize) {
        TextView textView = null;
        switch (selectedRsbInputLoc) {
            case REBAR_1:
                rebar1.setBarSize(rsbSize.getValue());
                textView = etRebar1Diameter;
                break;
            case REBAR_2:
                rebar2.setBarSize(rsbSize.getValue());
                textView = etRebar2Diameter;
                break;
            case STIRRUPS_G:
                stirrupsG.setDiameter(rsbSize.getValue());
                textView = etGDiameter;
                break;
            case SPLICING_1:
                splicing1.setDiameter(rsbSize.getValue());
                textView = etSplicingDiameter1;
                break;
            case SPLICING_2:
                splicing2.setDiameter(rsbSize.getValue());
                textView = etSplicingDiameter2;
                break;
        }

        if (textView != null) {
            textView.setText(Integer.toString(rsbSize.getValue()));
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void setInput(ColumnRebarProperties input) {
        this.input = input;
        if (input.getDescription() != null) {
            etDescription.setText(input.getDescription());
        }

        if (input.getBase() != 0) {
            etBase.setText(toDisplayFormat(input.getBase()));
        }

        if (input.getHeight() != 0) {
            etHeight.setText(toDisplayFormat(input.getHeight()));
        }

        etArea.setText(toDisplayFormat(input.getArea()));


        if (input.getRebar1() != null) {
            rebar1 = input.getRebar1();

            etRebar1Qty.setText(toDisplayFormat(rebar1.getQuantity()));
            etRebar1Diameter.setText(toDisplayFormat(rebar1.getBarSize()));
        }

        if (input.getRebar2() != null) {
            rebar2 = input.getRebar2();

            etRebar2Qty.setText(toDisplayFormat(rebar2.getQuantity()));
            etRebar2Diameter.setText(toDisplayFormat(rebar2.getBarSize()));
        }


        if (input.getRebarBendAtFooting() != null) {

            rebarBendAtFooting = input.getRebarBendAtFooting();

            if (rebarBendAtFooting.getProperty1() != null) {
                footingProperty1 = rebarBendAtFooting.getProperty1();

                etRebarBendFootingLength1.setText(toDisplayFormat(footingProperty1.getLength()));
                etRebarBendFootingQty1.setText(toDisplayFormat(footingProperty1.getQuantity()));
            }

            if (rebarBendAtFooting.getProperty2() != null) {
                footingProperty2 = rebarBendAtFooting.getProperty2();

                etRebarBendFootingLength2.setText(toDisplayFormat(footingProperty2.getLength()));
                etRebarBendFootingQty2.setText(toDisplayFormat(footingProperty2.getQuantity()));
            }
        }


        if (input.getRebarBendAtColumnEnd() != null) {
            rebarBendAtColumnEnd = input.getRebarBendAtColumnEnd();

            if (rebarBendAtColumnEnd.getProperty1() != null) {
                columnProperty1 = rebarBendAtColumnEnd.getProperty1();

                etRebarBendColumnLength1.setText(toDisplayFormat(columnProperty1.getLength()));
                etRebarBendColumnQty1.setText(toDisplayFormat(columnProperty1.getQuantity()));
            }

            if (rebarBendAtColumnEnd.getProperty2() != null) {
                columnProperty2 = rebarBendAtColumnEnd.getProperty2();

                etRebarBendColumnLength2.setText(toDisplayFormat(columnProperty2.getLength()));
                etRebarBendColumnQty2.setText(toDisplayFormat(columnProperty2.getQuantity()));
            }
        }



        if (input.getStirrups() != null) {
            stirrups = input.getStirrups();


            if (stirrups.getColumnTieA() != null) {
                stirrupsA = stirrups.getColumnTieA();

                etAQty.setText(toDisplayFormat(stirrupsA.getQuantity()));
                etASpacing.setText(toDisplayFormat(stirrupsA.getSpacing()));
            }


            if (stirrups.getColumnTieB() != null) {
                stirrupsB = stirrups.getColumnTieB();
                etBQty.setText(toDisplayFormat(stirrupsB.getQuantity()));
                etBSpacing.setText(toDisplayFormat(stirrupsB.getSpacing()));
            }


            if (stirrups.getColumnTieC() != null) {
                stirrupsC = stirrups.getColumnTieC();
                etCQty.setText(toDisplayFormat(stirrupsC.getQuantity()));
                etCSpacing.setText(toDisplayFormat(stirrupsC.getSpacing()));
            }


            if (stirrups.getColumnTieTheRest() != null) {
                stirrupsTheRest = stirrups.getColumnTieTheRest();
                etTheRestSpacing.setText(toDisplayFormat(stirrupsTheRest.getSpacing()));
            }


            if (stirrups.getColumnTieD() != null) {
                stirrupsD = stirrups.getColumnTieD();
                etDQty.setText(toDisplayFormat(stirrupsD.getQuantity()));
                etDSpacing.setText(toDisplayFormat(stirrupsD.getSpacing()));
            }


            if (stirrups.getColumnTieE() != null) {
                stirrupsE = stirrups.getColumnTieE();
                etEQty.setText(toDisplayFormat(stirrupsE.getQuantity()));
                etESpacing.setText(toDisplayFormat(stirrupsE.getSpacing()));
            }


            if (stirrups.getColumnTieF() != null) {
                stirrupsF = stirrups.getColumnTieF();
                etFQty.setText(toDisplayFormat(stirrupsF.getQuantity()));
                etFSpacing.setText(toDisplayFormat(stirrupsF.getSpacing()));
            }


            if (stirrups.getColumnTieG() != null) {
                stirrupsG = stirrups.getColumnTieG();
                etGLength.setText(toDisplayFormat(stirrupsG.getLength()));
                if (stirrupsG.getDiameter() != 0)
                    etGDiameter.setText(toDisplayFormat(stirrupsG.getDiameter()));

            }
        }


        if (input.getSplicing() != null) {
            splicing1 = input.getSplicing();
            etSplicingQty1.setText(toDisplayFormat(splicing1.getQuantity()));
            etSplicingLength1.setText(toDisplayFormat(splicing1.getLength()));

            if (splicing1.getDiameter() != 0)
                etSplicingDiameter1.setText(toDisplayFormat(splicing1.getDiameter()));
        }


        if (input.getSplicing2() != null) {
            splicing2 = input.getSplicing2();
            etSplicingQty2.setText(toDisplayFormat(splicing2.getQuantity()));
            etSplicingLength2.setText(toDisplayFormat(splicing2.getLength()));

            if (splicing2.getDiameter() != 0)
                etSplicingDiameter2.setText(toDisplayFormat(splicing2.getDiameter()));
        }

    }

    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }


    @Override
    public void displayResult(int splicingTotalQty, double area) {
        tvStirrupsTotalQty.setText(String.format(getString(R.string.label_total_quantity), Integer.toString(splicingTotalQty)));
        etArea.setText(Double.toString(area));
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }


    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setDescription(etDescription.getText().toString());

        String base = etBase.getText().toString();
        if (base.isEmpty()) {
            input.setBase(0);
        } else {
            input.setBase(Double.parseDouble(base));
        }

        String height = etHeight.getText().toString();
        if (height.isEmpty()) {
            input.setHeight(0);
        } else {
            input.setHeight(Double.parseDouble(height));
        }


        // REBAR 1
        if (!etRebar1Qty.getText().toString().isEmpty()) {
            rebar1.setQuantity(Integer.parseInt(etRebar1Qty.getText().toString()));
        }

        if (!etRebar1Diameter.getText().toString().isEmpty()) {
            rebar1.setBarSize(Integer.parseInt(etRebar1Diameter.getText().toString()));
        }


        // REBAR 2
        if (!etRebar2Qty.getText().toString().isEmpty()) {
            rebar2.setQuantity(Integer.parseInt(etRebar2Qty.getText().toString()));
        }

        if (!etRebar2Diameter.getText().toString().isEmpty()) {
            rebar2.setBarSize(Integer.parseInt(etRebar2Diameter.getText().toString()));
        }

        // FOOTING
        if (!etRebarBendFootingQty1.getText().toString().isEmpty()) {
            footingProperty1.setQuantity(Integer.parseInt(etRebarBendFootingQty1.getText().toString()));
        }

        if (!etRebarBendFootingLength1.getText().toString().isEmpty()) {
            footingProperty1.setLength(Double.parseDouble(etRebarBendFootingLength1.getText().toString()));
        }


        if (!etRebarBendFootingQty2.getText().toString().isEmpty()) {
            footingProperty2.setQuantity(Integer.parseInt(etRebarBendFootingQty2.getText().toString()));
        }

        if (!etRebarBendFootingLength2.getText().toString().isEmpty()) {
            footingProperty2.setLength(Double.parseDouble(etRebarBendFootingLength2.getText().toString()));
        }

        // COLUMN

        if (!etRebarBendColumnQty1.getText().toString().isEmpty()) {
            columnProperty1.setQuantity(Integer.parseInt(etRebarBendColumnQty1.getText().toString()));
        }

        if (!etRebarBendColumnLength1.getText().toString().isEmpty()) {
            columnProperty1.setLength(Double.parseDouble(etRebarBendColumnLength1.getText().toString()));
        }


        if (!etRebarBendColumnQty2.getText().toString().isEmpty()) {
            columnProperty2.setQuantity(Integer.parseInt(etRebarBendColumnQty2.getText().toString()));
        }

        if (!etRebarBendColumnLength2.getText().toString().isEmpty()) {
            columnProperty2.setLength(Double.parseDouble(etRebarBendColumnLength2.getText().toString()));
        }

        // STIRRUPS A
        if (!etAQty.getText().toString().isEmpty()) {
            stirrupsA.setQuantity(Integer.parseInt(etAQty.getText().toString()));
        }

        if (!etASpacing.getText().toString().isEmpty()){
            stirrupsA.setSpacing(Double.parseDouble(etASpacing.getText().toString()));
        }

        // STIRRUPS B
        if (!etBQty.getText().toString().isEmpty()) {
            stirrupsB.setQuantity(Integer.parseInt(etBQty.getText().toString()));
        }

        if (!etBSpacing.getText().toString().isEmpty()){
            stirrupsB.setSpacing(Double.parseDouble(etBSpacing.getText().toString()));
        }

        // STIRRUPS C
        if (!etCQty.getText().toString().isEmpty()) {
            stirrupsC.setQuantity(Integer.parseInt(etCQty.getText().toString()));
        }

        if (!etCSpacing.getText().toString().isEmpty()){
            stirrupsC.setSpacing(Double.parseDouble(etCSpacing.getText().toString()));
        }

        // STIRRUPS THE REST
        if (!etTheRestSpacing.getText().toString().isEmpty()) {
            stirrupsTheRest.setSpacing(Double.parseDouble(etTheRestSpacing.getText().toString()));
        }


        // STIRRUPS D
        if (!etDQty.getText().toString().isEmpty()) {
            stirrupsD.setQuantity(Integer.parseInt(etDQty.getText().toString()));
        }

        if (!etDSpacing.getText().toString().isEmpty()){
            stirrupsD.setSpacing(Double.parseDouble(etDSpacing.getText().toString()));
        }

        // STIRRUPS E
        if (!etEQty.getText().toString().isEmpty()) {
            stirrupsE.setQuantity(Integer.parseInt(etEQty.getText().toString()));
        }

        if (!etESpacing.getText().toString().isEmpty()){
            stirrupsE.setSpacing(Double.parseDouble(etESpacing.getText().toString()));
        }


        // STIRRUPS F
        if (!etFQty.getText().toString().isEmpty()) {
            stirrupsF.setQuantity(Integer.parseInt(etFQty.getText().toString()));
            stirrupsF.setSpacing(Double.parseDouble(etFSpacing.getText().toString()));
        }

        if (!etFSpacing.getText().toString().isEmpty()){
            stirrupsF.setSpacing(Double.parseDouble(etFSpacing.getText().toString()));
        }

        // STIRRUPS G
        if (!etGLength.getText().toString().isEmpty()) {
            stirrupsG.setLength(Double.parseDouble(etGLength.getText().toString()));
        }


        // SPLICING
        if (!etSplicingQty1.getText().toString().isEmpty()) {
            splicing1.setQuantity(Integer.parseInt(etSplicingQty1.getText().toString()));
        }

        if (!etSplicingLength1.getText().toString().isEmpty()) {
            splicing1.setLength(Double.parseDouble(etSplicingLength1.getText().toString()));
        }

        if (!etSplicingQty2.getText().toString().isEmpty()) {
            splicing2.setQuantity(Integer.parseInt(etSplicingQty2.getText().toString()));
        }

        if (!etSplicingLength2.getText().toString().isEmpty()) {
            splicing2.setLength(Double.parseDouble(etSplicingLength2.getText().toString()));
        }


        input.setRebar1(rebar1);
        input.setRebar2(rebar2);


        rebarBendAtFooting.setProperty1(footingProperty1);
        rebarBendAtFooting.setProperty2(footingProperty2);
        input.setRebarBendAtFooting(rebarBendAtFooting);

        rebarBendAtColumnEnd.setProperty1(columnProperty1);
        rebarBendAtColumnEnd.setProperty2(columnProperty2);
        input.setRebarBendAtColumnEnd(rebarBendAtColumnEnd);


        stirrups.setColumnTieA(stirrupsA);
        stirrups.setColumnTieB(stirrupsB);
        stirrups.setColumnTieC(stirrupsC);
        stirrups.setColumnTieTheRest(stirrupsTheRest);
        stirrups.setColumnTieD(stirrupsD);
        stirrups.setColumnTieE(stirrupsE);
        stirrups.setColumnTieF(stirrupsF);
        stirrups.setColumnTieG(stirrupsG);
        input.setStirrups(stirrups);

        input.setSplicing(splicing1);
        input.setSplicing2(splicing2);

        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etDescription.getText().toString().isEmpty()) {
            etDescription.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etDescription.setError(null);
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
