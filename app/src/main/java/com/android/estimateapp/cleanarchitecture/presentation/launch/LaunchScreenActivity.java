package com.android.estimateapp.cleanarchitecture.presentation.launch;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.WelcomeActivity;
import com.mantrixengineering.estimateapp.BuildConfig;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;

public class LaunchScreenActivity extends BaseActivity implements LaunchScreenContract.View {


    @Inject
    LaunchScreenContract.UserActionListener presenter;

    @BindView(R.id.tv_version)
    TextView tvVersion;

    @Override
    protected int getLayoutResource() {
        return R.layout.splash_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter.setView(this);
        presenter.start();

        tvVersion.setText(String.format("Version: %s", BuildConfig.VERSION_NAME));
    }

    @Override
    public void navigateToWelcomeScreen() {

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
            finish();
        }, 2000);

    }
}
