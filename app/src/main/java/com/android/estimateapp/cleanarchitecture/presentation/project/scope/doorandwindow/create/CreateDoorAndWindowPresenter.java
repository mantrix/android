package com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.create;

import android.view.Window;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.DoorWindowOpeningSet;
import com.android.estimateapp.cleanarchitecture.data.repository.source.cloud.WSDoorAndWindowStore;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.workandoor.DoorAndWindowCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

@PerActivity
public class CreateDoorAndWindowPresenter extends CreateScopeInputBasePresenter implements CreateDoorAndWindowContract.UserActionListener {

    private static final String TAG = CreateDoorAndWindowPresenter.class.getSimpleName();

    private CreateDoorAndWindowContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ScopeOfWork scopeOfWork;
    private ProjectRepository projectRepository;
    private DoorAndWindowCalculator doorAndWindowCalculator;

    private DoorWindowInputBase input;
    private DoorWindowOutputBase result;

    private int sets = 0;

    private List<CHBInput> chbInputs = new ArrayList<>();


    @Inject
    public CreateDoorAndWindowPresenter(ProjectRepository projectRepository,
                                        DoorAndWindowCalculator doorAndWindowCalculator,
                                        AccountSubscriptionUseCase accountSubscriptionUseCase,
                                        ThreadExecutorProvider threadExecutorProvider,
                                        PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase, threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.doorAndWindowCalculator = doorAndWindowCalculator;
    }

    @Override
    public void setView(CreateDoorAndWindowContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey, ScopeOfWork scopeOfWork) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
        this.scopeOfWork = scopeOfWork;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {

        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateDoorAndWindowInput(projectId, scopeKey, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createDoorAndWindowInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<DoorWindowInputBase> inputObservable) {

        view.showSavingDialog();

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    saveResult();
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    private void saveResult(){
        if (result != null && itemKey != null) {
            projectRepository.saveDoorAndWindowInput(projectId, scopeKey, itemKey, result)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(result -> {
                        this.result = result;
                    }, throwable -> {
                        throwable.printStackTrace();
                        view.savingError();
                    });
        }
    }

    @Override
    public void calculate(DoorWindowInputBase input) {
        result = doorAndWindowCalculator.calculate(sets, input);
        this.input = input;
        view.displayResult(result);
    }



    private void loadInput() {
        projectRepository.getDoorAndWindowInput(projectId, scopeKey, itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    this.input = input;
                    calculateSets();
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }


    private void calculateSets() {
        Observable.fromCallable(() -> {
            /* Compute total sets */
            int sets = 0;

            if (this.input != null && this.input.getKey() != null && !chbInputs.isEmpty()) {
                for (CHBInput input : chbInputs) {

                    List<DoorWindowOpeningSet> doorWindowOpeningSets = new ArrayList<>();

                    if (scopeOfWork == ScopeOfWork.DOORS) {
                        doorWindowOpeningSets = input.getDoors();
                    } else if (scopeOfWork == ScopeOfWork.WINDOWS) {
                        doorWindowOpeningSets = input.getWindows();
                    }

                    for (DoorWindowOpeningSet openingSet : doorWindowOpeningSets) {
                        if (this.input.getKey().equalsIgnoreCase(openingSet.getKey())) {
                            sets += openingSet.getSets();
                        }
                    }
                }
            }
            return sets;
        }).subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(count -> {
                    sets = count;
                    view.setInput(input, sets);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void loadFactors(boolean factorUpdate) {
        projectRepository.getProjectScopesOfWorks(projectId)
                .flatMap(works -> {
                    for (Work work : works){
                        if (ScopeOfWork.CHB_LAYING.getId() ==  work.getScopeID()){
                            return projectRepository.getCHBInputs(projectId,work.getKey());
                        }
                    }
                    List<CHBInput> inputs = new ArrayList<>();
                    return Observable.just(inputs);
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbInputs -> {
                    this.chbInputs = chbInputs;

                    view.enabledCalculateBtn(true);

                    if (!factorUpdate) {
                        if (itemKey != null) {
                            loadInput();
                        }
                    } else {
                        view.requestForReCalculation();
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }



}
