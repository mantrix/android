package com.android.estimateapp.cleanarchitecture.data.entity;

import com.google.gson.annotations.SerializedName;

public class UserPayload {

    @SerializedName("uid")
    public String uid;

    public UserPayload() {
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }
}