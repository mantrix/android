package com.android.estimateapp.cleanarchitecture.presentation.summary.columnfooting;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;

public interface ColumnFootingMaterialSummaryContract {

    interface View extends SummaryView<RebarMaterialSummary> {

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
