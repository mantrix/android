package com.android.estimateapp.configuration.injection.module.properties;

import com.android.estimateapp.cleanarchitecture.presentation.properties.membersection.MemberSectionPropertieContract;
import com.android.estimateapp.cleanarchitecture.presentation.properties.membersection.MemberSectionPropertiesPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class MemberSectionModule {

    @Provides
    @PerFragment
    MemberSectionPropertieContract.UserActionListener provideMemberSectionPresenter(MemberSectionPropertiesPresenter presenter) {
        return presenter;
    }
}
