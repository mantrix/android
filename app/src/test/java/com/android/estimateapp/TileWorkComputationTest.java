package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles.TileWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkMaterials;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkSummary;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class TileWorkComputationTest extends BaseTest {

    TileWorkCalculatorImpl tileWorkCalculator;

    @Before
    public void setup() {
        tileWorkCalculator = new TileWorkCalculatorImpl();
    }

    @Test
    public void tileTest() {

        TileWorkInput input = new TileWorkInput();
        input.setSets(2);
        Dimension dimension = new Dimension();
        dimension.setLength(20.2);
        dimension.setWidth(1.2);
        input.setDimension(dimension);
        input.setTileType(new TileType(30,30));
        input.setWastage(10);
        input.setMortarClass(Grade.A.getId());


        TileWorkFactor factor;
        switch (input.getMortarClassEnum()) {
            case A:
                factor = new TileWorkFactor(Grade.A.getId(), 0.03, 0.15, 0.2);
                break;
            case B:
                factor = new TileWorkFactor(Grade.B.getId(), 0.011, 0.087, 0.12);
                break;
            case C:
                factor = new TileWorkFactor(Grade.C.getId(), 0.012, 0.088, 0.13);
                break;
            default:
                factor = new TileWorkFactor(Grade.A.getId(), 0.03, 0.15, 0.2);
                break;
        }

        TileWorkResult result = tileWorkCalculator.calculate(factor, input);


        System.out.println("Area: " + result.getArea());
//        System.out.println("w1: " + result.getTileLength());
//        System.out.println("w2: " + result.getTileWidth());
        System.out.println("Tiles: " + result.getTiles());
        System.out.println("Sand: " + result.getSand());
        System.out.println("Cement: " + result.getCement());
        System.out.println("Adhesive: " + result.getAdhesive());
    }

    @Test
    public void generateSummaryTest(){

        TileType stall = new TileType();
        stall.setName("Stall");
        stall.setWidth(40);
        stall.setLength(40);

        TileType corridor = new TileType();
        corridor.setName("Corridor");
        corridor.setWidth(50);
        corridor.setLength(50);


        TileWorkResult tile1Result = new TileWorkResult();
        tile1Result.setTileType(stall);
        tile1Result.setArea(18);
        tile1Result.setTiles(124);
        tile1Result.setSand(0.54);
        tile1Result.setCement(2.70);
        tile1Result.setAdhesive(3.60);


        TileWorkResult tile2Result = new TileWorkResult();
        tile2Result.setTileType(stall);
        tile2Result.setArea(41.76);
        tile2Result.setTiles(288);
        tile2Result.setSand(0.46);
        tile2Result.setCement(3.63);
        tile2Result.setAdhesive(5.01);

        TileWorkResult corResult = new TileWorkResult();
        corResult.setTileType(corridor);
        corResult.setArea(8.75);
        corResult.setTiles(49);
        corResult.setSand(0.10);
        corResult.setCement(0.76);
        corResult.setAdhesive(1.05);

        TileWorkResult cor2Result = new TileWorkResult();
        cor2Result.setTileType(corridor);
        cor2Result.setArea(5.40);
        cor2Result.setTiles(30);
        cor2Result.setSand(0.06);
        cor2Result.setCement(0.47);
        cor2Result.setAdhesive(0.65);

        TileWorkSummary summary = tileWorkCalculator.generateSummary(Arrays.asList(tile1Result,cor2Result,tile2Result,corResult));

        for (String key : summary.getMaterialPerTileType().keySet()){
            TileWorkMaterials data = summary.getMaterialPerTileType().get(key);

            System.out.println("---------------------------------------------------------");

            System.out.println("Tile Type: " + data.tileType.getName());
            System.out.println("Area: " + data.area);
            System.out.println("Tiles: " + data.tiles);
            System.out.println("Cement: " + data.cement);
            System.out.println("Adhesive: " + data.adhesive);
            System.out.println("Sand: " + data.sand);
        }

        System.out.println("---------------------------------------------------------");

        TileWorkMaterials data = summary.getTotal();
        System.out.println("Total Cement: " + data.cement);
        System.out.println("Total Adhesive: " + data.adhesive);
        System.out.println("Total Sand: " + data.sand);
    }
}

