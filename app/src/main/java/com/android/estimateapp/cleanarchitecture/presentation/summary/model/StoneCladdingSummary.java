package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import java.util.HashMap;
import java.util.Map;

public class StoneCladdingSummary {

    Map<String, StoneCladdingMaterial> materialPerStoneType = new HashMap<>();

    StoneCladdingMaterial total;

    public StoneCladdingSummary(Map<String, StoneCladdingMaterial> materialPerStoneType,StoneCladdingMaterial total){
        this.materialPerStoneType = materialPerStoneType;
        this.total = total;
    }

    public Map<String, StoneCladdingMaterial> getMaterialPerStoneType() {
        return materialPerStoneType;
    }

    public StoneCladdingMaterial getTotal() {
        return total;
    }
}
