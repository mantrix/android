package com.android.estimateapp.cleanarchitecture.presentation.launch;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

public interface LaunchScreenContract {

    interface View {
        void navigateToWelcomeScreen();
    }

    interface UserActionListener extends Presenter<View> {

    }
}
