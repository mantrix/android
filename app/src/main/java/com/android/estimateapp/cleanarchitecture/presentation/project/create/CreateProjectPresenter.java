package com.android.estimateapp.cleanarchitecture.presentation.project.create;

import android.util.Log;

import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.CreateProjectUseCase;
import com.android.estimateapp.utils.DateUtils;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class CreateProjectPresenter implements CreateProjectContract.UserActionListener {

    private static final String TAG = CreateProjectPresenter.class.getSimpleName();

    private CreateProjectContract.View view;
    private CreateProjectUseCase createProjectUseCase;
    private ThreadExecutorProvider threadExecutorProvider;
    private PostExecutionThread postExecutionThread;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public CreateProjectPresenter(CreateProjectUseCase createProjectUseCase,
                                  ThreadExecutorProvider threadExecutorProvider,
                                  PostExecutionThread postExecutionThread){
        this.createProjectUseCase = createProjectUseCase;
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }


    @Override
    public void setView(CreateProjectContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        compositeDisposable.clear();
    }


    @Override
    public void createProject(String name, String owner, String location, String description) {
        view.displayCreateProjectLoader();
        compositeDisposable.add(createProjectUseCase.createProject(populateProject(name,owner,location,description))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .doOnTerminate(() ->  view.dismissCreateProjectLoader())
                .subscribe(projectId -> {
                    view.launchProjectDetail(projectId);
                },throwable -> {
                    view.projectCreationError();
                    throwable.printStackTrace();
                }));
    }

    private Project populateProject(String name, String owner, String location, String description){
        Project project = new Project(name,owner,location,description);
        long now = DateUtils.nowInUtc();
        project.setCreatedAt(now);
        project.setUpdatedAt(now);
        project.setPrivacy("private");
        return project;
    }

}
