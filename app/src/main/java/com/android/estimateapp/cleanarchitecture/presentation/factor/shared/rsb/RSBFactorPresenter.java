package com.android.estimateapp.cleanarchitecture.presentation.factor.shared.rsb;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class RSBFactorPresenter extends BasePresenter implements RSBFactorContract.UserActionListener {

    private RSBFactorContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;

    @Inject
    public RSBFactorPresenter(ProjectRepository projectRepository,
                              ThreadExecutorProvider threadExecutorProvider,
                              PostExecutionThread postExecutionThread){
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }

    @Override
    public void setView(RSBFactorContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public void start() {
        projectRepository.getProjectRSBFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(rsbFactors -> {
                    view.displayList(rsbFactors);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void updateFactors(List<RSBFactor> rsbFactors) {
        projectRepository.updateProjectRSBFactors(projectId,rsbFactors)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(updatedRsbFactors -> {
                    view.factorSaved();
                }, throwable -> {
                    view.factorSaved();
                    throwable.printStackTrace();
                });
    }
}
