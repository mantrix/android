package com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class WallFootingRebarResult extends Model {

    int traverseBarCount;

    double longitudinalBarsTotalLength;

    double tenRSB;

    double twelveRSB;

    double giTieWire;

    public WallFootingRebarResult(){
    }

    public int getTraverseBarCount() {
        return traverseBarCount;
    }

    public void setTraverseBarCount(int traverseBarCount) {
        this.traverseBarCount = traverseBarCount;
    }

    public double getLongitudinalBarsTotalLength() {
        return longitudinalBarsTotalLength;
    }

    public void setLongitudinalBarsTotalLength(double longitudinalBarsTotalLength) {
        this.longitudinalBarsTotalLength = longitudinalBarsTotalLength;
    }

    public double getTenRSB() {
        return tenRSB;
    }

    public void setTenRSB(double tenRSB) {
        this.tenRSB = tenRSB;
    }

    public double getTwelveRSB() {
        return twelveRSB;
    }

    public void setTwelveRSB(double twelveRSB) {
        this.twelveRSB = twelveRSB;
    }

    public double getGiTieWire() {
        return giTieWire;
    }

    public void setGiTieWire(double giTieWire) {
        this.giTieWire = giTieWire;
    }
}
