package com.android.estimateapp.cleanarchitecture.presentation.factor.shared.rsb;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.EdittextBaseOutputListener;
import com.android.estimateapp.utils.DisplayUtil;
import com.android.estimateapp.utils.NumberUtils;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RSBFactorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<RSBFactor>> {


    private List<RSBFactor> list;

    @Inject
    public RSBFactorAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.rsb_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<RSBFactor> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<RSBFactor> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    public List<RSBFactor> getData() {
        return list;
    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_size)
        TextView tvSize;

        @BindView(R.id.et_weight)
        EditText etWeight;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(RSBFactor rsbFactor, int position) {
            tvSize.setText(Integer.toString(rsbFactor.getSize()));
            etWeight.setText(DisplayUtil.toDisplayFormat(rsbFactor.getWeight()));

            etWeight.addTextChangedListener(new EdittextBaseOutputListener() {

                @Override
                public void afterTextChanged(Editable s) {

                    String input = etWeight.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setWeight(Double.parseDouble(etWeight.getText().toString()));
                    }
                }
            });
        }
    }


}