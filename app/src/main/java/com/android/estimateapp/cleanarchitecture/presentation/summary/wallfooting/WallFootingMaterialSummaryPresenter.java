package com.android.estimateapp.cleanarchitecture.presentation.summary.wallfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting.WallFootingWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class WallFootingMaterialSummaryPresenter extends BasePresenter implements WallFootingSummaryContract.UserActionListener {

    private WallFootingSummaryContract.View view;
    private ProjectRepository projectRepository;
    private WallFootingWorkCalculator wallFootingWorkCalculator;
    private String projectId;
    private String scopeOfWorkId;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<RSBFactor> rsbFactors;

    @Inject
    public WallFootingMaterialSummaryPresenter(ProjectRepository projectRepository,
                                               WallFootingWorkCalculator wallFootingWorkCalculator,
                                               ThreadExecutorProvider threadExecutorProvider,
                                               PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.wallFootingWorkCalculator = wallFootingWorkCalculator;
    }

    @Override
    public void setData(String projectId, String scopeOfWorkId) {
        this.projectId = projectId;
        this.scopeOfWorkId = scopeOfWorkId;
    }

    @Override
    public void setView(WallFootingSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadFactors();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void calculateSummary() {
        compositeDisposable.add(projectRepository.getWallFootingInputs(projectId, scopeOfWorkId)
                .map(inputs -> {
                    return wallFootingWorkCalculator.generateSummary(generateResults(inputs));
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    view.displaySummary(summary);
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    private List<WallFootingRebarResult> generateResults(List<WallFootingRebarInput> inputs) {
        List<WallFootingRebarResult> results = new ArrayList<>();

        for (WallFootingRebarInput input : inputs) {
            results.add(wallFootingWorkCalculator.calculate(53, input, rsbFactors));
        }

        return results;
    }


    public void loadFactors() {
        projectRepository.getProjectRSBFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(plasteringMortarFactors -> {
                    this.rsbFactors = plasteringMortarFactors;

                    calculateSummary();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}