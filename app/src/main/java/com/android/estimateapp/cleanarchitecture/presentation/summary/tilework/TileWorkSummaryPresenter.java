package com.android.estimateapp.cleanarchitecture.presentation.summary.tilework;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles.TileWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkSummary;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class TileWorkSummaryPresenter extends BasePresenter implements TileWorkSummaryContract.UserActionListener {

    private TileWorkSummaryContract.View view;
    private ProjectRepository projectRepository;
    private TileWorkCalculator tileWorkCalculator;
    private String projectId;
    private String scopeOfWorkId;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<TileWorkFactor> tileWorkFactors;
    private List<TileType> tileTypes;

    @Inject
    public TileWorkSummaryPresenter(ProjectRepository projectRepository,
                                    TileWorkCalculator tileWorkCalculator,
                                    ThreadExecutorProvider threadExecutorProvider,
                                    PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.tileWorkCalculator = tileWorkCalculator;
    }

    @Override
    public void setData(String projectId, String scopeOfWorkId) {
        this.projectId = projectId;
        this.scopeOfWorkId = scopeOfWorkId;
    }

    @Override
    public void setView(TileWorkSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadFactors();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void calculateSummary() {
        compositeDisposable.add(projectRepository.getTileWorkInputs(projectId, scopeOfWorkId)
                .map(inputs -> {
                    if (!inputs.isEmpty()) {
                        for (int i = 0 ; i < inputs.size() ; i++){
                            inputs.get(i).setTileType(getTileType(inputs.get(i).getTileTypeId()));
                        }
                    }
                    return tileWorkCalculator.generateSummary(generateResults(inputs));
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    view.displaySummary(summary);
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    private List<TileWorkResult> generateResults(List<TileWorkInput> inputs){
        List<TileWorkResult> results = new ArrayList<>();

        for (TileWorkInput input : inputs){
            TileWorkFactor factor = getTileWorkFactor(input.getMortarClassEnum());
            results.add(tileWorkCalculator.calculate(factor,input));
        }

        return results;
    }


    public void loadFactors(){
        Observable.zip(projectRepository.getTileWorkFactors(projectId), projectRepository.getTileTypeFactors(projectId),
                ((tileWorkFactors, tileTypes) -> new Pair(tileWorkFactors,tileTypes)))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {
                    this.tileWorkFactors = (List<TileWorkFactor>) pair.first;
                    this.tileTypes = (List<TileType>) pair.second;

                    calculateSummary();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private TileWorkFactor getTileWorkFactor(Grade grade){

        if (grade != null){
            for (TileWorkFactor tileWorkFactor : tileWorkFactors){
                if (tileWorkFactor.getMortarClass() == grade.getId()){
                    return tileWorkFactor;
                }
            }
        }
        return null;
    }

    private TileType getTileType(String tileKey){

        if (tileKey != null && tileTypes != null){
            for (TileType tileType : tileTypes){
                if (tileKey.equalsIgnoreCase(tileType.getKey())){
                    return tileType;
                }
            }
        }
        return null;
    }
}
