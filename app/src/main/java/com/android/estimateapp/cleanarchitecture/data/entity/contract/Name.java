package com.android.estimateapp.cleanarchitecture.data.entity.contract;

public interface Name extends NullChecker{

    String getFirstName();

    String getLastName();

    String getMiddleName();
}
