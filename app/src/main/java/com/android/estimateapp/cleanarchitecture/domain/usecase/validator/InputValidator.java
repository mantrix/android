package com.android.estimateapp.cleanarchitecture.domain.usecase.validator;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.BaseUseCase;

import java.util.regex.Pattern;

import javax.inject.Inject;

public class InputValidator extends BaseUseCase implements InputValidatorUseCase {

    private static final Pattern EMAIL_ADDRESS_PATTERN = Pattern
            .compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

    @Inject
    public InputValidator(ThreadExecutorProvider threadExecutorProvider, PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
    }

    @Override
    public boolean isValidEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    @Override
    public boolean isValidPassword(String password) {
        if (password == null || password.isEmpty()) {
            return false;
        } else {
            return password.length() >= 5;
        }
    }


}
