package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.presentation.summary.beamrebar.BeamRebarSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.CHBSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.columnfooting.ColumnFootingMaterialSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.columnrebar.ColumnRebarSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.generalconcrete.ConcreteWorkSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.plastering.PlasteringSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.stonecladding.StoneCladdingSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.tilework.TileWorkSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.wallfooting.WallFootingMaterialSummaryFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SummaryActivityBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {TileWorkSummaryModule.class})
    abstract TileWorkSummaryFragment bindTileWorkSummaryFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = PlasteringSummaryModule.class)
    abstract PlasteringSummaryFragment bindPlasteringSummaryFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = ConcreteWorkSummaryModule.class)
    abstract ConcreteWorkSummaryFragment bindConcreteWorkSummaryFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = StoneCladdingSummaryModule.class)
    abstract StoneCladdingSummaryFragment bindStoneCladdingSummaryFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = {CHBSummaryModule.class, CHBSummaryBuilderModule.class})
    abstract CHBSummaryFragment bindChbSummaryFragment();


    @PerFragment
    @ContributesAndroidInjector(modules = WallFootingMaterialSummaryModule.class)
    abstract WallFootingMaterialSummaryFragment bindWallFootingMaterialSummaryFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = ColumnFootingMaterialSummaryModule.class)
    abstract ColumnFootingMaterialSummaryFragment bindColumnFootingMaterialSummaryFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = BeamRebarSummaryModule.class)
    abstract BeamRebarSummaryFragment bindBeamRebarSummaryFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = ColumnRebarSummaryModule.class)
    abstract ColumnRebarSummaryFragment bindColumnRebarSummaryFragment();
}
