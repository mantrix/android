package com.android.estimateapp.configuration.injection.module.home;

import com.android.estimateapp.cleanarchitecture.presentation.project.listing.ProjectListFragment;
import com.android.estimateapp.configuration.injection.module.project.ProjectListModule;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HomeBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {ProjectListModule.class})
    abstract ProjectListFragment bindProjectListFragment();
}
