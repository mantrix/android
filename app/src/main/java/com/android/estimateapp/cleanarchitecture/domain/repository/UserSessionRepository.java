package com.android.estimateapp.cleanarchitecture.domain.repository;

import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;

public interface UserSessionRepository {

    void setCurrentActiveUser(Account activeUser);

    Account getCurrentActiveUser();

    boolean hasActiveUser();

    void setHasActiveUser(boolean hasActiveUser);
}
