package com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingSummary;

import java.util.List;

public interface StoneCladdingCalculator {

    StoneCladdingResult calculate(StoneCladdingInput input, StoneCladdingFactor factor);

    StoneCladdingSummary generateSummary(List<StoneCladdingResult> results);

}
