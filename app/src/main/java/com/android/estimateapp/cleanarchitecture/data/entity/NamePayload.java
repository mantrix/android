package com.android.estimateapp.cleanarchitecture.data.entity;

public class NamePayload {


    String firstName;

    String lastName;

    String middleName;


    public NamePayload() {

    }

    public NamePayload(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }


    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

}
