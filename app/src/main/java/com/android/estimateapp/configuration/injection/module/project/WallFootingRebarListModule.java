package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.list.WallFootingRebarListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.list.WallFootingRebarListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class WallFootingRebarListModule {

    @Provides
    @PerActivity
    WallFootingRebarListContract.UserActionListener provideWallFootingRebarListPresenter(WallFootingRebarListPresenter presenter) {
        return presenter;
    }
}
