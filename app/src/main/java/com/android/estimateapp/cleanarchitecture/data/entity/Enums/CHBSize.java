package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum CHBSize {


    FOUR(4),
    FIVE(5),
    SIX(6);

    int size;

    CHBSize(int size) {
        this.size = size;
    }

    public int getValue() {
        return size;
    }

    public static CHBSize parseInt(int value){
        for (CHBSize size : getCHBSizes()){
            if (value == size.getValue()){
                return size;
            }
        }
        throw new Error(value + " not supported as RSBFactor Size!");
    }

    public static List<CHBSize> getCHBSizes(){
        List<CHBSize> chbSizes = new ArrayList<>();
        chbSizes.add(FOUR);
        chbSizes.add(FIVE);
        chbSizes.add(SIX);
        return chbSizes;
    }
}
