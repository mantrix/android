package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import android.graphics.Bitmap;
import android.net.Uri;

import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.storage.UploadFileStore;
import com.android.estimateapp.configuration.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;

public class FirebaseUploadStore implements UploadFileStore {

    private StorageReference photosRef;

    @Inject
    public FirebaseUploadStore(){

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReference();
        photosRef = storageReference.child("photos");
    }

    @Override
    public Flowable<Double> uploadProfilePhoto(Bitmap bitmap) {
       return Flowable.fromCallable(() -> {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            String uid = firebaseUser.getUid();
            String fileName = Constants.PROFILE_PIC_PREFIX + uid;
            StorageReference profilePicRef = photosRef.child(uid).child(fileName);

            return profilePicRef;
        }).flatMap(profileRef -> uploadByteFile(profileRef, bitmap));
    }

    private Flowable<Double> uploadByteFile(StorageReference ref, Bitmap bitmap){
        return Flowable.create(emitter -> {

            // convert bitmap to byte[]
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            UploadTask uploadTask = ref.putBytes(data);
            uploadTask.addOnFailureListener(exception -> {
                emitter.onError(exception);
            }).addOnSuccessListener(taskSnapshot -> {
                emitter.onComplete();
            }).addOnProgressListener(taskSnapshot -> {
                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                emitter.onNext(progress);
            });
        }, BackpressureStrategy.LATEST);
    }
}
