package com.android.estimateapp.cleanarchitecture.presentation.summary.plastering;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.PlasteringSummary;

public interface PlasteringSummaryContract {

    interface View extends SummaryView<PlasteringSummary> {

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
