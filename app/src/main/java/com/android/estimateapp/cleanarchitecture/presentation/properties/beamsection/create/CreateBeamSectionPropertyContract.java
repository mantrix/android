package com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface CreateBeamSectionPropertyContract {

    interface View {

        void setInput(BeamSectionProperties input);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(int splicingTotalQty, double area);

        void inputSaved();

        void savingError();

        void requestForReCalculation();

        void setRSBPickerOptions(List<RsbSize> sizes);

        void showSavingDialog();

        void showSavingNotAllowedError();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String itemKey);

        void calculate(BeamSectionProperties input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
