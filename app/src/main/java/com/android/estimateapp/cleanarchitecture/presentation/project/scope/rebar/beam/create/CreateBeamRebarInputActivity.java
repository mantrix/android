package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.create;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.StirrupsOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.ComputationInclusions;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.RSBValuePair;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.list.BeamSectionPropertyListActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateBeamRebarInputActivity extends CreateScopeInputBaseActivity implements CreateBeamRebarInputContract.View {


    @Inject
    CreateBeamRebarInputContract.UserActionListener presenter;


    AlertDialog windowPickerDialog;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_storey)
    TextInputEditText etStorey;
    @BindView(R.id.til_storey)
    TextInputLayout tilStorey;
    @BindView(R.id.et_grid)
    TextInputEditText etGrid;
    @BindView(R.id.til_grid)
    TextInputLayout tilGrid;
    @BindView(R.id.et_span)
    TextInputEditText etSpan;
    @BindView(R.id.til_span)
    TextInputLayout tilSpan;
    @BindView(R.id.et_length_cc)
    TextInputEditText etLengthCc;
    @BindView(R.id.til_length_cc)
    TextInputLayout tilLengthCc;
    @BindView(R.id.et_main_rebar)
    TextInputEditText etMainRebar;
    @BindView(R.id.til_main_rebar)
    TextInputLayout tilMainRebar;
    @BindView(R.id.et_offset_1)
    TextInputEditText etOffset1;
    @BindView(R.id.til_offset_1)
    TextInputLayout tilOffset1;
    @BindView(R.id.et_offset_2)
    TextInputEditText etOffset2;
    @BindView(R.id.til_offset_2)
    TextInputLayout tilOffset2;
    @BindView(R.id.et_wastage)
    TextInputEditText etWastage;
    @BindView(R.id.til_wastage)
    TextInputLayout tilWastage;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.ll_header)
    LinearLayout llHeader;
    @BindView(R.id.tv_rsb_top_1)
    TextView tvRsbTop1;
    @BindView(R.id.tv_qty_top_1)
    TextView tvQtyTop1;
    @BindView(R.id.tv_rsb_top_2)
    TextView tvRsbTop2;
    @BindView(R.id.tv_qty_top_2)
    TextView tvQtyTop2;
    @BindView(R.id.tv_rsb_bottom_1)
    TextView tvRsbBottom1;
    @BindView(R.id.tv_qty_bottom_1)
    TextView tvQtyBottom1;
    @BindView(R.id.tv_rsb_bottom_2)
    TextView tvRsbBottom2;
    @BindView(R.id.tv_qty_bottom_2)
    TextView tvQtyBottom2;
    @BindView(R.id.tv_rsb_left_1)
    TextView tvRsbLeft1;
    @BindView(R.id.tv_qty_left_1)
    TextView tvQtyLeft1;
    @BindView(R.id.tv_rsb_left_2)
    TextView tvRsbLeft2;
    @BindView(R.id.tv_qty_left_2)
    TextView tvQtyLeft2;
    @BindView(R.id.tv_rsb_right_1)
    TextView tvRsbRight1;
    @BindView(R.id.tv_qty_right_1)
    TextView tvQtyRight1;
    @BindView(R.id.tv_rsb_right_2)
    TextView tvRsbRight2;
    @BindView(R.id.tv_qty_right_2)
    TextView tvQtyRight2;
    @BindView(R.id.tv_rsb_mid_1)
    TextView tvRsbMid1;
    @BindView(R.id.tv_qty_mid_1)
    TextView tvQtyMid1;
    @BindView(R.id.tv_rsb_mid_2)
    TextView tvRsbMid2;
    @BindView(R.id.tv_qty_mid_2)
    TextView tvQtyMid2;
    @BindView(R.id.tv_rsb_splicing_1)
    TextView tvRsbSplicing1;
    @BindView(R.id.tv_qty_splicing_1)
    TextView tvQtySplicing1;
    @BindView(R.id.tv_rsb_splicing_2)
    TextView tvRsbSplicing2;
    @BindView(R.id.tv_qty_splicing_2)
    TextView tvQtySplicing2;
    @BindView(R.id.tv_rsb_hookes_1)
    TextView tvRsbHookes1;
    @BindView(R.id.tv_qty_hookes_1)
    TextView tvQtyHookes1;
    @BindView(R.id.tv_rsb_hookes_2)
    TextView tvRsbHookes2;
    @BindView(R.id.tv_qty_hookes_2)
    TextView tvQtyHookes2;
    @BindView(R.id.tv_stirrups_qty_pc)
    TextView tvStirrupsQtyPc;
    @BindView(R.id.tv_stirrups_length)
    TextView tvStirrupsLength;
    @BindView(R.id.tv_stirrups_rsb_size)
    TextView tvStirrupsRsbSize;
    @BindView(R.id.tv_stirrups_qty_kg)
    TextView tvStirrupsQtyKg;
    @BindView(R.id.cv_results)
    CardView cvResults;

    @BindView(R.id.tv_lclear)
    TextView tvLClear;
    @BindView(R.id.et_compute_left_bar1)
    TextInputEditText etComputeLeftBar1;
    @BindView(R.id.til_compute_left_bar1)
    TextInputLayout tilComputeLeftBar1;
    @BindView(R.id.et_compute_right_bar1)
    TextInputEditText etComputeRightBar1;
    @BindView(R.id.til_compute_right_bar1)
    TextInputLayout tilComputeRightBar1;
    @BindView(R.id.et_compute_left_bar2)
    TextInputEditText etComputeLeftBar2;
    @BindView(R.id.til_compute_left_bar2)
    TextInputLayout tilComputeLeftBar2;
    @BindView(R.id.et_compute_right_bar2)
    TextInputEditText etComputeRightBar2;
    @BindView(R.id.til_compute_right_bar2)
    TextInputLayout tilComputeRightBar2;
    @BindView(R.id.et_compute_splicing)
    TextInputEditText etComputeSplicing;
    @BindView(R.id.til_compute_splicing)
    TextInputLayout tilComputeSplicing;
    @BindView(R.id.et_compute_extra_left_bar1)
    TextInputEditText etComputeExtraLeftBar1;
    @BindView(R.id.til_compute_extra_left_bar1)
    TextInputLayout tilComputeExtraLeftBar1;
    @BindView(R.id.et_compute_extra_right_bar1)
    TextInputEditText etComputeExtraRightBar1;
    @BindView(R.id.til_compute_extra_right_bar1)
    TextInputLayout tilComputeExtraRightBar1;
    @BindView(R.id.et_compute_extra_left_bar2)
    TextInputEditText etComputeExtraLeftBar2;
    @BindView(R.id.til_compute_extra_left_bar2)
    TextInputLayout tilComputeExtraLeftBar2;
    @BindView(R.id.et_compute_extra_right_bar2)
    TextInputEditText etComputeExtraRightBar2;
    @BindView(R.id.til_compute_extra_right_bar2)
    TextInputLayout tilComputeExtraRightBar2;
    private List<BeamSectionProperties> beamSectionProperties;

    private String projectId;
    private String scopeKey;
    private String itemKey;

    private BeamRebarInput input;

    private ProgressDialog saveProgressDialog;

    private AlertDialog sectionPropertyDialog;

    private boolean updateContent;


    ComputeFieldLoc selectedComputeFieldLoc;
    AlertDialog computePickerDialog;

    ComputationInclusions computationInclusions;
    enum ComputeFieldLoc {
        LEFT_BAR_HOOK_1,
        LEFT_BAR_HOOK_2,
        RIGHT_BAR_HOOK_1,
        RIGHT_BAR_HOOK_2,
        SPLICING,
        EXTRA_BAR_LEFT_BAR_HOOK_1,
        EXTRA_BAR_LEFT_BAR_HOOK_2,
        EXTRA_BAR_RIGHT_BAR_HOOK_1,
        EXTRA_BAR_RIGHT_BAR_HOOK_2
    }

    public static Intent createIntent(Context context, String projectId, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateBeamRebarInputActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.create_beam_rebar_input_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.BEAM_REBAR.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        input = new BeamRebarInput();
        computationInclusions = new ComputationInclusions();
        beamSectionProperties = new ArrayList<>();

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();


        String[] options = new String[2];
        options[0] = getString(R.string.label_yes);
        options[1] = getString(R.string.label_no);
        computePickerDialog = createChooser(getString(R.string.label_compute), options, (dialog, which) -> {

            setIncludeValue(options[which].equals(getString(R.string.label_yes)));

            dialog.dismiss();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_w_property_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.BEAM_REBAR.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            case R.id.menu_property:
                startActivityForResult(BeamSectionPropertyListActivity.createIntent(this, projectId), Constants.REQUEST_CODE);
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }


    private boolean validateInputs() {
        boolean valid = true;
        if (etGrid.getText().toString().isEmpty()) {
            tilGrid.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilGrid.setError(null);
        }

        if (etStorey.getText().toString().isEmpty()) {
            tilStorey.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilStorey.setError(null);
        }
        return valid;
    }

    @Override
    public void setPropertiesOption(List<BeamSectionProperties> propertiesOption) {
        beamSectionProperties = propertiesOption;
        String[] options = new String[propertiesOption.size()];
        for (int i = 0; i < propertiesOption.size(); i++) {
            options[i] = propertiesOption.get(i).getDescription();
        }

        sectionPropertyDialog = createChooser(getString(R.string.label_beam_section_properties), options, (dialog, which) -> {
            etMainRebar.setText(propertiesOption.get(which).getDescription());
            input.setMainRebarPropertyKey(propertiesOption.get(which).getKey());
            input.setPropertyReferenceName(String.format("%1$s::%2$s",propertiesOption.get(which).getKey(),propertiesOption.get(which).getDescription()));
            dialog.dismiss();
        });
    }

    @OnClick({R.id.et_compute_left_bar1,
            R.id.et_compute_left_bar2,
            R.id.et_compute_right_bar1,
            R.id.et_compute_right_bar2,
            R.id.et_compute_splicing,
            R.id.et_compute_extra_left_bar1,
            R.id.et_compute_extra_left_bar2,
            R.id.et_compute_extra_right_bar1,
            R.id.et_compute_extra_right_bar2})
    protected void computeFieldClick(View view) {

        switch (view.getId()) {
            case R.id.et_compute_left_bar1:
                selectedComputeFieldLoc = ComputeFieldLoc.LEFT_BAR_HOOK_1;
                break;
            case R.id.et_compute_left_bar2:
                selectedComputeFieldLoc = ComputeFieldLoc.LEFT_BAR_HOOK_2;
                break;
            case R.id.et_compute_right_bar1:
                selectedComputeFieldLoc =  ComputeFieldLoc.RIGHT_BAR_HOOK_1;
                break;
            case R.id.et_compute_right_bar2:
                selectedComputeFieldLoc =  ComputeFieldLoc.RIGHT_BAR_HOOK_2;
                break;
            case R.id.et_compute_splicing:
                selectedComputeFieldLoc =  ComputeFieldLoc.SPLICING;
                break;
            case R.id.et_compute_extra_left_bar1:
                selectedComputeFieldLoc =  ComputeFieldLoc.EXTRA_BAR_LEFT_BAR_HOOK_1;
                break;
            case R.id.et_compute_extra_left_bar2:
                selectedComputeFieldLoc =  ComputeFieldLoc.EXTRA_BAR_LEFT_BAR_HOOK_2;
                break;
            case R.id.et_compute_extra_right_bar1:
                selectedComputeFieldLoc =  ComputeFieldLoc.EXTRA_BAR_RIGHT_BAR_HOOK_1;
                break;
            case R.id.et_compute_extra_right_bar2:
                selectedComputeFieldLoc =  ComputeFieldLoc.EXTRA_BAR_RIGHT_BAR_HOOK_2;
                break;
        }

        computePickerDialog.show();

    }

    private void setIncludeValue(boolean include) {
        TextView textView = null;
        switch (selectedComputeFieldLoc) {
            case LEFT_BAR_HOOK_1:
                computationInclusions.setComputeTopBar1(include);
                textView = etComputeLeftBar1;
                break;
            case LEFT_BAR_HOOK_2:
                computationInclusions.setComputeTopBar2(include);
                textView = etComputeLeftBar2;
                break;
            case RIGHT_BAR_HOOK_1:
                computationInclusions.setComputeBottomBar1(include);
                textView = etComputeRightBar1;
                break;
            case RIGHT_BAR_HOOK_2:
                computationInclusions.setComputeBottomBar2(include);
                textView = etComputeRightBar2;
                break;
            case SPLICING:
                computationInclusions.setComputeSplicing(include);
                textView = etComputeSplicing;
                break;
            case EXTRA_BAR_LEFT_BAR_HOOK_1:
                computationInclusions.setComputeExtraBarLeft1(include);
                textView = etComputeExtraLeftBar1;
                break;
            case EXTRA_BAR_LEFT_BAR_HOOK_2:
                computationInclusions.setComputeExtraBarLeft2(include);
                textView = etComputeExtraLeftBar2;
                break;
            case EXTRA_BAR_RIGHT_BAR_HOOK_1:
                computationInclusions.setComputeExtraBarRight1(include);
                textView = etComputeExtraRightBar1;
                break;
            case EXTRA_BAR_RIGHT_BAR_HOOK_2:
                computationInclusions.setComputeExtraBarRight2(include);
                textView = etComputeExtraRightBar2;
                break;
        }

        if (textView != null){
            textView.setText(include ? getString(R.string.label_yes) : getString(R.string.label_no));
        }
    }

    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }

    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setInput(BeamRebarInput input) {
        this.input = input;

        if (input.getGrid() != null) {
            etGrid.setText(input.getGrid());
        }

        if (input.getStorey() != null) {
            etStorey.setText(input.getStorey());
        }


        if (input.getGroupSpanMember() != null) {
            etSpan.setText(input.getGroupSpanMember());
        }

        if (input.getLengthCC() != 0) {
            etLengthCc.setText(toDisplayFormat(input.getLengthCC()));
        }

        if (input.getOffset() != 0) {
            etOffset1.setText(toDisplayFormat(input.getOffset()));
        }

        if (input.getOffset2() != 0) {
            etOffset2.setText(toDisplayFormat(input.getOffset2()));
        }

        if (input.getWastage() != 0) {
            etWastage.setText(toDisplayFormat(input.getWastage()));
        }

        if (input.getMainRebarPropertyKey() != null && beamSectionProperties != null) {
            for (BeamSectionProperties properties : beamSectionProperties) {
                if (input.getMainRebarPropertyKey().equalsIgnoreCase(properties.getKey())) {
                    etMainRebar.setText(properties.getDescription());
                    break;
                }
            }
        }

        if (input.getInclusions() != null){

            computationInclusions = input.getInclusions();

            etComputeLeftBar1.setText(computationInclusions.isComputeTopBar1() ? getString(R.string.label_yes) : getString(R.string.label_no));
            etComputeLeftBar2.setText(computationInclusions.isComputeTopBar2() ? getString(R.string.label_yes) : getString(R.string.label_no));
            etComputeRightBar1.setText(computationInclusions.isComputeBottomBar1() ? getString(R.string.label_yes) : getString(R.string.label_no));
            etComputeRightBar2.setText(computationInclusions.isComputeBottomBar2() ? getString(R.string.label_yes) : getString(R.string.label_no));
            etComputeSplicing.setText(computationInclusions.isComputeSplicing() ? getString(R.string.label_yes) : getString(R.string.label_no));
            etComputeExtraLeftBar1.setText(computationInclusions.isComputeExtraBarLeft1() ? getString(R.string.label_yes) : getString(R.string.label_no));
            etComputeExtraLeftBar2.setText(computationInclusions.isComputeExtraBarLeft2() ? getString(R.string.label_yes) : getString(R.string.label_no));
            etComputeExtraRightBar1.setText(computationInclusions.isComputeExtraBarRight1() ? getString(R.string.label_yes) : getString(R.string.label_no));
            etComputeExtraRightBar2.setText(computationInclusions.isComputeExtraBarRight2() ? getString(R.string.label_yes) : getString(R.string.label_no));
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void displayResult(BeamRebarOutput result) {

        tvLClear.setText("L (clear span): " + result.getlClear());

        // TOP
        if (result.getTopContinuousBars() != null) {
            RSBValuePair top1 = result.getTopContinuousBars();
            if (top1.getRsbFactor() != null) {
                tvRsbTop1.setText(Double.toString(top1.getRsbFactor().getSize()));
                tvQtyTop1.setText(toDisplayFormat(top1.getValue()));
            }
        }  else {
            tvRsbTop1.setText("");
            tvQtyTop1.setText("");
        }

        if (result.getTopContinuousBars2() != null) {
            RSBValuePair top2 = result.getTopContinuousBars2();
            if (top2.getRsbFactor() != null) {
                tvRsbTop2.setText(Double.toString(top2.getRsbFactor().getSize()));
                tvQtyTop2.setText(toDisplayFormat(top2.getValue()));
            }
        } else {
            tvRsbTop2.setText("");
            tvQtyTop2.setText("");
        }

        // BOTTOM
        if (result.getBottomContinuousBars() != null) {
            RSBValuePair bot1 = result.getBottomContinuousBars();
            if (bot1.getRsbFactor() != null) {
                tvRsbBottom1.setText(Double.toString(bot1.getRsbFactor().getSize()));
                tvQtyBottom1.setText(toDisplayFormat(bot1.getValue()));
            }
        } else {
            tvRsbBottom1.setText("");
            tvQtyBottom1.setText("");
        }

        if (result.getBottomContinuousBars2() != null) {
            RSBValuePair bot2 = result.getBottomContinuousBars2();
            if (bot2.getRsbFactor() != null) {
                tvRsbBottom2.setText(Double.toString(bot2.getRsbFactor().getSize()));
                tvQtyBottom2.setText(toDisplayFormat(bot2.getValue()));
            }
        } else {
            tvRsbBottom2.setText("");
            tvQtyBottom2.setText("");
        }

        // LEFT
        if (result.getLeftSupport() != null) {
            RSBValuePair left1 = result.getLeftSupport();
            if (left1.getRsbFactor() != null) {
                tvRsbLeft1.setText(Double.toString(left1.getRsbFactor().getSize()));
                tvQtyLeft1.setText(toDisplayFormat(left1.getValue()));
            }
        }  else {
            tvRsbLeft1.setText("");
            tvQtyLeft1.setText("");
        }

        if (result.getLeftSupport2() != null) {
            RSBValuePair left2 = result.getLeftSupport2();
            if (left2.getRsbFactor() != null) {
                tvRsbLeft2.setText(Double.toString(left2.getRsbFactor().getSize()));
                tvQtyLeft2.setText(toDisplayFormat(left2.getValue()));
            }
        }  else {
            tvRsbLeft2.setText("");
            tvQtyLeft2.setText("");
        }

        // RIGHT
        if (result.getRightSupport() != null) {
            RSBValuePair right1 = result.getRightSupport();
            if (right1.getRsbFactor() != null) {
                tvRsbRight1.setText(Double.toString(right1.getRsbFactor().getSize()));
                tvQtyRight1.setText(toDisplayFormat(right1.getValue()));
            }
        }  else {
            tvRsbRight1.setText("");
            tvQtyRight1.setText("");
        }

        if (result.getRightSupport2() != null) {
            RSBValuePair right2 = result.getRightSupport2();
            if (right2.getRsbFactor() != null) {
                tvRsbRight2.setText(Double.toString(right2.getRsbFactor().getSize()));
                tvQtyRight2.setText(toDisplayFormat(right2.getValue()));
            }
        }  else {
            tvRsbRight2.setText("");
            tvQtyRight2.setText("");
        }


        // MID
        if (result.getExtraBarsMid() != null) {
            RSBValuePair mid1 = result.getExtraBarsMid();
            if (mid1.getRsbFactor() != null) {
                tvRsbMid1.setText(Double.toString(mid1.getRsbFactor().getSize()));
                tvQtyMid1.setText(toDisplayFormat(mid1.getValue()));
            }
        }  else {
            tvRsbMid1.setText("");
            tvQtyMid1.setText("");
        }

        if (result.getExtraBarsMid2() != null) {
            RSBValuePair mid2 = result.getExtraBarsMid2();
            if (mid2.getRsbFactor() != null) {
                tvRsbMid2.setText(Double.toString(mid2.getRsbFactor().getSize()));
                tvQtyMid2.setText(toDisplayFormat(mid2.getValue()));
            }
        }  else {
            tvRsbMid2.setText("");
            tvQtyMid2.setText("");
        }

        // SPLICING
        if (result.getSplicing() != null) {
            RSBValuePair splicing1 = result.getSplicing();
            if (splicing1.getRsbFactor() != null) {
                tvRsbSplicing1.setText(Double.toString(splicing1.getRsbFactor().getSize()));
                tvQtySplicing1.setText(toDisplayFormat(splicing1.getValue()));
            }
        }  else {
            tvRsbSplicing1.setText("");
            tvQtySplicing1.setText("");
        }

        if (result.getSplicing2() != null) {
            RSBValuePair splicing2 = result.getSplicing2();
            if (splicing2.getRsbFactor() != null) {
                tvRsbSplicing2.setText(Double.toString(splicing2.getRsbFactor().getSize()));
                tvQtySplicing2.setText(toDisplayFormat(splicing2.getValue()));
            }
        }  else {
            tvRsbSplicing2.setText("");
            tvQtySplicing2.setText("");
        }

        // HOOKES
        if (result.getHookes() != null) {
            RSBValuePair hookes = result.getHookes();
            if (hookes.getRsbFactor() != null) {
                tvRsbHookes1.setText(Double.toString(hookes.getRsbFactor().getSize()));
                tvQtyHookes1.setText(toDisplayFormat(hookes.getValue()));
            }
        } else {
            tvRsbHookes1.setText("");
            tvQtyHookes1.setText("");
        }

        if (result.getHookes2() != null) {
            RSBValuePair hookes2 = result.getHookes2();
            if (hookes2.getRsbFactor() != null) {
                tvRsbHookes2.setText(Double.toString(hookes2.getRsbFactor().getSize()));
                tvQtyHookes2.setText(toDisplayFormat(hookes2.getValue()));
            }
        }  else {
            tvRsbHookes2.setText("");
            tvQtyHookes2.setText("");
        }

        // STIRRUPS
        if (result.getStirrupsOutput() != null) {
            StirrupsOutput stirrupsOutput = result.getStirrupsOutput();

            if (stirrupsOutput.getQuantity() != 0) {
                tvStirrupsQtyPc.setText(Double.toString(stirrupsOutput.getQuantity()));
            }

            if (stirrupsOutput.getLength() != 0) {
                tvStirrupsLength.setText(Double.toString(stirrupsOutput.getLength()));
            }

            if (stirrupsOutput.getTotal() != 0) {
                tvStirrupsQtyKg.setText(toDisplayFormat(stirrupsOutput.getTotal()));
            }
            if (stirrupsOutput.getSelectedRsb() != 0) {
                tvStirrupsRsbSize.setText(Integer.toString(stirrupsOutput.getSelectedRsb()));
            }
        }

        cvResults.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.et_main_rebar)
    protected void mainRebarClick() {
        if (beamSectionProperties != null && !beamSectionProperties.isEmpty() && sectionPropertyDialog != null) {
            sectionPropertyDialog.show();
        } else {
            startActivityForResult(BeamSectionPropertyListActivity.createIntent(this, projectId), Constants.REQUEST_CODE);
        }
    }


    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setGrid(etGrid.getText().toString());
        input.setStorey(etStorey.getText().toString());
        input.setGroupSpanMember(etSpan.getText().toString());

        String length = etLengthCc.getText().toString();
        if (length.isEmpty()) {
            input.setLengthCC(0);
        } else {
            input.setLengthCC(Double.parseDouble(length));
        }

        String offset = etOffset1.getText().toString();
        if (offset.isEmpty()) {
            input.setOffset(0);
        } else {
            input.setOffset(Double.parseDouble(offset));
        }

        String offset2 = etOffset2.getText().toString();
        if (offset2.isEmpty()) {
            input.setOffset2(0);
        } else {
            input.setOffset2(Double.parseDouble(offset2));
        }

        String wastage = etWastage.getText().toString();
        if (wastage.isEmpty()) {
            input.setWastage(0);
        } else {
            input.setWastage(Integer.parseInt(wastage));
        }


        input.setInclusions(computationInclusions);
        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
