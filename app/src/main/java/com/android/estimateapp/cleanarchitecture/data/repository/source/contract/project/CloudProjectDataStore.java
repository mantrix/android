package com.android.estimateapp.cleanarchitecture.data.repository.source.contract.project;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface CloudProjectDataStore {

    Observable<String> createProject(Project project);

    Observable<List<String>> getUserProjectIds();

    Observable<Project> getProject(String id);

    Completable deleteProject(String id);

    Observable<List<Project>> getUserProjects();

    Observable<WorkerGroup> createLaborCost(String projectId, String scopeOfWorkId, WorkerGroup group);

    Observable<WorkerGroup> updateLaborCost(String projectId, String scopeOfWorkId, String itemId, WorkerGroup input);

    Observable<List<WorkerGroup>> getLaborCosts(String projectId, String scopeOfWorkId);

    Completable deleteLaborCost(String projectId, String scopeOfWorkId, String inputId);

    Observable<WorkerGroup> getLaborCost(String projectId, String scopeOfWorkId, String inputId);

    <T extends Model> Observable<T> createMaterial(String projectId, String scopeOfWorkId, T model);

    <T extends Model> Observable<T> getMaterial(String projectId, String scopeOfWorkId,Class<T> type);

    <T extends Model> Observable<T> updateMaterial(String projectId, String scopeOfWorkId, T input);
}
