package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.perchbsize.CHBSummaryPerSizeFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.perstorey.CHBSummaryPerStoreyFragment;
import com.android.estimateapp.configuration.injection.scope.PerChildFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CHBSummaryBuilderModule {

    @PerChildFragment
    @ContributesAndroidInjector
    abstract CHBSummaryPerStoreyFragment bindChbSummaryPerStoreyFragment();

    @PerChildFragment
    @ContributesAndroidInjector
    abstract CHBSummaryPerSizeFragment bindChbSummaryPerSizeFragment();
}
