package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.create.CreateConcreteWorkContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.create.CreateConcreteWorkPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateConcreteWorkModule {

    @Provides
    @PerActivity
    CreateConcreteWorkContract.UserActionListener provideCreateConcretePresenter(CreateConcreteWorkPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ConcreteWorkCalculator provideConcreteWorkCalculator(ConcreteWorkCalculatorImpl calculator) {
        return calculator;
    }
}
