package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.FormworkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RectangularConcreteSection;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnforms.ColumnFormInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnforms.ColumnFormOutput;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnforms.ColumnFormWorkCalculatorImpl;

import org.junit.Before;
import org.junit.Test;

public class ColumnFormWorkComputationTest extends BaseTest {

    ColumnFormWorkCalculatorImpl columnFormWorkCalculator;

    @Before
    public void setup() {
        columnFormWorkCalculator = new ColumnFormWorkCalculatorImpl();
        initRSBSizes();
    }

    @Test
    public void columnFormTest() {

        ColumnFormInput input = new ColumnFormInput();
        FormworkFactor factor = new FormworkFactor();
        factor.setEffectiveArea(1.6);
        factor.setNail(0.1);

        RectangularConcreteSection property = new RectangularConcreteSection();
        input.setRectangularConcreteSection(property);

        input.sethCC(20);
        input.setOffset1(1);
        input.setOffset2(1);

        ColumnFormOutput result = columnFormWorkCalculator.calculate(false,input,factor);

        System.out.println("Area: " + result.getArea());
        System.out.println("Contact Area: " + result.getContactArea());
        System.out.println("Plywood: " + result.getPlywood());
        System.out.println("Wood Frame: " + result.getWoodFrame());
        System.out.println("Nails: " + result.getNails());
    }

}
