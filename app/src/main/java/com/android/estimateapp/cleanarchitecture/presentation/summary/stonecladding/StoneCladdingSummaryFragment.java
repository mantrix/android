package com.android.estimateapp.cleanarchitecture.presentation.summary.stonecladding;

import android.os.Bundle;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingMaterial;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingSummary;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class StoneCladdingSummaryFragment extends BaseFragment implements StoneCladdingSummaryContract.View {


    @Inject
    StoneCladdingSummaryContract.UserActionListener presenter;

    @Inject
    StoneCladdingSummaryAdapter adapter;
    @BindView(R.id.tv_area)
    TextView tvArea;
    @BindView(R.id.tv_cement)
    TextView tvCement;
    @BindView(R.id.tv_adhesive)
    TextView tvAdhesive;
    @BindView(R.id.tv_sand)
    TextView tvSand;
    @BindView(R.id.tv_quantity_title)
    TextView tvQuantityTitle;
    @BindView(R.id.tile_adhesive_label)
    TextView tvTileAdhesiveLabel;
    @BindView(R.id.tv_summary_title)
    TextView tvSummaryTitle;

    public static StoneCladdingSummaryFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        StoneCladdingSummaryFragment fragment = new StoneCladdingSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rv_summary)
    RecyclerView recyclerView;

    private String projectId;
    private String scopeKey;
    NpaLinearLayoutManager npaLinearLayoutManager;

    @Override
    protected int getLayoutResource() {
        return R.layout.tile_work_summary_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        initRecyclerView();
        extractExtras();
        presenter.setData(projectId, scopeKey);
        presenter.setView(this);
        presenter.start();

        tvSummaryTitle.setText(getString(R.string.stone_cladding_summary));
        tvTileAdhesiveLabel.setText(getString(R.string.adhesive_label));
        tvQuantityTitle.setText(getString(R.string.quantity));
    }

    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
    }

    @Override
    public void displaySummary(StoneCladdingSummary summary) {
        List<StoneCladdingMaterial> materials = new ArrayList<>();
        for (String key : summary.getMaterialPerStoneType().keySet()) {
            StoneCladdingMaterial material = summary.getMaterialPerStoneType().get(key);
            materials.add(material);
        }
        adapter.setItems(materials);

        BaseActivity baseActivity = (BaseActivity) getActivity();

        StoneCladdingMaterial totals = summary.getTotal();

        tvArea.setText(String.format(getString(R.string.sqm_result), baseActivity.toDisplayFormat(totals.area)));
        tvCement.setText(String.format(getString(R.string.bag_result), baseActivity.toDisplayFormat(totals.cement)));
        tvSand.setText(String.format(getString(R.string.cum_result), baseActivity.toDisplayFormat(totals.sand)));
        tvAdhesive.setText(String.format(getString(R.string.bag_result), baseActivity.toDisplayFormat(totals.adhesive)));
    }
}
