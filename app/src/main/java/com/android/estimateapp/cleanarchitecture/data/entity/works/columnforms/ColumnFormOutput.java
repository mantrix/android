package com.android.estimateapp.cleanarchitecture.data.entity.works.columnforms;

public class ColumnFormOutput {

    double area;

    double contactArea;

    double plywood;

    double woodFrame;

    double nails;

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getContactArea() {
        return contactArea;
    }

    public void setContactArea(double contactArea) {
        this.contactArea = contactArea;
    }

    public double getPlywood() {
        return plywood;
    }

    public void setPlywood(double plywood) {
        this.plywood = plywood;
    }

    public double getWoodFrame() {
        return woodFrame;
    }

    public void setWoodFrame(double woodFrame) {
        this.woodFrame = woodFrame;
    }

    public double getNails() {
        return nails;
    }

    public void setNails(double nails) {
        this.nails = nails;
    }
}
