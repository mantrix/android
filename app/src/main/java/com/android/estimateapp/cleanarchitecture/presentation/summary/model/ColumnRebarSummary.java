package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import java.util.Map;

public class ColumnRebarSummary {

    Map<Integer, ColumnRebarMaterial> columnRebarMaterialMap;

    public ColumnRebarSummary(){

    }

    public ColumnRebarSummary(Map<Integer, ColumnRebarMaterial> columnRebarMaterialMap){
        this.columnRebarMaterialMap = columnRebarMaterialMap;
    }

    public Map<Integer, ColumnRebarMaterial> getColumnRebarMaterialMap() {
        return columnRebarMaterialMap;
    }
}
