package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface WallFootingRebarListContract {

    interface View {

        void displayList(List<WallFootingRebarInput> inputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
