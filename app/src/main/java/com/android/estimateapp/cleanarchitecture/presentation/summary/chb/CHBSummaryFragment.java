package com.android.estimateapp.cleanarchitecture.presentation.summary.chb;

import android.os.Bundle;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBLayingSummary;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.tabs.TabLayout;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;

public class CHBSummaryFragment extends BaseFragment implements CHBSummaryContract.View {


    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @Inject
    CHBSummaryContract.UserActionListener presenter;


    private String projectId;
    private String scopeKey;

    private CHBSummaryPagerAdapter chbSummaryPagerAdapter;

    public static CHBSummaryFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        CHBSummaryFragment fragment = new CHBSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.chb_summary_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {

        extractExtras();
        presenter.setData(projectId, scopeKey);
        presenter.setView(this);
        presenter.start();
    }

    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
    }

    private void setupViewPager(CHBLayingSummary summary) {
        chbSummaryPagerAdapter = new CHBSummaryPagerAdapter(getActivity(), getChildFragmentManager(), summary);

        viewPager.setAdapter(chbSummaryPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.requestFocus();

        viewPager.setOffscreenPageLimit(chbSummaryPagerAdapter.getCount());
    }

    @Override
    public void displaySummary(CHBLayingSummary chbLayingSummary) {
        setupViewPager(chbLayingSummary);
    }
}
