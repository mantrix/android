package com.android.estimateapp.cleanarchitecture.data.clients;

public interface DatabaseClient {
    void setup();
}
