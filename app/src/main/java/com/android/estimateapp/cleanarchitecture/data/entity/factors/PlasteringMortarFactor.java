package com.android.estimateapp.cleanarchitecture.data.entity.factors;


import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class PlasteringMortarFactor extends Model {

    int mortarClass;

    double cement;

    double sand;

    public PlasteringMortarFactor(){

    }

    public PlasteringMortarFactor(int mortarClass, double cement, double sand) {
        this.mortarClass = mortarClass;
        this.cement = cement;
        this.sand = sand;
    }

    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }
}
