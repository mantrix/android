package com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarSummary;

import java.util.List;

public interface BeamRebarCalculator {

    BeamRebarSummary generateSummary(List<BeamRebarOutput> results);

    BeamRebarOutput calculate(boolean autoCompute, BeamRebarInput input, BeamSectionProperties properties, List<RSBFactor> rsbFactors);


}
