package com.android.estimateapp.cleanarchitecture.presentation.summary.tilework;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkSummary;

public interface TileWorkSummaryContract {

    interface View extends SummaryView<TileWorkSummary> {

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
