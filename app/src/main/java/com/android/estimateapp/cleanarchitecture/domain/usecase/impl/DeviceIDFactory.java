package com.android.estimateapp.cleanarchitecture.domain.usecase.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

import javax.inject.Inject;

public class DeviceIDFactory {

    private Context context;

    @Inject
    public DeviceIDFactory(Context context) {
        this.context = context;
    }

    @SuppressLint("HardwareIds")
    public String getDeviceId() {

        String androidId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);

        if (androidId == null){
            androidId = android.os.Build.SERIAL;
        }

        return androidId;
    }


}
