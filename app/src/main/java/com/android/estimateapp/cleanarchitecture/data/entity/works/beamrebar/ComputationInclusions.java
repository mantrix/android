package com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class ComputationInclusions extends Model {

    private boolean computeTopBar1;
    private boolean computeTopBar2;

    private boolean computeBottomBar1;
    private boolean computeBottomBar2;

    private boolean computeSplicing;

    private boolean computeExtraBarLeft1;
    private boolean computeExtraBarLeft2;

    private boolean computeExtraBarRight1;
    private boolean computeExtraBarRight2;

    public ComputationInclusions(){
    }

    public boolean isComputeTopBar1() {
        return computeTopBar1;
    }

    public void setComputeTopBar1(boolean computeTopBar1) {
        this.computeTopBar1 = computeTopBar1;
    }

    public boolean isComputeTopBar2() {
        return computeTopBar2;
    }

    public void setComputeTopBar2(boolean computeTopBar2) {
        this.computeTopBar2 = computeTopBar2;
    }

    public boolean isComputeBottomBar1() {
        return computeBottomBar1;
    }

    public void setComputeBottomBar1(boolean computeBottomBar1) {
        this.computeBottomBar1 = computeBottomBar1;
    }

    public boolean isComputeBottomBar2() {
        return computeBottomBar2;
    }

    public void setComputeBottomBar2(boolean computeBottomBar2) {
        this.computeBottomBar2 = computeBottomBar2;
    }

    public boolean isComputeSplicing() {
        return computeSplicing;
    }

    public void setComputeSplicing(boolean computeSplicing) {
        this.computeSplicing = computeSplicing;
    }

    public boolean isComputeExtraBarLeft1() {
        return computeExtraBarLeft1;
    }

    public void setComputeExtraBarLeft1(boolean computeExtraBarLeft1) {
        this.computeExtraBarLeft1 = computeExtraBarLeft1;
    }

    public boolean isComputeExtraBarLeft2() {
        return computeExtraBarLeft2;
    }

    public void setComputeExtraBarLeft2(boolean computeExtraBarLeft2) {
        this.computeExtraBarLeft2 = computeExtraBarLeft2;
    }

    public boolean isComputeExtraBarRight1() {
        return computeExtraBarRight1;
    }

    public void setComputeExtraBarRight1(boolean computeExtraBarRight1) {
        this.computeExtraBarRight1 = computeExtraBarRight1;
    }

    public boolean isComputeExtraBarRight2() {
        return computeExtraBarRight2;
    }

    public void setComputeExtraBarRight2(boolean computeExtraBarRight2) {
        this.computeExtraBarRight2 = computeExtraBarRight2;
    }
}
