package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

public enum LumberSize {

    TWO_BY_TWO(1,"2x2"),
    TWO_BY_THREE(2,"2x3"),
    TWO_BY_FOUR(3,"2x4");

    String name;
    int id;

    LumberSize(int id,String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
