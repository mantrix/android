package com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.QtyDiameterPair;

public class VerticalBar extends Model {

    QtyDiameterPair property1;

    QtyDiameterPair property2;

    public VerticalBar(){

    }

    public VerticalBar(QtyDiameterPair property1, QtyDiameterPair property2) {
        this.property1 = property1;
        this.property2 = property2;
    }

    public QtyDiameterPair getProperty1() {
        return property1;
    }

    public void setProperty1(QtyDiameterPair property1) {
        this.property1 = property1;
    }

    public QtyDiameterPair getProperty2() {
        return property2;
    }

    public void setProperty2(QtyDiameterPair property2) {
        this.property2 = property2;
    }
}


