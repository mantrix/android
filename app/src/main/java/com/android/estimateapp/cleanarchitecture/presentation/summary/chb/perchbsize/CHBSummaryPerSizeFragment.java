package com.android.estimateapp.cleanarchitecture.presentation.summary.chb.perchbsize;

import android.os.Bundle;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerSizeData;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.mantrixengineering.estimateapp.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import io.reactivex.Observable;

public class CHBSummaryPerSizeFragment extends BaseFragment {

    private static final String PER_SIZE_SUMMARY = "summaryPerSizeData";

    List<CHBSummaryPerSizeData> list = new ArrayList<>();


    public static CHBSummaryPerSizeFragment newInstance(Map<CHBSize, CHBSummaryPerSizeData> summaryPerSizeDataMap) {

        Bundle args = new Bundle();

        args.putSerializable(PER_SIZE_SUMMARY, (Serializable) summaryPerSizeDataMap);
        CHBSummaryPerSizeFragment fragment = new CHBSummaryPerSizeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    SummaryPerSizeAdapter adapter;

    NpaLinearLayoutManager npaLinearLayoutManager;

    @Override
    protected int getLayoutResource() {
        return R.layout.chb_summary_list;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {

        extractExtras();
        initRecyclerView();
    }

    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setItems(list);
    }


    private void extractExtras() {
        Bundle bundle = getArguments();
        Map<CHBSize, CHBSummaryPerSizeData> summaryPerSizeMap = (Map<CHBSize, CHBSummaryPerSizeData>) bundle.getSerializable(PER_SIZE_SUMMARY);

        for (CHBSize key : summaryPerSizeMap.keySet()) {
            list.add(summaryPerSizeMap.get(key));
        }

        list = Observable.fromIterable(list)
                .toSortedList((o1, o2) -> new Integer(o1.chbSize.getValue()).compareTo(new Integer(o2.chbSize.getValue()))).blockingGet();
    }

}