package com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.tiletype;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;

import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class TileTypeFactorFragment extends BaseFragment implements TileTypeFactorContract.View {

    @BindView(R.id.et_tile)
    EditText etTile;
    @BindView(R.id.et_length)
    EditText etLength;
    @BindView(R.id.et_width)
    EditText etWidth;

    public static TileTypeFactorFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();
        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        TileTypeFactorFragment fragment = new TileTypeFactorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    TileTypeAdapter adapter;

    @Inject
    TileTypeFactorContract.UserActionListener presenter;

    NpaLinearLayoutManager npaLinearLayoutManager;

    private ProgressDialog saveProgressDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.tile_type_factor_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        initRecyclerView();

        presenter.setData(getArguments().getString(Constants.PROJECT_ID), getArguments().getString(Constants.SCOPE_KEY));
        presenter.setView(this);
        presenter.start();

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

    }


    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void displayList(List<TileType> factors) {
        adapter.setItems(factors);
    }

    @OnClick(R.id.rl_save)
    public void onSaveClick() {
        if (saveProgressDialog != null) {
            saveProgressDialog.show();
        }
        presenter.updateFactors(adapter.getData());
    }

    @Override
    public void factorSaved() {
        if (saveProgressDialog != null) {
            saveProgressDialog.dismiss();
        }
    }

    @OnClick(R.id.iv_add)
    public void onAddTileTypeClick() {
        boolean valid = true;

        if (etTile.getText().toString().isEmpty()) {
            etTile.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etTile.setError(null);
        }

        if (etLength.getText().toString().isEmpty()) {
            etLength.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etLength.setError(null);
        }

        if (etWidth.getText().toString().isEmpty()) {
            etWidth.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etWidth.setError(null);
        }

        if (valid) {
            String tileName = etTile.getText().toString().trim();
            String tileLength = etLength.getText().toString().trim();
            String tileWidth = etWidth.getText().toString().trim();

            presenter.createTileType(tileName,Double.parseDouble(tileLength),Double.parseDouble(tileWidth));

            etTile.setText("");
            etLength.setText("");
            etWidth.setText("");
        }
    }
}