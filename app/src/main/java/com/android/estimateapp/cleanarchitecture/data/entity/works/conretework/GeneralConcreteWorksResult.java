package com.android.estimateapp.cleanarchitecture.data.entity.works.conretework;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;

public class GeneralConcreteWorksResult extends Model {

    @Exclude
    public Category category;

    private double volume;
    private double cement;
    private double sand;
    private double threeFourthGravel;
    private double g1Gravel;

    public GeneralConcreteWorksResult(){

    }


    @Exclude
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    public double getThreeFourthGravel() {
        return threeFourthGravel;
    }

    public void setThreeFourthGravel(double threeFourthGravel) {
        this.threeFourthGravel = threeFourthGravel;
    }

    public double getG1Gravel() {
        return g1Gravel;
    }

    public void setG1Gravel(double g1Gravel) {
        this.g1Gravel = g1Gravel;
    }
}
