package com.android.estimateapp.cleanarchitecture.presentation.cost.material.plastering;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.PlasteringSummary;

public interface PlasteringMaterialContract {

    interface View {

        void setInput(PlasteringSummary materials);

        void inputSaved();

        void showSavingNotAllowedError();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void save(PlasteringSummary materials);
    }
}
