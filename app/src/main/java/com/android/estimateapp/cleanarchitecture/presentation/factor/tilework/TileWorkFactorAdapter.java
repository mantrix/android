package com.android.estimateapp.cleanarchitecture.presentation.factor.tilework;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.EdittextBaseOutputListener;
import com.android.estimateapp.utils.DisplayUtil;
import com.android.estimateapp.utils.NumberUtils;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TileWorkFactorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<TileWorkFactor>> {


    private List<TileWorkFactor> list;

    @Inject
    public TileWorkFactorAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.tile_work_factor_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<TileWorkFactor> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<TileWorkFactor> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    public List<TileWorkFactor> getData() {
        return list;
    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_concrete_class)
        TextView tvMortarClass;

        @BindView(R.id.et_cement)
        EditText etCement;

        @BindView(R.id.et_sand)
        EditText etSand;

        @BindView(R.id.et_adhesive)
        EditText etAdhesive;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(TileWorkFactor factor, int position) {

            Grade grade = Grade.parseInt(factor.getMortarClass());
            tvMortarClass.setText(grade.toString());
            etCement.setText(DisplayUtil.toPlainString(factor.getCement()));
            etSand.setText(DisplayUtil.toPlainString(factor.getSand()));
            etAdhesive.setText(DisplayUtil.toPlainString(factor.getAdhesive()));

            etCement.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);
                    String input = etCement.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setCement(Double.parseDouble(etCement.getText().toString()));
                    }
                }
            });


            etSand.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etSand.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setSand(Double.parseDouble(etSand.getText().toString()));
                    }
                }
            });

            etAdhesive.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etAdhesive.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setAdhesive(Double.parseDouble(etAdhesive.getText().toString()));
                    }
                }
            });
        }
    }
}
