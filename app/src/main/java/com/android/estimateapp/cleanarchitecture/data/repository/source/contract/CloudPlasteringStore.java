package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;

import java.util.List;

import io.reactivex.Observable;

public interface CloudPlasteringStore {

    Observable<PlasteringInput> createPlasteringInput(String projectId, String scopeOfWorkId, PlasteringInput plasteringInput);

    Observable<PlasteringInput> updatePlasteringInput(String projectId, String scopeOfWorkId, String itemId, PlasteringInput plasteringInput);

    Observable<List<PlasteringInput>> getPlasteringInputs(String projectId, String scopeOfWorkId);

    Observable<PlasteringInput> getPlasteringInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<List<PlasteringMortarFactor>> createPlasteringMortarFactors(String projectId, List<PlasteringMortarFactor> plasteringMortarFactors);

    Observable<List<PlasteringMortarFactor>> updatePlasteringMortarFactors(String projectId, List<PlasteringMortarFactor> plasteringMortarFactors);

    Observable<List<PlasteringMortarFactor>> getPlasteringMortarFactors(String projectId);

    Observable<PlasteringResult> savePlasteringInputResult(String projectId, String scopeOfWorkId, String itemId, PlasteringResult result);

}
