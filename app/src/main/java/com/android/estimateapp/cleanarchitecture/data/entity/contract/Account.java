package com.android.estimateapp.cleanarchitecture.data.entity.contract;

public interface Account extends NullChecker{

    String getUid();

    String getEmail();

    Long getCreatedAt();

    String mobileNo();

    String getPicURL();

    Name getName();

    Subscription getSubscription();
}
