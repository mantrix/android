package com.android.estimateapp.configuration.injection.module.welcome;

import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserDialogFragment;
import com.android.estimateapp.configuration.injection.scope.PerChildFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CreateUserBuilderModule {


    @PerChildFragment
    @ContributesAndroidInjector(modules = {CreateUserModule.class})
    abstract CreateUserDialogFragment bindCreateUserDialogFragment();
}
