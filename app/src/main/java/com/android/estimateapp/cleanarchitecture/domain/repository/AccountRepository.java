package com.android.estimateapp.cleanarchitecture.domain.repository;

import com.android.estimateapp.cleanarchitecture.data.entity.CreateUserPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.SignupPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.AccountCredential;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Name;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface AccountRepository {

    Observable<AccountCredential> login(String email, String password);

    Observable<Object> authorizeUser(String idToken, String deviceId);

    Completable logout();

    Single<Boolean> hasActiveUser();

    Observable<Account> getLoggedInUser();

    Observable<Account> getUser(String uid);

    Observable<String> updateProfilePhoto();

    Observable<Name> updateName(Name name);

    Observable<AccountCredential>  createUser(CreateUserPayload createUserPayload);

    Observable<AccountCredential>  registerUser(SignupPayload signupPayload);

    Single<Boolean> userExist(String uid);

    Observable<AccountCredential> getIDToken(String uid);

}

