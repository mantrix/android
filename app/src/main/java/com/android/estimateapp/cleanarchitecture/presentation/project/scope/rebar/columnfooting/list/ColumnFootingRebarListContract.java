package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface ColumnFootingRebarListContract {

    interface View {

        void displayList(List<ColumnFootingRebarInput> inputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
