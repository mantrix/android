package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.android.estimateapp.cleanarchitecture.data.entity.AccountEntity;
import com.android.estimateapp.cleanarchitecture.data.entity.CreateUserPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.NameEntity;
import com.android.estimateapp.cleanarchitecture.data.entity.SignupPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.AccountCredential;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Name;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.account.CloudAccountDataStore;
import com.android.estimateapp.cleanarchitecture.domain.exception.ArCiRuntimeException;
import com.android.estimateapp.cleanarchitecture.domain.exception.AuthenticationException;
import com.android.estimateapp.cleanarchitecture.domain.exception.NoConnectionException;
import com.android.estimateapp.configuration.Constants;
import com.android.estimateapp.configuration.service.WebService;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import io.reactivex.Completable;
import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static com.android.estimateapp.configuration.Constants.Code.INVALID_TOKEN;


@Singleton
public class WSAccountStore extends BaseStore implements CloudAccountDataStore {

    private static final String TAG = WSAccountStore.class.getSimpleName();

    private static String FIREBASE_USER_REF = "users";
    private static String FIREBASE_DATA_REF = "data";

    private FirebaseAuth mAuth;

    private DatabaseReference userRef;
    private Context context;

    private StorageReference photosRef;

    private WebService service;


    @Inject
    public WSAccountStore(Context context, WebService webService){
        super();

        mAuth = FirebaseAuth.getInstance();
        userRef = baseReference.child(FIREBASE_USER_REF);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReference();
        photosRef = storageReference.child("photos");
        this.context = context;
        this.service = webService;

    }


    @Override
    public Completable logout() {
        return Completable.fromCallable(() -> {
            mAuth.signOut();
            return "";
        });
    }

    @Override
    public Observable<AccountCredential> login(String email, String password) {
        return Observable.<String>create(emitter -> {
            mAuth.signInWithEmailAndPassword(email,password)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = task.getResult().getUser();
                            String uid = firebaseUser.getUid();
                            emitter.onNext(uid);
                            emitter.onComplete();
                        } else {
                           Exception exception = task.getException();

                           if (exception instanceof FirebaseAuthInvalidCredentialsException || exception instanceof FirebaseAuthInvalidUserException){
                               emitter.onError(new AuthenticationException(Constants.Code.INVALID_CREDENTIALS,exception));
                           } else if (exception instanceof FirebaseNetworkException){
                               emitter.onError(new NoConnectionException(exception));
                           } else {
                               emitter.onError(exception);
                           }
                        }
                    });
        }).flatMap(uid -> getIDToken(uid));
    }


    @Override
    public Observable<Object> authorizeUser(String token, String deviceId) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        return service.authorizeUser(token, firebaseUser.getUid(), "android", deviceId);
    }

    @Override
    public Observable<AccountCredential> createUser(CreateUserPayload createUserPayload) {

        return getIDToken(createUserPayload.getUid())
                .flatMap(credential -> {
                    return service.createUser(credential.getToken(),createUserPayload)
                            .andThen(Observable.just(credential))
                            .subscribeOn(Schedulers.io());
                });
    }

    @Override
    public Observable<AccountCredential> registerUser(SignupPayload signupPayload) {
        return service.registerUser(signupPayload)
                .andThen(login(signupPayload.email, signupPayload.password));
    }

    @Override
    public Observable<AccountCredential> getIDToken(String uid){
        return Observable.<AccountCredential>create(emitter -> {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            firebaseUser.getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                @Override
                public void onSuccess(GetTokenResult result) {

                    String idToken = "Bearer " + result.getToken();

                    emitter.onNext(new AccountCredential(){
                        @Override
                        public String getID() {
                            return uid;
                        }

                        @Override
                        public String getToken() {
                            return idToken;
                        }
                    });
                    emitter.onComplete();
                }
            }).addOnFailureListener(e -> emitter.onError(new AuthenticationException(INVALID_TOKEN,e)));
        });
    }

    @Override
    public Observable<Account> getAccount(String uid) {
        return Observable.create(emitter -> {
            userRef.child(FIREBASE_DATA_REF)
                    .child(uid).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){
                        AccountEntity accountEntity = dataSnapshot.getValue(AccountEntity.class);
                        emitter.onNext(accountEntity);
                    } else {
                        emitter.onNext(new AccountEntity(Model.InstanceType.NULL));
                    }
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            });
        });
    }

    @Override
    public Observable<Account> getLoggedInAccount() {
        return Observable.create(emitter -> {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            if (firebaseUser != null && firebaseUser.getUid() != null){
                Log.d(TAG, "getLoggedInAccount: " + firebaseUser.getUid());

                userRef.child(FIREBASE_DATA_REF)
                        .child(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            AccountEntity accountEntity = dataSnapshot.getValue(AccountEntity.class);
                            emitter.onNext(accountEntity);
                        } else {
                            emitter.onNext(new AccountEntity(Model.InstanceType.NULL));
                        }
                        emitter.onComplete();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                });
            } else {
                emitter.onError(new ArCiRuntimeException(Constants.Code.NO_LOGIN_USER));
            }
        });
    }


    @Override
    public Single<Boolean> hasActiveUser() {
        return Single.fromCallable(() -> {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            return firebaseUser != null && firebaseUser.getUid() != null;
        });
    }

    @Override
    public Single<Boolean> userExist(String uid){
        return Single.create(emitter -> {

            userRef.child(FIREBASE_DATA_REF)
                    .child(uid)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            emitter.onSuccess(dataSnapshot.exists());
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            emitter.onError(databaseError.toException());
                        }
                    });
        });
    }

    @Override
    public Observable<String> updateProfilePhoto() {
        return Observable.create(emitter -> {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            String uid = firebaseUser.getUid();
            String fileName = Constants.PROFILE_PIC_PREFIX + uid;
            StorageReference profilePicRef = photosRef.child(uid).child(fileName);

            profilePicRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    if (uri != null){
                        userRef.child(FIREBASE_DATA_REF).child(uid).child("picURL").setValue(uri.toString(), (databaseError, databaseReference) -> {
                            if (databaseError != null){
                                emitter.onError(databaseError.toException());
                            } else {
                                emitter.onNext(uri.toString());
                                emitter.onComplete();
                            }
                        });
                    } else {
                        emitter.onError(new RuntimeException("Failed to get user photo."));
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                    emitter.onError(new RuntimeException("Failed to get user photo.",exception));
                }
            });


        });
    }

    @Override
    public Observable<Name> updateAccountName(Name name) {
        return Observable.create(emitter -> {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            String uid = firebaseUser.getUid();

            NameEntity nameEntity = (NameEntity) name;
            userRef.child(FIREBASE_DATA_REF)
                    .child(uid)
                    .child("name")
                    .setValue(nameEntity.toMap(),(databaseError, databaseReference) -> {
                        if (databaseError != null){
                            emitter.onError(interpretAndEmitError(databaseError.toException()));
                        } else {
                            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
                                        NameEntity nameEntity1 = dataSnapshot.getValue(NameEntity.class);
                                        emitter.onNext(nameEntity1);
                                    } else {
                                        emitter.onNext(new NameEntity(Model.InstanceType.NULL));
                                    }
                                    emitter.onComplete();
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                   emitter.onError(interpretAndEmitError(databaseError.toException()));
                                }
                            });
                        }
                    });
        });
    }

    private Exception interpretAndEmitError(Exception exception){
        Exception ex;
        if (exception instanceof FirebaseAuthInvalidCredentialsException || exception instanceof FirebaseAuthInvalidUserException){
            ex = new AuthenticationException(Constants.Code.INVALID_CREDENTIALS,exception);
        } else if (exception instanceof FirebaseNetworkException){
            ex = new NoConnectionException(exception);
        } else {
            ex = exception;
        }
        return ex;
    }


}
