package com.android.estimateapp.cleanarchitecture.domain.usecase.impl;

import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Subscription;
import com.android.estimateapp.cleanarchitecture.domain.repository.AccountRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.BaseUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;

public class AccountSubscriptionImpl extends BaseUseCase implements AccountSubscriptionUseCase {

    private AccountRepository accountRepository;

    @Inject
    public AccountSubscriptionImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Observable<Boolean> isPayingUser() {
        return accountRepository.getLoggedInUser()
                .map(Account::getSubscription)
                .map(subscription -> {
                    boolean isPaying = false;
                    if (subscription != null) {
                        isPaying = subscription.isSubscribed() || subscription.isPaid();
                    }
                    return isPaying;
                });
    }

    @Override
    public Observable<Boolean> isTrialUser() {
        return accountRepository.getLoggedInUser()
                .map(Account::getSubscription)
                .map(subscription -> {
                    boolean isTrial = false;
                    if (subscription != null) {
                        isTrial = subscription.isTrial();
                    }
                    return isTrial;
                });
    }

    @Override
    public Observable<Boolean> isPayingUserExperience() {
        return accountRepository.getLoggedInUser()
                .map(Account::getSubscription)
                .map(subscription -> {
                    if (subscription != null) {
                        return subscription.isSubscribed() || subscription.isPaid() ||  subscription.isTrial();
                    }
                    return false;
                });
    }

    @Override
    public Observable<Subscription> getSubscription() {
        return accountRepository.getLoggedInUser()
                .map(Account::getSubscription);
    }
}
