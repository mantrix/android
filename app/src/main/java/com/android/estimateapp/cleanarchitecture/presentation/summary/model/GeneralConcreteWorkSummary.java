package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import java.util.HashMap;
import java.util.Map;

public class GeneralConcreteWorkSummary<T> {

    Map<T, ConcreteWorkMaterials> materialPerCategory = new HashMap<>();

    ConcreteWorkMaterials totalMaterials;

    public GeneralConcreteWorkSummary(Map<T, ConcreteWorkMaterials> materialPerCategory, ConcreteWorkMaterials totalMaterials) {
        this.materialPerCategory = materialPerCategory;
        this.totalMaterials = totalMaterials;
    }

    public ConcreteWorkMaterials getTotalMaterials() {
        return totalMaterials;
    }

    public Map<T, ConcreteWorkMaterials> getMaterialPerCategory() {
        return materialPerCategory;
    }
}
