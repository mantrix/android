package com.android.estimateapp.cleanarchitecture.presentation.cost.material.columnwallfooting;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;

public interface ColumnAndWallFootingMaterialCostContract {


    interface View {

        void setInput(RebarMaterialSummary materials);

        void inputSaved();

        void showSavingNotAllowedError();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void save(RebarMaterialSummary materials);
    }
}