package com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.stonetype;

import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class StoneTypeFactorPresenter extends BasePresenter implements StoneTypeFactorContract.UserActionListener {

    private StoneTypeFactorContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeKey;

    @Inject
    public StoneTypeFactorPresenter(ProjectRepository projectRepository,
                                    ThreadExecutorProvider threadExecutorProvider,
                                    PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }


    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(StoneTypeFactorContract.View view) {
        this.view = view;
    }

    @Override
    public void updateFactors(List<StoneType> factors) {
        projectRepository.updateStoneTypeFactors(projectId, factors)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(updatedRsbFactors -> {
                    view.factorSaved();
                }, throwable -> {
                    view.factorSaved();
                    throwable.printStackTrace();
                });
    }


    @Override
    public void start() {
        projectRepository.getStoneTypeFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(factors -> {
                    view.displayList(factors);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void createStoneType(String name, double w1, double w2) {
        StoneType stoneType = new StoneType();
        stoneType.setName(name);
        stoneType.setLength(w1);
        stoneType.setWidth(w2);

        projectRepository.createStoneTypeFactor(projectId, stoneType)
                .flatMap(tileTypeResult -> projectRepository.getStoneTypeFactors(projectId))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(factors -> {
                    /*
                     * Display updated Stone Types
                     */
                    view.displayList(factors);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}