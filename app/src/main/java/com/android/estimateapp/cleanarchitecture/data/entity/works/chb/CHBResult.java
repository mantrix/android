package com.android.estimateapp.cleanarchitecture.data.entity.works.chb;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;

public class CHBResult extends Model {

    String storeyLocation;

    double openings;

    double area;

    double chbCount;

    int chbSize;

    @Exclude
    CHBSize chbSizeEnum;

    double cement;

    double sand;

    double tenRsb;

    double twelveRsb;

    double tieWire;

    public CHBResult() {
    }


    public String getStoreyLocation() {
        return storeyLocation;
    }

    public void setStoreyLocation(String storeyLocation) {
        this.storeyLocation = storeyLocation;
    }

    public double getOpenings() {
        return openings;
    }

    public void setOpenings(double openings) {
        this.openings = openings;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getChbCount() {
        return chbCount;
    }

    public void setChbCount(double chbCount) {
        this.chbCount = chbCount;
    }

    @Exclude
    public CHBSize getChbSizeEnum() {
        return chbSizeEnum;
    }

    public void setChbSizeEnum(CHBSize chbSizeEnum) {
        this.chbSizeEnum = chbSizeEnum;
        setChbSize(chbSizeEnum.getValue());
    }

    public int getChbSize() {
        return chbSize;
    }

    public void setChbSize(int chbSize) {
        this.chbSize = chbSize;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    public double getTenRsb() {
        return tenRsb;
    }

    public void setTenRsb(double tenRsb) {
        this.tenRsb = tenRsb;
    }

    public double getTwelveRsb() {
        return twelveRsb;
    }

    public void setTwelveRsb(double twelveRsb) {
        this.twelveRsb = twelveRsb;
    }

    public double getTieWire() {
        return tieWire;
    }

    public void setTieWire(double tieWire) {
        this.tieWire = tieWire;
    }

}
