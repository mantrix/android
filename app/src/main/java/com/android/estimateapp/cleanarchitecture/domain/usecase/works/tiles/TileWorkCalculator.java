package com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkResult;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkSummary;

import java.util.List;

public interface TileWorkCalculator {

    TileWorkResult calculate(TileWorkFactor factor, TileWorkInput input);

    TileWorkSummary generateSummary(List<TileWorkResult> results);
}
