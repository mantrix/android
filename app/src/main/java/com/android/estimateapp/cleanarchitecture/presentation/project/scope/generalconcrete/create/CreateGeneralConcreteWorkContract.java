package com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateGeneralConcreteWorkContract {

    interface View {

        void setInput(ConcreteWorksInput input);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(GeneralConcreteWorksResult result);

        void inputSaved();

        void savingError();

        void requestForReCalculation();

        void setMortarClassPickerOptions(List<Grade> classes);

        void setGravelSizesOptions(List<GravelSize> gravelSizes);

        void setCategoryOptions(List<Category> categoryOptions);

        void showSavingDialog();

        void showSavingNotAllowedError();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(ConcreteWorksInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
