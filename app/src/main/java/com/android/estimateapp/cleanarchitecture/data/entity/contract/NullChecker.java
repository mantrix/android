package com.android.estimateapp.cleanarchitecture.data.entity.contract;

import java.io.Serializable;

public interface NullChecker extends Serializable {

    boolean isEmpty();

}
