package com.android.estimateapp.cleanarchitecture.presentation.project.listing;

import android.app.Activity;
import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<Project>> {

    Context context;

    private List<Project> items;

    private ListItemClickLister<Project> itemClickLister;


    @Inject
    public ProjectListAdapter(Activity context){
        this.context = context;
        items = new ArrayList<>();
    }

    public void setItemClickLister(ListItemClickLister<Project> itemClickLister){
        this.itemClickLister = itemClickLister;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.project_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(items.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(items.get(position) != null){
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void setItems(List<Project> list) {
        items.clear();
        items = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<Project> list) {
        items.addAll(list);
        notifyDataSetChanged();
    }

    public void deleteItem(String id){
        Integer position = null;
        for (int i = 0 ; i < items.size() ; i++){
           if (items.get(i).getKey().equalsIgnoreCase(id)){
               position = i;
               items.remove(i);
               break;
           }
        }
        if (position != null){
            notifyItemRemoved(position);
        }
    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv_item)
        CardView cardView;

        @BindView(R.id.tv_created_at_span)
        TextView tvCreatedSpan;

        @BindView(R.id.tv_project_name)
        TextView tvProjectName;

        @BindView(R.id.tv_description)
        TextView tvDescription;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Project project) {

            CharSequence span = DateUtils.getRelativeTimeSpanString(project.getCreatedAt());
            tvCreatedSpan.setText(span);

            tvProjectName.setText(project.getName());

            if (project.getLocation() != null){
                tvDescription.setVisibility(View.VISIBLE);
                tvDescription.setText(project.getLocation());
            } else {
                if (project.getDescription() != null){
                    tvDescription.setVisibility(View.VISIBLE);
                    tvDescription.setText(project.getDescription());
                } else {
                    tvDescription.setVisibility(View.GONE);
                }
            }

            cardView.setOnClickListener(v -> {
                itemClickLister.itemClick(project);
            });

            cardView.setOnLongClickListener(v -> {
                itemClickLister.onLongClick(project);
                return false;
            });
        }
    }
}
