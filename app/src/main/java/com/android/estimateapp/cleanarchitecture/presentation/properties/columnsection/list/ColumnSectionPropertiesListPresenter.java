package com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.list;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import javax.inject.Inject;

public class ColumnSectionPropertiesListPresenter extends BasePresenter implements ColumnSectionPropertiesListContract.UserActionListener {


    private ColumnSectionPropertiesListContract.View view;
    private ProjectPropertiesRepository projectPropertiesRepository;
    private String projectId;

    @Inject
    public ColumnSectionPropertiesListPresenter(ProjectPropertiesRepository projectPropertiesRepository,
                                                ThreadExecutorProvider threadExecutorProvider,
                                                PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectPropertiesRepository = projectPropertiesRepository;
    }

    @Override
    public void setData(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public void setView(ColumnSectionPropertiesListContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadInputs();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void reloadList() {
        loadInputs();
    }

    private void loadInputs() {
        projectPropertiesRepository.getColumnProperties(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(inputs -> {
                    if (!inputs.isEmpty()) {
                        view.displayList(inputs);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void deleteItem(String inputId) {
//        projectPropertiesRepository.deleteScopeInput(projectId, scopeOfWorkId, inputId)
//                .subscribeOn(threadExecutorProvider.computationScheduler())
//                .observeOn(postExecutionThread.getScheduler())
//                .subscribe(() -> {
//                    view.removeItem(inputId);
//                }, throwable -> {
//                    throwable.printStackTrace();
//                });
    }
}
