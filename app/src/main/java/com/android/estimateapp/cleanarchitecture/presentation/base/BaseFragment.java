package com.android.estimateapp.cleanarchitecture.presentation.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

public abstract class BaseFragment extends DaggerFragment {

    /**
     * Abstract method for providing child {@link Fragment} layouts
     *
     * @return id of the layout that will be rendered on the {@link Fragment}
     */
    protected abstract int getLayoutResource();

    protected abstract void onCreateView(Bundle savedInstanceState);

    protected Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutResource(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        onCreateView(savedInstanceState);

        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null)
            unbinder.unbind();
    }


    protected void addFragment(FragmentManager fragmentManager, int containerId, Fragment fragment, boolean addToBackStack, boolean allowStateLoss) {
        if (fragmentManager.isDestroyed() ||
                isDetached() ||
                getActivity() == null ||
                getActivity().isChangingConfigurations() ||
                (Build.VERSION.SDK_INT >= 17 && getActivity().isDestroyed())) {
            return;
        }
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(containerId, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        if (allowStateLoss)
            fragmentTransaction.commitAllowingStateLoss();
        else fragmentTransaction.commit();
    }

    protected void showFragment(FragmentManager fragmentManager, int containerId, Fragment fragment, boolean addToBackStack, boolean allowStateLoss) {
        if (fragmentManager.isDestroyed() ||
                isDetached() ||
                getActivity() == null ||
                getActivity().isChangingConfigurations() ||
                (Build.VERSION.SDK_INT >= 17 && getActivity().isDestroyed())) {
            return;
        }
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerId, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        if (allowStateLoss)
            fragmentTransaction.commitAllowingStateLoss();
        else fragmentTransaction.commit();
    }

    protected Fragment getFragmentById(FragmentManager fragmentManager, int containerId) {
        return fragmentManager.findFragmentById(containerId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        propagateOnActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void propagateOnActivityResult(int requestCode, int resultCode, Intent data) {
        final FragmentManager fragmentManager = getChildFragmentManager();
        if (fragmentManager != null) {
            final List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments != null) {
                for (int i = 0; i < fragments.size(); ++i) {
                    final Fragment fragment = fragments.get(i);
                    if (fragment != null) {
                        fragment.onActivityResult(requestCode, resultCode, data);
                    }
                }
            }
        }
    }

    protected AlertDialog createAlertDialog(String title, String message, String positiveBtnText, String negativeBtnText,
                                            DialogInterface.OnClickListener positiveOnClickListener,
                                            DialogInterface.OnClickListener negativeOnClickListener) {
        if (getActivity() instanceof BaseActivity){
            return ((BaseActivity) getActivity()).createAlertDialog(title,message,positiveBtnText,negativeBtnText,positiveOnClickListener,negativeOnClickListener);
        }
        return null;
    }

    public ProgressDialog createLoadingAlert(String title, String message){
        ProgressDialog dialog = new ProgressDialog(getActivity());
        if (title != null){
            dialog.setTitle(title);
        }

        dialog.setMessage(message);

        return dialog;
    }

}
