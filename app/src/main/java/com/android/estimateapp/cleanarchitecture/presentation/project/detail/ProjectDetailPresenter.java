package com.android.estimateapp.cleanarchitecture.presentation.project.detail;

import android.util.Log;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.ProjectRetrieverUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.factory.DefaultFactorFactory;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ProjectDetailPresenter extends BasePresenter implements ProjectDetailContract.UserActionListener {

    private static final String TAG = ProjectDetailPresenter.class.getSimpleName();

    private ProjectDetailContract.View view;
    private ProjectRetrieverUseCase projectRetrieverUseCase;
    private ProjectRepository projectRepository;

    private String projectId;

    @Inject
    public ProjectDetailPresenter(ProjectRetrieverUseCase projectRetrieverUseCase,
                                  ProjectRepository projectRepository,
                                  ThreadExecutorProvider threadExecutorProvider,
                                  PostExecutionThread postExecutionThread){
        super(threadExecutorProvider, postExecutionThread);
        this.projectRetrieverUseCase = projectRetrieverUseCase;
        this.projectRepository = projectRepository;
    }

    @Override
    public void setView(ProjectDetailContract.View view) {
        this.view = view;
    }

    @Override
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public void start() {
        loadProject();
        loadProjectScopes();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void createProjectScope(ScopeOfWork scopeOfWork) {
        Work work = new Work(scopeOfWork.getId(),scopeOfWork.toString());
        projectRepository.createProjectScopeOfWork(projectId,work)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(workResult -> { // Refresh project scopes
                    loadProjectScopes();

                    createScopeFactors(ScopeOfWork.parseInt(workResult.getScopeID()),workResult.getKey());
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    private void loadProject() {
        projectRetrieverUseCase.getProject(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(project -> {
                    view.setProjectName(project.getName());
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    /**
     * Load Scope Chooser options and Project Scopes
     */
    private void loadProjectScopes(){
        projectRepository.getProjectScopesOfWorks(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(works -> {
                    view.displayProjectScopes(works);

                    List<ScopeOfWork> options = getUnselectedScopes(works);
                    if (options.isEmpty()){
                        view.setAddScopeVisibility(false);
                    } else {
                        view.setAddScopeVisibility(true);
                        view.setScopeChooserOption(options);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private List<ScopeOfWork> getUnselectedScopes(List<Work> addedScopes){
        List<ScopeOfWork> allScopes = ScopeOfWork.allScopeOfWorks();
        Map<Integer, ScopeOfWork> optionsMap = ScopeOfWork.allScopeOfMap();
        for (int i = 0 ; i <  allScopes.size(); i++){
            ScopeOfWork scopeOfWork = allScopes.get(i);
            for (Work work : addedScopes){
                if (scopeOfWork.getId() == work.getScopeID()){
                    optionsMap.remove(scopeOfWork.getId());
                    continue;
                }
            }
        }
        return new ArrayList<>(optionsMap.values());
    }

    private void createScopeFactors(ScopeOfWork scopeOfWork, String scopeOfWorkKey) {
        switch (scopeOfWork) {
            case CHB_LAYING:
                createCHBFactors();
                break;
            case CHB_PLASTERING:
                createPlasteringMortarFactors();
                break;
            case CONCRETE_WORKS:
            case BEAM_CONCRETE:
            case COLUMN_CONCRETE:
                createConcreteFactors();
                break;
            case TILE_WORKS:
                createTileWorkFactors();
                break;
            case STONE_CLADDING:
                createStoneCladdingFactors();
                break;
            case WALL_FOOTING_REBAR:
                break;
            case COLUMN_FOOTING_REBAR:
                break;
        }
    }




    private void createCHBFactors(){
        projectRepository.createCHBFactor(projectId, DefaultFactorFactory.getDefaultCHBFactor())
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbFactor -> {
                    Log.d(TAG, "CHB Factor Created!");
                },throwable -> {
                    throwable.printStackTrace();
                    // TODO: add error handling when this operation failed
                });
    }

    private void createPlasteringMortarFactors(){
        projectRepository.createPlasteringMortarFactors(projectId, DefaultFactorFactory.getDefaultPlasteringFactors())
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbFactors -> {
                    Log.d(TAG, "Plastering Factor Created!");
                },throwable -> {
                    throwable.printStackTrace();
                    // TODO: add error handling when this operation failed
                });
    }

    private void createConcreteFactors(){
        projectRepository.getConcreteWorkFactors(projectId)
                .flatMap(concreteFactors -> {
                    if (concreteFactors.isEmpty()){
                        return projectRepository.createConcreteWorksFactors(projectId, DefaultFactorFactory.getDefaultConcreteFactors());
                    } else {
                        return Observable.just(concreteFactors);
                    }
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(concreteFactors -> {
                    Log.d(TAG, "Concrete Factor Created!");
                },throwable -> {
                    Log.d("TestDebug", "Error: " + throwable.getMessage());
                    throwable.printStackTrace();
                    // TODO: add error handling when this operation failed
                });
    }

    private void createTileWorkFactors(){
        projectRepository.createTileWorkFactors(projectId, DefaultFactorFactory.getDefaultTileWorkFactors())
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(tileWorkFactors -> {
                    Log.d(TAG, "Tile Work Factor Created!");
                },throwable -> {
                    throwable.printStackTrace();
                    // TODO: add error handling when this operation failed
                });
    }


    private void createStoneCladdingFactors(){
        projectRepository.createStoneCladdingFactors(projectId, DefaultFactorFactory.getStoneCladdingFactors())
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(stoneCladdingFactors -> {
                    Log.d(TAG, "Stone Cladding Factor Created!");
                },throwable -> {
                    throwable.printStackTrace();
                    // TODO: add error handling when this operation failed
                });
    }


}
