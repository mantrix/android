package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;

public class CHBReinforcement implements CHBReinforcementFactorContract.ReinforcementItem {

    VerticalSpacing verticalSpacing;
    HorizontalSpacing horizontalSpacing;

    public CHBReinforcement(VerticalSpacing verticalSpacing, HorizontalSpacing horizontalSpacing){
        setVerticalSpacing(verticalSpacing);
        setHorizontalSpacing(horizontalSpacing);
    }

    @Override
    public VerticalSpacing getVerticalSpacing() {
        return verticalSpacing;
    }

    @Override
    public HorizontalSpacing getHorizontalSpacing() {
        return horizontalSpacing;
    }

    @Override
    public void setVerticalSpacing(VerticalSpacing spacing) {
        this.verticalSpacing = spacing;
    }

    @Override
    public void setHorizontalSpacing(HorizontalSpacing spacing) {
        this.horizontalSpacing = spacing;
    }
}
