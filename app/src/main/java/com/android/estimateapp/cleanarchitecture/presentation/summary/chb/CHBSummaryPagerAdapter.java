package com.android.estimateapp.cleanarchitecture.presentation.summary.chb;

import android.content.Context;

import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.perchbsize.CHBSummaryPerSizeFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.perstorey.CHBSummaryPerStoreyFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBLayingSummary;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class CHBSummaryPagerAdapter extends FragmentPagerAdapter {


    private List<Fragment> fragments;
    private List<String> titles;
    private Context context;

    private CHBLayingSummary chbLayingSummary;

    public CHBSummaryPagerAdapter(Context context, FragmentManager fm, CHBLayingSummary chbLayingSummary) {
        super(fm);
        this.context = context;
        titles = new ArrayList<>();
        this.chbLayingSummary = chbLayingSummary;
        fragments = factorFragments();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    private List<Fragment> factorFragments() {
        List<Fragment> fragments = new ArrayList<>();

        titles.add(context.getString(R.string.chb_material_summary));
        fragments.add(CHBSummaryPerSizeFragment.newInstance(chbLayingSummary.getSummaryPerCHBSize()));

        titles.add(context.getString(R.string.per_storey_summary));
        fragments.add(CHBSummaryPerStoreyFragment.newInstance(chbLayingSummary.getSummaryPerStorey()));

        return fragments;
    }
}