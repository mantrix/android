package com.android.estimateapp.cleanarchitecture.presentation.cost.labor.add;

import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create.CreatePlasteringPresenter;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class AddLaborCostPresenter extends CreateScopeInputBasePresenter implements AddLaborCostContract.UserActionListener {

    private static final String TAG = CreatePlasteringPresenter.class.getSimpleName();

    private AddLaborCostContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;

    private WorkerGroup input;


    @Inject
    public AddLaborCostPresenter(ProjectRepository projectRepository,
                                     AccountSubscriptionUseCase accountSubscriptionUseCase,
                                     ThreadExecutorProvider threadExecutorProvider,
                                     PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase, threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;

    }

    @Override
    public void setView(AddLaborCostContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {

        if (itemKey != null){
            loadInput();
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save(WorkerGroup workerGroup) {
        this.input = workerGroup;

        if (allowPaidExperience){
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateLaborCost(projectId, scopeKey, itemKey,input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createLaborCost(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<WorkerGroup> inputObservable){

        view.showSavingDialog();

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }




    private void loadInput(){
        projectRepository.getLaborCost(projectId, scopeKey,itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbInput -> {
                    this.input = chbInput;

                    view.setInput(input);

                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

}
