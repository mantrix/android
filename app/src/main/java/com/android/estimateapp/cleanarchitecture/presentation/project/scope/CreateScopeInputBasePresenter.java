package com.android.estimateapp.cleanarchitecture.presentation.project.scope;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

public class CreateScopeInputBasePresenter extends BasePresenter {

    private AccountSubscriptionUseCase subscriptionUseCase;
    protected boolean allowPaidExperience;

    public CreateScopeInputBasePresenter(AccountSubscriptionUseCase subscriptionUseCase,
                                         ThreadExecutorProvider threadExecutorProvider,
                                         PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.subscriptionUseCase = subscriptionUseCase;

        loadAccountEligibility();
    }

    private void loadAccountEligibility() {
        subscriptionUseCase.isPayingUserExperience()
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(allowPaidExperience -> {
                    this.allowPaidExperience = allowPaidExperience;
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}
