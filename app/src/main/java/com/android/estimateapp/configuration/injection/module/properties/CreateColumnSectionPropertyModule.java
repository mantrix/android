package com.android.estimateapp.configuration.injection.module.properties;

import com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.create.CreateColumnSecionPropertyContract;
import com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.create.CreateColumnSectionPropertyPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateColumnSectionPropertyModule {

    @Provides
    @PerActivity
    CreateColumnSecionPropertyContract.UserActionListener provideCreateColumnSecionPropertyPresenter(CreateColumnSectionPropertyPresenter presenter){
        return presenter;
    }
}
