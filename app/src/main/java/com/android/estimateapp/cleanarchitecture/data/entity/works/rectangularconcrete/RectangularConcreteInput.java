package com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete;


import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class RectangularConcreteInput extends Model {

    int pouringBatchNum;
    int memberNum;
    double lengthCC;
    double offset;
    double pouringCut;
    double sets;
    double wastage;
    String gravelSizeType;
    int mortarClass;


    String propertyReferenceId;
    String propertyReferenceName;

    MemberSectionProperty propertyReference;

    public RectangularConcreteInput(){

    }

    public int getPouringBatchNum() {
        return pouringBatchNum;
    }

    public void setPouringBatchNum(int pouringBatchNum) {
        this.pouringBatchNum = pouringBatchNum;
    }

    public int getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(int memberNum) {
        this.memberNum = memberNum;
    }

    public double getLengthCC() {
        return lengthCC;
    }

    public void setLengthCC(double lengthCC) {
        this.lengthCC = lengthCC;
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public double getWastage() {
        return wastage;
    }

    public void setWastage(double wastage) {
        this.wastage = wastage;
    }

    public double getPouringCut() {
        return pouringCut;
    }

    public void setPouringCut(double pouringCut) {
        this.pouringCut = pouringCut;
    }

    public double getSets() {
        return sets;
    }

    public void setSets(double sets) {
        this.sets = sets;
    }

    public String getGravelSizeType() {
        return gravelSizeType;
    }

    public void setGravelSizeType(String gravelSizeType) {
        this.gravelSizeType = gravelSizeType;
    }

    @Exclude
    public GravelSize getGravelSizeTypeEnum() {
        return GravelSize.parseString(gravelSizeType);
    }


    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    @Exclude
    public Grade getConcreteClassEnum() {
        return Grade.parseInt(mortarClass);
    }


    public String getPropertyReferenceId() {
        return propertyReferenceId;
    }

    public void setPropertyReferenceId(String propertyReferenceId) {
        this.propertyReferenceId = propertyReferenceId;
    }

    public void setPropertyReferenceName(String propertyReferenceName) {
        this.propertyReferenceName = propertyReferenceName;
    }

    public String getPropertyReferenceName() {
        return propertyReferenceName;
    }

    @Exclude
    public MemberSectionProperty getPropertyReference() {
        return propertyReference;
    }

    @Exclude
    public void setPropertyReference(MemberSectionProperty propertyReference) {
        this.propertyReference = propertyReference;
    }
}
