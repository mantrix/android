package com.android.estimateapp.cleanarchitecture.presentation.factor.tilework;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface TileWorkFactorContract {

    interface View {

        void displayList(List<TileWorkFactor> factors);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(List<TileWorkFactor> factors);
    }
}
