package com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.works.StirrupsOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.RSBValuePair;

public class BeamRebarOutput extends Model {

    double lClear;

    RSBValuePair topContinuousBars;
    RSBValuePair topContinuousBars2;
    double topTotal;

    RSBValuePair bottomContinuousBars;
    RSBValuePair bottomContinuousBars2;
    double bottomTotal;

    RSBValuePair leftSupport;
    RSBValuePair leftSupport2;
    double leftTotal;

    RSBValuePair rightSupport;
    RSBValuePair rightSupport2;
    double rightTotal;

    RSBValuePair extraBarsMid;
    RSBValuePair extraBarsMid2;
    double midTotal;

    StirrupsOutput stirrupsOutput;

    RSBValuePair splicing;
    RSBValuePair splicin2;
    double splicingTotal;


    RSBValuePair hookes;
    RSBValuePair hookes2;
    double hookesTotal;

    public BeamRebarOutput(){

    }


    public double getlClear() {
        return lClear;
    }

    public void setlClear(double lClear) {
        this.lClear = lClear;
    }

    public RSBValuePair getTopContinuousBars() {
        return topContinuousBars;
    }

    public void setTopContinuousBars(RSBValuePair topContinuousBars) {
        this.topContinuousBars = topContinuousBars;
    }

    public RSBValuePair getTopContinuousBars2() {
        return topContinuousBars2;
    }

    public void setTopContinuousBars2(RSBValuePair topContinuousBars2) {
        this.topContinuousBars2 = topContinuousBars2;
    }

    public double getTopTotal() {
        return topTotal;
    }

    public void setTopTotal(double topTotal) {
        this.topTotal = topTotal;
    }

    public RSBValuePair getBottomContinuousBars() {
        return bottomContinuousBars;
    }

    public void setBottomContinuousBars(RSBValuePair bottomContinuousBars) {
        this.bottomContinuousBars = bottomContinuousBars;
    }

    public RSBValuePair getBottomContinuousBars2() {
        return bottomContinuousBars2;
    }

    public void setBottomContinuousBars2(RSBValuePair bottomContinuousBars2) {
        this.bottomContinuousBars2 = bottomContinuousBars2;
    }

    public double getBottomTotal() {
        return bottomTotal;
    }

    public void setBottomTotal(double bottomTotal) {
        this.bottomTotal = bottomTotal;
    }

    public RSBValuePair getLeftSupport() {
        return leftSupport;
    }

    public void setLeftSupport(RSBValuePair leftSupport) {
        this.leftSupport = leftSupport;
    }

    public RSBValuePair getLeftSupport2() {
        return leftSupport2;
    }

    public void setLeftSupport2(RSBValuePair leftSupport2) {
        this.leftSupport2 = leftSupport2;
    }

    public double getLeftTotal() {
        return leftTotal;
    }

    public void setLeftTotal(double leftTotal) {
        this.leftTotal = leftTotal;
    }

    public RSBValuePair getRightSupport() {
        return rightSupport;
    }

    public void setRightSupport(RSBValuePair rightSupport) {
        this.rightSupport = rightSupport;
    }

    public RSBValuePair getRightSupport2() {
        return rightSupport2;
    }

    public void setRightSupport2(RSBValuePair rightSupport2) {
        this.rightSupport2 = rightSupport2;
    }

    public double getRightTotal() {
        return rightTotal;
    }

    public void setRightTotal(double rightTotal) {
        this.rightTotal = rightTotal;
    }

    public RSBValuePair getExtraBarsMid() {
        return extraBarsMid;
    }

    public void setExtraBarsMid(RSBValuePair extraBarsMid) {
        this.extraBarsMid = extraBarsMid;
    }

    public RSBValuePair getExtraBarsMid2() {
        return extraBarsMid2;
    }

    public void setExtraBarsMid2(RSBValuePair extraBarsMid2) {
        this.extraBarsMid2 = extraBarsMid2;
    }

    public double getMidTotal() {
        return midTotal;
    }

    public void setMidTotal(double midTotal) {
        this.midTotal = midTotal;
    }

    public StirrupsOutput getStirrupsOutput() {
        return stirrupsOutput;
    }

    public void setStirrupsOutput(StirrupsOutput stirrupsOutput) {
        this.stirrupsOutput = stirrupsOutput;
    }

    public RSBValuePair getSplicing() {
        return splicing;
    }

    public void setSplicing(RSBValuePair splicing) {
        this.splicing = splicing;
    }

    public RSBValuePair getSplicing2() {
        return splicin2;
    }

    public void setSplicin2(RSBValuePair splicin2) {
        this.splicin2 = splicin2;
    }

    public double getSplicingTotal() {
        return splicingTotal;
    }

    public void setSplicingTotal(double splicingTotal) {
        this.splicingTotal = splicingTotal;
    }

    public RSBValuePair getHookes() {
        return hookes;
    }

    public void setHookes(RSBValuePair hookes) {
        this.hookes = hookes;
    }

    public RSBValuePair getHookes2() {
        return hookes2;
    }

    public void setHookes2(RSBValuePair hookes2) {
        this.hookes2 = hookes2;
    }

    public double getHookesTotal() {
        return hookesTotal;
    }

    public void setHookesTotal(double hookesTotal) {
        this.hookesTotal = hookesTotal;
    }
}
