package com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingMaterial;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingSummary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class StoneCladdingCalculatorImpl extends BaseCalculator implements StoneCladdingCalculator {

    @Inject
    public StoneCladdingCalculatorImpl() {

    }


    @Override
    public StoneCladdingSummary generateSummary(List<StoneCladdingResult> results){

        Map<String, StoneCladdingMaterial> materialPerStoneTypeMap = new HashMap<>();
        StoneCladdingMaterial total = new StoneCladdingMaterial();

        for (StoneCladdingResult result : results) {
            if (result.getStoneType() != null){

                // compute totals per category
                if (materialPerStoneTypeMap.containsKey(result.getStoneType().getName())) {
                    // get previous data and add the result to it.
                    materialPerStoneTypeMap.put(result.getStoneType().getName(), addResult(materialPerStoneTypeMap.get(result.getStoneType().getName()), result));
                } else {
                    // create a new map entry and set result to it.
                    materialPerStoneTypeMap.put(result.getStoneType().getName(), addResult(new StoneCladdingMaterial(), result));
                }

                // compute totals regardless of category
                total.area += result.getArea();
                total.cement += result.getCement();
                total.sand += result.getSand();
                total.quantity += result.getStoneCladdingPcs();
                total.adhesive += result.getAdhesive();

            }
        }

        // normalize results
        for (String key : materialPerStoneTypeMap.keySet()){
            StoneCladdingMaterial data = materialPerStoneTypeMap.get(key);

            data.area = roundHalfUp(data.area);
            data.cement = roundHalfUp(data.cement);
            data.sand = roundHalfUp(data.sand);
            data.adhesive = roundHalfUp(data.adhesive);

            materialPerStoneTypeMap.put(key,data);
        }

        // normalize results
        total.area = roundHalfUp(total.area);
        total.cement = roundUpToNearestWholeNumber(total.cement);
        total.sand = roundUpToNearestWholeNumber(total.sand);
        total.adhesive = roundUpToNearestWholeNumber(total.adhesive);


        return new StoneCladdingSummary(materialPerStoneTypeMap,total);
    }


    private StoneCladdingMaterial addResult(StoneCladdingMaterial materials, StoneCladdingResult result) {
        materials.stoneType = result.getStoneType();
        materials.area += result.getArea();
        materials.cement += result.getCement();
        materials.sand += result.getSand();
        materials.quantity += result.getStoneCladdingPcs();
        materials.adhesive += result.getAdhesive();
        return materials;
    }

    @Override
    public StoneCladdingResult calculate(StoneCladdingInput input, StoneCladdingFactor factor) {

        Dimension dimension = input.getDimension();

        double area = 0;
        if (dimension != null) {
            area = input.getSets() * dimension.getLength() * dimension.getWidth();
        }

        double netArea = area - input.getOpenings();
        double wastage = (1 + toDoublePercentage(input.getWastage()));

        double sizeArea = 0;
        double stoneCladding = 0;


        StoneType stoneType = input.getStoneType();
        if (stoneType != null) {
            sizeArea = stoneType.getLength() * stoneType.getWidth();
            stoneCladding = Math.ceil(netArea / (sizeArea / 10000) * wastage);
        }


        double sand = 0;
        double cement = 0;
        double adhesive = 0;
        if (factor != null){
           sand = (netArea * factor.getSand()) * wastage;
           cement = (netArea * factor.getCement()) * wastage;
           adhesive = (netArea * factor.getAdhesive()) * wastage;
        }

        StoneCladdingResult output = new StoneCladdingResult();
        output.setArea(area);
        output.setNetArea(netArea);

        if (stoneType != null){
            output.setStoneType(stoneType);
        }

        output.setSizeArea(sizeArea);
        output.setStoneCladdingPcs(stoneCladding);
        output.setSand(sand);
        output.setCement(cement);
        output.setAdhesive(adhesive);

        return output;
    }
}
