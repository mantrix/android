package com.android.estimateapp.cleanarchitecture.presentation.properties.membersection;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class MemberSectionPropertiesFragment extends BaseFragment implements MemberSectionPropertieContract.View {

    @BindView(R.id.et_item_num)
    EditText etItemNum;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_area)
    EditText etArea;
    @BindView(R.id.et_base)
    EditText etBase;
    @BindView(R.id.et_width)
    EditText etWidth;
    @BindView(R.id.btn_add)
    Button btnAdd;
    @BindView(R.id.ll_header)
    LinearLayout llHeader;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.rl_save)
    RelativeLayout rlSave;

    public static MemberSectionPropertiesFragment newInstance(String projectId) {

        Bundle args = new Bundle();
        args.putString(Constants.PROJECT_ID, projectId);
        MemberSectionPropertiesFragment fragment = new MemberSectionPropertiesFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Inject
    MemberSectionAdapter adapter;

    @Inject
    MemberSectionPropertieContract.UserActionListener presenter;

    NpaLinearLayoutManager npaLinearLayoutManager;

    private ProgressDialog saveProgressDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.member_section_properties_fragment;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        initRecyclerView();

        presenter.setData(getArguments().getString(Constants.PROJECT_ID));
        presenter.setView(this);
        presenter.start();

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

    }


    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void displayProperties(List<MemberSectionProperty> properties) {
        adapter.setItems(properties);
    }

    @Override
    public void saved() {
        if (saveProgressDialog != null) {
            saveProgressDialog.dismiss();
        }
    }

    @OnClick(R.id.rl_save)
    public void onSaveClick() {
        if (saveProgressDialog != null) {
            saveProgressDialog.show();
        }
        presenter.update(adapter.getData());
    }


    @OnClick(R.id.btn_add)
    public void onAddClick() {
        boolean valid = true;

        if (etItemNum.getText().toString().isEmpty()) {
            etItemNum.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etItemNum.setError(null);
        }

        if (etName.getText().toString().isEmpty()) {
            etName.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etName.setError(null);
        }

        if (etArea.getText().toString().isEmpty()) {
            etArea.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etArea.setError(null);
        }

        if (etBase.getText().toString().isEmpty()) {
            etBase.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etBase.setError(null);
        }

        if (etWidth.getText().toString().isEmpty()) {
            etWidth.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etWidth.setError(null);
        }

        if (valid) {

            String itemNum = etItemNum.getText().toString().trim();
            String name = etName.getText().toString().trim();

            String area = etArea.getText().toString().trim();
            String base = etBase.getText().toString().trim();
            String width = etWidth.getText().toString().trim();

            MemberSectionProperty property = new MemberSectionProperty(Integer.parseInt(itemNum),name,Double.parseDouble(area),Double.parseDouble(base),Double.parseDouble(width));
            presenter.createProperty(property);

            etItemNum.setText("");
            etName.setText("");
            etArea.setText("");
            etBase.setText("");
            etWidth.setText("");
        }
    }
}