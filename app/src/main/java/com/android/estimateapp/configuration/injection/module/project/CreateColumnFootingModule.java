package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnfooting.ColumnFootingWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnfooting.ColumnFootingWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.create.CreateColumnFootingInputPresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.create.CreateColumnFootingRebarInputContract;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateColumnFootingModule {


    @Provides
    @PerActivity
    CreateColumnFootingRebarInputContract.UserActionListener provideCreateColumnFootingRebarPresenter(CreateColumnFootingInputPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ColumnFootingWorkCalculator provideColumnFootingWorkCalculator(ColumnFootingWorkCalculatorImpl calculator) {
        return calculator;
    }
}
