package com.android.estimateapp.cleanarchitecture.data.entity;

import com.google.gson.annotations.SerializedName;

/**
 * {
 *   "paid" : false,
 *   "plan" : "free",
 *   "subscribed" : false,
 *   "trial" : false,
 *   "trialEndAt" : "05-01-2019"
 * }
 */
public class SubscriptionPayload {

    @SerializedName("plan")
    public String planName;

    @SerializedName("paid")
    public boolean isPaid;

    public SubscriptionPayload(){

    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }
}
