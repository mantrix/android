package com.android.estimateapp.cleanarchitecture.presentation.cost.material.chb;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerLocData;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatDialogFragment;

public class CHBLayingMaterialCostDialogFragment extends DaggerAppCompatDialogFragment implements CHBLayingMaterialCostContract.View {

    private final static String PROJECT_ID = "projectId";
    private final static String SCOPE_KEY = "scopeKey";
    @BindView(R.id.et_chb_4)
    EditText etChb4;
    @BindView(R.id.et_chb_5)
    EditText etChb5;
    @BindView(R.id.et_chb_6)
    EditText etChb6;
    @BindView(R.id.et_cement)
    EditText etCement;
    @BindView(R.id.et_sand)
    EditText etSand;
    @BindView(R.id.et_rsb_10)
    EditText etRsb10;
    @BindView(R.id.et_rsb_12)
    EditText etRsb12;
    @BindView(R.id.et_tie_wire)
    EditText etTieWire;
    @BindView(R.id.btn_save)
    Button btnSave;


    private CHBSummaryPerLocData input;


    @Inject
    CHBLayingMaterialCostContract.UserActionListener presenter;


    public static CHBLayingMaterialCostDialogFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();
        args.putString(PROJECT_ID, projectId);
        args.putString(SCOPE_KEY, scopeKey);
        CHBLayingMaterialCostDialogFragment fragment = new CHBLayingMaterialCostDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.chb_laying_material_cost_layout, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        input = new CHBSummaryPerLocData();

        presenter.setData(getArguments().getString(PROJECT_ID), getArguments().getString(SCOPE_KEY));
        presenter.setView(this);
        presenter.start();

        return rootView;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void setInput(CHBSummaryPerLocData materials) {
        this.input = materials;

        if (input.getChbFourInch() != 0){
            etChb4.setText(DisplayUtil.toDisplayFormat(input.getChbFourInch()));
        }

        if (input.getChbFiveInch() != 0){
            etChb5.setText(DisplayUtil.toDisplayFormat(input.getChbFiveInch()));
        }

        if (input.getChbSixInch() != 0){
            etChb6.setText(DisplayUtil.toDisplayFormat(input.getChbSixInch()));
        }

        if (input.getCement() != 0) {
            etCement.setText(DisplayUtil.toDisplayFormat(input.getCement()));
        }

        if (input.getSand() != 0) {
            etSand.setText(DisplayUtil.toDisplayFormat(input.getSand()));
        }

        if (input.getTenRsb() != 0) {
            etRsb10.setText(DisplayUtil.toDisplayFormat(input.getTenRsb()));
        }

        if (input.getTwelveRsb() != 0) {
            etRsb12.setText(DisplayUtil.toDisplayFormat(input.getTwelveRsb()));
        }

        if (input.getTieWire() != 0) {
            etTieWire.setText(DisplayUtil.toDisplayFormat(input.getTieWire()));
        }
    }

    @Override
    public void inputSaved() {
        dismiss();
    }


    @Override
    public void showSavingNotAllowedError() {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_save)
    public void onSaveClick() {
        String chb4 = etChb4.getText().toString();
        String chb5 = etChb5.getText().toString();
        String chb6 = etChb6.getText().toString();
        String sand = etSand.getText().toString();
        String cement = etCement.getText().toString();
        String rsb10 = etRsb10.getText().toString();
        String rsb12 = etRsb12.getText().toString();
        String tieWire = etTieWire.getText().toString();


        if (chb4.isEmpty()) {
            input.setChbFourInch(0);
        } else {
            input.setChbFourInch(Double.parseDouble(chb4));
        }

        if (chb5.isEmpty()) {
            input.setChbFiveInch(0);
        } else {
            input.setChbFiveInch(Double.parseDouble(chb5));
        }

        if (chb6.isEmpty()) {
            input.setChbSixInch(0);
        } else {
            input.setChbSixInch(Double.parseDouble(chb6));
        }

        if (sand.isEmpty()) {
            input.setSand(0);
        } else {
            input.setSand(Double.parseDouble(sand));
        }

        if (cement.isEmpty()) {
            input.setCement(0);
        } else {
            input.setCement(Double.parseDouble(cement));
        }


        if (rsb10.isEmpty()) {
            input.setTenRsb(0);
        } else {
            input.setTenRsb(Double.parseDouble(rsb10));
        }

        if (rsb12.isEmpty()) {
            input.setTwelveRsb(0);
        } else {
            input.setTwelveRsb(Double.parseDouble(rsb12));
        }

        if (tieWire.isEmpty()) {
            input.setTieWire(0);
        } else {
            input.setTieWire(Double.parseDouble(tieWire));
        }

        presenter.save(input);
    }
}