package com.android.estimateapp.cleanarchitecture.data.entity.works;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class DoorWindowInputBase extends Model {

    String name;

    String code;

    double length;

    double width;

    public DoorWindowInputBase(){

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
