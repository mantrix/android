package com.android.estimateapp.cleanarchitecture.presentation.summary.chb.perstorey;

import android.os.Bundle;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerLocData;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.mantrixengineering.estimateapp.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import io.reactivex.Observable;

public class CHBSummaryPerStoreyFragment extends BaseFragment {

    private static final String PER_LOC_SUMMARY = "summaryPerLocData";

    List<CHBSummaryPerLocData> list = new ArrayList<>();


    public static CHBSummaryPerStoreyFragment newInstance(Map<String, CHBSummaryPerLocData> summaryPerLocMap) {

        Bundle args = new Bundle();

        args.putSerializable(PER_LOC_SUMMARY, (Serializable) summaryPerLocMap);
        CHBSummaryPerStoreyFragment fragment = new CHBSummaryPerStoreyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    SummaryPerStoreyAdapter adapter;

    NpaLinearLayoutManager npaLinearLayoutManager;

    @Override
    protected int getLayoutResource() {
        return R.layout.chb_summary_list;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {

        extractExtras();
        initRecyclerView();
    }

    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setItems(list);
    }


    private void extractExtras() {
        Bundle bundle = getArguments();
        Map<String, CHBSummaryPerLocData> summaryPerLocMap = (Map<String, CHBSummaryPerLocData>) bundle.getSerializable(PER_LOC_SUMMARY);

        for (String key : summaryPerLocMap.keySet()) {
            list.add(summaryPerLocMap.get(key));
        }
    }

}