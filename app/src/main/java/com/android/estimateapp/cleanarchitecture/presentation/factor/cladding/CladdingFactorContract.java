package com.android.estimateapp.cleanarchitecture.presentation.factor.cladding;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface CladdingFactorContract {

    interface View {

        void displayList(List<StoneCladdingFactor> factors);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(List<StoneCladdingFactor> factors);
    }
}
