package com.android.estimateapp.cleanarchitecture.presentation.summary.stonecladding;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding.StoneCladdingCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles.TileWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.tilework.TileWorkSummaryContract;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class StoneCladdingSummaryPresenter extends BasePresenter implements StoneCladdingSummaryContract.UserActionListener {

    private StoneCladdingSummaryContract.View view;
    private ProjectRepository projectRepository;
    private StoneCladdingCalculator stoneCladdingCalculator;
    private String projectId;
    private String scopeKey;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<StoneCladdingFactor> stoneCladdingFactors;
    private List<StoneType> stoneTypes;

    @Inject
    public StoneCladdingSummaryPresenter(ProjectRepository projectRepository,
                                         StoneCladdingCalculator stoneCladdingCalculator,
                                    ThreadExecutorProvider threadExecutorProvider,
                                    PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.stoneCladdingCalculator = stoneCladdingCalculator;
    }

    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(StoneCladdingSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadFactors();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void calculateSummary() {
        compositeDisposable.add(projectRepository.getStoneCladdingInputs(projectId, scopeKey)
                .map(inputs -> {
                    if (!inputs.isEmpty()) {
                        for (int i = 0 ; i < inputs.size() ; i++){
                            inputs.get(i).setStoneType(getStoneType(inputs.get(i).getStoneTypeId()));
                        }
                    }
                    return stoneCladdingCalculator.generateSummary(generateResults(inputs));
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    view.displaySummary(summary);
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    private List<StoneCladdingResult> generateResults(List<StoneCladdingInput> inputs){
        List<StoneCladdingResult> results = new ArrayList<>();

        for (StoneCladdingInput input : inputs){
            StoneCladdingFactor factor = getStoneCladdingFactor(input.getMortarClassEnum());
            results.add(stoneCladdingCalculator.calculate(input,factor));
        }

        return results;
    }


    public void loadFactors(){
        Observable.zip(projectRepository.getStoneCladdingFactors(projectId), projectRepository.getStoneTypeFactors(projectId),
                ((stoneCladdingFactors, stoneTypes) -> new Pair(stoneCladdingFactors,stoneTypes)))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {
                    this.stoneCladdingFactors = (List<StoneCladdingFactor>) pair.first;
                    this.stoneTypes = (List<StoneType>) pair.second;

                    calculateSummary();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private StoneType getStoneType(String key){

        if (key != null && stoneTypes != null){
            for (StoneType stoneType : stoneTypes){
                if (key.equalsIgnoreCase(stoneType.getKey())){
                    return stoneType;
                }
            }
        }
        return null;
    }


    private StoneCladdingFactor getStoneCladdingFactor(Grade grade) {

        if (grade != null) {
            for (StoneCladdingFactor stoneCladdingFactor : stoneCladdingFactors) {
                if (stoneCladdingFactor.getMortarClass() == grade.getId()) {
                    return stoneCladdingFactor;
                }
            }
        }
        return null;
    }

}