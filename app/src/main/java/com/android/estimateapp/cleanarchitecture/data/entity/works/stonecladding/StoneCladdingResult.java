package com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;

public class StoneCladdingResult extends Model {

    @Exclude
    StoneType stoneType;

    double area;

    double netArea;

    @Exclude
    double sizeArea;

    double stoneCladdingPcs;

    double sand;

    double cement;

    double adhesive;

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getNetArea() {
        return netArea;
    }

    public void setNetArea(double netArea) {
        this.netArea = netArea;
    }

    @Exclude
    public StoneType getStoneType() {
        return stoneType;
    }

    public void setStoneType(StoneType stoneType) {
        this.stoneType = stoneType;
    }

    @Exclude
    public double getSizeArea() {
        return sizeArea;
    }

    public void setSizeArea(double sizeArea) {
        this.sizeArea = sizeArea;
    }

    public double getStoneCladdingPcs() {
        return stoneCladdingPcs;
    }

    public void setStoneCladdingPcs(double stoneCladdingPcs) {
        this.stoneCladdingPcs = stoneCladdingPcs;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getAdhesive() {
        return adhesive;
    }

    public void setAdhesive(double adhesive) {
        this.adhesive = adhesive;
    }
}
