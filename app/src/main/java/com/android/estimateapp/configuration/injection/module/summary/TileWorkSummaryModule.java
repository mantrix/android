package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles.TileWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles.TileWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.tilework.TileWorkSummaryContract;
import com.android.estimateapp.cleanarchitecture.presentation.summary.tilework.TileWorkSummaryPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class TileWorkSummaryModule {

    @Provides
    @PerFragment
    TileWorkCalculator provideTileWorkCalculator(TileWorkCalculatorImpl calculator){
        return calculator;
    }

    @Provides
    @PerFragment
    TileWorkSummaryContract.UserActionListener provideTileSummaryPresenter(TileWorkSummaryPresenter presenter) {
        return presenter;
    }
}
