package com.android.estimateapp.cleanarchitecture.data.entity.works.conretework;

import java.util.List;

public class ConcreteWorks {

    private List<ConcreteWorks> concreteWorks;

    public List<ConcreteWorks> getConcreteWorks() {
        return concreteWorks;
    }

    public void setConcreteWorks(List<ConcreteWorks> concreteWorks) {
        this.concreteWorks = concreteWorks;
    }
}
