package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class CHBReinforcementFactorFragment extends BaseFragment implements CHBReinforcementFactorContract.View {

    public static CHBReinforcementFactorFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();
        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        CHBReinforcementFactorFragment fragment = new CHBReinforcementFactorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    CHBReinforcementAdapter adapter;

    @Inject
    CHBReinforcementFactorContract.UserActionListener presenter;

    NpaLinearLayoutManager npaLinearLayoutManager;

    private ProgressDialog saveProgressDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.chb_reinforcement_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        initRecyclerView();

        presenter.setData(getArguments().getString(Constants.PROJECT_ID), getArguments().getString(Constants.SCOPE_KEY));
        presenter.setView(this);
        presenter.start();

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

    }


    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void displayList(List<CHBReinforcementFactorContract.ReinforcementItem> factors) {
        adapter.setItems(factors);
    }

    @OnClick(R.id.rl_save)
    public void onSaveClick() {
        if (saveProgressDialog != null) {
            saveProgressDialog.show();
        }
        presenter.updateFactors(adapter.getData());
    }

    @Override
    public void factorSaved() {
        if (saveProgressDialog != null) {
            saveProgressDialog.dismiss();
        }
    }

}