package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Stirrups;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Rebar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyLength;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar.ColumnRebarWorkCalculatorImpl;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class ColumnRebarComputationTest extends BaseTest {
    ColumnRebarWorkCalculatorImpl columnRebarWorkCalculator;

    @Before
    public void setup() {
        columnRebarWorkCalculator = new ColumnRebarWorkCalculatorImpl();
        initRSBSizes();
    }

    @Test
    public void columnRebarTest() {

        ColumnRebarInput columnRebarInput = new ColumnRebarInput();
        ColumnRebarProperties properties = new ColumnRebarProperties();

        columnRebarInput.setLengthCC(1.25);
        columnRebarInput.setWastage(10);

        properties.setBase(0.38);
        properties.setHeight(0.38);

        Rebar rebar1 = new Rebar();
        rebar1.setQuantity(6);
        rebar1.setBarSize(sixteenMM);

        QtyLength qtyLengthAtFooting = new QtyLength();
        qtyLengthAtFooting.setLength(0.5);
        qtyLengthAtFooting.setQuantity(6);

        rebar1.setRebarBendAtFooting(qtyLengthAtFooting);

        Stirrups stirrups = new Stirrups();

        QtyDiameterSpacing A = new QtyDiameterSpacing(2,0.05);
        QtyDiameterSpacing theRest = new QtyDiameterSpacing(0,0.1);
        QtyDiameterSpacing G = new QtyDiameterSpacing();
        G.setDiameter(tenMM.getSize());
        G.setLength(1.2);


        stirrups.setColumnTieA(A);
        stirrups.setColumnTieTheRest(theRest);
        stirrups.setColumnTieG(G);

        stirrups.setData1(0.05);
        stirrups.setData2(2);

        properties.setStirrups(stirrups);
        properties.setRebar1(rebar1);

        ColumnRebarOutput result = columnRebarWorkCalculator.calculate(false,columnRebarInput,properties, Arrays.asList(tenMM,twelveMM, sixteenMM, twentyMM));

        System.out.println("L Clear: " + result.getlClear());
        System.out.println("Rebar 1: " + result.getRebar1().getRsbFactor().getSize() + "  = " + result.getRebar1().getValue());
        System.out.println("Rebar Bend At Footing: " + result.getRebarBendAtFooting1().getRsbFactor().getSize() + "  = " + result.getRebarBendAtFooting1().getValue());
        System.out.println("Qty: " + result.getStirrupsOutput().getQuantity());
        System.out.println("Length: " + result.getStirrupsOutput().getLength());
        System.out.println("10 mm rsb: " + result.getStirrupsOutput().getTenMM());
        System.out.println("12 mm rsb: " + result.getStirrupsOutput().getTwelveMM());
        System.out.println("Total: " + result.getStirrupsOutput().getTotal());
    }
}
