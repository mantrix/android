package com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.list;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.list.PlasteringListContract;

import javax.inject.Inject;

public class TileWorkListPresenter  extends BasePresenter implements TileWorkListContract.UserActionListener {


    private TileWorkListContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeOfWorkId;

    @Inject
    public TileWorkListPresenter(ProjectRepository projectRepository,
                                   ThreadExecutorProvider threadExecutorProvider, PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }

    @Override
    public void setData(String projectId, String scopeOfWorkId) {
        this.projectId = projectId;
        this.scopeOfWorkId = scopeOfWorkId;
    }

    @Override
    public void setView(TileWorkListContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadInputs();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void reloadList() {
        loadInputs();
    }

    private void loadInputs(){
        projectRepository.getTileWorkInputs(projectId,scopeOfWorkId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(tileWorkInputs -> {
                    if (!tileWorkInputs.isEmpty()){
                        view.displayList(tileWorkInputs);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void deleteItem(String inputId) {
        projectRepository.deleteScopeInput(projectId, scopeOfWorkId, inputId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(() -> {
                    view.removeItem(inputId);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

}