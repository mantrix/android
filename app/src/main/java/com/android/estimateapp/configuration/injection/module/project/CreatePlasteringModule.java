package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering.PlasteringWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering.PlasteringWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create.CreatePlasteringContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create.CreatePlasteringPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreatePlasteringModule {

    @Provides
    @PerActivity
    CreatePlasteringContract.UserActionListener provideCreatePlasteringPresenter(CreatePlasteringPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    PlasteringWorkCalculator providePlasteringWorkCalculator(PlasteringWorkCalculatorImpl calculator){
        return calculator;
    }
}
