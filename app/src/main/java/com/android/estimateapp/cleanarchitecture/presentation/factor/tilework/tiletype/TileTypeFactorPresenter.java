package com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.tiletype;

import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class TileTypeFactorPresenter extends BasePresenter implements TileTypeFactorContract.UserActionListener {

    private TileTypeFactorContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeKey;

    @Inject
    public TileTypeFactorPresenter(ProjectRepository projectRepository,
                                   ThreadExecutorProvider threadExecutorProvider,
                                   PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }


    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(TileTypeFactorContract.View view) {
        this.view = view;
    }

    @Override
    public void updateFactors(List<TileType> factors) {
        projectRepository.updateTileTypeFactors(projectId, factors)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(updatedRsbFactors -> {
                    view.factorSaved();
                }, throwable -> {
                    view.factorSaved();
                    throwable.printStackTrace();
                });
    }


    @Override
    public void start() {
        projectRepository.getTileTypeFactors(projectId )
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(factors -> {
                    view.displayList(factors);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void createTileType(String name, double w1, double w2) {
        TileType tileType = new TileType();
        tileType.setName(name);
        tileType.setLength(w1);
        tileType.setWidth(w2);

        projectRepository.createTileTypeFactor(projectId, tileType)
                .flatMap(tileTypeResult -> projectRepository.getTileTypeFactors(projectId))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(factors -> {
                    /*
                     * Display updated Tile Types
                     */
                    view.displayList(factors);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}