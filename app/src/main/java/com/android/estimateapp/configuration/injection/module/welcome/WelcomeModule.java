package com.android.estimateapp.configuration.injection.module.welcome;


import com.android.estimateapp.cleanarchitecture.presentation.welcome.WelcomeActivity;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.WelcomePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.contract.WelcomeContract;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class WelcomeModule {

    @Provides
    @PerActivity
    WelcomeContract.View provideWelcomeView(WelcomeActivity welcomeActivity){
        return welcomeActivity;
    }

    @Provides
    @PerActivity
    WelcomeContract.UserActionListener providesWelcomePresenter(WelcomePresenter presenter) {
        return presenter;
    }
}
