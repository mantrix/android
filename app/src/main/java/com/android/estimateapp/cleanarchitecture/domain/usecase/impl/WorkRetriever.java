package com.android.estimateapp.cleanarchitecture.domain.usecase.impl;

import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.BaseUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.WorkRetrieverUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class WorkRetriever extends BaseUseCase implements WorkRetrieverUseCase {

    @Inject
    public WorkRetriever(){

    }

    @Override
    public Observable<List<Work>> getProjectScopes(String projectId) {
        return null;
    }
}
