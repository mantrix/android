package com.android.estimateapp.configuration.injection.module.cost;


import com.android.estimateapp.cleanarchitecture.presentation.cost.material.tiles.TileWorkMaterialDialogFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TileWorkBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {TileWorkMaterialModule.class})
    abstract TileWorkMaterialDialogFragment bindTileWorkMaterialDialogFragment();
}
