package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum RsbSize {

    TEN_MM(10),
    TWELVE_MM(12),
    SIXTEEN_MM(16),
    TWENTY_MM(20),
    TWENTY_TWO_MM(22),
    TWENTY_FIVE_MM(25),
    TWENTY_EIGHT_MM(28),
    THIRTY_TWO_MM(32),
    THIRTY_SIX_MM(36);

    int size;
    String name;

    RsbSize(int size) {
        this.size = size;
    }

    public int getValue() {
        return size;
    }


    public static RsbSize parseInt(int value){
        for (RsbSize size : getRsbSizes()){
            if (value == size.getValue()){
                return size;
            }
        }
        throw new Error(value + " not supported as RSBFactor Size!");
    }

    @Override
    public String toString() {
        return Integer.toString(size);
    }

    public static List<RsbSize> getRsbSizes(){
        List<RsbSize> rsbSizes = new ArrayList<>();
        rsbSizes.add(TEN_MM);
        rsbSizes.add(TWELVE_MM);
        rsbSizes.add(SIXTEEN_MM);
        rsbSizes.add(TWENTY_MM);
        rsbSizes.add(TWENTY_TWO_MM);
        rsbSizes.add(TWENTY_FIVE_MM);
        rsbSizes.add(TWENTY_EIGHT_MM);
        rsbSizes.add(THIRTY_TWO_MM);
        rsbSizes.add(THIRTY_SIX_MM);
        return rsbSizes;
    }
}
