package com.android.estimateapp.cleanarchitecture.presentation.factor;

import android.content.Context;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.mortarclass.CHBConcreteClassFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement.CHBReinforcementFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.tiewire.CHBTieWireFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.StoneCladdingFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.stonetype.StoneTypeFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.concrete.ConcreteClassFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.plastering.PlasteringMortarFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.shared.rsb.RSBFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.TileFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.tiletype.TileTypeFactorFragment;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class FactorPagerAdapter extends FragmentPagerAdapter {

    private ScopeOfWork scopeOfWork;
    private List<Fragment> fragments;
    private List<String> titles;
    private Context context;
    private String projectId;
    private String scopeKey;

    public FactorPagerAdapter(Context context, FragmentManager fm, ScopeOfWork scopeOfWork, String projectId, String scopeKey) {
        super(fm);
        this.scopeOfWork = scopeOfWork;
        this.context = context;
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        titles = new ArrayList<>();
        fragments = factorFragments();

    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    private List<Fragment> factorFragments() {
        List<Fragment> fragments = new ArrayList<>();
        switch (scopeOfWork) {
            case CHB_LAYING:
                titles.add(context.getString(R.string.rsb));
                titles.add(context.getString(R.string.mortar_class));
                titles.add(context.getString(R.string.reinforcements));
                titles.add(context.getString(R.string.tie_wire));
                fragments.add(RSBFactorFragment.newInstance(projectId));
                fragments.add(CHBConcreteClassFactorFragment.newInstance(projectId, scopeKey));
                fragments.add(CHBReinforcementFactorFragment.newInstance(projectId,scopeKey));
                fragments.add(CHBTieWireFactorFragment.newInstance(projectId,scopeKey));
                break;
            case CHB_PLASTERING:
                titles.add(context.getString(R.string.plastering_mortar));
                fragments.add(PlasteringMortarFactorFragment.newInstance(projectId, scopeKey));
                break;
            case CONCRETE_WORKS:
            case BEAM_CONCRETE:
            case COLUMN_CONCRETE:
                titles.add(context.getString(R.string.concrete_class));
                fragments.add(ConcreteClassFactorFragment.newInstance(projectId, scopeKey));
                break;
            case TILE_WORKS:
                titles.add(context.getString(R.string.tile_work));
                titles.add(context.getString(R.string.tile_type));
                fragments.add(TileFactorFragment.newInstance(projectId, scopeKey));
                fragments.add(TileTypeFactorFragment.newInstance(projectId, scopeKey));
                break;
            case STONE_CLADDING:
                titles.add(context.getString(R.string.stone_cladding));
                titles.add(context.getString(R.string.stone_type));
                fragments.add(StoneCladdingFactorFragment.newInstance(projectId, scopeKey));
                fragments.add(StoneTypeFactorFragment.newInstance(projectId, scopeKey));
                break;
            case WALL_FOOTING_REBAR:
            case COLUMN_FOOTING_REBAR:
            case BEAM_REBAR:
            case COLUMN_REBAR:
                titles.add(context.getString(R.string.rsb));
                fragments.add(RSBFactorFragment.newInstance(projectId));
                break;
        }

        return fragments;

    }
}
