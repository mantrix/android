package com.android.estimateapp.cleanarchitecture.presentation.cost.labor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.add.AddLaborDialogFragment;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create.CreatePlasteringActivity;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserDialogFragment;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LaborCostActivity extends BaseActivity implements LaborCostInputContract.View, ListItemClickLister<WorkerGroup> {


    private final int REQUEST_CODE = 1011;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    LaborCostInputContract.UserActionListener presenter;

    @Inject
    LaborCostAdapter adapter;

    String projectId;
    String scopeKey;

    NpaLinearLayoutManager linearLayoutManager;

    public static Intent createIntent(Context context, String projectId, String scopeKey) {
        Intent intent = new Intent(context, LaborCostActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.labor_cost_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(getString(R.string.label_labor));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        adapter.setContext(this);
        adapter.setListItemClickLister(this);
        initRecyclerView();

        presenter.setView(this);
        presenter.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        presenter.setData(projectId, scopeKey);
    }


    @Override
    public void itemClick(WorkerGroup group) {
        AddLaborDialogFragment createUserDialogFragment = AddLaborDialogFragment.newInstance(projectId,scopeKey, group.getKey());
        FragmentManager fragmentManager = getSupportFragmentManager();
        createUserDialogFragment.show(fragmentManager, CreateUserDialogFragment.TAG);
    }

    @Override
    public void onLongClick(WorkerGroup input) {
        AlertDialog alertDialog = createAlertDialog(getString(R.string.delete),
                getString(R.string.delete_input_msg),
                getString(R.string.delete),
                getString(R.string.cancel), (dialog, which) -> {
                    presenter.deleteItem(input.getKey());
                    dialog.dismiss();
                }, (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }



        @Override
    public void removeItem(String inputId) {
        adapter.deleteItem(inputId);
    }


    @Override
    public void displayList(List<WorkerGroup> inputs) {
        adapter.setItems(inputs);
    }

    private void initRecyclerView() {
        linearLayoutManager = new NpaLinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    @OnClick(R.id.fab)
    public void addItemClick() {
        AddLaborDialogFragment createUserDialogFragment = AddLaborDialogFragment.newInstance(projectId,scopeKey,null);
        FragmentManager fragmentManager = getSupportFragmentManager();
        createUserDialogFragment.show(fragmentManager, CreateUserDialogFragment.TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    public void reload(){
        presenter.reloadList();
    }

    //    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == Constants.REQUEST_CODE) {
//            switch (resultCode) {
//                case Constants.RESULT_CONTENT_UPDATE:
//                    presenter.reloadList();
//                    break;
//            }
//        }
//    }
}
