package com.android.estimateapp.configuration;

import com.mantrixengineering.estimateapp.BuildConfig;

public class AppConfig {

    public static String getBaseUrl() {
        return BuildConfig.BASE_URL;
    }

}
