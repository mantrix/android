package com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.create;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding.StoneCladdingCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class CreateStoneCladdingPresenter extends CreateScopeInputBasePresenter implements CreateStoneCladdingContract.UserActionListener {

    private CreateStoneCladdingContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;
    private StoneCladdingCalculator stoneCladdingCalculator;


    private List<StoneCladdingFactor> stoneCladdingFactors;
    private List<StoneType> stoneTypes;
    private StoneCladdingInput input;
    private StoneCladdingResult result;

    @Inject
    public CreateStoneCladdingPresenter(ProjectRepository projectRepository,
                                        AccountSubscriptionUseCase accountSubscriptionUseCase,
                                        StoneCladdingCalculator stoneCladdingCalculator,
                                        ThreadExecutorProvider threadExecutorProvider,
                                        PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase,threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.stoneCladdingCalculator = stoneCladdingCalculator;

        stoneCladdingFactors = new ArrayList<>();
    }

    @Override
    public void setView(CreateStoneCladdingContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        setPickerOptions();
        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {
        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateStoneCladdingInput(projectId, scopeKey, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createStoneCladdingInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<StoneCladdingInput> inputObservable) {

        view.showSavingDialog();

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    view.inputSaved();
                    saveResult();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    private void saveResult(){
        if (result != null && itemKey != null) {
            projectRepository.saveStoneCladdingInputResult(projectId, scopeKey, itemKey,result)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(result -> {
                        this.result = result;
                    }, throwable -> {
                        throwable.printStackTrace();
                    });
        }
    }

    @Override
    public void calculate(StoneCladdingInput input) {
        StoneCladdingFactor factor = getStoneCladdingFactor(input.getMortarClassEnum());
        StoneType stoneType = getStoneType(input.getStoneTypeId());
        input.setStoneType(stoneType);

        result = stoneCladdingCalculator.calculate(input, factor);

        this.input = input;

        view.displayResult(result);
    }



    private StoneCladdingFactor getStoneCladdingFactor(Grade grade) {

        if (grade != null) {
            for (StoneCladdingFactor stoneCladdingFactor : stoneCladdingFactors) {
                if (stoneCladdingFactor.getMortarClass() == grade.getId()) {
                    return stoneCladdingFactor;
                }
            }
        }
        return null;
    }

    private void setPickerOptions() {
        view.setMortarClassPickerOptions(Arrays.asList(Grade.A, Grade.B, Grade.C));

    }

    private void loadStoneWorkInput() {
        projectRepository.getStoneCladdingInput(projectId, scopeKey, itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {

                    input.setStoneType(getStoneType(input.getStoneTypeId()));

                    this.input = input;

                    view.setInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate) {


        Observable.zip(projectRepository.getStoneCladdingFactors(projectId), projectRepository.getStoneTypeFactors(projectId),
                ((stoneCladdingFactors, stoneTypes) -> new Pair(stoneCladdingFactors,stoneTypes)))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {
                    this.stoneCladdingFactors = (List<StoneCladdingFactor>) pair.first;
                    this.stoneTypes = (List<StoneType>) pair.second;

                    if (!stoneTypes.isEmpty()){
                        view.setStoneTypes(stoneTypes);
                    }



                    if (!factorUpdate) {
                        view.enabledCalculateBtn(true);

                        if (itemKey != null) {
                            loadStoneWorkInput();
                        }
                    } else {
                        /*
                         * Factors were updated hence input data and ui should be be updated.
                         */
                        if (itemKey != null){
                            input.setStoneType(getStoneType(input.getStoneTypeId()));

                            view.setInput(input);

                            view.requestForReCalculation();
                        }
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }

    private StoneType getStoneType(String key){

        if (key != null && stoneTypes != null){
            for (StoneType stoneType : stoneTypes){
                if (key.equalsIgnoreCase(stoneType.getKey())){
                    return stoneType;
                }
            }
        }
        return null;
    }

}
