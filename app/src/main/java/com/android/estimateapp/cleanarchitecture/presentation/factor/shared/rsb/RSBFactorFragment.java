package com.android.estimateapp.cleanarchitecture.presentation.factor.shared.rsb;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class RSBFactorFragment extends BaseFragment implements RSBFactorContract.View {

    public static RSBFactorFragment newInstance(String projectId) {

        Bundle args = new Bundle();
        args.putString(Constants.PROJECT_ID,projectId);
        RSBFactorFragment fragment = new RSBFactorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Inject
    RSBFactorAdapter adapter;

    @Inject
    RSBFactorContract.UserActionListener presenter;

    NpaLinearLayoutManager npaLinearLayoutManager;


    private ProgressDialog saveProgressDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.rsb_factor_fragment;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {

        initRecyclerView();

        presenter.setData(getArguments().getString(Constants.PROJECT_ID));
        presenter.setView(this);
        presenter.start();

        saveProgressDialog = createLoadingAlert(null,getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

    }

    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void displayList(List<RSBFactor> rsbFactors) {
        adapter.setItems(rsbFactors);
    }

    @OnClick(R.id.rl_save)
    public void onSaveClick(){
        if (saveProgressDialog != null) {
            saveProgressDialog.show();
        }
        presenter.updateFactors(adapter.getData());
    }

    @Override
    public void factorSaved() {
        if (saveProgressDialog != null){
            saveProgressDialog.dismiss();
        }
    }
}
