package com.android.estimateapp.cleanarchitecture.domain.usecase.factory;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Shape;

import java.util.ArrayList;
import java.util.List;

public class OptionFactory {

    public static List<Shape> getSupportedShapes(Category category){
        List<Shape> shapes = new ArrayList<>();
        switch (category){
            case COLUMN_FOOTING:
                shapes.add(Shape.RECTANGLE);
                shapes.add(Shape.SQUARE);
                shapes.add(Shape.TRAPEZOID);
                break;
            case COLUMN:
                shapes.add(Shape.RECTANGLE);
                shapes.add(Shape.SQUARE);
                shapes.add(Shape.CIRCLE);
                break;
            case WALL_FOOTING:
            case TIE_BEAM:
            case GRADE_BEAM:
                shapes.add(Shape.RECTANGLE);
                break;
            case BEAM:
                shapes.add(Shape.RECTANGLE);
                shapes.add(Shape.TAPPERED);
                break;
            case PILES:
                shapes.add(Shape.CIRCLE);
                shapes.add(Shape.RECTANGLE);
                break;
        }

        return shapes;
    }

    public static List<Category> getSupportedWorkCategory(ScopeOfWork scopeOfWork){
        List<Category> categories  = new ArrayList<>();

        switch (scopeOfWork){
            case EARTH_WORKS:
                categories = getEarthWorkCategory();
                break;
            case CONCRETE_WORKS:
                categories = getConcreteWorkCategory();
                break;
            case REBAR_WORKS:
                categories = getRebarWorkCategory();
                break;
            case FORM_WORKS:
                categories = getFormWorkCategory();
                break;
            case SCAFFOLDINGS:
                categories = getScaffoldingsCategory();
                break;
            case TILE_WORKS:
                categories = getTileWorkCategory();
                break;
        }

        return categories;
    }


    private static List<Category> getEarthWorkCategory(){
        List<Category> categories  = new ArrayList<>();
        categories.add(Category.EXCAVATION);
        categories.add(Category.BACKFILL);
        categories.add(Category.GRAVEL_BEDDING);
        return categories;
    }

    private static List<Category> getConcreteWorkCategory(){
        List<Category> categories  = new ArrayList<>();
        categories.add(Category.COLUMN_FOOTING);
        categories.add(Category.WALL_FOOTING);
        categories.add(Category.COLUMN);
        categories.add(Category.TIE_BEAM);
        categories.add(Category.GRADE_BEAM);
        categories.add(Category.BEAM);
        categories.add(Category.SLAB_ON_GRADE);
        categories.add(Category.SUSPENDED_SLAB);
        categories.add(Category.STAIR);
        categories.add(Category.SHEAR_WALL);
        categories.add(Category.PLAIN_CONCRETE);
        categories.add(Category.PILES);
        return categories;
    }

    private static List<Category> getRebarWorkCategory(){
        List<Category> categories  = new ArrayList<>();
        categories.add(Category.COLUMN_FOOTING);
        categories.add(Category.WALL_FOOTING);
        categories.add(Category.COLUMN);
        categories.add(Category.TIE_BEAM);
        categories.add(Category.GRADE_BEAM);
        categories.add(Category.BEAM);
        categories.add(Category.SLAB_ON_GRADE);
        categories.add(Category.SUSPENDED_SLAB);
        categories.add(Category.STAIR);
        categories.add(Category.SHEAR_WALL);
        return categories;
    }

    private static List<Category> getFormWorkCategory(){
        List<Category> categories  = new ArrayList<>();
        categories.add(Category.COLUMN_FOOTING);
        categories.add(Category.COLUMN);
        categories.add(Category.TIE_BEAM);
        categories.add(Category.GRADE_BEAM);
        categories.add(Category.BEAM);
        categories.add(Category.SLAB_ON_GRADE);
        categories.add(Category.SUSPENDED_SLAB);
        categories.add(Category.STAIR);
        categories.add(Category.SHEAR_WALL);
        return categories;
    }

    private static List<Category> getScaffoldingsCategory(){
        List<Category> categories  = new ArrayList<>();
        categories.add(Category.COLUMN);
        categories.add(Category.BEAM);
        categories.add(Category.SUSPENDED_SLAB);
        categories.add(Category.STAIR);
        return categories;
    }

    private static List<Category> getTileWorkCategory(){
        List<Category> categories  = new ArrayList<>();
        categories.add(Category.FLOOR_TILES);
        categories.add(Category.WALL_TILES);
        categories.add(Category.SINK_TILES);
        return categories;
    }
}
