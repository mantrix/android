package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.stonetype.StoneTypeFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.stonetype.StoneTypeFactorPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class StoneTypeFactorModule {

    @Provides
    @PerFragment
    StoneTypeFactorContract.UserActionListener providesStoneTypeFactorPresenter(StoneTypeFactorPresenter presenter) {
        return presenter;
    }
}
