package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.columnwallfooting.ColumnAndWallFootingMaterialCostContract;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.columnwallfooting.ColumnAndWallFootingMaterialCostPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class ColumnAndWallFootingMaterialCostModule {

    @Provides
    @PerFragment
    ColumnAndWallFootingMaterialCostContract.UserActionListener provideColumnAndWallFootingMaterialCostPresenter(ColumnAndWallFootingMaterialCostPresenter presenter) {
        return presenter;
    }
}
