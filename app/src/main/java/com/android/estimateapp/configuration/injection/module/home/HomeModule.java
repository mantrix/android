package com.android.estimateapp.configuration.injection.module.home;

import android.app.Activity;

import com.android.estimateapp.cleanarchitecture.presentation.home.HomeActivity;
import com.android.estimateapp.cleanarchitecture.presentation.home.HomeContract;
import com.android.estimateapp.cleanarchitecture.presentation.home.HomePresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

    @Provides
    @PerActivity
    Activity provideHomeActivity(HomeActivity homeActivity) {
        return homeActivity;
    }

    @Provides
    @PerActivity
    HomeContract.UserActionListener provideHomePresenter(HomePresenter presenter) {
        return presenter;
    }
}
