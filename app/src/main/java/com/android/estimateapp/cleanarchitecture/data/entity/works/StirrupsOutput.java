package com.android.estimateapp.cleanarchitecture.data.entity.works;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class StirrupsOutput extends Model {

    double quantity;

    double length;

    double total;

    int selectedRsb;

    double totalWeight;

    public StirrupsOutput(){

    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getRSBTotalWeight(){
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public int getSelectedRsb() {
        return selectedRsb;
    }

    public void setSelectedRsb(int selectedRsb) {
        this.selectedRsb = selectedRsb;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
