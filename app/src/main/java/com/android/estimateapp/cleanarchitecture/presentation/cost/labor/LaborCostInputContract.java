package com.android.estimateapp.cleanarchitecture.presentation.cost.labor;

import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface LaborCostInputContract {


    interface View {
        void displayList(List<WorkerGroup> groups);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void reloadList();

        void deleteItem(String inputId);
    }
}
