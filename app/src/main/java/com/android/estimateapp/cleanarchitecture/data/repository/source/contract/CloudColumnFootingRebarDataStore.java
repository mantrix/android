package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;

import java.util.List;

import io.reactivex.Observable;

public interface CloudColumnFootingRebarDataStore {

    Observable<ColumnFootingRebarInput> createColumnFootingInput(String projectId, String scopeOfWorkId, ColumnFootingRebarInput input);

    Observable<ColumnFootingRebarInput> updateColumnFootingInput(String projectId, String scopeOfWorkId, String itemId, ColumnFootingRebarInput input);

    Observable<List<ColumnFootingRebarInput>> getColumnFootingInputs(String projectId, String scopeOfWorkId);

    Observable<ColumnFootingRebarInput> getColumnFootingInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<ColumnFootingRebarResult> saveColumnFootingInputResult(String projectId, String scopeOfWorkId, String itemId, ColumnFootingRebarResult result);

}
