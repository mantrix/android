package com.android.estimateapp.configuration.injection.module.properties;

import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.create.CreateBeamSectionPropertyContract;
import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.create.CreateBeamSectionPropertyPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateBeamPropertyModule {


    @Provides
    @PerActivity
    CreateBeamSectionPropertyContract.UserActionListener provideCreateBeamSectionPresenter(CreateBeamSectionPropertyPresenter presenter) {
        return presenter;
    }
}
