package com.android.estimateapp.cleanarchitecture.presentation.cost.material.concrete;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ConcreteWorkMaterials;

public interface GeneralConcreteMaterialContract {

    interface View {

        void setInput(ConcreteWorkMaterials materials);

        void inputSaved();

        void showSavingNotAllowedError();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void save(ConcreteWorkMaterials materials);
    }
}
