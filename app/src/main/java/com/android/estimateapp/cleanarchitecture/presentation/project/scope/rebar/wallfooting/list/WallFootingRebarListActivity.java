package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.LaborCostActivity;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.columnwallfooting.ColumnAndWallFootingMaterialCostDialogFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.create.CreateWallFootingInputActivity;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryActivity;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class WallFootingRebarListActivity extends BaseActivity implements WallFootingRebarListContract.View, ListItemClickLister<WallFootingRebarInput> {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    WallFootingRebarListContract.UserActionListener presenter;

    @Inject
    WallFootingRebarListAdapter adapter;

    String projectId;
    String scopeKey;

    NpaLinearLayoutManager linearLayoutManager;

    public static Intent createIntent(Context context, String projectId, String scopeKey) {
        Intent intent = new Intent(context, WallFootingRebarListActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.scope_list_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.WALL_FOOTING_REBAR.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        initRecyclerView();

        presenter.setView(this);
        presenter.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.work_list_w_cost, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.WALL_FOOTING_REBAR.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            case R.id.menu_summary:
                startActivityForResult(SummaryActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.WALL_FOOTING_REBAR.getId()), Constants.REQUEST_CODE);
                break;
            case R.id.menu_labor:
                startActivityForResult(LaborCostActivity.createIntent(this,projectId,scopeKey), Constants.REQUEST_CODE);
                break;
            case R.id.menu_material:
                ColumnAndWallFootingMaterialCostDialogFragment dialogFragment = ColumnAndWallFootingMaterialCostDialogFragment.newInstance(projectId,scopeKey);
                FragmentManager fragmentManager = getSupportFragmentManager();
                dialogFragment.show(fragmentManager, "");
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        presenter.setData(projectId, scopeKey);
    }


    @Override
    public void itemClick(WallFootingRebarInput input) {
        Intent intent = CreateWallFootingInputActivity.createIntent(this, projectId, scopeKey, input.getKey());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onLongClick(WallFootingRebarInput input) {
        AlertDialog alertDialog = createAlertDialog(getString(R.string.delete),
                getString(R.string.delete_input_msg),
                getString(R.string.delete),
                getString(R.string.cancel), (dialog, which) -> {
                    presenter.deleteItem(input.getKey());
                    dialog.dismiss();
                }, (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    @Override
    public void removeItem(String inputId) {
        adapter.deleteItem(inputId);
    }

    @Override
    public void displayList(List<WallFootingRebarInput> inputs) {
        adapter.setItems(inputs);
    }

    private void initRecyclerView() {
        linearLayoutManager = new NpaLinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setListItemClickLister(this);
    }


    @OnClick(R.id.fab)
    public void addItemClick() {
        Intent intent = CreateWallFootingInputActivity.createIntent(this, projectId, scopeKey, null);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_CONTENT_UPDATE:
                    presenter.reloadList();
                    break;
            }
        }
    }
}