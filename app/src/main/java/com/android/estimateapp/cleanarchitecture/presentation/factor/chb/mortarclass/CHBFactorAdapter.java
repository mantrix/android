package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.mortarclass;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSizeFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.EdittextBaseOutputListener;
import com.android.estimateapp.utils.DisplayUtil;
import com.android.estimateapp.utils.NumberUtils;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CHBFactorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<CHBMortarClass>> {


    private List<CHBMortarClass> list;

    @Inject
    public CHBFactorAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.chb_factor_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<CHBMortarClass> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<CHBMortarClass> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    public List<CHBMortarClass> getData() {
        return list;
    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_concrete_class)
        TextView tvMortarClass;

        @BindView(R.id.et_4_inch_cement)
        EditText et4InchCement;

        @BindView(R.id.et_5_inch_cement)
        EditText et5InchCement;

        @BindView(R.id.et_6_inch_cement)
        EditText et6InchCement;

        @BindView(R.id.et_4_inch_sand)
        EditText et4InchSand;

        @BindView(R.id.et_5_inch_sand)
        EditText et5InchSand;

        @BindView(R.id.et_6_inch_sand)
        EditText et6InchSand;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(CHBMortarClass factor, int position) {

            Grade grade = Grade.parseInt(factor.getMortarClass());
            tvMortarClass.setText(grade.toString());
            List<EditText> etCements = new ArrayList<>();
            etCements.add(et4InchCement);
            etCements.add(et5InchCement);
            etCements.add(et6InchCement);

            List<EditText> etSands = new ArrayList<>();
            etSands.add(et4InchSand);
            etSands.add(et5InchSand);
            etSands.add(et6InchSand);

            List<CHBSizeFactor> chbSizeFactors = factor.getChbSizeFactors();
            for (int i = 0 ; i < chbSizeFactors.size() ; i++){
                final int lisPos = i;

                CHBSizeFactor chbSizeFactor = chbSizeFactors.get(i);
                etCements.get(i).setText(DisplayUtil.toPlainString(chbSizeFactor.getCement()));

                etCements.get(i).addTextChangedListener(new EdittextBaseOutputListener() {
                    @Override
                    public void afterTextChanged(Editable s) {

                        String input = etCements.get(lisPos).getText().toString();

                        if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                            for (int j = 0 ; j < chbSizeFactors.size() ; j++){
                                if (lisPos == j){
                                    list.get(position).getChbSizeFactors().get(j).setCement(Double.parseDouble(input));
                                    break;
                                }
                            }
                        }
                    }
                });

                etSands.get(i).setText(DisplayUtil.toPlainString(chbSizeFactor.getSand()));

                etSands.get(i).addTextChangedListener(new EdittextBaseOutputListener() {
                    @Override
                    public void afterTextChanged(Editable s) {

                        String input = etSands.get(lisPos).getText().toString();

                        if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                            for (int j = 0 ; j < chbSizeFactors.size() ; j++){
                                if (lisPos == j){
                                    list.get(position).getChbSizeFactors().get(j).setSand(Double.parseDouble(input));
                                    break;
                                }
                            }
                        }
                    }
                });
            }




        }
    }
}