package com.android.estimateapp.cleanarchitecture.presentation.base;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;

import java.text.DecimalFormat;

public class BasePresenter {

    protected ThreadExecutorProvider threadExecutorProvider;
    protected PostExecutionThread postExecutionThread;

    public BasePresenter(ThreadExecutorProvider threadExecutorProvider,
                         PostExecutionThread postExecutionThread) {
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }
}
