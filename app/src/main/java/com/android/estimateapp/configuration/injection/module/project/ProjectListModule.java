package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.ProjectRetrieverUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.impl.ProjectRetrieverV2;
import com.android.estimateapp.cleanarchitecture.presentation.project.listing.ProjectListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.listing.ProjectListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class ProjectListModule {

    @Provides
    @PerFragment
    ProjectListContract.UserActionListener provideProjectListPresenter(ProjectListPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerFragment
    ProjectRetrieverUseCase provideProjectRetrieverUseCase(ProjectRetrieverV2 projectRetriever){
        return projectRetriever;
    }
}
