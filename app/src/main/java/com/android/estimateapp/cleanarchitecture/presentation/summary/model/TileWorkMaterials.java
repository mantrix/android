package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.google.firebase.database.Exclude;


public class TileWorkMaterials extends Model {

    public TileType tileType;

    @Exclude
    public double area;

    @Exclude
    public double tiles;

    public double cement;

    public double adhesive;

    public double sand;

    public TileWorkMaterials(){
    }

    @Exclude
    public TileType getTileType() {
        return tileType;
    }

    public void setTileType(TileType tileType) {
        this.tileType = tileType;
    }

    @Exclude
    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    @Exclude
    public double getTiles() {
        return tiles;
    }

    public void setTiles(double tiles) {
        this.tiles = tiles;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getAdhesive() {
        return adhesive;
    }

    public void setAdhesive(double adhesive) {
        this.adhesive = adhesive;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }
}
