package com.android.estimateapp.cleanarchitecture.domain.usecase.impl;

import android.util.Log;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.BaseUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.CreateProjectUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.factory.DefaultFactorFactory;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CreateProjectHelper extends BaseUseCase implements CreateProjectUseCase {

    private static final String TAG = CreateProjectHelper.class.getSimpleName();

    ProjectRepository projectRepository;

    @Inject
    public CreateProjectHelper(ProjectRepository projectRepository,
                               ThreadExecutorProvider threadExecutorProvider, PostExecutionThread postExecutionThread){
        super(threadExecutorProvider,postExecutionThread);
        this.projectRepository = projectRepository;
    }

    @Override
    public Observable<String> createProject(Project project) {
        return projectRepository.createProject(project)
                .doOnNext(projectId -> {
                    createRSBFactors(projectId);
                });
    }

    private void createRSBFactors(String projectId){
        Observable.fromIterable(DefaultFactorFactory.getDefaultRSBFactors())
                .flatMap(rsbFactor -> projectRepository.createRSBFactor(projectId,rsbFactor))
                .toList()
                .subscribeOn(threadExecutorProvider.ioScheduler())
                .subscribe(rsbFactors -> {
                   for (RSBFactor rsbFactor : rsbFactors){
                       Log.d(TAG, "RSB " + rsbFactor.getKey() +  " " + rsbFactor.getSize() + " " + rsbFactor.getWeight());
                   }
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}
