package com.android.estimateapp.cleanarchitecture.presentation.cost.material.columnwallfooting;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatDialogFragment;

public class ColumnAndWallFootingMaterialCostDialogFragment extends DaggerAppCompatDialogFragment implements ColumnAndWallFootingMaterialCostContract.View {

    private final static String PROJECT_ID = "projectId";
    private final static String SCOPE_KEY = "scopeKey";

    @BindView(R.id.et_rsb_10)
    EditText etRsb10;
    @BindView(R.id.et_rsb_12)
    EditText etRsb12;
    @BindView(R.id.et_rsb_16)
    EditText etRsb16;
    @BindView(R.id.et_rsb_20)
    EditText etRsb20;
    @BindView(R.id.et_rsb_25)
    EditText etRsb25;
    @BindView(R.id.et_tie_wire)
    EditText etTieWire;


    private RebarMaterialSummary input;


    @Inject
    ColumnAndWallFootingMaterialCostContract.UserActionListener presenter;


    public static ColumnAndWallFootingMaterialCostDialogFragment newInstance(String projectId, String scopeKey) {
        Bundle args = new Bundle();
        args.putString(PROJECT_ID, projectId);
        args.putString(SCOPE_KEY, scopeKey);
        ColumnAndWallFootingMaterialCostDialogFragment fragment = new ColumnAndWallFootingMaterialCostDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.column_footing_material_cost, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        input = new RebarMaterialSummary();

        presenter.setData(getArguments().getString(PROJECT_ID), getArguments().getString(SCOPE_KEY));
        presenter.setView(this);
        presenter.start();

        return rootView;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void setInput(RebarMaterialSummary materials) {
        this.input = materials;


        if (input.getTenRsb() != 0) {
            etRsb10.setText(DisplayUtil.toDisplayFormat(input.getTenRsb()));
        }

        if (input.getTwelveRsb() != 0) {
            etRsb12.setText(DisplayUtil.toDisplayFormat(input.getTwelveRsb()));
        }

        if (input.getSixteenRsb() != 0) {
            etRsb16.setText(DisplayUtil.toDisplayFormat(input.getSixteenRsb()));
        }

        if (input.getTwentyRsb() != 0) {
            etRsb20.setText(DisplayUtil.toDisplayFormat(input.getTwentyRsb()));
        }

        if (input.getTwentyFiveRsb() != 0) {
            etRsb25.setText(DisplayUtil.toDisplayFormat(input.getTwentyFiveRsb()));
        }

        if (input.getGiTieWire() != 0) {
            etTieWire.setText(DisplayUtil.toDisplayFormat(input.getGiTieWire()));
        }
    }

    @Override
    public void inputSaved() {
        dismiss();
    }


    @Override
    public void showSavingNotAllowedError() {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_save)
    public void onSaveClick() {
        String rsb10 = etRsb10.getText().toString();
        String rsb12 = etRsb12.getText().toString();
        String rsb16 = etRsb16.getText().toString();
        String rsb20 = etRsb20.getText().toString();
        String rsb25 = etRsb25.getText().toString();
        String tieWire = etTieWire.getText().toString();


        if (rsb10.isEmpty()) {
            input.setTenRsb(0);
        } else {
            input.setTenRsb(Double.parseDouble(rsb10));
        }

        if (rsb12.isEmpty()) {
            input.setTwelveRsb(0);
        } else {
            input.setTwelveRsb(Double.parseDouble(rsb12));
        }

        if (rsb16.isEmpty()) {
            input.setSixteenRsb(0);
        } else {
            input.setSixteenRsb(Double.parseDouble(rsb16));
        }

        if (rsb20.isEmpty()) {
            input.setTwentyRsb(0);
        } else {
            input.setTwentyRsb(Double.parseDouble(rsb20));
        }

        if (rsb25.isEmpty()) {
            input.setTwentyFiveRsb(0);
        } else {
            input.setTwentyFiveRsb(Double.parseDouble(rsb25));
        }

        if (tieWire.isEmpty()) {
            input.setGiTieWire(0);
        } else {
            input.setGiTieWire(Double.parseDouble(tieWire));
        }

        presenter.save(input);
    }
}