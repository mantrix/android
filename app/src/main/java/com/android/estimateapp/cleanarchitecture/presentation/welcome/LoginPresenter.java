package com.android.estimateapp.cleanarchitecture.presentation.welcome;

import android.util.Log;

import com.android.estimateapp.cleanarchitecture.domain.exception.AuthenticationException;
import com.android.estimateapp.cleanarchitecture.domain.exception.NoConnectionException;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.AccountRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.UserSessionRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.impl.DeviceIDFactory;
import com.android.estimateapp.cleanarchitecture.domain.usecase.validator.InputValidatorUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.contract.LoginContract;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserContract;
import com.android.estimateapp.configuration.Constants;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.HttpException;

public class LoginPresenter implements LoginContract.UserActionListener {

    private static final String TAG = LoginPresenter.class.getSimpleName();

    private LoginContract.View view;
    private AccountRepository accountRepository;
    private UserSessionRepository userSessionRepository;
    private ThreadExecutorProvider threadExecutorProvider;
    private PostExecutionThread postExecutionThread;
    private InputValidatorUseCase inputValidator;
    private DeviceIDFactory deviceIDFactory;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public LoginPresenter(AccountRepository accountRepository,
                          UserSessionRepository userSessionRepository,
                          InputValidatorUseCase inputValidator,
                          DeviceIDFactory deviceIDFactory,
                          ThreadExecutorProvider threadExecutorProvider,
                          PostExecutionThread postExecutionThread) {
        this.accountRepository = accountRepository;
        this.inputValidator = inputValidator;
        this.deviceIDFactory = deviceIDFactory;
        this.userSessionRepository = userSessionRepository;
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }

    @Override
    public void setView(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        compositeDisposable.clear();
    }

    @Override
    public void loginUsingGoogleSuccess(String userId) {
        accountRepository.userExist(userId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(exists -> {
                    if (exists){
                        authorize(userId);
                    } else {
                        view.showCreateUserFragment(userId, CreateUserContract.RegistrationMode.GOOGLE);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    view.showUnhandledError();
                });
    }

    private void authorize(String uid){
        view.googleLoginProgressVisibility(true);
        accountRepository.getIDToken(uid)
                .flatMap(credential -> accountRepository.authorizeUser(credential.getToken(),deviceIDFactory.getDeviceId())
                        .subscribeOn(threadExecutorProvider.computationScheduler()))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(authorize -> {
                    loginSuccessful();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.googleLoginProgressVisibility(false);

                    if (throwable instanceof AuthenticationException) {
                        AuthenticationException authenticationException = (AuthenticationException) throwable;
                        handleErrorCode(authenticationException.getCode());
                    } else if (throwable instanceof HttpException){
                        HttpException httpException = (HttpException) throwable;
                        handleErrorCode(Constants.Code.parseInt(httpException.code()));
                    } else if (throwable instanceof NoConnectionException) {
                        view.showNoConnectionError();
                    } else {
                        view.showUnhandledError();
                        logout();
                    }
                });
    }


    @Override
    public void login(String email, String password) {
        if (isValidInputs(email, password)) {

            view.loginProgressVisibility(true);

            compositeDisposable.add(accountRepository.login(email, password)
                    .flatMap(accountCredential -> accountRepository
                            .authorizeUser(accountCredential.getToken(),deviceIDFactory.getDeviceId())
                            .subscribeOn(threadExecutorProvider.computationScheduler()))
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(authorize -> {
                        loginSuccessful();

                    }, throwable -> {
                        throwable.printStackTrace();
                        view.loginProgressVisibility(false);

                        if (throwable instanceof AuthenticationException) {
                            AuthenticationException authenticationException = (AuthenticationException) throwable;
                            handleErrorCode(authenticationException.getCode());
                        } else if (throwable instanceof HttpException){
                            HttpException httpException = (HttpException) throwable;
                            handleErrorCode(Constants.Code.parseInt(httpException.code()));
                        } else if (throwable instanceof NoConnectionException) {
                            view.showNoConnectionError();
                        } else {
                            view.showUnhandledError();
                            logout();
                        }
                    }));
        }
    }



    private void handleErrorCode(Constants.Code errorCode) {
        Log.d(TAG, "handleErrorCode: " + errorCode);
        if (errorCode != null) {
            switch (errorCode) {
                case INVALID_DEVICE:
                    logout();
                    view.showInvalidDeviceError();
                    break;
                case INVALID_TOKEN:
                    logout();
                    view.showUnhandledError();
                    break;
                case INVALID_CREDENTIALS:
                    view.showInvalidCredentialError();
                    break;
                default:
                    view.showUnhandledError();
            }
        } else {
            view.showUnhandledError();
        }
    }

    private void logout() {
        accountRepository.logout()
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(() -> {
                    userSessionRepository.setCurrentActiveUser(null);
                }, throwable -> {
                    throwable.printStackTrace();
                    userSessionRepository.setCurrentActiveUser(null);
                });
    }

    private boolean isValidInputs(String email, String password) {
        boolean validInputs = true;
        if (email.isEmpty()) {
            view.showEmailRequiredError();
            validInputs = false;
        } else if (!inputValidator.isValidEmail(email)) {
            view.showInvalidEmailError();
            validInputs = false;
        } else {
            view.removeEmailError();
        }

        if (password.isEmpty()) {
            view.showPasswordRequiredError();
            validInputs = false;
        } else {
            view.removePasswordError();
        }
        return validInputs;
    }


    @Override
    public void loginSuccessful() {
        accountRepository.getLoggedInUser()
                .observeOn(threadExecutorProvider.ioScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(account -> {
                    Log.d(TAG, "Active User Name: " + account.getName());
                    userSessionRepository.setCurrentActiveUser(account);
                    view.googleLoginProgressVisibility(false);
                    view.loginProgressVisibility(false);
                    view.loginSuccessful();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.googleLoginProgressVisibility(false);
                    view.loginProgressVisibility(false);
                    view.showUnhandledError();
                });
    }

}
