package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;

public class StoneCladdingMaterial {

    public StoneType stoneType;

    public double area;

    public double quantity;

    public double sand;

    public double cement;

    public double adhesive;
}
