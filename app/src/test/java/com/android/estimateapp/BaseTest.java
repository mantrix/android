package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.domain.usecase.factory.DefaultFactorFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BaseTest {

    protected static final double DELTA =  0.001;

    DefaultFactorFactory defaultFactorFactory;

    protected RSBFactor tenMM;
    protected RSBFactor twelveMM;
    protected RSBFactor sixteenMM;
    protected RSBFactor twentyMM;
    protected RSBFactor twentyFiveMM;

    protected void initRSBSizes(){
        tenMM = new RSBFactor();
        tenMM.setSize(10);
        tenMM.setWeight(0.617);

        twelveMM = new RSBFactor();
        twelveMM.setSize(12);
        twelveMM.setWeight(0.888);

        sixteenMM = new RSBFactor();
        sixteenMM.setSize(16);
        sixteenMM.setWeight(1.58);

        twentyMM = new RSBFactor();
        twentyMM.setSize(20);
        twentyMM.setWeight(2.47);

        twentyFiveMM = new RSBFactor();
        twentyFiveMM.setSize(25);
        twentyFiveMM.setWeight(3.85);
    }

    protected double round(double value) {
        int places = 2;
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
