package com.android.estimateapp.cleanarchitecture.presentation.summary.stonecladding;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingSummary;

public interface StoneCladdingSummaryContract {


    interface View extends SummaryView<StoneCladdingSummary> {

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
