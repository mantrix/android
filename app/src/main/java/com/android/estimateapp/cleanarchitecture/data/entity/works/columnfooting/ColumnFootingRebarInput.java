package com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class ColumnFootingRebarInput extends Model {

    String code;

    int sets;

    int numberOfBarsAtL;

    int numberOfBarsAtW;

    int diameterAtL;

    int diameterAtW;

    double lengthAtL;

    double lengthAtW;

    double lengthOfTieWire;

    public ColumnFootingRebarInput(){

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getNumberOfBarsAtL() {
        return numberOfBarsAtL;
    }

    public void setNumberOfBarsAtL(int numberOfBarsAtL) {
        this.numberOfBarsAtL = numberOfBarsAtL;
    }

    public int getNumberOfBarsAtW() {
        return numberOfBarsAtW;
    }

    public void setNumberOfBarsAtW(int numberOfBarsAtW) {
        this.numberOfBarsAtW = numberOfBarsAtW;
    }

    public int getDiameterAtL() {
        return diameterAtL;
    }

    public void setDiameterAtL(int diameterAtL) {
        this.diameterAtL = diameterAtL;
    }

    public int getDiameterAtW() {
        return diameterAtW;
    }

    public void setDiameterAtW(int diameterAtW) {
        this.diameterAtW = diameterAtW;
    }

    public double getLengthOfTieWire() {
        return lengthOfTieWire;
    }

    public void setLengthOfTieWire(double lengthOfTieWire) {
        this.lengthOfTieWire = lengthOfTieWire;
    }

    public double getLengthAtL() {
        return lengthAtL;
    }

    public void setLengthAtL(double lengthAtL) {
        this.lengthAtL = lengthAtL;
    }

    public double getLengthAtW() {
        return lengthAtW;
    }

    public void setLengthAtW(double lengthAtW) {
        this.lengthAtW = lengthAtW;
    }
}
