package com.android.estimateapp.cleanarchitecture.data.entity;


import com.google.gson.annotations.SerializedName;

/**
 * {
 *   "user": {
 *     "uid": ""
 *   },
 *   "data": {
 *     "email": "",
 *     "name": {
 *       "firstName": "",
 *       "middleName": "",
 *       "lastName": ""
 *     },
 *     "mobileNo": "",
 *     "subscription": {
 *       "plan": "free"
 *     }
 *   }
 * }
 */
public class CreateUserPayload {

    @SerializedName("uid")
    String uid;

    @SerializedName("user")
    UserPayload userPayload;

    @SerializedName("data")
    DataPayload dataPayload;

    public CreateUserPayload(String uid, UserPayload userPayload, DataPayload dataPayload){
        this.uid = uid;
        this.userPayload = userPayload;
        this.dataPayload = dataPayload;
    }

    public String getUid() {
        return uid;
    }
}
