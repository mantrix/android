package com.android.estimateapp.cleanarchitecture.data.entity.factors.chb;


import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class VerticalSpacing extends Model{

    double meter;

    double length;

    public VerticalSpacing(){

    }

    public VerticalSpacing(double meter, double length){
        this.meter = meter;
        this.length = length;
    }

    public double getMeter() {
        return meter;
    }

    public void setMeter(double meter) {
        this.meter = meter;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
