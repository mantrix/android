package com.android.estimateapp.cleanarchitecture.presentation.factor.concrete;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface ConcreteClassFactorContract {

    interface View {
        void displayList(List<ConcreteFactor> factors);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(List<ConcreteFactor> concreteFactors);
    }
}
