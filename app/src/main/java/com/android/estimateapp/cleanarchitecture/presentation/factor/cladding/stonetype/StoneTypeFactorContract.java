package com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.stonetype;

import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface StoneTypeFactorContract {

    interface View {

        void displayList(List<StoneType> stoneTypes);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(List<StoneType> stoneTypes);

        void createStoneType(String name, double w1, double w2);
    }
}
