package com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CHBListItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<CHBInput>> {


    private List<CHBInput> list;

    ListItemClickLister<CHBInput> listItemClickLister;

    @Inject
    public CHBListItemAdapter() {
        list = new ArrayList<>();
    }

    public void setListItemClickLister(ListItemClickLister<CHBInput> listItemClickLister) {
        this.listItemClickLister = listItemClickLister;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.chb_input_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<CHBInput> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<CHBInput> list) {

    }

    public void deleteItem(String id){
        Integer position = null;
        for (int i = 0 ; i < list.size() ; i++){
            if (list.get(i).getKey().equalsIgnoreCase(id)){
                position = i;
                list.remove(i);
                break;
            }
        }
        if (position != null){
            notifyItemRemoved(position);
        }
    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv_item)
        CardView cvItem;

        @BindView(R.id.tv_location)
        TextView tvLocation;

        @BindView(R.id.tv_grid)
        TextView tvGrid;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(CHBInput chbInput) {
            tvLocation.setText(chbInput.getStoreyLocation());
            if (chbInput.getGrid() != null) {
                tvGrid.setText(chbInput.getGrid());
            }

            cvItem.setOnClickListener(v -> {
                listItemClickLister.itemClick(chbInput);
            });

            cvItem.setOnLongClickListener(v -> {
                listItemClickLister.onLongClick(chbInput);
                return false;
            });
        }
    }
}