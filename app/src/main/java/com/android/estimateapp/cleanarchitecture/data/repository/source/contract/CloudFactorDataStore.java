package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;

import java.util.List;

import io.reactivex.Observable;

public interface CloudFactorDataStore {

    Observable<RSBFactor> createProjectShareRSBFactors(String projectId, RSBFactor rsbFactor);

    Observable<List<RSBFactor>> getProjectRSBFactors(String projectId);

    Observable<List<RSBFactor>> updateProjectRSBFactors(String projectId, List<RSBFactor> rsbFactors);
}
