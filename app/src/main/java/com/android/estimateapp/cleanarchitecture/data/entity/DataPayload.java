package com.android.estimateapp.cleanarchitecture.data.entity;

import com.google.gson.annotations.SerializedName;

/**
 * {
 *   "user": {
 *     "uid": ""
 *   },
 *   "data": {
 *     "email": "",
 *     "name": {
 *       "firstName": "",
 *       "middleName": "",
 *       "lastName": ""
 *     },
 *     "mobileNo": "",
 *     "subscription": {
 *       "plan": "free"
 *     }
 *   }
 * }
 */
public class DataPayload {

    @SerializedName("email")
    public String email;

    @SerializedName("mobileNo")
    private String mobileNum;

    @SerializedName("subscription")
    public SubscriptionPayload subscriptionPayload;

    @SerializedName("name")
    NamePayload nameEntity;

    public DataPayload() {

    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public void setSubscriptionPayload(SubscriptionPayload subscriptionPayload) {
        this.subscriptionPayload = subscriptionPayload;
    }

    public void setNameEntity(NamePayload nameEntity) {
        this.nameEntity = nameEntity;
    }
}