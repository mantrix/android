package com.android.estimateapp.cleanarchitecture.presentation.factor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.tabs.TabLayout;
import com.mantrixengineering.estimateapp.R;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_FACTOR_UPDATE;

public class FactorActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    FactorPagerAdapter factorPagerAdapter;

    private ScopeOfWork scopeOfWork;

    private String projectId;

    private String scopeKey;

    private int defaultTabPos;

    public static Intent createIntent(Context context, String projectId, String scopeKey, int scopeOfWork){
        return createIntent(context,projectId,scopeKey,scopeOfWork, null);
    }

    public static Intent createIntent(Context context, String projectId, String scopeKey, int scopeOfWork, @Nullable Integer defaultTab){
        Intent intent = new Intent(context, FactorActivity.class);
        intent.putExtra(Constants.SCOPE_OF_WORK, scopeOfWork);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);

        if (defaultTab != null){
            intent.putExtra(Constants.DEFAULT_TAB, defaultTab);
        }

        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.factor_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(getString(R.string.factors));

        setupViewPager();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void extractExtras() {
        Intent intent = getIntent();
        scopeOfWork = ScopeOfWork.parseInt(intent.getIntExtra(Constants.SCOPE_OF_WORK, 0));
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        defaultTabPos = intent.getIntExtra(Constants.DEFAULT_TAB,0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_content_save:

                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void setupViewPager() {
        factorPagerAdapter = new FactorPagerAdapter(this,getSupportFragmentManager(),scopeOfWork,projectId, scopeKey);

        viewPager.setAdapter(factorPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.requestFocus();

        viewPager.setOffscreenPageLimit(factorPagerAdapter.getCount());
        viewPager.setCurrentItem(defaultTabPos);
    }


    @Override
    public void onBackPressed() {
        setResult(RESULT_FACTOR_UPDATE);
        finish();
    }

}
