package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateWallFootingRebarContract {

    interface View {

        void setInput(WallFootingRebarInput input);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(WallFootingRebarResult result);

        void inputSaved();

        void savingError();

        void requestForReCalculation();

        void setRSBPickerOptions(List<RsbSize> sizes);

        void showSavingDialog();

        void showSavingNotAllowedError();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(WallFootingRebarInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
