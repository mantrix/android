package com.android.estimateapp.cleanarchitecture.presentation.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.contract.LoginContract;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.contract.WelcomeContract;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserContract;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserDialogFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class LoginFragment extends BaseFragment implements LoginContract.View {

    private static int RC_SIGN_IN = 200;
    private final int REQUEST_CODE = 1011;

    private static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.et_email)
    TextInputEditText etEmail;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.progress_bar)
    ProgressBar loginProgressBar;

    @BindView(R.id.til_email)
    TextInputLayout textInputEmail;

    @BindView(R.id.til_password)
    TextInputLayout textInputPassword;

    @BindView(R.id.google_progress_bar)
    ProgressBar googleProgressBar;

    @BindView(R.id.btn_signin_with_google)
    SignInButton googleSignInButton;

    @Inject
    LoginContract.UserActionListener presenter;

    @Inject
    WelcomeContract.View welcomeView;

    private GoogleSignInOptions googleSignInOptions;
    private GoogleSignInClient googleSignInClient;

    private FirebaseAuth mAuth;
    private String email = "";


    @Override
    protected int getLayoutResource() {
        return R.layout.login_fragment_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        presenter.setView(this);
        presenter.start();

//        if (AppConfig.getEnvironment().equalsIgnoreCase(AppConfig.DEV)){
//            etEmail.setText("ericklester.dev@gmail.com");
//            etPassword.setText("password");
//        }

        mAuth = FirebaseAuth.getInstance();

        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();



        // Build a GoogleSignInClient with the options specified by gso.
        googleSignInClient = GoogleSignIn.getClient(getActivity(), googleSignInOptions);
    }

    @Override
    public void loginProgressVisibility(boolean show) {
        loginProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        btnLogin.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
        btnLogin.setEnabled(!show);
    }

    @Override
    public void googleLoginProgressVisibility(boolean show) {
        googleProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        googleSignInButton.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
        googleSignInButton.setEnabled(!show);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }


    @OnClick(R.id.btn_login)
    protected void loginClick(){
        presenter.login(etEmail.getText().toString(),etPassword.getText().toString());
    }

    @OnClick(R.id.btn_register)
    protected void registerClick() {
        // reset email
        email = "";
        showCreateUserFragment(null, CreateUserContract.RegistrationMode.EMAIL);
    }

    @OnClick(R.id.btn_signin_with_google)
    protected void signInWGoogleClick(){
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void showInvalidCredentialError() {
        Toast.makeText(getActivity(), getString(R.string.authentication_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoConnectionError() {
        Toast.makeText(getActivity(), getString(R.string.no_connection_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUnhandledError() {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInvalidDeviceError() {
        AlertDialog alertDialog = createAlertDialog(null,
                getString(R.string.invalid_device_msg),
                getString(R.string.ok),null, (dialog, which) -> {
                    dialog.dismiss();
                },null);
        alertDialog.show();
    }

    @Override
    public void showEmailRequiredError() {
        textInputEmail.setError(getString(R.string.enter_email_error));
    }

    @Override
    public void showPasswordRequiredError() {
        textInputPassword.setError(getString(R.string.enter_password_error));
    }

    @Override
    public void showInvalidEmailError() {
        textInputEmail.setError(getString(R.string.invalid_email_error));
    }

    @Override
    public void removePasswordError() {
        textInputPassword.setError(null);
        textInputPassword.setErrorEnabled(false);
    }

    @Override
    public void removeEmailError() {
        textInputEmail.setError(null);
        textInputEmail.setErrorEnabled(false);
    }

    @Override
    public void loginSuccessful() {
        welcomeView.navigateToHomeScreen();
    }

    public void showCreateUserFragment(String uid, CreateUserContract.RegistrationMode registrationMode){
        CreateUserDialogFragment createUserDialogFragment = CreateUserDialogFragment.newInstance(uid,email, registrationMode);
        FragmentManager fragmentManager = getFragmentManager();
        createUserDialogFragment.setTargetFragment(this, REQUEST_CODE);
        createUserDialogFragment.show(fragmentManager, CreateUserDialogFragment.TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                e.printStackTrace();
                Snackbar.make(getView().findViewById(R.id.main_layout), getString(R.string.failed_to_login), Snackbar.LENGTH_SHORT).show();
                // ...
            }
        } else if (requestCode == REQUEST_CODE){
            if (resultCode == RESULT_OK){
                presenter.loginSuccessful();
            } else if (resultCode == CreateUserDialogFragment.FAILED) {
                showUnhandledError();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            email = acct.getEmail();
                            presenter.loginUsingGoogleSuccess(user.getUid());
                        } else {
                            if (task.getException() != null){
                                task.getException().printStackTrace();
                            }
                            // If sign in fails, display a message to the user.
                            Snackbar.make(getView().findViewById(R.id.main_layout), getString(R.string.failed_to_login), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
