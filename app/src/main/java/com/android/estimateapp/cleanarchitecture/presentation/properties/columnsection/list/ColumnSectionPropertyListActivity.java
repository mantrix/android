package com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.create.CreateBeamSectionPropertyActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.create.CreateColumnSectionPropertyActivity;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

import static com.android.estimateapp.configuration.Constants.RESULT_FACTOR_UPDATE;

public class ColumnSectionPropertyListActivity extends BaseActivity implements ColumnSectionPropertiesListContract.View, ListItemClickLister<ColumnRebarProperties> {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    ColumnSectionPropertiesListContract.UserActionListener presenter;

    @Inject
    ColumnSectionPropertiesAdapter adapter;

    String projectId;

    NpaLinearLayoutManager linearLayoutManager;

    public static Intent createIntent(Context context, String projectId) {
        Intent intent = new Intent(context, ColumnSectionPropertyListActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.scope_list_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(getString(R.string.label_column_section_properties));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        initRecyclerView();

        presenter.setView(this);
        presenter.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        presenter.setData(projectId);
    }


    @Override
    public void itemClick(ColumnRebarProperties input) {
        Intent intent = CreateColumnSectionPropertyActivity.createIntent(this, projectId, input.getKey());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onLongClick(ColumnRebarProperties input) {
//        AlertDialog alertDialog = createAlertDialog(getString(R.string.delete),
//                getString(R.string.delete_input_msg),
//                getString(R.string.delete),
//                getString(R.string.cancel), (dialog, which) -> {
//                    presenter.deleteItem(input.getKey());
//                    dialog.dismiss();
//                }, (dialog, which) -> {
//                    dialog.dismiss();
//                });
//        alertDialog.show();
    }

    @Override
    public void removeItem(String inputId) {
        adapter.deleteItem(inputId);
    }

    @Override
    public void displayList(List<ColumnRebarProperties> inputs) {
        adapter.setItems(inputs);
    }

    private void initRecyclerView() {
        linearLayoutManager = new NpaLinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setListItemClickLister(this);
    }


    @OnClick(R.id.fab)
    public void addItemClick() {
        Intent intent = CreateColumnSectionPropertyActivity.createIntent(this, projectId, null);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_CONTENT_UPDATE:
                    presenter.reloadList();
                    break;
            }
        }
    }


    @Override
    public void onBackPressed() {
        setResult(RESULT_FACTOR_UPDATE);
        finish();
    }
}