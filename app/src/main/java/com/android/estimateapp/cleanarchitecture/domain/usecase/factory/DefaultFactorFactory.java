package com.android.estimateapp.cleanarchitecture.domain.usecase.factory;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.HorizontalRSBpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.TieWireSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.VerticalRSBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSizeFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DefaultFactorFactory {

    public static ConcreteFactor getConcreteFactor(Grade grade) {

        ConcreteFactor concreteFactor;

        switch (grade) {

            case AA:
                concreteFactor = new ConcreteFactor(Grade.AA.getId(), 11, 0.42, 0.85, 1);
                break;

            case A:
                concreteFactor = new ConcreteFactor(Grade.A.getId(), 7.85, 0.42, 0.84, 1);
                break;

            case B:
                concreteFactor = new ConcreteFactor(Grade.B.getId(), 6.49, 0.44, 0.87, 1);
                break;

            case C:
                concreteFactor = new ConcreteFactor(Grade.C.getId(), 5.49, 0.44, 0.89, 1);
                break;

            case D:
                concreteFactor = new ConcreteFactor(Grade.D.getId(), 4.82, 0.45, 0.91, 1);
                break;

            case SPECIAL_CASE_ONE:
                concreteFactor = new ConcreteFactor(Grade.SPECIAL_CASE_ONE.getId(), 9, 0.8, 0.5, 1);
                break;

            case SPECIAL_CASE_TWO:
                concreteFactor = new ConcreteFactor(Grade.SPECIAL_CASE_TWO.getId(), 7, 0.8, 0.45, 1);
                break;

            case SPECIAL_CASE_THREE:
                concreteFactor = new ConcreteFactor(Grade.SPECIAL_CASE_THREE.getId(), 9, 0.5, 0.1, 1);
                break;

            default:
                throw new RuntimeException("No equivalent ConcreteFactor for grade " + grade);
        }

        return concreteFactor;
    }

    public static List<ConcreteFactor> getDefaultConcreteFactors() {
        List<ConcreteFactor> factors = new ArrayList<>();

        factors.add(getConcreteFactor(Grade.AA));
        factors.add(getConcreteFactor(Grade.A));
        factors.add(getConcreteFactor(Grade.B));
        factors.add(getConcreteFactor(Grade.C));
        factors.add(getConcreteFactor(Grade.D));
        factors.add(getConcreteFactor(Grade.SPECIAL_CASE_ONE));
        factors.add(getConcreteFactor(Grade.SPECIAL_CASE_TWO));
        factors.add(getConcreteFactor(Grade.SPECIAL_CASE_THREE));

        return factors;
    }

    public static PlasteringMortarFactor getPlasteringFactor(Grade grade) {
        PlasteringMortarFactor plasteringMortarFactor;

        switch (grade) {
            case A:
                plasteringMortarFactor = new PlasteringMortarFactor(Grade.A.getId(), 0.0169, 0.00094);
                break;

            case B:
                plasteringMortarFactor = new PlasteringMortarFactor(Grade.B.getId(), 0.0123, 0.0011);
                break;

            case C:
                plasteringMortarFactor = new PlasteringMortarFactor(Grade.C.getId(), 0.0099, 0.00113);
                break;

            case D:
                plasteringMortarFactor = new PlasteringMortarFactor(Grade.D.getId(), 0.0083, 0.00115);
                break;
            default:
                throw new RuntimeException("No equivalent PlasteringMortarFactor for grade " + grade);
        }

        return plasteringMortarFactor;
    }

    public static List<StoneCladdingFactor> getStoneCladdingFactors() {
        List<StoneCladdingFactor> list = new ArrayList<>();
        list.add(new StoneCladdingFactor(Grade.A.getId(), 0.03, 0.15, 0.2));
        list.add(new StoneCladdingFactor(Grade.B.getId(), 0.011, 0.087, 0.12));
        list.add(new StoneCladdingFactor(Grade.C.getId(), 0.012, 0.088, 0.13));

        return list;
    }

    public static List<TileWorkFactor> getDefaultTileWorkFactors() {
        List<TileWorkFactor> list = new ArrayList<>();
        list.add(new TileWorkFactor(Grade.A.getId(), 0.03, 0.15, 0.2));
        list.add(new TileWorkFactor(Grade.B.getId(), 0.011, 0.087, 0.12));
        list.add(new TileWorkFactor(Grade.C.getId(), 0.012, 0.088, 0.13));

        return list;
    }

    public static List<PlasteringMortarFactor> getDefaultPlasteringFactors() {
        List<PlasteringMortarFactor> plasteringMortarFactors = new ArrayList<>();

        plasteringMortarFactors.add(getPlasteringFactor(Grade.A));
        plasteringMortarFactors.add(getPlasteringFactor(Grade.B));
        plasteringMortarFactors.add(getPlasteringFactor(Grade.C));
        plasteringMortarFactors.add(getPlasteringFactor(Grade.D));

        return plasteringMortarFactors;
    }

    public static List<RSBFactor> getDefaultRSBFactors() {
        List<RSBFactor> rsbFactors = new ArrayList<>();

        rsbFactors.add(new RSBFactor(RsbSize.TEN_MM.getValue(), 0.617));
        rsbFactors.add(new RSBFactor(RsbSize.TWELVE_MM.getValue(), 0.888));
        rsbFactors.add(new RSBFactor(RsbSize.SIXTEEN_MM.getValue(), 1.58));
        rsbFactors.add(new RSBFactor(RsbSize.TWENTY_MM.getValue(), 2.47));
        rsbFactors.add(new RSBFactor(RsbSize.TWENTY_TWO_MM.getValue(), 2.99));
        rsbFactors.add(new RSBFactor(RsbSize.TWENTY_FIVE_MM.getValue(), 3.85));
        rsbFactors.add(new RSBFactor(RsbSize.TWENTY_EIGHT_MM.getValue(), 4.84));
        rsbFactors.add(new RSBFactor(RsbSize.THIRTY_TWO_MM.getValue(), 6.31));
        rsbFactors.add(new RSBFactor(RsbSize.THIRTY_SIX_MM.getValue(), 8));

        return rsbFactors;
    }


    public static CHBFactor getDefaultCHBFactor() {
        CHBFactor chbFactor = new CHBFactor(12.5);

        List<CHBMortarClass> chbMortarClasses = new ArrayList<>();

        List<Grade> supportedClass = Arrays.asList(Grade.A, Grade.B, Grade.C, Grade.D);
        for (Grade grade : supportedClass) {
            CHBMortarClass mortarClass = new CHBMortarClass(grade.getId());
            mortarClass.setChbSizeFactors(getChbSizeFactors(grade));
            chbMortarClasses.add(mortarClass);
        }

        List<VerticalSpacing> verticalSpacings = new ArrayList<>();
        verticalSpacings.add(new VerticalSpacing(VerticalRSBSpacing.FOUR_TENTHS.getValue(), 2.93));
        verticalSpacings.add(new VerticalSpacing(VerticalRSBSpacing.SIX_TENTHS.getValue(), 2.13));
        verticalSpacings.add(new VerticalSpacing(VerticalRSBSpacing.EIGHT_TENTHS.getValue(), 1.6));

        List<HorizontalSpacing> horizontalSpacings = new ArrayList<>();
        horizontalSpacings.add(new HorizontalSpacing(HorizontalRSBpacing.TWO.getValue(), 3.3));
        horizontalSpacings.add(new HorizontalSpacing(HorizontalRSBpacing.THREE.getValue(), 2.15));
        horizontalSpacings.add(new HorizontalSpacing(HorizontalRSBpacing.FOUR.getValue(), 1.72));


        List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors = new ArrayList<>();
        List<Double> values = Arrays.asList(0.00216, 0.00156, 0.00096, 0.00144, 0.00104, 0.0008, 0.00108, 0.0008, 0.0006);
        List<TieWireSpacing> tieWireSpacings = TieWireSpacing.getTieWireSpacings();
        for (int i = 0; i < tieWireSpacings.size(); i++) {
            TieWireSpacing tieWireSpacing = tieWireSpacings.get(i);
            tieWireRebarSpacingFactors.add(new TieWireRebarSpacingFactor(tieWireSpacing.getMeter(), tieWireSpacing.getLayer(), values.get(i)));
        }

        chbFactor.setConcreteMortarClasses(chbMortarClasses);

        CHBSpacing chbSpacing = new CHBSpacing();
        chbSpacing.setVerticalSpacings(verticalSpacings);
        chbSpacing.setHorizontalSpacings(horizontalSpacings);
        chbSpacing.setTieWireRebarSpacingFactors(tieWireRebarSpacingFactors);
        chbFactor.setChbSpacing(chbSpacing);
        return chbFactor;
    }

    public static List<CHBSizeFactor> getChbSizeFactors(Grade grade) {
        List<CHBSizeFactor> chbSizeFactors = new ArrayList<>();

        switch (grade) {
            case A:
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.FOUR.getValue(), 0.792, 0.0435));
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.FIVE.getValue(), 1.16, 0.064));
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.SIX.getValue(), 1.526, 0.0844));
                break;
            case B:
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.FOUR.getValue(), 0.522, 0.0435));
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.FIVE.getValue(), 0.77, 0.064));
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.SIX.getValue(), 1.018, 0.0844));
                break;

            case C:
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.FOUR.getValue(), 0.394, 0.0435));
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.FIVE.getValue(), 0.58, 0.064));
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.SIX.getValue(), 0.763, 0.0844));
                break;

            case D:
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.FOUR.getValue(), 0.328, 0.0435));
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.FIVE.getValue(), 0.49, 0.064));
                chbSizeFactors.add(new CHBSizeFactor(CHBSize.SIX.getValue(), 0.633, 0.0844));
                break;
            default:
                throw new RuntimeException("No equivalent CHBSizeFactor for grade " + grade);
        }
        return chbSizeFactors;
    }


}
