package com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.create;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles.TileWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class CreateTileWorkPresenter extends CreateScopeInputBasePresenter implements CreateTileWorkContract.UserActionListener {

    private CreateTileWorkContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;
    private TileWorkCalculator tileWorkCalculator;


    private List<TileWorkFactor> tileWorkFactors;
    private List<TileType> tileTypes;
    private TileWorkInput input;


    @Inject
    public CreateTileWorkPresenter(ProjectRepository projectRepository,
                                   AccountSubscriptionUseCase accountSubscriptionUseCase,
                                   TileWorkCalculator tileWorkCalculator,
                                   ThreadExecutorProvider threadExecutorProvider,
                                   PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase,threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.tileWorkCalculator = tileWorkCalculator;

        tileWorkFactors = new ArrayList<>();
        tileTypes = new ArrayList<>();
    }

    @Override
    public void setView(CreateTileWorkContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        setPickerOptions();
        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {
        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateTileWorkInput(projectId, scopeKey, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createTileWorkInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<TileWorkInput> inputObservable) {
        view.showSavingDialog();
        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    @Override
    public void calculate(TileWorkInput input) {
        TileWorkFactor factor = getTileWorkFactor(input.getMortarClassEnum());
        TileType tileType = getTileType(input.getTileTypeId());
        input.setTileType(tileType);

        TileWorkResult result = tileWorkCalculator.calculate(factor, input);

        this.input = input;

        view.displayResult(result);
    }

    private TileWorkFactor getTileWorkFactor(Grade grade) {

        if (grade != null) {
            for (TileWorkFactor tileWorkFactor : tileWorkFactors) {
                if (tileWorkFactor.getMortarClass() == grade.getId()) {
                    return tileWorkFactor;
                }
            }
        }
        return null;
    }

    private TileType getTileType(String tileKey) {

        if (tileKey != null && tileTypes != null) {
            for (TileType tileType : tileTypes) {
                if (tileKey.equalsIgnoreCase(tileType.getKey())) {
                    return tileType;
                }
            }
        }
        return null;
    }

    private void setPickerOptions() {
        view.setMortarClassPickerOptions(Arrays.asList(Grade.A, Grade.B, Grade.C));
    }

    private void loadTileWorkInput() {
        projectRepository.getTileWorkInput(projectId, scopeKey, itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {

                    input.setTileType(getTileType(input.getTileTypeId()));

                    this.input = input;

                    view.setInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate) {

        Observable.zip(projectRepository.getTileWorkFactors(projectId), projectRepository.getTileTypeFactors(projectId),
                ((tileWorkFactors, tileTypes) -> new Pair(tileWorkFactors, tileTypes)))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {
                    this.tileWorkFactors = (List<TileWorkFactor>) pair.first;
                    this.tileTypes = (List<TileType>) pair.second;

                    if (!tileTypes.isEmpty()) {
                        view.setTileTypes(tileTypes);
                    }

                    if (!factorUpdate) {
                        view.enabledCalculateBtn(true);

                        if (itemKey != null) {
                            loadTileWorkInput();
                        }
                    } else {
                        /*
                         * Factors were updated hence input data and ui should be be updated.
                         */
                        if (itemKey != null) {
                            input.setTileType(getTileType(input.getTileTypeId()));

                            view.setInput(input);

                            view.requestForReCalculation();
                        }
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }
}
