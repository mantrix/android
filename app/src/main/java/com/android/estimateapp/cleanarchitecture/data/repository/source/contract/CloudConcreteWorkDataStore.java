package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;

import java.util.List;

import io.reactivex.Observable;

public interface CloudConcreteWorkDataStore {

    Observable<ConcreteWorksInput> createConcreteWorkInput(String projectId, String scopeOfWorkId, ConcreteWorksInput input);

    Observable<ConcreteWorksInput> updateConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId, ConcreteWorksInput input);

    Observable<GeneralConcreteWorksResult> saveConcreteWorkInputResult(String projectId, String scopeOfWorkId, String itemId, GeneralConcreteWorksResult result);

    Observable<List<ConcreteWorksInput>> getConcreteWorkInputs(String projectId, String scopeOfWorkId);

    Observable<ConcreteWorksInput> getConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<List<ConcreteFactor>> updateConcreteWorkFactors(String projectId, List<ConcreteFactor> concreteFactors);

    Observable<List<ConcreteFactor>> getConcreteWorkFactors(String projectId);

    Observable<List<ConcreteFactor>> createConcreteWorksFactors(String projectId, List<ConcreteFactor> concreteFactors);


    Observable<RectangularConcreteInput> createRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, RectangularConcreteInput input);

    Observable<RectangularConcreteInput> updateRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId, RectangularConcreteInput input);

    Observable<RectangularGeneralConcreteResult> saveRectangularConcreteWorkInputResult(String projectId, String scopeOfWorkId, String itemId, RectangularGeneralConcreteResult result);

    Observable<List<RectangularConcreteInput>> getRectangularConcreteWorkInputs(String projectId, String scopeOfWorkId);

    Observable<RectangularConcreteInput> getRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId);
}
