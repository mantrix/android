package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;

import java.util.List;

import io.reactivex.Observable;

public interface CloudProjectPropertiesStore {

    Observable<MemberSectionProperty> createMemberSectionProperty(String projectId, MemberSectionProperty memberSectionProperty);

    Observable<List<MemberSectionProperty>> getMemberSectionProperty(String projectId);

    Observable<List<MemberSectionProperty>> updateMemberSectionProperty(String projectId, List<MemberSectionProperty> memberSectionProperties);

    Observable<BeamSectionProperties> createBeamSectionProperty(String projectId, BeamSectionProperties properties);

    Observable<List<BeamSectionProperties>> getBeamSectionProperties(String projectId);

    Observable<BeamSectionProperties> updateBeamSectionProperty(String projectId, String itemId, BeamSectionProperties properties);

    Observable<BeamSectionProperties> getBeamSectionProperty(String projectId, String itemId);

    Observable<ColumnRebarProperties> createColumnProperties(String projectId, ColumnRebarProperties properties);

    Observable<List<ColumnRebarProperties>> getColumnProperties(String projectId);

    Observable<ColumnRebarProperties> updateColumnProperties(String projectId, String itemId ,ColumnRebarProperties properties);

    Observable<ColumnRebarProperties> getColumnProperties(String projectId, String itemId);
}
