package com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface StoneCLaddingListContract {

    interface View {
        void displayList(List<StoneCladdingInput> inputs);
        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
