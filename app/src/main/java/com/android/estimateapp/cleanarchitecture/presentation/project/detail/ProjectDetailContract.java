package com.android.estimateapp.cleanarchitecture.presentation.project.detail;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface ProjectDetailContract {

    interface View {

        void setProjectName(String projectName);

        void setScopeChooserOption(List<ScopeOfWork> works);

        void displayProjectScopes(List<Work> works);

        void setAddScopeVisibility(boolean visible);
    }

    interface UserActionListener extends Presenter<View> {

        void setProjectId(String projectId);

        void createProjectScope(ScopeOfWork scopeOfWork);
    }
}
