package com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork;


import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class TileType extends Model{

    String name;

    double length;

    double width;


    public TileType(){

    }

    public TileType(double length, double width) {
        this.length = length;
        this.width = width;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Exclude
    public boolean equalValues(TileType tileType) {
        return (this.getKey().equalsIgnoreCase(tileType.getKey()))
                && (this.getName().equalsIgnoreCase(tileType.getName()))
                && (this.getLength() == tileType.getLength())
                && (this.getWidth() == tileType.getWidth());
    }
}
