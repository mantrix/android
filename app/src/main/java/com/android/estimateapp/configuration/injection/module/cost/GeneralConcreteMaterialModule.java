package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.concrete.GeneralConcreteMaterialContract;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.concrete.GeneralConcreteMaterialPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class GeneralConcreteMaterialModule {

    @Provides
    @PerFragment
    GeneralConcreteMaterialContract.UserActionListener provideGeneralConcreteMaterialPresenter(GeneralConcreteMaterialPresenter presenter) {
        return presenter;
    }
}
