package com.android.estimateapp.cleanarchitecture.data.entity.project;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class WorkerEntity extends Model {

    String designation;
    double hourlyRate;

    public WorkerEntity(){

    }

    public WorkerEntity(String designation, double hourlyRate){
        this.designation = designation;
        this.hourlyRate = hourlyRate;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
}
