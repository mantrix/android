package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;

import java.util.List;

import io.reactivex.Observable;

public interface CloudWallFootingRebarDataStore {

    Observable<WallFootingRebarInput> createWallFootingInput(String projectId, String scopeOfWorkId, WallFootingRebarInput input);

    Observable<WallFootingRebarInput> updateWallFootingInput(String projectId, String scopeOfWorkId, String itemId, WallFootingRebarInput input);

    Observable<List<WallFootingRebarInput>> getWallFootingInputs(String projectId, String scopeOfWorkId);

    Observable<WallFootingRebarInput> getWallFootingInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<WallFootingRebarResult> saveWallFootingInputResult(String projectId, String scopeOfWorkId, String itemId, WallFootingRebarResult result);

}
