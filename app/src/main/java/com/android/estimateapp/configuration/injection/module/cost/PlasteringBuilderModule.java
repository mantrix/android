package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.plastering.PlasteringMaterialDialogFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PlasteringBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {PlasteringMaterialModule.class})
    abstract PlasteringMaterialDialogFragment bindPlasteringMaterialDialogFragment();
}
