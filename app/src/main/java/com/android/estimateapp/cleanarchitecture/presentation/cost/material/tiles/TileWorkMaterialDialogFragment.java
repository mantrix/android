package com.android.estimateapp.cleanarchitecture.presentation.cost.material.tiles;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkMaterials;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatDialogFragment;

public class TileWorkMaterialDialogFragment extends DaggerAppCompatDialogFragment implements TileWorkMaterialContract.View {

    private final static String PROJECT_ID = "projectId";
    private final static String SCOPE_KEY = "scopeKey";
    @BindView(R.id.et_cement)
    EditText etCement;
    @BindView(R.id.et_adhesive)
    EditText etAdhesive;
    @BindView(R.id.et_sand)
    EditText etSand;


    private TileWorkMaterials input;


    @Inject
    TileWorkMaterialContract.UserActionListener presenter;


    public static TileWorkMaterialDialogFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();
        args.putString(PROJECT_ID, projectId);
        args.putString(SCOPE_KEY, scopeKey);
        TileWorkMaterialDialogFragment fragment = new TileWorkMaterialDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tile_work_materials_layout, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        input = new TileWorkMaterials();

        presenter.setData(getArguments().getString(PROJECT_ID), getArguments().getString(SCOPE_KEY));
        presenter.setView(this);
        presenter.start();

        return rootView;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void setInput(TileWorkMaterials materials) {
        this.input = materials;

        if (input.getCement() != 0){
            etCement.setText(DisplayUtil.toDisplayFormat(input.getCement()));
        }

        if (input.getAdhesive() != 0){
            etAdhesive.setText(DisplayUtil.toDisplayFormat(input.getAdhesive()));
        }

        if (input.getSand() != 0){
            etSand.setText(DisplayUtil.toDisplayFormat(input.getSand()));
        }
    }

    @Override
    public void inputSaved() {
        dismiss();
    }


    @Override
    public void showSavingNotAllowedError() {
        Toast.makeText(getActivity(), getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_save)
    public void onSaveClick() {

        String adhesive = etAdhesive.getText().toString();
        String sand = etSand.getText().toString();
        String cement = etCement.getText().toString();

        if (adhesive.isEmpty()){
            input.setAdhesive(0);
        } else {
            input.setAdhesive(Double.parseDouble(adhesive));
        }

        if (sand.isEmpty()){
            input.setSand(0);
        } else {
            input.setSand(Double.parseDouble(sand));
        }

        if (cement.isEmpty()){
            input.setCement(0);
        } else {
            input.setCement(Double.parseDouble(cement));
        }

        presenter.save(input);
    }
}