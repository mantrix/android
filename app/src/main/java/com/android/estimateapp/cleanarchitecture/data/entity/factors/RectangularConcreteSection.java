package com.android.estimateapp.cleanarchitecture.data.entity.factors;

public class RectangularConcreteSection {

    String name;

    double ax;

    double lz;

    double ly;

    double lx;

    double yd;

    double yz;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAx() {
        return ax;
    }

    public void setAx(double ax) {
        this.ax = ax;
    }

    public double getLz() {
        return lz;
    }

    public void setLz(double lz) {
        this.lz = lz;
    }

    public double getLy() {
        return ly;
    }

    public void setLy(double ly) {
        this.ly = ly;
    }

    public double getLx() {
        return lx;
    }

    public void setLx(double lx) {
        this.lx = lx;
    }

    public double getYd() {
        return yd;
    }

    public void setYd(double yd) {
        this.yd = yd;
    }

    public double getYz() {
        return yz;
    }

    public void setYz(double yz) {
        this.yz = yz;
    }
}
