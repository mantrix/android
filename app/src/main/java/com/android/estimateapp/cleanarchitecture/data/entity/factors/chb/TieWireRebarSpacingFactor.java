package com.android.estimateapp.cleanarchitecture.data.entity.factors.chb;


import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class TieWireRebarSpacingFactor extends Model {

    double meter;

    int layer;

    double tieWirePerSqm;

    public TieWireRebarSpacingFactor(){

    }

    public TieWireRebarSpacingFactor(double meter,int layer,double tieWirePerSqm){
        this.meter = meter;
        this.layer = layer;
        this.tieWirePerSqm = tieWirePerSqm;
    }

    public double getTieWirePerSqm() {
        return tieWirePerSqm;
    }

    public void setTieWirePerSqm(double tieWirePerSqm) {
        this.tieWirePerSqm = tieWirePerSqm;
    }

    public double getMeter() {
        return meter;
    }

    public void setMeter(double meter) {
        this.meter = meter;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }
}
