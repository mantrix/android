package com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class QtyLength extends Model {

    double quantity;

    double length;

    public QtyLength(){

    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
