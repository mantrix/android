package com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Stirrups;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CreateColumnSectionPropertyPresenter extends CreateScopeInputBasePresenter implements CreateColumnSecionPropertyContract.UserActionListener {

    private CreateColumnSecionPropertyContract.View view;

    private String projectId;
    private String itemKey;
    private ProjectPropertiesRepository projectPropertiesRepository;
    private ProjectRepository projectRepository;

    private ColumnRebarProperties input;

    private List<RSBFactor> rsbFactors;


    @Inject
    public CreateColumnSectionPropertyPresenter(ProjectRepository projectRepository,
                                                ProjectPropertiesRepository projectPropertiesRepository,
                                                AccountSubscriptionUseCase accountSubscriptionUseCase,
                                                ThreadExecutorProvider threadExecutorProvider,
                                                PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase, threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.projectPropertiesRepository = projectPropertiesRepository;

        rsbFactors = new ArrayList<>();
    }

    @Override
    public void setView(CreateColumnSecionPropertyContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String itemKey) {
        this.projectId = projectId;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        setPickerOptions();
        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {
        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectPropertiesRepository.updateColumnProperty(projectId, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectPropertiesRepository.createColumnProperty(projectId, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<ColumnRebarProperties> inputObservable) {

        view.showSavingDialog();

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    @Override
    public void calculate(ColumnRebarProperties input) {
        this.input = input;

        Stirrups stirrups = input.getStirrups();
        List<QtyDiameterSpacing> qtyDiameterSpacings = new ArrayList<>();
        qtyDiameterSpacings.add(stirrups.getColumnTieA());
        qtyDiameterSpacings.add(stirrups.getColumnTieB());
        qtyDiameterSpacings.add(stirrups.getColumnTieC());
        qtyDiameterSpacings.add(stirrups.getColumnTieD());
        qtyDiameterSpacings.add(stirrups.getColumnTieE());
        qtyDiameterSpacings.add(stirrups.getColumnTieF());

        int total = 0;
        for (QtyDiameterSpacing spacing : qtyDiameterSpacings){
            if (spacing != null){
                total += spacing.getQuantity();
            }
        }

        view.displayResult(total, input.getArea());
    }


    private void setPickerOptions() {
        view.setRSBPickerOptions(RsbSize.getRsbSizes());
    }

    private void loadBeamSectionPropertyInput() {
        projectPropertiesRepository.getColumnProperty(projectId, itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {

                    this.input = input;

                    view.setInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate) {

        projectRepository.getProjectRSBFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(rsbFactors -> {
                    this.rsbFactors = rsbFactors;


                    if (!factorUpdate) {
                        view.enabledCalculateBtn(true);

                        if (itemKey != null) {
                            loadBeamSectionPropertyInput();
                        }
                    } else {
                        /*
                         * Factors were updated hence input data and ui should be be updated.
                         */
                        if (itemKey != null) {

                            view.setInput(input);

                            view.requestForReCalculation();
                        }
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }
}