package com.android.estimateapp.cleanarchitecture.data.entity.factors;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.PlywoodSize;

public class FormworkFactor {

    PlywoodSize plywoodSize;

    double effectiveArea;

    double nail;

    public PlywoodSize getPlywoodSize() {
        return plywoodSize;
    }

    public void setPlywoodSize(PlywoodSize plywoodSize) {
        this.plywoodSize = plywoodSize;
    }

    public double getEffectiveArea() {
        return effectiveArea;
    }

    public void setEffectiveArea(double effectiveArea) {
        this.effectiveArea = effectiveArea;
    }

    public double getNail() {
        return nail;
    }

    public void setNail(double nail) {
        this.nail = nail;
    }
}
