package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;

import java.util.List;

import io.reactivex.Observable;

public interface CloudBeamRebarStore {

    Observable<BeamRebarInput> createBeamRebarInputInput(String projectId, String scopeOfWorkId, BeamRebarInput input);

    Observable<BeamRebarInput> updateBeamRebarInputInput(String projectId, String scopeOfWorkId, String itemId, BeamRebarInput input);

    Observable<List<BeamRebarInput>> getBeamRebarInputInputs(String projectId, String scopeOfWorkId);

    Observable<BeamRebarInput> getBeamRebarInputInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<BeamRebarOutput> saveBeamRebarInputResult(String projectId, String scopeOfWorkId, String itemId, BeamRebarOutput input);
}
