package com.android.estimateapp.cleanarchitecture.data.repository;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudProjectPropertiesStore;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ProjectPropertiesDataRepository implements ProjectPropertiesRepository {

    private CloudProjectPropertiesStore cloudProjectPropertiesStore;

    @Inject
    public ProjectPropertiesDataRepository(CloudProjectPropertiesStore cloudProjectPropertiesStore) {
        this.cloudProjectPropertiesStore = cloudProjectPropertiesStore;
    }

    @Override
    public Observable<MemberSectionProperty> createMemberSectionProperty(String projectId, MemberSectionProperty memberSectionProperty) {
        return cloudProjectPropertiesStore.createMemberSectionProperty(projectId, memberSectionProperty);
    }

    @Override
    public Observable<List<MemberSectionProperty>> getMemberSectionProperties(String projectId) {
        return cloudProjectPropertiesStore.getMemberSectionProperty(projectId);
    }

    @Override
    public Observable<List<MemberSectionProperty>> updateMemberSectionProperty(String projectId, List<MemberSectionProperty> memberSectionProperties) {
        return cloudProjectPropertiesStore.updateMemberSectionProperty(projectId, memberSectionProperties);
    }

    @Override
    public Observable<BeamSectionProperties> createBeamSectionProperty(String projectId, BeamSectionProperties properties) {
        return cloudProjectPropertiesStore.createBeamSectionProperty(projectId, properties);
    }

    @Override
    public Observable<List<BeamSectionProperties>> getBeamSectionProperties(String projectId) {
        return cloudProjectPropertiesStore.getBeamSectionProperties(projectId);
    }

    @Override
    public Observable<BeamSectionProperties> updateBeamSectionProperty(String projectId, String itemId, BeamSectionProperties properties) {
        return cloudProjectPropertiesStore.updateBeamSectionProperty(projectId, itemId, properties);
    }

    @Override
    public Observable<BeamSectionProperties> getBeamSectionProperty(String projectId, String itemId) {
        return cloudProjectPropertiesStore.getBeamSectionProperty(projectId, itemId);
    }

    @Override
    public Observable<ColumnRebarProperties> createColumnProperty(String projectId, ColumnRebarProperties properties) {
        return cloudProjectPropertiesStore.createColumnProperties(projectId, properties);
    }

    @Override
    public Observable<List<ColumnRebarProperties>> getColumnProperties(String projectId) {
        return cloudProjectPropertiesStore.getColumnProperties(projectId);
    }

    @Override
    public Observable<ColumnRebarProperties> updateColumnProperty(String projectId, String itemId, ColumnRebarProperties properties) {
        return cloudProjectPropertiesStore.updateColumnProperties(projectId, itemId, properties);
    }

    @Override
    public Observable<ColumnRebarProperties> getColumnProperty(String projectId, String itemId) {
        return cloudProjectPropertiesStore.getColumnProperties(projectId, itemId);
    }
}
