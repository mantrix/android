package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import android.content.Context;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.project.CloudProjectDataStore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;

@Singleton
public class WSProjectStore extends BaseStore implements CloudProjectDataStore {

    Context context;
    DatabaseReference projectsRef;
    DatabaseReference userProjectRef;
    DatabaseReference projectScopeOfWorksRef;


    private static final String TAG = WSProjectStore.class.getSimpleName();

    @Inject
    public WSProjectStore(Context context){
        super();
        this.context = context;
        projectsRef = baseReference.child(FIREBASE_PROJECT_REF).child(FIREBASE_DATA_REF);
        userProjectRef = baseReference.child(FIREBASE_USER_PROJECT_REF);
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
    }


    @Override
    public Observable<String> createProject(Project project) {
        return Observable.fromCallable(() -> {
            String uid = getLoggedInUserUid();
            project.setCreatedBy(uid);
            return projectsRef;
        }).flatMap(databaseReference -> push(databaseReference, project))
                .map(projectResult -> projectResult.getKey());
    }

    @Override
    public Observable<List<String>> getUserProjectIds() {
        return Observable.fromCallable(() -> {
            String uid = getLoggedInUserUid();
            Query query = userProjectRef
                    .child(uid)
                    .child(FIREBASE_DATA_REF);

            query.keepSynced(true);

            return query;
        }).flatMap(query -> queryListKey(query));
    }

    @Override
    public Observable<Project> getProject(String id) {
        return Observable.fromCallable(() -> {
            Query query = projectsRef.child(id);
            query.keepSynced(true);
            return query;
        }).flatMap(query -> get(query,Project.class));
    }

    @Override
    public Observable<List<Project>> getUserProjects() {
        return Observable.fromCallable(() -> {
            String uid = getLoggedInUserUid();
            Query query = projectsRef
                    .orderByChild("createdBy")
                    .equalTo(uid);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, Project.class));
    }

    @Override
    public Completable deleteProject(String id) {
        return Observable.fromCallable(() -> {
            Query query = projectsRef.child(id);
            query.keepSynced(true);
            return query;
        }).flatMapCompletable(query -> delete(query));
    }

    @Override
    public Observable<WorkerGroup> createLaborCost(String projectId, String scopeOfWorkId, WorkerGroup group) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_LABOR);
        }).flatMap(databaseReference -> push(databaseReference,group));
    }

    @Override
    public Observable<WorkerGroup> updateLaborCost(String projectId, String scopeOfWorkId, String itemId, WorkerGroup input){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_LABOR)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference,input));
    }

    @Override
    public Observable<List<WorkerGroup>> getLaborCosts(String projectId, String scopeOfWorkId){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_LABOR);
        }).flatMap(query -> queryListValue(query,WorkerGroup.class));
    }

    @Override
    public Observable<WorkerGroup> getLaborCost(String projectId, String scopeOfWorkId, String inputId){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_LABOR)
                    .child(inputId);
        }).flatMap(query -> get(query,WorkerGroup.class));
    }

    @Override
    public Completable deleteLaborCost(String projectId, String scopeOfWorkId, String inputId) {
        return Observable.fromCallable(() -> {
            Query query = projectScopeOfWorksRef
                    .child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_LABOR)
                    .child(inputId);
            query.keepSynced(true);
            return query;
        }).flatMapCompletable(query -> delete(query));
    }

    @Override
    public <T extends Model> Observable<T> createMaterial(String projectId, String scopeOfWorkId, T model) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_MATERIALS);
        }).flatMap(databaseReference -> setValue(databaseReference,model));
    }

    @Override
    public <T extends Model> Observable<T> getMaterial(String projectId, String scopeOfWorkId,Class<T> type){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_MATERIALS);
        }).flatMap(query -> get(query,type));
    }

    @Override
    public <T extends Model> Observable<T> updateMaterial(String projectId, String scopeOfWorkId, T input){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_MATERIALS);
        }).flatMap(databaseReference -> setValue(databaseReference,input));
    }
}
