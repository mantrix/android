package com.android.estimateapp.cleanarchitecture.presentation.cost.labor.add;

import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import io.reactivex.annotations.Nullable;

public interface AddLaborCostContract {

    interface View {

        void setInput(WorkerGroup input);

        void inputSaved();

        void showSavingDialog();

        void savingError();

        void showSavingNotAllowedError();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void save(WorkerGroup group);
    }
}