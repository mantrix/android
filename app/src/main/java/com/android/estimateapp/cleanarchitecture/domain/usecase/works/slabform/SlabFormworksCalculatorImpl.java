package com.android.estimateapp.cleanarchitecture.domain.usecase.works.slabform;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.LumberSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.FormworkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.slabformworks.SlabFormInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.slabformworks.SlabFormOutput;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SlabFormworksCalculatorImpl extends BaseCalculator implements SlabFormworksCalculator {

    @Inject
    public SlabFormworksCalculatorImpl(){

    }

    @Override
    public SlabFormOutput calculate(SlabFormInput input, FormworkFactor factor){

        double area = input.getSets() * input.getLength() * input.getWidth();
        double plywood = area / factor.getEffectiveArea();
        double nails = area * factor.getNail();

        double twoByTwoVal = 0;
        double twoByTwoThreeVal = 0;
        double twoByTwoFourVal = 0;

        List<LumberSize> lumberSizes = new ArrayList<>();
        if (input.getLumberSizeAtX() != null){
            lumberSizes.add(input.getLumberSizeAtX());
        }

        if (input.getLumberSizeAtY() != null){
            lumberSizes.add(input.getLumberSizeAtY());
        }

        if (input.getPermiterBrace() != null){
            lumberSizes.add(input.getPermiterBrace());
        }

        // TODO: fix lumber wood computation
        for (int i = 0 ; i < lumberSizes.size() ; i++){

            if (lumberSizes.get(i) == LumberSize.TWO_BY_TWO){

                double partOne = Math.ceil(input.getLength() / input.getSpacingX()) * (input.getWidth() - .1) * 3.28 * 2 *  2 / 12;
                double partTwo = Math.floor(input.getWidth() / input.getSpacingY()) * (input.getLength() - .1) * (3.28 / 3);
                double partThree = (2 * input.getWidth()) + ((2 * input.getLength()) - .2) * (3.28 / 3);

                twoByTwoVal = twoByTwoVal + (partOne + partTwo + partThree);
            } else if (lumberSizes.get(i) == LumberSize.TWO_BY_THREE){

                double partOne = Math.ceil(input.getLength() / input.getSpacingX()) * (input.getWidth() - .1) * 3.28 * 2 * 3 / 12;
                double partTwo = Math.floor(input.getWidth() / input.getSpacingY()) * (input.getLength() - .1) *  3.28 / 2;
                double partThree = (2 * input.getWidth()) + ((2 * input.getLength()) - .2) * 3.28 / 2;

                twoByTwoThreeVal = twoByTwoThreeVal + (partOne + partTwo + partThree);
            } else if (lumberSizes.get(i) == LumberSize.TWO_BY_FOUR){

                double partOne = Math.ceil(input.getLength() / input.getSpacingX()) * (input.getWidth() - .1) * 3.28 * 2 / 3;
                double partTwo = Math.floor(input.getWidth() / input.getSpacingY()) * (input.getLength() - .1) * 3.28 * 2 / 3;
                double partThree = (2 * input.getWidth()) + ((2 * input.getLength()) - .2) * 3.28 * 2 / 3;

                twoByTwoFourVal = twoByTwoFourVal + (partOne + partTwo + partThree);
            }

        }

        SlabFormOutput output = new SlabFormOutput();
        output.setArea(area);
        output.setPlywood(plywood);
        output.setTwoByTwo(twoByTwoVal);
        output.setTwoByThree(twoByTwoThreeVal);
        output.setTwoByFour(twoByTwoFourVal);
        output.setNails(nails);

        return output;
    }
}
