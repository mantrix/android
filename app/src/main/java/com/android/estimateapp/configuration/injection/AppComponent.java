package com.android.estimateapp.configuration.injection;

import android.app.Application;
import android.content.Context;

import com.android.estimateapp.cleanarchitecture.presentation.application.ArCiApplication;
import com.android.estimateapp.configuration.injection.module.app.ApplicationModule;
import com.android.estimateapp.configuration.injection.module.app.BuilderModule;
import com.android.estimateapp.configuration.injection.module.app.DataStoreModule;
import com.android.estimateapp.configuration.injection.module.app.NetworkModule;
import com.android.estimateapp.configuration.injection.module.app.RepositoryModule;
import com.android.estimateapp.configuration.injection.module.app.SubscriptionModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ApplicationModule.class,
        RepositoryModule.class,
        DataStoreModule.class,
        SubscriptionModule.class,
        NetworkModule.class,
        BuilderModule.class})
public interface AppComponent extends AndroidInjector<ArCiApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        @BindsInstance
        Builder context(Context context);

        AppComponent build();
    }

    void inject(ArCiApplication app);
}
