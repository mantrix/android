package com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class TileWorkInput extends Model {

    String floor;

    int sets;

    Dimension dimension;

    double wastage;

    int mortarClass;

    String tileTypeId;

    TileType tileType;

    public TileWorkInput(){

    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public double getWastage() {
        return wastage;
    }

    public void setWastage(double wastage) {
        this.wastage = wastage;
    }

    @Exclude
    public Grade getMortarClassEnum() {
        return Grade.parseInt(mortarClass);
    }

    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    public String getTileTypeId() {
        return tileTypeId;
    }

    public void setTileTypeId(String tileTypeId) {
        this.tileTypeId = tileTypeId;
    }

    @Exclude
    public TileType getTileType(){
        return tileType;
    }

    @Exclude
    public void setTileType(TileType tileType) {
        this.tileType = tileType;
    }
}
