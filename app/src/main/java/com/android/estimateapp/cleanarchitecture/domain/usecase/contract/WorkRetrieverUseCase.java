package com.android.estimateapp.cleanarchitecture.domain.usecase.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.UseCase;

import java.util.List;

import io.reactivex.Observable;

public interface WorkRetrieverUseCase extends UseCase {

    Observable<List<Work>> getProjectScopes(String projectId);
}
