package com.android.estimateapp.cleanarchitecture.data.entity.factors.chb;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class HorizontalSpacing extends Model {

    int layer;

    double length;

    public HorizontalSpacing(){

    }

    public HorizontalSpacing(int layer, double length){
        this.layer = layer;
        this.length = length;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
