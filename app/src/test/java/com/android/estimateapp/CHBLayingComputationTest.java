package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.HorizontalRSBpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.VerticalRSBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSizeFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.RSBSpacing;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying.CHBLayingCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBLayingSummary;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerLocData;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerSizeData;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CHBLayingComputationTest extends BaseTest {
    CHBLayingCalculatorImpl chbLayingCalculator;

    @Before
    public void setup() {
        chbLayingCalculator = new CHBLayingCalculatorImpl();
        initRSBSizes();
    }

    @Test
    public void CHBLayingTest() {
        CHBInput input = new CHBInput();

        List<DoorWindowOutputBase> doorWindowOutputBases = new ArrayList<>();
        CHBFactor chbFactor = new CHBFactor();
        List<HorizontalSpacing> horizontalSpacings = new ArrayList<>();
        List<VerticalSpacing> verticalSpacings = new ArrayList<>();
        TieWireRebarSpacingFactor tieWireRebarSpacingFactor = new TieWireRebarSpacingFactor();


        // Inputs
        Dimension dimension = new Dimension();
        dimension.setLength(4);
        dimension.setWidth(2);
        input.setDimension(dimension);

        input.setChbSize(CHBSize.FOUR.getValue());
        input.setSets(2);
        input.setMiscellaneousOpenings(2.2);

        RSBSpacing rsbSpacing = new RSBSpacing();
        rsbSpacing.setHorizontal(HorizontalRSBpacing.TWO.getValue());
        rsbSpacing.setVertical(VerticalRSBSpacing.FOUR_TENTHS.getValue());
        input.setRsbSpacing(rsbSpacing);

        input.setDiameterOfRSBVerticalBar(RsbSize.TWELVE_MM.getValue());
        input.setDiameterOfRSBHorizontalBar(RsbSize.TEN_MM.getValue());
        input.setTieWireLength(2);
        input.setMortarClass(Grade.A.getId());

        // CHB Factor
        chbFactor.setCHB(12.5);
        CHBMortarClass chbMortarClass = new CHBMortarClass();
        chbMortarClass.setMortarClass(Grade.A.getId());
        CHBSizeFactor chbSizeFactor = new CHBSizeFactor();
        chbSizeFactor.setSize(CHBSize.FOUR.getValue());
        chbSizeFactor.setCement(0.792);
        chbSizeFactor.setSand(0.0435);
        chbMortarClass.setChbSizeFactors(Arrays.asList(chbSizeFactor));
        chbFactor.setConcreteMortarClasses(Arrays.asList(chbMortarClass));

        VerticalSpacing verticalSpacing = new VerticalSpacing();
        verticalSpacing.setMeter(VerticalRSBSpacing.FOUR_TENTHS.getValue());
        verticalSpacing.setLength(2.93);
        verticalSpacings.add(verticalSpacing);

        HorizontalSpacing horizontalSpacing = new HorizontalSpacing();
        horizontalSpacing.setLayer(HorizontalRSBpacing.TWO.getValue());
        horizontalSpacing.setLength(3.3);
        horizontalSpacings.add(horizontalSpacing);


        tieWireRebarSpacingFactor.setLayer(HorizontalRSBpacing.TWO.getValue());
        tieWireRebarSpacingFactor.setMeter(VerticalRSBSpacing.FOUR_TENTHS.getValue());
        tieWireRebarSpacingFactor.setTieWirePerSqm(0.00216);

        CHBSpacing chbSpacing = new CHBSpacing();
        chbSpacing.setHorizontalSpacings(horizontalSpacings);
        chbSpacing.setVerticalSpacings(verticalSpacings);
        chbSpacing.setTieWireRebarSpacingFactors(Arrays.asList(tieWireRebarSpacingFactor));

        chbFactor.setChbSpacing(chbSpacing);

        input.setChbFactor(chbFactor);
//
//        List<RSBFactor> rsbFactorFactors = Arrays.asList(tenMM,twelveMM);
//        CHBResult result = chbLayingCalculator.calculate(input,doorWindowOutputBases, rsbFactorFactors);
//
//        System.out.println("Openings: " + result.getOpenings());
//        System.out.println("Area: " + result.getArea());
//        System.out.println("CHB Size: " + result.getChbSizeEnum());
//        System.out.println("CHB Count: " + result.getChbCount());
//        System.out.println("Cement: " + result.getCement());
//        System.out.println("Sand: " + result.getSand());
//        System.out.println("10 RSBFactor: " + result.getTenRsb());
//        System.out.println("12 RSBFactor: " + result.getTwelveRsb());
//        System.out.println("Tie Wire: " + result.getTieWire());
    }

    @Test
    public void generateCHBSummaryTest(){

        CHBResult firstResult = new CHBResult();
        firstResult.setChbSizeEnum(CHBSize.FOUR);
        firstResult.setStoreyLocation("First Floor");
        firstResult.setChbCount(175);
        firstResult.setCement(11.09);
        firstResult.setSand(0.61);
        firstResult.setTenRsb(28.51);
        firstResult.setTwelveRsb(36.43);
        firstResult.setTieWire(0.060);

        CHBResult secondResult = new CHBResult();
        secondResult.setStoreyLocation("Second Floor");
        secondResult.setChbSizeEnum(CHBSize.FIVE);
        secondResult.setChbCount(1550);
        secondResult.setCement(60.76);
        secondResult.setSand(7.94);
        secondResult.setTenRsb(122.41);
        secondResult.setTwelveRsb(189.39);
        secondResult.setTieWire(1.488);

        CHBResult thirdResult = new CHBResult();
        thirdResult.setStoreyLocation("First Floor");
        thirdResult.setChbSizeEnum(CHBSize.SIX);
        thirdResult.setChbCount(2038);
        thirdResult.setCement(124.37);
        thirdResult.setSand(13.76);
        thirdResult.setTenRsb(0);
        thirdResult.setTwelveRsb(901.76);
        thirdResult.setTieWire(4.577);

        CHBLayingSummary summary = chbLayingCalculator.generateSummary(Arrays.asList(firstResult,secondResult,thirdResult));

        for (CHBSize chbSize : summary.getSummaryPerCHBSize().keySet()){
            CHBSummaryPerSizeData data = summary.getSummaryPerCHBSize().get(chbSize);

            System.out.println("---------------------------------------------------------");

            System.out.println(chbSize.getValue() + "\" CHB: " + data.chbCount);
            System.out.println("Cement: " + data.cement);
            System.out.println("Sand: " + data.sand);
            System.out.println("10mm RSB: " + data.tenRsb);
            System.out.println("12mm RSB: " + data.twelveRsb);
            System.out.println("Tie Wire: " + data.tieWire);
        }

        for (String location : summary.getSummaryPerStorey().keySet()){
            CHBSummaryPerLocData data = summary.getSummaryPerStorey().get(location);

            System.out.println("---------------------------------------------------------");

            System.out.println("Storey: " + location);
            System.out.println("4\" CHB: " + data.chbFourInch);
            System.out.println("5\" CHB: " + data.chbFiveInch);
            System.out.println("6\" CHB: " + data.chbSixInch);
            System.out.println("Cement: " + data.cement);
            System.out.println("Sand: " + data.sand);
            System.out.println("10mm RSB: " + data.tenRsb);
            System.out.println("12mm RSB: " + data.twelveRsb);
            System.out.println("Tie Wire: " + data.tieWire);
        }
    }


}
