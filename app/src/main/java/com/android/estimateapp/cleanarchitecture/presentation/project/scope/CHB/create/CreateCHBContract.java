package com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.HorizontalRSBpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.VerticalRSBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateCHBContract {

    interface View {

        void setMortarClassPickerOptions(List<Grade> classes);

        void setCHBSizePickerOptions(List<CHBSize> chbSize);

        void setHorizontalSpacingPickerOptions(List<HorizontalRSBpacing> horizontalSpacingOptions);

        void setVerticalSpacingPickerOptions(List<VerticalRSBSpacing> verticalSpacingOptions);

        void setDoorOptions(List<DoorWindowInputBase> doorOptions);

        void setWindowOptions(List<DoorWindowInputBase> windowOptions);

        void setRSBSizePickerOptions(List<RsbSize> rsbSizeOptions);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(CHBResult output);

        void inputSaved();

        void savingError();

        void setCHBInput(CHBInput input);

        void requestForReCalculation();

        void showSavingDialog();

        void showSavingNotAllowedError();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(CHBInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
