package com.android.estimateapp.configuration.injection.module.factor;


import com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.tiletype.TileTypeFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.tiletype.TileTypeFactorPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class TileTypeFactorModule {

    @Provides
    @PerFragment
    TileTypeFactorContract.UserActionListener providesTileTypeFactorPresenter(TileTypeFactorPresenter presenter) {
        return presenter;
    }
}
