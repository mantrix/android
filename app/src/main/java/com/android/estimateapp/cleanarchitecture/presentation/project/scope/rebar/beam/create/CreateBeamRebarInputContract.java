package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.create;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateBeamRebarInputContract {

    interface View {

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(BeamRebarOutput output);

        void inputSaved();

        void savingError();

        void setInput(BeamRebarInput input);

        void requestForReCalculation();

        void showSavingDialog();

        void showSavingNotAllowedError();

        void setPropertiesOption(List<BeamSectionProperties> propertiesOption);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(BeamRebarInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}