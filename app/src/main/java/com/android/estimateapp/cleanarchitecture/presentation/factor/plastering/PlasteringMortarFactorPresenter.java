package com.android.estimateapp.cleanarchitecture.presentation.factor.plastering;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class PlasteringMortarFactorPresenter extends BasePresenter implements PlasteringMortarFactorContract.UserActionListener {

    private PlasteringMortarFactorContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeKey;

    @Inject
    public PlasteringMortarFactorPresenter(ProjectRepository projectRepository,
                                           ThreadExecutorProvider threadExecutorProvider,
                                           PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }


    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(PlasteringMortarFactorContract.View view) {
        this.view = view;
    }

    @Override
    public void updateFactors(List<PlasteringMortarFactor> plasteringMortarFactors) {
        projectRepository.updatePlasteringMortarFactors(projectId, plasteringMortarFactors)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(updatedRsbFactors -> {
                    view.factorSaved();
                }, throwable -> {
                    view.factorSaved();
                    throwable.printStackTrace();
                });
    }


    @Override
    public void start() {
        projectRepository.getPlasteringMortarFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(plasteringMortarFactors -> {
                    view.displayList(plasteringMortarFactors);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}