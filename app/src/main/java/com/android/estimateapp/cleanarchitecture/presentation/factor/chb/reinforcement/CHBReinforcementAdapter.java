package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.EdittextBaseOutputListener;
import com.android.estimateapp.utils.DisplayUtil;
import com.android.estimateapp.utils.NumberUtils;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CHBReinforcementAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<CHBReinforcementFactorContract.ReinforcementItem>> {


    private List<CHBReinforcementFactorContract.ReinforcementItem> list;

    @Inject
    public CHBReinforcementAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.chb_reinforcement_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<CHBReinforcementFactorContract.ReinforcementItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<CHBReinforcementFactorContract.ReinforcementItem> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    public List<CHBReinforcementFactorContract.ReinforcementItem> getData() {
        return list;
    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_vertical_spacing)
        TextView tvVerticalpacing;

        @BindView(R.id.tv_horizontal_spacing)
        TextView tvHorizontalSpacing;

        @BindView(R.id.et_vertical_spacing)
        EditText etVerticalpacing;

        @BindView(R.id.et_horizontal_spacing)
        EditText etHorizontalSpacing;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(CHBReinforcementFactorContract.ReinforcementItem item, int position) {


            VerticalSpacing verticalSpacing = item.getVerticalSpacing();
            tvVerticalpacing.setText(DisplayUtil.toPlainString(verticalSpacing.getMeter()));
            etVerticalpacing.setText(DisplayUtil.toPlainString(verticalSpacing.getLength()));

            HorizontalSpacing horizontalSpacing = item.getHorizontalSpacing();
            tvHorizontalSpacing.setText(DisplayUtil.toPlainString(horizontalSpacing.getLayer()));
            etHorizontalSpacing.setText(DisplayUtil.toPlainString(horizontalSpacing.getLength()));


            etVerticalpacing.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etVerticalpacing.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        verticalSpacing.setLength(Double.parseDouble(input));
                        list.get(position).setVerticalSpacing(verticalSpacing);
                    }
                }
            });

            etHorizontalSpacing.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etHorizontalSpacing.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        horizontalSpacing.setLength(Double.parseDouble(input));
                        list.get(position).setHorizontalSpacing(horizontalSpacing);
                    }
                }
            });
        }
    }
}

