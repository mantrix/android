package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.list.ColumnFootingRebarListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.list.ColumnFootingRebarListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ColumnFootingRebarListModule {


    @Provides
    @PerActivity
    ColumnFootingRebarListContract.UserActionListener provideColumnFootingrebarListPresenter(ColumnFootingRebarListPresenter presenter) {
        return presenter;
    }
}
