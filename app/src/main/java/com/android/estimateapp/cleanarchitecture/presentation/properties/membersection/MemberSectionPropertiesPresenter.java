package com.android.estimateapp.cleanarchitecture.presentation.properties.membersection;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.data.repository.ProjectPropertiesDataRepository;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class MemberSectionPropertiesPresenter extends BasePresenter implements MemberSectionPropertieContract.UserActionListener {

    private MemberSectionPropertieContract.View view;
    private ProjectPropertiesDataRepository propertiesDataRepository;
    private String projectId;

    @Inject
    public MemberSectionPropertiesPresenter(ProjectPropertiesDataRepository propertiesDataRepository,
                                            ThreadExecutorProvider threadExecutorProvider,
                                            PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.propertiesDataRepository = propertiesDataRepository;
    }


    @Override
    public void setData(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public void setView(MemberSectionPropertieContract.View view) {
        this.view = view;
    }


    @Override
    public void update(List<MemberSectionProperty> properties) {
        propertiesDataRepository.updateMemberSectionProperty(projectId, properties)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(updatedProperties -> {
                    view.saved();
                }, throwable -> {
                    view.saved();
                    throwable.printStackTrace();
                });
    }

    @Override
    public void createProperty(MemberSectionProperty property) {
        propertiesDataRepository.createMemberSectionProperty(projectId, property)
                .flatMap(tileTypeResult -> propertiesDataRepository.getMemberSectionProperties(projectId))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(properties -> {
                    /*
                     * Display updated Properties
                     */
                    view.displayProperties(properties);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void start() {
        propertiesDataRepository.getMemberSectionProperties(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(properties -> {
                    view.displayProperties(properties);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}