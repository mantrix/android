package com.android.estimateapp.cleanarchitecture.data.entity.works;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;

public class DoorWindowOutputBase extends Model {

    @Exclude
    String key;

    @Exclude
    int sets;

    double area;

    double perimeter;

    public DoorWindowOutputBase(){

    }

    @Exclude
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Exclude
    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }
}
