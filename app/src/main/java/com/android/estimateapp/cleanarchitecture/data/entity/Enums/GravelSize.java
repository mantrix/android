package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum GravelSize {
    G1(1, "G1"),
    THREE_FOURTHS(1, "3/4");


    int id;
    String name;

    GravelSize(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }


    public static GravelSize parseString(String value){
        for (GravelSize gravelSize : getGravelSizes()){
            if (value.equalsIgnoreCase(gravelSize.toString())){
                return gravelSize;
            }
        }
        return null;
    }

    public static List<GravelSize> getGravelSizes(){
        List<GravelSize> list = new ArrayList<>();
        list.add(G1);
        list.add(THREE_FOURTHS);
        return list;
    }
}