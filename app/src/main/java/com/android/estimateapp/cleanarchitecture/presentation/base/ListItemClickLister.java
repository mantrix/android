package com.android.estimateapp.cleanarchitecture.presentation.base;

public interface ListItemClickLister<T> {

    void itemClick(T t);

    void onLongClick(T t);
}
