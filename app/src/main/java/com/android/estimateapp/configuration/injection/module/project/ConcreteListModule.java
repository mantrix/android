package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.list.ConcreteWorkListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.list.ConcreteWorkListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;


@Module
public class ConcreteListModule {

    @Provides
    @PerActivity
    ConcreteWorkListContract.UserActionListener provideConreteListPresenter(ConcreteWorkListPresenter presenter) {
        return presenter;
    }
}
