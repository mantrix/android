package com.android.estimateapp.cleanarchitecture.domain.usecase.contract;


import com.android.estimateapp.cleanarchitecture.data.entity.contract.Subscription;

import io.reactivex.Observable;

public interface AccountSubscriptionUseCase {

    Observable<Boolean> isPayingUser();

    Observable<Boolean> isTrialUser();

    Observable<Boolean> isPayingUserExperience();

    Observable<Subscription> getSubscription();
}
