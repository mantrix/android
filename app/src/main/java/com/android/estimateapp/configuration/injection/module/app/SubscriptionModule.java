package com.android.estimateapp.configuration.injection.module.app;

import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.impl.AccountSubscriptionImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SubscriptionModule {

    @Provides
    @Singleton
    AccountSubscriptionUseCase provideAccountSubscriptionUseCase(AccountSubscriptionImpl accountSubscription) {
        return accountSubscription;
    }
}
