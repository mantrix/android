package com.android.estimateapp.cleanarchitecture.presentation.project.listing;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.cleanarchitecture.presentation.project.create.CreateProjectActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.detail.ProjectDetailActivity;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class ProjectListFragment extends BaseFragment implements ProjectListContract.View, ListItemClickLister<Project> {

    private static final String TAG = ProjectListFragment.class.getSimpleName();


    public static ProjectListFragment newInstance() {

        Bundle args = new Bundle();

        ProjectListFragment fragment = new ProjectListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.ll_banner)
    LinearLayout llBanner;

    @BindView(R.id.tv_banner_notice)
    TextView tvBannerNotice;

    FloatingActionButton fabAdd;

    @Inject
    ProjectListContract.UserActionListener presenter;

    @Inject
    ProjectListAdapter projectListAdapter;


    private NpaLinearLayoutManager linearLayoutManager;

    @Override
    protected int getLayoutResource() {
        return R.layout.project_list_fragment;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {

        fabAdd = getActivity().findViewById(R.id.fab);
        fabAdd.setOnClickListener(v -> {
            presenter.addProjectClick();
        });

        presenter.setView(this);
        presenter.start();

        initRecyclerView();
        setFabBehavior();

    }

    private void setFabBehavior() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0)
                    fabAdd.hide();
                else if (dy < 0)
                    fabAdd.show();
            }
        });
    }

    private void initRecyclerView() {
        linearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(onScrollListener);
        projectListAdapter.setItemClickLister(this);
        recyclerView.setAdapter(projectListAdapter);
    }

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            presenter.onListScrolled(linearLayoutManager.getItemCount(), linearLayoutManager.findLastVisibleItemPosition());
        }
    };

    @Override
    public void showAddProjectScreen() {
        startActivityForResult(new Intent(getActivity(), CreateProjectActivity.class), Constants.REQUEST_CODE);
    }

    @Override
    public void subscriptionStatus(String dateStr, boolean today) {
        llBanner.setVisibility(View.VISIBLE);
        if (today){
            tvBannerNotice.setText(getString(R.string.subscription_will_end_today));
        } else {
            tvBannerNotice.setText(String.format(getString(R.string.subscription_will_end_at), dateStr));
        }
    }

    @Override
    public void trialStatus(String dateStr,  boolean today) {
        llBanner.setVisibility(View.VISIBLE);
        if (today){
            tvBannerNotice.setText(getString(R.string.trial_will_end_today));
        } else {
            tvBannerNotice.setText(String.format(getString(R.string.trial_will_end_at), dateStr));
        }
    }

    @Override
    public void displayUpgradeToAddProject() {
        AlertDialog alertDialog = createAlertDialog(getString(R.string.upgrade),
                getString(R.string.upgrade_to_add_project),
                getString(R.string.ok), null, (dialog, which) -> {
                    dialog.dismiss();
                }, null);
        alertDialog.show();
    }

    @Override
    public void itemClick(Project project) {
        Intent intent = ProjectDetailActivity.createIntent(getActivity(), project.getKey());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onLongClick(Project project) {
        AlertDialog alertDialog = createAlertDialog(getString(R.string.delete_project),
                String.format(getString(R.string.delete_msg), project.getName()),
                getString(R.string.delete),
                getString(R.string.cancel), (dialog, which) -> {
                    presenter.deleteItem(project.getKey());
                    dialog.dismiss();
                }, (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    @Override
    public void removeProject(String key) {
        projectListAdapter.deleteItem(key);
    }

    @Override
    public void displayProjects(List<Project> projects) {
        if (projectListAdapter.getItemCount() > 0) {
            projectListAdapter.addItems(projects);
        } else {
            projectListAdapter.setItems(projects);
        }
    }

    @Override
    public void displayUpgradeAlert() {
        AlertDialog alertDialog = createAlertDialog(getString(R.string.upgrade),
                getString(R.string.generic_upgrade_message),
                getString(R.string.ok), null, (dialog, which) -> {
                    dialog.dismiss();
                }, null);
        alertDialog.show();
    }

    @Override
    public void refreshProjects(List<Project> projects) {
        projectListAdapter.setItems(projects);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.resume();
        if (projectListAdapter != null) {
            projectListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_CONTENT_UPDATE:
                    presenter.reloadProjectList();
                    break;
            }
        }
    }
}
