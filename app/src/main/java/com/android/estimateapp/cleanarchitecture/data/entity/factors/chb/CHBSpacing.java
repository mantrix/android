package com.android.estimateapp.cleanarchitecture.data.entity.factors.chb;

import java.util.List;

public class CHBSpacing {

    List<VerticalSpacing> verticalSpacings;

    List<HorizontalSpacing> horizontalSpacings;

    List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors;

    public List<VerticalSpacing> getVerticalSpacings() {
        return verticalSpacings;
    }

    public void setVerticalSpacings(List<VerticalSpacing> verticalSpacings) {
        this.verticalSpacings = verticalSpacings;
    }

    public List<HorizontalSpacing> getHorizontalSpacings() {
        return horizontalSpacings;
    }

    public void setHorizontalSpacings(List<HorizontalSpacing> horizontalSpacings) {
        this.horizontalSpacings = horizontalSpacings;
    }

    public List<TieWireRebarSpacingFactor> getTieWireRebarSpacingFactors() {
        return tieWireRebarSpacingFactors;
    }

    public void setTieWireRebarSpacingFactors(List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors) {
        this.tieWireRebarSpacingFactors = tieWireRebarSpacingFactors;
    }
}
