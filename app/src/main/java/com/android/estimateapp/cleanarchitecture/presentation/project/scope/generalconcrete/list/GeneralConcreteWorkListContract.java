package com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface GeneralConcreteWorkListContract {


    interface View {

        void displayList(List<ConcreteWorksInput> concreteWorksInputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
