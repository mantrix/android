package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.CladdingFactorContract;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;

public class CHBReinforcementPresenter extends BasePresenter implements CHBReinforcementFactorContract.UserActionListener {

    private CHBReinforcementFactorContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeKey;

    @Inject
    public CHBReinforcementPresenter(ProjectRepository projectRepository,
                                  ThreadExecutorProvider threadExecutorProvider,
                                  PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }


    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(CHBReinforcementFactorContract.View view) {
        this.view = view;
    }

    @Override
    public void updateFactors(List<CHBReinforcementFactorContract.ReinforcementItem> reinforcementItems) {
        Pair<List<VerticalSpacing>,List<HorizontalSpacing>> pair = convertViewModelSpacings(reinforcementItems);

        List<Completable> completables = new ArrayList<>();
        completables.add(projectRepository.updateCHBVerticalSpacing(projectId, pair.first).ignoreElements());
        completables.add(projectRepository.updateCHBHorizontalSpacing(projectId, pair.second).ignoreElements());

        Completable.concat(completables)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(() -> {
                    view.factorSaved();
                }, throwable -> {
                    view.factorSaved();
                    throwable.printStackTrace();
                });
    }


    @Override
    public void start() {
        projectRepository.getCHBFactor(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbFactor -> {
                    List<CHBReinforcementFactorContract.ReinforcementItem> reinforcementItems = convertViewModel(chbFactor.getChbSpacing());
                    view.displayList(reinforcementItems);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private List<CHBReinforcementFactorContract.ReinforcementItem> convertViewModel(CHBSpacing chbSpacing){
        List<CHBReinforcementFactorContract.ReinforcementItem> items = new ArrayList<>();
        for (int i = 0 ; i < chbSpacing.getHorizontalSpacings().size() ; i++){
            items.add(new CHBReinforcement(chbSpacing.getVerticalSpacings().get(i),chbSpacing.getHorizontalSpacings().get(i)));
        }
        return items;
    }

    private Pair<List<VerticalSpacing>,List<HorizontalSpacing>> convertViewModelSpacings(List<CHBReinforcementFactorContract.ReinforcementItem> items){
        List<VerticalSpacing> verticalSpacings = new ArrayList<>();
        List<HorizontalSpacing> horizontalSpacings = new ArrayList<>();

        for (int i = 0 ; i < items.size() ; i++){
            verticalSpacings.add(items.get(i).getVerticalSpacing());
            horizontalSpacings.add(items.get(i).getHorizontalSpacing());
        }

        return new Pair<>(verticalSpacings,horizontalSpacings);
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}