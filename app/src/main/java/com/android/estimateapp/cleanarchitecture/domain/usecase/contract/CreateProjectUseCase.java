package com.android.estimateapp.cleanarchitecture.domain.usecase.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.UseCase;

import io.reactivex.Observable;

public interface CreateProjectUseCase extends UseCase {

    Observable<String> createProject(Project project);
}
