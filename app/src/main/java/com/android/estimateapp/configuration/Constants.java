package com.android.estimateapp.configuration;

public class Constants {

    public static final int REQUEST_CODE = 1;


    public static final int RESULT_CODE_HOME_BACK_BUTTON = 1000;
    public static final int RESULT_LOGOUT = 1001;
    public static final int RESULT_CODE_BACK_BUTTON = 1002;
    public static final int RESULT_CONTENT_UPDATE = 1003;
    public static final int RESULT_FACTOR_UPDATE = 1004;
    public static final int RESULT_PROPERTY_UPDATE = 1005;

    public static String BASE_URL_DEV = "https://us-central1-mantrix-test.cloudfunctions.net/";
    public static String BASE_URL_PROD = "https://us-central1-mantrix-engineering.cloudfunctions.net/";

    public static String FIREBASE_DB_VERSION = "v1";
    public static String FIREBASE_DB_VERSION_TEST = "test";
    public static String PROFILE_PIC_PREFIX = "profile_pic_";


    /* VALUE KEYS */
    public static final String PROJECT_ID = "projectID";
    public static final String PROJECT_NAME = "projectName";
    public static final String SCOPE_OF_WORK = "scopeOfWork";
    public static final String SCOPE_KEY = "scopeKey";
    public static final String ITEM_KEY = "itemKey";
    public static final String DEFAULT_TAB = "defaultTab";

    public enum Code {

        INVALID_CREDENTIALS("INVALID_CREDENTIALS"),
        INVALID_DEVICE(403,"INVALID_DEVICE"),
        INVALID_TOKEN("INVALID_TOKEN"),
        NO_LOGIN_USER(404,"NO_LOGIN_USER");

        private int rawCode;
        private String matcher;

        Code(){

        }

        Code(String matcher){
            this.matcher = matcher;
        }

        Code(int rawCode,String matcher){
            this.rawCode = rawCode;
            this.matcher = matcher;
        }

        public int getRawCode() {
            return rawCode;
        }

        public String getMatcher() {
            return matcher;
        }

        public static Code parseInt(int errorCode){
           for (Code code : Code.values()){
               if (code.getRawCode() == errorCode){
                   return code;
               }
           }
           return null;
        }
    }


}
