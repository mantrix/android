package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum TieWireSpacing {

    FOUR_TENTHS_BY_TWO(0.4, 2, "0.4x2"),
    FOUR_TENTHS_BY_THREE(0.4, 3, "0.4x3"),
    FOUR_TENTHS_BY_FOUR(0.4, 4, "0.4x4"),
    SIX_TENTHS_BY_TWO(0.6, 2, "0.6x2"),
    SIX_TENTHS_BY_THREE(0.6, 3, "0.6x3"),
    SIX_TENTHS_BY_FOUR(0.6, 4, "0.6x4"),
    EIGHT_TENTHS_BY_TWO(0.8, 2,"0.8x2"),
    EIGHT_TENTHS_BY_THREE(0.8, 3,"0.8x3"),
    EIGHT_TENTHS_BY_FOUR(0.8, 4,"0.8x4");

    String name;
    double meter;
    int layer;

    TieWireSpacing(double meter, int layer,String name) {
        this.meter = meter;
        this.layer = layer;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public double getMeter() {
        return meter;
    }

    public int getLayer() {
        return layer;
    }

    public static List<TieWireSpacing> getTieWireSpacings(){
        List<TieWireSpacing> spacings = new ArrayList<>();
        spacings.add(FOUR_TENTHS_BY_TWO);
        spacings.add(FOUR_TENTHS_BY_THREE);
        spacings.add(FOUR_TENTHS_BY_FOUR);

        spacings.add(SIX_TENTHS_BY_TWO);
        spacings.add(SIX_TENTHS_BY_THREE);
        spacings.add(SIX_TENTHS_BY_FOUR);

        spacings.add(EIGHT_TENTHS_BY_TWO);
        spacings.add(EIGHT_TENTHS_BY_THREE);
        spacings.add(EIGHT_TENTHS_BY_FOUR);

        return spacings;
    }

}
