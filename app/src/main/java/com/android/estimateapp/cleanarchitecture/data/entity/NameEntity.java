package com.android.estimateapp.cleanarchitecture.data.entity;

import com.android.estimateapp.cleanarchitecture.data.entity.contract.Name;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class NameEntity extends Model implements Name {


    String firstName;

    String lastName;

    String middleName;


    public NameEntity() {

    }

    public NameEntity(@Model.InstanceType int type) {
        super(type);
    }


    public NameEntity(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getMiddleName() {
        return middleName;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(firstName + " ")
                .append(middleName + " ")
                .append(lastName)
                .toString();
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("firstName", firstName);
        result.put("middleName", middleName);
        result.put("lastName", lastName);
        return result;
    }
}
