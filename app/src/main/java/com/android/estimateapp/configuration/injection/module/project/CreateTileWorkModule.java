package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles.TileWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles.TileWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.create.CreateTileWorkContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.create.CreateTileWorkPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateTileWorkModule {

    @Provides
    @PerActivity
    CreateTileWorkContract.UserActionListener provideCreateTileWorkPresenter(CreateTileWorkPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    TileWorkCalculator provideTileWorkCalculator(TileWorkCalculatorImpl calculator) {
        return calculator;
    }
}
