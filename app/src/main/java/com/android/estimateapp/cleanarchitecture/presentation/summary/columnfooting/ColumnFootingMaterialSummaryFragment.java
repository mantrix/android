package com.android.estimateapp.cleanarchitecture.presentation.summary.columnfooting;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;

public class ColumnFootingMaterialSummaryFragment extends BaseFragment implements ColumnFootingMaterialSummaryContract.View {


    @BindView(R.id.tv_ten_mm_label)
    TextView tvTenMmLabel;
    @BindView(R.id.tv_ten_mm)
    TextView tvTenMm;
    @BindView(R.id.tv_twelve_mm_label)
    TextView tvTwelveMmLabel;
    @BindView(R.id.tv_twelve_mm)
    TextView tvTwelveMm;
    @BindView(R.id.tv_tie_wire)
    TextView tvTieWire;
    @BindView(R.id.tv_summary_title)
    TextView tvSummaryTitle;
    @BindView(R.id.ll_header)
    LinearLayout llHeader;
    @BindView(R.id.tv_sixteen_mm_label)
    TextView tvSixteenMmLabel;
    @BindView(R.id.tv_sixteen_mm)
    TextView tvSixteenMm;
    @BindView(R.id.tv_twenty_mm_label)
    TextView tvTwentyMmLabel;
    @BindView(R.id.tv_twenty_mm)
    TextView tvTwentyMm;
    @BindView(R.id.tv_twenty_five_mm_label)
    TextView tvTwentyFiveMmLabel;
    @BindView(R.id.tv_twenty_five_mm)
    TextView tvTwentyFiveMm;

    @Inject
    ColumnFootingMaterialSummaryContract.UserActionListener presenter;


    public static ColumnFootingMaterialSummaryFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        ColumnFootingMaterialSummaryFragment fragment = new ColumnFootingMaterialSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String projectId;
    private String scopeKey;

    @Override
    protected int getLayoutResource() {
        return R.layout.column_footing_summmary_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        extractExtras();
        presenter.setData(projectId, scopeKey);
        presenter.setView(this);
        presenter.start();

        tvTenMmLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 10));
        tvTwelveMmLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 12));
        tvSixteenMmLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 16));
        tvTwentyMmLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 20));
        tvTwentyFiveMmLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 25));
    }


    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
    }

    @Override
    public void displaySummary(RebarMaterialSummary summary) {

        BaseActivity baseActivity = (BaseActivity) getActivity();

        tvTenMm.setText(baseActivity.toDisplayFormat(summary.getTenRsb()));
        tvTwelveMm.setText(baseActivity.toDisplayFormat(summary.getTwelveRsb()));
        tvSixteenMm.setText(baseActivity.toDisplayFormat(summary.getSixteenRsb()));
        tvTwentyMm.setText(baseActivity.toDisplayFormat(summary.getTwentyRsb()));
        tvTwentyFiveMm.setText(baseActivity.toDisplayFormat(summary.getTwentyFiveRsb()));
        tvTieWire.setText(baseActivity.toDisplayFormat(summary.getGiTieWire()));
    }

}