package com.android.estimateapp.cleanarchitecture.data.entity.works.plastering;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;

public class PlasteringWorkCategory {

    ScopeOfWork scopeOfWork;

    Category category;

    PlasteringInput input;

    PlasteringResult result;

    public PlasteringWorkCategory(){
        scopeOfWork = ScopeOfWork.CHB_PLASTERING;
    }

    public ScopeOfWork getScopeOfWork() {
        return scopeOfWork;
    }

    public void setScopeOfWork(ScopeOfWork scopeOfWork) {
        this.scopeOfWork = scopeOfWork;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public PlasteringInput getInput() {
        return input;
    }

    public void setInput(PlasteringInput input) {
        this.input = input;
    }

    public PlasteringResult getResult() {
        return result;
    }

    public void setResult(PlasteringResult result) {
        this.result = result;
    }
}
