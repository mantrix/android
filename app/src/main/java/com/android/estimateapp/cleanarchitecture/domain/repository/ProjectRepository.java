package com.android.estimateapp.cleanarchitecture.domain.repository;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface ProjectRepository {


    // PROJECT

    /**
     * @param project object to be save
     * @return Project key or Id
     */
    Observable<String> createProject(Project project);

    Observable<List<String>> getUserProjectsIds();

    Observable<Project> getProject(String id);

    Observable<List<Project>> getUserProjects();

    Completable deleteProject(String id);


    // SCOPES
    Observable<List<Work>> getProjectScopesOfWorks(String projectId);

    Observable<Work> createProjectScopeOfWork(String projectId, Work work);

    Completable deleteScopeInput(String projectId, String scopeOfWorkId, String inputId);


    // SHARED FACTORS
    Observable<RSBFactor> createRSBFactor(String projectId, RSBFactor rsbFactor);

    Observable<List<RSBFactor>> getProjectRSBFactors(String projectId);

    Observable<List<RSBFactor>> updateProjectRSBFactors(String projectId, List<RSBFactor> rsbFactors);

    // CHB
    Observable<CHBFactor> createCHBFactor(String projectId, CHBFactor chbFactor);

    Observable<CHBResult> saveCHBInputResult(String projectId, String scopeOfWorkId, String itemId, CHBResult result);

    Observable<CHBFactor> getCHBFactor(String projectId);

    Observable<CHBInput> createCHBInput(String projectId, String scopeOfWorkId, CHBInput chbInput);

    Observable<CHBInput> updateCHBInput(String projectId, String scopeOfWorkId, String itemId, CHBInput chbInput);

    Observable<List<CHBInput>> getCHBInputs(String projectId, String scopeOfWorkId);

    Observable<CHBInput> getCHBInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<List<CHBMortarClass>> updateCHBMortarClasses(String projectId, double chb, List<CHBMortarClass> chbMortarClasses);

    Observable<List<VerticalSpacing>> updateCHBVerticalSpacing(String projectId, List<VerticalSpacing> verticalSpacings);

    Observable<List<HorizontalSpacing>> updateCHBHorizontalSpacing(String projectId, List<HorizontalSpacing> horizontalSpacings);

    Observable<List<TieWireRebarSpacingFactor>> updateCHBTieWireRebarSpacingFactor(String projectId, List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors);

    // PLASTERING
    Observable<PlasteringInput> createPlasteringInput(String projectId, String scopeOfWorkId, PlasteringInput plasteringInput);

    Observable<PlasteringInput> updatePlasteringInput(String projectId, String scopeOfWorkId, String itemId, PlasteringInput plasteringInput);

    Observable<PlasteringResult> savePlasteringInputResult(String projectId, String scopeOfWorkId, String itemId, PlasteringResult result);

    Observable<List<PlasteringInput>> getPlasteringInputs(String projectId, String scopeOfWorkId);

    Observable<PlasteringInput> getPlasteringInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<List<PlasteringMortarFactor>> createPlasteringMortarFactors(String projectId, List<PlasteringMortarFactor> plasteringMortarFactors);

    Observable<List<PlasteringMortarFactor>> getPlasteringMortarFactors(String projectId);

    Observable<List<PlasteringMortarFactor>> updatePlasteringMortarFactors(String projectId, List<PlasteringMortarFactor> plasteringMortarFactors);

    // CONCRETE WORK
    Observable<ConcreteWorksInput> createConcreteWorkInput(String projectId, String scopeOfWorkId, ConcreteWorksInput input);

    Observable<ConcreteWorksInput> updateConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId, ConcreteWorksInput input);

    Observable<List<ConcreteWorksInput>> getConcreteWorkInputs(String projectId, String scopeOfWorkId);

    Observable<ConcreteWorksInput> getConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<List<ConcreteFactor>> updateConcreteWorkFactors(String projectId, List<ConcreteFactor> concreteFactors);

    Observable<List<ConcreteFactor>> getConcreteWorkFactors(String projectId);

    Observable<List<ConcreteFactor>> createConcreteWorksFactors(String projectId, List<ConcreteFactor> concreteFactors);

    Observable<RectangularConcreteInput> createRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, RectangularConcreteInput input);

    Observable<RectangularConcreteInput> updateRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId, RectangularConcreteInput input);

    Observable<List<RectangularConcreteInput>> getRectangularConcreteWorkInputs(String projectId, String scopeOfWorkId);

    Observable<RectangularConcreteInput> getRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<GeneralConcreteWorksResult> saveConcreteWorkInputResult(String projectId, String scopeOfWorkId, String itemId, GeneralConcreteWorksResult result);

    Observable<RectangularGeneralConcreteResult> saveRectangularConcreteWorkInputResult(String projectId, String scopeOfWorkId, String itemId, RectangularGeneralConcreteResult result);

    // TILE WORK
    Observable<TileWorkInput> getTileWorkInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<List<TileWorkInput>> getTileWorkInputs(String projectId, String scopeOfWorkId);

    Observable<TileWorkInput> updateTileWorkInput(String projectId, String scopeOfWorkId, String itemId, TileWorkInput input);

    Observable<TileWorkInput> createTileWorkInput(String projectId, String scopeOfWorkId, TileWorkInput input);

    Observable<List<TileWorkFactor>> updateTileWorkFactors(String projectId, List<TileWorkFactor> tileWorkFactors);

    Observable<List<TileWorkFactor>> getTileWorkFactors(String projectId);

    Observable<List<TileWorkFactor>> createTileWorkFactors(String projectId,  List<TileWorkFactor> factors);

    Observable<TileType> createTileTypeFactor(String projectId, TileType tileType);

    Observable<List<TileType>> getTileTypeFactors(String projectId);

    Observable<List<TileType>> updateTileTypeFactors(String projectId, List<TileType> factors);

    // STONE CLADDING
    Observable<StoneCladdingInput> createStoneCladdingInput(String projectId, String scopeOfWorkId, StoneCladdingInput input);

    Observable<StoneCladdingInput> updateStoneCladdingInput(String projectId, String scopeOfWorkId, String itemId, StoneCladdingInput input);

    Observable<List<StoneCladdingInput>> getStoneCladdingInputs(String projectId, String scopeOfWorkId);

    Observable<StoneCladdingInput> getStoneCladdingInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<StoneCladdingResult> saveStoneCladdingInputResult(String projectId, String scopeOfWorkId, String itemId, StoneCladdingResult result);

    Observable<List<StoneCladdingFactor>> createStoneCladdingFactors(String projectId, List<StoneCladdingFactor> factors);

    Observable<List<StoneCladdingFactor>> getStoneCladdingFactors(String projectId);

    Observable<List<StoneCladdingFactor>> updateStoneCladdingFactors(String projectId, List<StoneCladdingFactor> factors);

    Observable<StoneType> createStoneTypeFactor(String projectId, StoneType stoneType);

    Observable<List<StoneType>> getStoneTypeFactors(String projectId);

    Observable<List<StoneType>> updateStoneTypeFactors(String projectId, List<StoneType> factors);

    // DOORS & WINDOWS
    Observable<DoorWindowInputBase> createDoorAndWindowInput(String projectId, String scopeOfWorkId, DoorWindowInputBase input);

    Observable<DoorWindowInputBase> updateDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId, DoorWindowInputBase input);

    Observable<List<DoorWindowInputBase>> getDoorAndWindowInputs(String projectId, String scopeOfWorkId);

    Observable<DoorWindowInputBase> getDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<DoorWindowOutputBase> saveDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId, DoorWindowOutputBase result);

    // WALL FOOTING REBAR
    Observable<WallFootingRebarInput> createWallFootingInput(String projectId, String scopeOfWorkId, WallFootingRebarInput input);

    Observable<WallFootingRebarInput> updateWallFootingInput(String projectId, String scopeOfWorkId, String itemId, WallFootingRebarInput input);

    Observable<List<WallFootingRebarInput>> getWallFootingInputs(String projectId, String scopeOfWorkId);

    Observable<WallFootingRebarInput> getWallFootingInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<WallFootingRebarResult> saveWallFootingInputResult(String projectId, String scopeOfWorkId, String itemId, WallFootingRebarResult result);

    // COLUMN FOOTING REBAR
    Observable<ColumnFootingRebarInput> createColumnFootingInput(String projectId, String scopeOfWorkId, ColumnFootingRebarInput input);

    Observable<ColumnFootingRebarInput> updateColumnFootingInput(String projectId, String scopeOfWorkId, String itemId, ColumnFootingRebarInput input);

    Observable<List<ColumnFootingRebarInput>> getColumnFootingInputs(String projectId, String scopeOfWorkId);

    Observable<ColumnFootingRebarInput> getColumnFootingInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<ColumnFootingRebarResult> saveColumnFootingInputResult(String projectId, String scopeOfWorkId, String itemId, ColumnFootingRebarResult result);

    // BEAM REBAR
    Observable<BeamRebarInput> createBeamRebarInputInput(String projectId, String scopeOfWorkId, BeamRebarInput input);

    Observable<BeamRebarInput> updateBeamRebarInputInput(String projectId, String scopeOfWorkId, String itemId, BeamRebarInput input);

    Observable<List<BeamRebarInput>> getBeamRebarInputs(String projectId, String scopeOfWorkId);

    Observable<BeamRebarInput> getBeamRebarInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<BeamRebarOutput> saveBeamRebarInputResult(String projectId, String scopeOfWorkId, String itemId, BeamRebarOutput result);


    // COLUMN REBAR

    Observable<ColumnRebarInput> createColumnRebarInput(String projectId, String scopeOfWorkId, ColumnRebarInput input);

    Observable<ColumnRebarInput> updateColumnRebarInput(String projectId, String scopeOfWorkId, String itemId, ColumnRebarInput input);

    Observable<List<ColumnRebarInput>> getColumnRebarInputs(String projectId, String scopeOfWorkId);

    Observable<ColumnRebarInput> getColumnRebarInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<ColumnRebarOutput> saveColumnRebarInputResult(String projectId, String scopeOfWorkId, String itemId, ColumnRebarOutput output);


    // LABOR COST
    Observable<WorkerGroup> createLaborCost(String projectId, String scopeOfWorkId, WorkerGroup group);

    Observable<WorkerGroup> updateLaborCost(String projectId, String scopeOfWorkId, String itemId, WorkerGroup input);

    Observable<List<WorkerGroup>> getLaborCosts(String projectId, String scopeOfWorkId);

    Completable deleteLaborCost(String projectId, String scopeOfWorkId, String inputId);

    Observable<WorkerGroup> getLaborCost(String projectId, String scopeOfWorkId, String inputId);

    // MATERIALS COST
    <T extends Model> Observable<T> createMaterial(String projectId, String scopeOfWorkId, T model);

    <T extends Model> Observable<T> getMaterial(String projectId, String scopeOfWorkId,Class<T> type);

    <T extends Model> Observable<T> updateMaterial(String projectId, String scopeOfWorkId, T input);


}
