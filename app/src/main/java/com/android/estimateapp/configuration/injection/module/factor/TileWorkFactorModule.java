package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.TileWorkFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.TileWorkFactorPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class TileWorkFactorModule {

    @Provides
    @PerFragment
    TileWorkFactorContract.UserActionListener providesTileWorkFactorPresenter(TileWorkFactorPresenter presenter) {
        return presenter;
    }
}
