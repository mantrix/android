package com.android.estimateapp.cleanarchitecture.presentation.factor.tilework;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class TileWorkFactorPresenter extends BasePresenter implements TileWorkFactorContract.UserActionListener {

    private TileWorkFactorContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeKey;

    @Inject
    public TileWorkFactorPresenter(ProjectRepository projectRepository,
                                   ThreadExecutorProvider threadExecutorProvider,
                                   PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }


    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(TileWorkFactorContract.View view) {
        this.view = view;
    }

    @Override
    public void updateFactors(List<TileWorkFactor> factors) {
        projectRepository.updateTileWorkFactors(projectId, factors)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(updatedRsbFactors -> {
                    view.factorSaved();
                }, throwable -> {
                    view.factorSaved();
                    throwable.printStackTrace();
                });
    }


    @Override
    public void start() {
        projectRepository.getTileWorkFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(factors -> {
                    view.displayList(factors);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}