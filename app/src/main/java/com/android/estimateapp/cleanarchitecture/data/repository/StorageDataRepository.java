package com.android.estimateapp.cleanarchitecture.data.repository;

import android.graphics.Bitmap;

import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.storage.UploadFileStore;
import com.android.estimateapp.cleanarchitecture.domain.repository.StorageRepository;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class StorageDataRepository implements StorageRepository {


    @Inject
    UploadFileStore uploadFileStore;

    @Inject
    public StorageDataRepository(UploadFileStore uploadFileStore){
        this.uploadFileStore = uploadFileStore;
    }

    @Override
    public Flowable<Double> uploadProfilePhoto(Bitmap bitmap) {
        return uploadFileStore.uploadProfilePhoto(bitmap);
    }
}
