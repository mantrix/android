package com.android.estimateapp.cleanarchitecture.domain.usecase.works.slabform;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.FormworkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.slabformworks.SlabFormInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.slabformworks.SlabFormOutput;

public interface SlabFormworksCalculator {

    SlabFormOutput calculate(SlabFormInput input, FormworkFactor factor);
}
