package com.android.estimateapp.cleanarchitecture.domain.usecase.works.workandoor;

import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;

public interface DoorAndWindowCalculator {

    DoorWindowOutputBase calculate(int sets, DoorWindowInputBase input);
}
