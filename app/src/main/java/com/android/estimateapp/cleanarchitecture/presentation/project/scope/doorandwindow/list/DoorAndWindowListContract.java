package com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface DoorAndWindowListContract {

    interface View {

        void displayList(List<DoorWindowInputBase> doorWindowInputBases);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void reloadList();

        void deleteItem(String inputId);
    }
}
