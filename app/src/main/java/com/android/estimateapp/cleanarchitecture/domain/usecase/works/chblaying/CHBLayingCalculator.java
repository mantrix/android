package com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBLayingSummary;

import java.util.List;

public interface CHBLayingCalculator {

    CHBResult calculate(CHBInput input, List<DoorWindowInputBase> doors, List<DoorWindowInputBase> windows, List<RSBFactor> rsbFactorFactors);

    CHBLayingSummary generateSummary(List<CHBResult> results);

}
