package com.android.estimateapp.cleanarchitecture.presentation.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;

import java.text.DecimalFormat;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;


/**
 * Base Grade for the Activity inside the app.
 *
 * This will be responsible for setting layout and binding ButterKnife to our Activities
 */
public abstract class BaseActivity extends DaggerAppCompatActivity {

    /**
     * Abstract method for providing child Activity layouts
     *
     * @return id of the layout that will be rendered on the Activity
     */
    protected abstract int getLayoutResource();

    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getLayoutResource() != 0) {
            setContentView(getLayoutResource());
        }

        // ButterKnife binding for all the activities
        ButterKnife.bind(this);

        fragmentManager = getSupportFragmentManager();
    }

    protected void setToolbarTitle(int stringId){
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(stringId);
    }

    protected void setToolbarTitle(String title){
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(title);
    }


    /**
     * Show provided fragment in containerId. This the same as replacing the fragment in containerId
     * using {@link FragmentTransaction#replace(int, Fragment)}.
     * @param containerId containerId which will contain the fragment
     * @param fragment the fragment that will be displayed in the containerId
     * @param addToBackStack if true, setValue a backstack entry
     */
    protected void showFragment(int containerId, Fragment fragment, boolean addToBackStack) {
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerId, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }



    public ProgressDialog createLoadingAlert(String title, String message){
        ProgressDialog dialog = new ProgressDialog(this);
        if (title != null){
            dialog.setTitle(title);
        }

        dialog.setMessage(message);

        return dialog;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public AlertDialog createAlertDialog(String title, String message, String positiveBtnText, String negativeBtnText,
                                         DialogInterface.OnClickListener positiveOnClickListener,
                                         DialogInterface.OnClickListener negativeOnClickListener){
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (title != null){
            builder.setTitle(title);
        }
        builder.setMessage(message);
        if (positiveBtnText != null){
            builder.setPositiveButton(positiveBtnText,positiveOnClickListener);
        }

        if (negativeBtnText != null) {
            builder.setNegativeButton(negativeBtnText, negativeOnClickListener);
        }
        return builder.create();
    }

    protected AlertDialog createChooser(String title, String[] options, DialogInterface.OnClickListener onClickListener){
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (title != null){
            builder.setTitle(title);
        }
        builder.setItems(options, onClickListener);
        return builder.create();
    }

    public String toDisplayFormat(double val){
        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        return format.format(val);
    }
}
