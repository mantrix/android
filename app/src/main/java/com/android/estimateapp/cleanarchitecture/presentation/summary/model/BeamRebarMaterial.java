package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;

public class BeamRebarMaterial {

    public RsbSize rsbSize;

    public double topBars;

    public double bottomBars;

    public double extraBarLeft;

    public double extraBarRight;

    public double extraBarMid;

    public double splicings;

    public double hookes;

    public double getRebarTotal() {
        return topBars
                + bottomBars
                + extraBarLeft
                + extraBarRight
                + extraBarMid
                + splicings
                + hookes;
    }

    public void setRsbSize(RsbSize rsbSize) {
        this.rsbSize = rsbSize;
    }
}
