package com.android.estimateapp.cleanarchitecture.domain.usecase.works;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public class BaseCalculator {

    // BAG - round up to nearest whole number
    // CU.M - round up to nearest 1 decimal number
    // KG - half up. Can have 2 decimal number

    protected double roundHalfUp(double value) {
        int places = 2;
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    protected double roundDownToNearestWholeNumber(double value) {
        /*
         * This will round down to nearest 1 decimal number.
         */
        return  Math.floor(value);
    }

    protected double roundUpToNearestWholeNumber(double value) {
        /*
         * This will round up to nearest whole number.
         */
        return  Math.ceil(value);
    }

    protected double truncateToTwoDecimal(double val, RoundingMode roundingMode){
        /*
         * This will round up to nearest 2 decimal number.
         */
        DecimalFormat df = new DecimalFormat("#.##");

        df.setRoundingMode(roundingMode);

        return new Double(df.format(val));
    }

    protected double truncateToOneDecimal(double val, RoundingMode roundingMode){
        /*
         * This will round up to nearest 1 decimal number.
         */
        DecimalFormat df = new DecimalFormat("#.#");

        df.setRoundingMode(roundingMode);

        return new Double(df.format(val));
    }

    protected double toDoublePercentage(double input){
        return input / 100;
    }

    protected RSBFactor getRSB(List<RSBFactor> RSBFactors, int value) {
        for (RSBFactor rsbFactor : RSBFactors) {
            if (value == rsbFactor.getSize()) {
                return rsbFactor;
            }
        }
        return null;
    }

}
