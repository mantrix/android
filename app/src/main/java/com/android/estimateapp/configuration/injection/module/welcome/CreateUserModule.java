package com.android.estimateapp.configuration.injection.module.welcome;


import com.android.estimateapp.cleanarchitecture.domain.usecase.validator.InputValidator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.validator.InputValidatorUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserContract;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateUserModule {


    @Provides
    @PerFragment
    CreateUserContract.UserActionListener providesCreateUserPresenter(CreateUserPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerFragment
    InputValidatorUseCase provideInputValidator(InputValidator inputValidator){
        return inputValidator;
    }
}
