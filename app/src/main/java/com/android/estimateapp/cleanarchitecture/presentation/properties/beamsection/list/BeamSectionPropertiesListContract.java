package com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.list;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface BeamSectionPropertiesListContract {


    interface View {

        void displayList(List<BeamSectionProperties> inputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
