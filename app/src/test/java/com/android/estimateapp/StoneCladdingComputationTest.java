package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding.StoneCladdingCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingMaterial;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingSummary;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class StoneCladdingComputationTest {


    StoneCladdingCalculatorImpl stoneCladdingCalculator;

    @Before
    public void setup() {
        stoneCladdingCalculator = new StoneCladdingCalculatorImpl();
    }

    @Test
    public void stoneCladdingTest() {

        StoneCladdingFactor factor = new StoneCladdingFactor();
        factor.setSand(0.03);
        factor.setCement(0.15);
        factor.setAdhesive(0.2);

//        StoneType stoneType = new StoneType();
//        stoneType.setLength(40);
//        stoneType.setWidth(40);

        StoneCladdingInput input = new StoneCladdingInput();
        input.setSets(1);
        Dimension dimension = new Dimension();
        dimension.setLength(10);
        dimension.setWidth(2);
        input.setDimension(dimension);
        input.setOpenings(8);
        input.setStoneType(null);
        input.setWastage(10);

        StoneCladdingResult result = stoneCladdingCalculator.calculate(input,factor);

        System.out.println("Area: " + result.getArea());
        System.out.println("Net Area: " + result.getNetArea());
        if (result.getStoneType() != null){
            System.out.println("Size W1: " + result.getStoneType().getWidth());
            System.out.println("Size W2: " + result.getStoneType().getLength());
        }
        System.out.println("Size Area: " + result.getSizeArea());
        System.out.println("Stone Cladding: " + result.getStoneCladdingPcs());
        System.out.println("Sand: " + result.getSand());
        System.out.println("Cement: " + result.getCement());
        System.out.println("Adhesive: " + result.getAdhesive());
    }

    @Test
    public void generateSummaryTest(){

        StoneType stone1 = new StoneType();
        stone1.setName("Stone1");
        stone1.setWidth(40);
        stone1.setLength(40);

        StoneType stone2 = new StoneType();
        stone2.setName("Stone2");
        stone2.setWidth(40);
        stone2.setLength(40);


        StoneCladdingResult stoneCladdingResult1 = new StoneCladdingResult();
        stoneCladdingResult1.setStoneType(stone1);
        stoneCladdingResult1.setArea(12);
        stoneCladdingResult1.setStoneCladdingPcs(83);
        stoneCladdingResult1.setSand(0.396);
        stoneCladdingResult1.setCement(1.980);
        stoneCladdingResult1.setAdhesive(2.640);


        StoneCladdingResult stoneCladdingResult2 = new StoneCladdingResult();
        stoneCladdingResult2.setStoneType(stone1);
        stoneCladdingResult2.setArea(48);
        stoneCladdingResult2.setStoneCladdingPcs(315);
        stoneCladdingResult2.setSand(1.512);
        stoneCladdingResult2.setCement(7.560);
        stoneCladdingResult2.setAdhesive(10.080);

        StoneCladdingResult stoneCladdingResult3 = new StoneCladdingResult();
        stoneCladdingResult3.setStoneType(stone2);
        stoneCladdingResult3.setArea(52);
        stoneCladdingResult3.setStoneCladdingPcs(332);
        stoneCladdingResult3.setSand(1.591);
        stoneCladdingResult3.setCement(7.956);
        stoneCladdingResult3.setAdhesive(10.608);

        StoneCladdingResult stoneCladdingResult4 = new StoneCladdingResult();
        stoneCladdingResult4.setStoneType(stone2);
        stoneCladdingResult4.setArea(189);
        stoneCladdingResult4.setStoneCladdingPcs(1217);
        stoneCladdingResult4.setSand(2.141);
        stoneCladdingResult4.setCement(16.936);
        stoneCladdingResult4.setAdhesive(23.360);

        StoneCladdingSummary summary = stoneCladdingCalculator.generateSummary(Arrays.asList(stoneCladdingResult1,stoneCladdingResult2,stoneCladdingResult3,stoneCladdingResult4));

        for (String key : summary.getMaterialPerStoneType().keySet()){
            StoneCladdingMaterial data = summary.getMaterialPerStoneType().get(key);

            System.out.println("---------------------------------------------------------");

            System.out.println("Stone Type: " + data.stoneType.getName());
            System.out.println("Area: " + data.area);
            System.out.println("Quantity: " + data.quantity);
            System.out.println("Sand: " + data.sand);
            System.out.println("Cement: " + data.cement);
            System.out.println("Adhesive: " + data.adhesive);
        }

        System.out.println("---------------------------------------------------------");

        StoneCladdingMaterial data = summary.getTotal();
        System.out.println("Total Sand: " + data.sand);
        System.out.println("Total Cement: " + data.cement);
        System.out.println("Total Adhesive: " + data.adhesive);
    }

}
