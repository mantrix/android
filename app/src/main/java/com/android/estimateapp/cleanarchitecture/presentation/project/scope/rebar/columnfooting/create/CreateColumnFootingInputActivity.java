package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.create;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateColumnFootingInputActivity extends CreateScopeInputBaseActivity implements CreateColumnFootingRebarInputContract.View {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_description)
    TextInputEditText etDescription;
    @BindView(R.id.til_description)
    TextInputLayout tilDescription;
    @BindView(R.id.et_sets)
    TextInputEditText etSets;
    @BindView(R.id.til_sets)
    TextInputLayout tilSets;
    @BindView(R.id.et_bars_at_l)
    TextInputEditText etBarsAtL;
    @BindView(R.id.til_bars_at_l)
    TextInputLayout tilBarsAtL;
    @BindView(R.id.et_diameter_at_l)
    TextInputEditText etDiameterAtL;
    @BindView(R.id.til_diameter_at_l)
    TextInputLayout tilDiameterAtL;
    @BindView(R.id.et_length_at_l)
    TextInputEditText etLengthAtL;
    @BindView(R.id.til_length_at_l)
    TextInputLayout tilLengthAtL;
    @BindView(R.id.et_bars_at_w)
    TextInputEditText etBarsAtW;
    @BindView(R.id.til_bars_at_w)
    TextInputLayout tilBarsAtW;
    @BindView(R.id.et_diameter_at_w)
    TextInputEditText etDiameterAtW;
    @BindView(R.id.til_diameter_at_w)
    TextInputLayout tilDiameterAtW;
    @BindView(R.id.et_length_at_w)
    TextInputEditText etLengthAtW;
    @BindView(R.id.til_length_at_w)
    TextInputLayout tilLengthAtW;
    @BindView(R.id.et_tie_wire)
    TextInputEditText etTieWire;
    @BindView(R.id.til_tie_wire)
    TextInputLayout tilTieWire;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.tv_qty_of_l)
    TextView tvQtyOfL;
    @BindView(R.id.tv_qty_w)
    TextView tvQtyW;
    @BindView(R.id.tv_ten_rsb_label)
    TextView tvTenRsbLabel;
    @BindView(R.id.tv_ten_rsb_result)
    TextView tvTenRsbResult;
    @BindView(R.id.tv_twelve_rsb_label)
    TextView tvTwelveRsbLabel;
    @BindView(R.id.tv_twelve_rsb_result)
    TextView tvTwelveRsbResult;
    @BindView(R.id.tv_sixteen_rsb_label)
    TextView tvSixteenRsbLabel;
    @BindView(R.id.tv_sixteen_rsb_result)
    TextView tvSixteenRsbResult;
    @BindView(R.id.tv_twenty_rsb_label)
    TextView tvTwentyRsbLabel;
    @BindView(R.id.tv_twenty_rsb_result)
    TextView tvTwentyRsbResult;
    @BindView(R.id.tv_twenty_five_rsb_label)
    TextView tvTwentyFiveRsbLabel;
    @BindView(R.id.tv_twenty_five_rsb_result)
    TextView tvTwentyFiveRsbResult;
    @BindView(R.id.tv_g1)
    TextView tvG1;
    @BindView(R.id.cv_results)
    CardView cvResults;

    @Inject
    CreateColumnFootingRebarInputContract.UserActionListener presenter;

    private String projectId;
    private String scopeKey;
    private String itemKey;

    private ColumnFootingRebarInput input;

    private ProgressDialog saveProgressDialog;

    private boolean updateContent;

    AlertDialog rsbSizePickerDialog;

    boolean isDiameterAtL;

    public static Intent createIntent(Context context, String projectId, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateColumnFootingInputActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.create_column_footing_rebar_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.WALL_FOOTING_REBAR.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        input = new ColumnFootingRebarInput();

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();

        tvTenRsbLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 10));
        tvTwelveRsbLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 12));
        tvSixteenRsbLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 16));
        tvTwentyRsbLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 20));
        tvTwentyFiveRsbLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 25));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.COLUMN_FOOTING_REBAR.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }

    @Override
    public void setRSBPickerOptions(List<RsbSize> sizes) {
        String[] options = new String[sizes.size()];
        for (int i = 0; i < sizes.size(); i++) {
            options[i] = sizes.get(i).toString();
        }

        rsbSizePickerDialog = createChooser(getString(R.string.rsb_label), options, (dialog, which) -> {

            if (isDiameterAtL) {
                etDiameterAtL.setText(options[which]);
                input.setDiameterAtL(sizes.get(which).getValue());
            } else {
                etDiameterAtW.setText(options[which]);
                input.setDiameterAtW(sizes.get(which).getValue());
            }

            dialog.dismiss();
        });
    }

    @OnClick(R.id.et_diameter_at_l)
    protected void diameterAtLClick() {
        if (rsbSizePickerDialog != null) {
            isDiameterAtL = true;
            rsbSizePickerDialog.setTitle(getString(R.string.diameter_at_l));
            rsbSizePickerDialog.show();
        }
    }

    @OnClick(R.id.et_diameter_at_w)
    protected void diameterAtWClick() {
        if (rsbSizePickerDialog != null) {
            isDiameterAtL = false;
            rsbSizePickerDialog.setTitle(getString(R.string.diameter_at_w));
            rsbSizePickerDialog.show();
        }
    }


    @Override
    public void setInput(ColumnFootingRebarInput input) {
        this.input = input;
        if (input.getCode() != null) {
            etDescription.setText(input.getCode());
        }

        if (input.getSets() != 0) {
            etSets.setText(toDisplayFormat(input.getSets()));
        }

        // L
        if (input.getNumberOfBarsAtL() != 0) {
            etBarsAtL.setText(toDisplayFormat(input.getNumberOfBarsAtL()));
        }

        if (input.getLengthAtL() != 0) {
            etLengthAtL.setText(toDisplayFormat(input.getLengthAtL()));
        }

        if (input.getDiameterAtL() != 0) {
            etDiameterAtL.setText(toDisplayFormat(input.getDiameterAtL()));
        }

        // W
        if (input.getNumberOfBarsAtW() != 0) {
            etBarsAtW.setText(toDisplayFormat(input.getNumberOfBarsAtW()));
        }

        if (input.getLengthAtW() != 0) {
            etLengthAtW.setText(toDisplayFormat(input.getLengthAtW()));
        }

        if (input.getDiameterAtW() != 0) {
            etDiameterAtW.setText(toDisplayFormat(input.getDiameterAtW()));
        }

        if (input.getLengthOfTieWire() != 0) {
            etTieWire.setText(toDisplayFormat(input.getLengthOfTieWire()));
        }

    }

    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayResult(ColumnFootingRebarResult result) {

        tvQtyOfL.setText(String.format(getString(R.string.pcs_result), toDisplayFormat(result.getQtyOfL())));
        tvQtyW.setText(String.format(getString(R.string.pcs_result), toDisplayFormat(result.getQtyOfW())));
        tvTenRsbResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getTenRsb())));
        tvTwelveRsbResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getTwelveRsb())));
        tvSixteenRsbResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getSixteenRsb())));
        tvTwentyRsbResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getTwentyRsb())));
        tvTwentyFiveRsbResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getTwentyFiveRsb())));
        tvG1.setText(String.format(getString(R.string.kg_result), toDisplayFormat(result.getSixteenGiTieWire())));

        cvResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }


    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setCode(etDescription.getText().toString());

        String sets = etSets.getText().toString();
        if (sets.isEmpty()) {
            input.setSets(0);
        } else {
            input.setSets(Integer.parseInt(sets));
        }

        String barsAtL = etBarsAtL.getText().toString();
        if (barsAtL.isEmpty()) {
            input.setNumberOfBarsAtL(0);
        } else {
            input.setNumberOfBarsAtL(Integer.parseInt(barsAtL));
        }

        String lengthAtL = etLengthAtL.getText().toString();
        if (lengthAtL.isEmpty()) {
            input.setLengthAtL(0);
        } else {
            input.setLengthAtL(Double.parseDouble(lengthAtL));
        }


        String barsAtW = etBarsAtW.getText().toString();
        if (barsAtW.isEmpty()) {
            input.setNumberOfBarsAtW(0);
        } else {
            input.setNumberOfBarsAtW(Integer.parseInt(barsAtW));
        }

        String lengthAtW = etLengthAtW.getText().toString();
        if (lengthAtW.isEmpty()) {
            input.setLengthAtW(0);
        } else {
            input.setLengthAtW(Double.parseDouble(lengthAtW));
        }

        String tieWire = etTieWire.getText().toString();
        if (tieWire.isEmpty()) {
            input.setLengthOfTieWire(0);
        } else {
            input.setLengthOfTieWire(Double.parseDouble(tieWire));
        }

        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etDescription.getText().toString().isEmpty()) {
            etDescription.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etDescription.setError(null);
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
