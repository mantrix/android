package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.chb.CHBLayingMaterialCostContract;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.chb.CHBLayingMaterialCostPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class CHBLayingMaterialCostDialogModule {

    @Provides
    @PerFragment
    CHBLayingMaterialCostContract.UserActionListener provideCHBLayingMaterialPresenter(CHBLayingMaterialCostPresenter presenter) {
        return presenter;
    }
}
