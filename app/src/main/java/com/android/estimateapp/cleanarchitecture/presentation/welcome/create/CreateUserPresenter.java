package com.android.estimateapp.cleanarchitecture.presentation.welcome.create;

import androidx.annotation.Nullable;

import com.android.estimateapp.cleanarchitecture.data.entity.CreateUserPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.DataPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.NamePayload;
import com.android.estimateapp.cleanarchitecture.data.entity.SignupPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.SubscriptionPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.UserPayload;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.AccountRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.impl.DeviceIDFactory;
import com.android.estimateapp.cleanarchitecture.domain.usecase.validator.InputValidatorUseCase;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class CreateUserPresenter implements CreateUserContract.UserActionListener {

    CreateUserContract.View view;

    private AccountRepository accountRepository;
    private ThreadExecutorProvider threadExecutorProvider;
    private PostExecutionThread postExecutionThread;
    private DeviceIDFactory deviceIDFactory;
    private CreateUserContract.RegistrationMode mode;
    private InputValidatorUseCase inputValidator;

    private static final String PH_MOBILE_NO_REGEX = "^(09|\\+639|639)\\d{9}$";
    private static final String PASSWORD_REGEX = "^.{6,20}$";



    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public CreateUserPresenter(AccountRepository accountRepository,
                               DeviceIDFactory deviceIDFactory,
                               InputValidatorUseCase inputValidator,
                               ThreadExecutorProvider threadExecutorProvider,
                               PostExecutionThread postExecutionThread) {
        this.accountRepository = accountRepository;
        this.deviceIDFactory = deviceIDFactory;
        this.inputValidator = inputValidator;
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }

    @Override
    public void setView(CreateUserContract.View view) {
        this.view = view;
    }

    @Override
    public void setMode(CreateUserContract.RegistrationMode mode) {
        this.mode = mode;
    }

    @Override
    public void start() {
        if (mode == CreateUserContract.RegistrationMode.EMAIL){
            view.showEmailMode();
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void create(@Nullable String uid,
                       String email,
                       String mobileNum,
                       String firstName,
                       String middleName,
                       String lastName,
                       @Nullable String password) {

        boolean register = true;

        if (firstName.isEmpty()) {
            register = false;

            view.invalidFirstName();
        }

        if (middleName.isEmpty()) {
            register = false;

            view.invalidMiddleName();
        }

        if (lastName.isEmpty()) {
            register = false;

            view.invalidLastName();
        }

        if (mobileNum.isEmpty() || !mobileNum.matches(PH_MOBILE_NO_REGEX)){
            register = false;

            view.invalidPhoneNumber();
        }

        if (mode == CreateUserContract.RegistrationMode.EMAIL){
            if (!inputValidator.isValidEmail(email)){
                register = false;

                view.invalidEmail();
            }

            if (password != null && !password.matches(PASSWORD_REGEX)){
                register = false;

                view.invalidPassword();
            }
        }

        if (register) {
            view.startLoading();

            NamePayload namePayload = new NamePayload(firstName, middleName, lastName);

            SubscriptionPayload subscriptionPayload = new SubscriptionPayload();
            subscriptionPayload.setPlanName("free");
            subscriptionPayload.setPaid(false);

            if (mode == CreateUserContract.RegistrationMode.GOOGLE){
                UserPayload userPayload = new UserPayload();
                userPayload.setUid(uid);

                DataPayload dataPayload = new DataPayload();
                dataPayload.setEmail(email);
                dataPayload.setMobileNum(mobileNum);
                dataPayload.setNameEntity(namePayload);
                dataPayload.setSubscriptionPayload(subscriptionPayload);

                CreateUserPayload payload = new CreateUserPayload(uid,userPayload, dataPayload);

                compositeDisposable.add(accountRepository.createUser(payload).flatMap(accountCredential -> accountRepository
                        .authorizeUser(accountCredential.getToken(),deviceIDFactory.getDeviceId())
                        .subscribeOn(threadExecutorProvider.computationScheduler()))
                        .subscribeOn(threadExecutorProvider.computationScheduler())
                        .observeOn(postExecutionThread.getScheduler())
                        .subscribe( authorize -> {
                            view.stopLoading();
                            view.registered();
                        }, throwable -> {
                            throwable.printStackTrace();
                            view.stopLoading();
                            view.registrationFailed();
                        }));
            } else {
                SignupPayload signupPayload = new SignupPayload(email, password, mobileNum);
                signupPayload.setNameEntity(namePayload);
                signupPayload.setSubscriptionPayload(subscriptionPayload);

                compositeDisposable.add(accountRepository.registerUser(signupPayload).flatMap(accountCredential -> accountRepository
                        .authorizeUser(accountCredential.getToken(),deviceIDFactory.getDeviceId())
                        .subscribeOn(threadExecutorProvider.computationScheduler()))
                        .subscribeOn(threadExecutorProvider.computationScheduler())
                        .observeOn(postExecutionThread.getScheduler())
                        .subscribe( authorize -> {
                            view.stopLoading();
                            view.registered();
                        }, throwable -> {
                            throwable.printStackTrace();
                            view.stopLoading();
                            view.registrationFailed();
                        }));
            }

        }
    }




}
