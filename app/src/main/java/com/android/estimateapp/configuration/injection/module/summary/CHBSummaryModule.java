package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying.CHBLayingCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying.CHBLayingCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.CHBSummaryContract;
import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.CHBSummaryPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class CHBSummaryModule {

    @Provides
    @PerFragment
    CHBLayingCalculator provideChbLayingCalculator(CHBLayingCalculatorImpl calculator) {
        return calculator;
    }

    @Provides
    @PerFragment
    CHBSummaryContract.UserActionListener provideCHBSummaryPresenter(CHBSummaryPresenter presenter) {
        return presenter;
    }
}
