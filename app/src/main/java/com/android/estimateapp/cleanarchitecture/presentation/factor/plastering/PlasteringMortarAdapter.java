package com.android.estimateapp.cleanarchitecture.presentation.factor.plastering;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.EdittextBaseOutputListener;
import com.android.estimateapp.utils.DisplayUtil;
import com.android.estimateapp.utils.NumberUtils;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PlasteringMortarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<PlasteringMortarFactor>> {


    private List<PlasteringMortarFactor> list;

    @Inject
    public PlasteringMortarAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.plastering_mortar_factor_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<PlasteringMortarFactor> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<PlasteringMortarFactor> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    public List<PlasteringMortarFactor> getData() {
        return list;
    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_mortar_class)
        TextView tvMortarClass;

        @BindView(R.id.et_cement)
        EditText etCement;

        @BindView(R.id.et_sand)
        EditText etSand;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(PlasteringMortarFactor factor, int position) {

            Grade grade = Grade.parseInt(factor.getMortarClass());
            tvMortarClass.setText(grade.toString());
            etCement.setText(DisplayUtil.toPlainString(factor.getCement()));
            etSand.setText(DisplayUtil.toPlainString(factor.getSand()));


            etCement.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etCement.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setCement(Double.parseDouble(etCement.getText().toString()));
                    }
                }

            });

            etSand.addTextChangedListener(new EdittextBaseOutputListener() {

                @Override
                public void afterTextChanged(Editable s) {

                    String input = etSand.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setSand(Double.parseDouble(etSand.getText().toString()));
                    }
                }
            });
        }
    }


}
