package com.android.estimateapp.utils;

import java.text.DecimalFormat;

public class DisplayUtil {

    public static String toDisplayFormat(double val){
        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        return format.format(val);
    }

    public static String toPlainString(double val){
        DecimalFormat df = new DecimalFormat("0");
        df.setMaximumFractionDigits(340);
        return df.format(val);
    }
}
