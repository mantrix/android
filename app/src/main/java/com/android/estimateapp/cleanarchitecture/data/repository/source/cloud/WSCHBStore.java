package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSizeFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudCHBStore;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@Singleton
public class WSCHBStore extends BaseStore implements CloudCHBStore {

    private DatabaseReference projectScopeOfWorksRef;

    @Inject
    public WSCHBStore() {
        super();
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
    }


    @Override
    public Observable<CHBFactor> createCHBFactor(String projectId, CHBFactor chbFactor) {

        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_LAYING_FACTOR);
        }).flatMap(databaseReference -> setValue(databaseReference, chbFactor))
                .flatMap(chbFactorResult -> {

                    Observable<List<CHBMortarClass>> creationOfMortarClassObs = Observable.fromIterable(chbFactor.getConcreteMortarClasses())
                            .flatMap(chbMortarClass -> {

                                return createCHBMortarClasses(projectId, chbMortarClass)
                                        .flatMap(chbMortarClassResult -> {
                                            final String mortarClassKey = chbMortarClassResult.getKey();
                                            return Observable.fromIterable(chbMortarClass.getChbSizeFactors())
                                                    .flatMap(chbSizeFactor -> createCHBSize(projectId, mortarClassKey, chbSizeFactor))
                                                    .toList()
                                                    .map(chbSizeFactors -> {
                                                        chbMortarClassResult.setChbSizeFactors(chbSizeFactors);
                                                        return chbMortarClassResult;
                                                    }).toObservable();
                                        }).toList().toObservable();
                            }).subscribeOn(Schedulers.computation());


                    Observable<List<VerticalSpacing>> verticalListObservable = Observable.fromIterable(chbFactor.getChbSpacing().getVerticalSpacings())
                            .flatMap(verticalSpacing -> createCHBVerticalSpacingObservable(projectId, verticalSpacing))
                            .toList().toObservable()
                            .subscribeOn(Schedulers.computation());

                    Observable<List<HorizontalSpacing>> horizontalListObservable = Observable.fromIterable(chbFactor.getChbSpacing().getHorizontalSpacings())
                            .flatMap(horizontalSpacing -> createCHBHorizontalSpacingObservable(projectId, horizontalSpacing))
                            .toList().toObservable()
                            .subscribeOn(Schedulers.computation());

                    Observable<List<TieWireRebarSpacingFactor>> tieWireRebarSpacingFactorObservable = Observable.fromIterable(chbFactor.getChbSpacing().getTieWireRebarSpacingFactors())
                            .flatMap(tieWireRebarSpacingFactor -> createCHBTieWireRebarSpacingFactorObservable(projectId, tieWireRebarSpacingFactor))
                            .toList().toObservable()
                            .subscribeOn(Schedulers.computation());

                    return Observable.zip(creationOfMortarClassObs, verticalListObservable, horizontalListObservable, tieWireRebarSpacingFactorObservable,
                            (chbMortarClasses, verticalSpacings, horizontalSpacings, tieWireRebarSpacingFactors) -> {
                                CHBSpacing chbSpacing = new CHBSpacing();
                                chbSpacing.setVerticalSpacings(verticalSpacings);
                                chbSpacing.setHorizontalSpacings(horizontalSpacings);
                                chbSpacing.setTieWireRebarSpacingFactors(tieWireRebarSpacingFactors);

                                chbFactorResult.setChbSpacing(chbSpacing);
                                chbFactorResult.setConcreteMortarClasses(chbMortarClasses);
                                return chbFactorResult;
                            });
                });

    }

    private Observable<CHBMortarClass> createCHBMortarClasses(String projectId, CHBMortarClass chbMortarClass) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_LAYING_FACTOR)
                    .child(FIREBASE_CHB_CONCRETE_MORTAR_CLASSES);

        }).flatMap(databaseReference -> push(databaseReference, chbMortarClass));
    }

    private Observable<CHBSizeFactor> createCHBSize(String projectId, String mortarClassId, CHBSizeFactor chbSizeFactor) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_LAYING_FACTOR)
                    .child(FIREBASE_CHB_CONCRETE_MORTAR_CLASSES)
                    .child(mortarClassId);
        }).flatMap(databaseReference -> push(databaseReference, chbSizeFactor));
    }

    private Observable<VerticalSpacing> createCHBVerticalSpacingObservable(String projectId, VerticalSpacing verticalSpacing) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_LAYING_FACTOR)
                    .child(FIREBASE_CHB_SPACING)
                    .child(FIREBASE_CHB_VERTICAL);
        }).flatMap(databaseReference -> push(databaseReference, verticalSpacing));
    }

    private Observable<HorizontalSpacing> createCHBHorizontalSpacingObservable(String projectId, HorizontalSpacing horizontalSpacing) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_LAYING_FACTOR)
                    .child(FIREBASE_CHB_SPACING)
                    .child(FIREBASE_CHB_HORIZONTAL);
        }).flatMap(databaseReference -> push(databaseReference, horizontalSpacing));
    }

    private Observable<TieWireRebarSpacingFactor> createCHBTieWireRebarSpacingFactorObservable(String projectId, TieWireRebarSpacingFactor tieWireRebarSpacingFactor) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_LAYING_FACTOR)
                    .child(FIREBASE_CHB_SPACING)
                    .child(FIREBASE_CHB_TIE_WIRE_REBAR_SPACING);
        }).flatMap(databaseReference -> push(databaseReference, tieWireRebarSpacingFactor));
    }

    private static final String TAG = WSCHBStore.class.getSimpleName();

    @Override
    public Observable<CHBFactor> getCHBFactor(String projectId) {
        return Observable.create(emitter -> {

            Query query = projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_LAYING_FACTOR);

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot chbFactorDataSnapshot) {
                    if (chbFactorDataSnapshot.exists()) {
                        CHBFactor chbFactor = chbFactorDataSnapshot.getValue(CHBFactor.class);
                        chbFactor.setKey(chbFactorDataSnapshot.getKey());


                        List<CHBMortarClass> chbMortarClasses = new ArrayList<>();
                        DataSnapshot concreteClassesSnapshot = chbFactorDataSnapshot.child(FIREBASE_CHB_CONCRETE_MORTAR_CLASSES);

                        for (DataSnapshot snapshot : concreteClassesSnapshot.getChildren()) {
                            CHBMortarClass chbMortarClass = snapshot.getValue(CHBMortarClass.class);
                            chbMortarClass.setKey(snapshot.getKey());


                            List<CHBSizeFactor> chbSizeFactors = new ArrayList<>();
                            for (DataSnapshot chbSizeSnapshot : snapshot.getChildren()) {
                                if (!chbSizeSnapshot.getKey().equalsIgnoreCase("mortarClass")) {
                                    CHBSizeFactor chbSizeFactor = chbSizeSnapshot.getValue(CHBSizeFactor.class);
                                    chbSizeFactor.setKey(chbSizeSnapshot.getKey());

                                    chbSizeFactors.add(chbSizeFactor);
                                }
                            }

                            chbMortarClass.setChbSizeFactors(chbSizeFactors);
                            chbMortarClasses.add(chbMortarClass);

                        }

                        DataSnapshot spacingSnapshot = chbFactorDataSnapshot.child(FIREBASE_CHB_SPACING);

                        DataSnapshot horizontalSnapshot = spacingSnapshot.child(FIREBASE_CHB_HORIZONTAL);
                        List<HorizontalSpacing> horizontalSpacings = new ArrayList<>();
                        for (DataSnapshot snapshot : horizontalSnapshot.getChildren()) {
                            HorizontalSpacing data = snapshot.getValue(HorizontalSpacing.class);
                            data.setKey(snapshot.getKey());

                            horizontalSpacings.add(data);
                        }

                        DataSnapshot verticalSnapshot = spacingSnapshot.child(FIREBASE_CHB_VERTICAL);
                        List<VerticalSpacing> verticalSpacings = new ArrayList<>();
                        for (DataSnapshot snapshot : verticalSnapshot.getChildren()) {
                            VerticalSpacing data = snapshot.getValue(VerticalSpacing.class);
                            data.setKey(snapshot.getKey());

                            verticalSpacings.add(data);
                        }

                        DataSnapshot tieWireSnapshot = spacingSnapshot.child(FIREBASE_CHB_TIE_WIRE_REBAR_SPACING);
                        List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors = new ArrayList<>();
                        for (DataSnapshot snapshot : tieWireSnapshot.getChildren()) {
                            TieWireRebarSpacingFactor data = snapshot.getValue(TieWireRebarSpacingFactor.class);
                            data.setKey(snapshot.getKey());

                            tieWireRebarSpacingFactors.add(data);
                        }

                        chbFactor.setConcreteMortarClasses(chbMortarClasses);
                        CHBSpacing spacing = new CHBSpacing();
                        spacing.setHorizontalSpacings(horizontalSpacings);
                        spacing.setVerticalSpacings(verticalSpacings);
                        spacing.setTieWireRebarSpacingFactors(tieWireRebarSpacingFactors);
                        chbFactor.setChbSpacing(spacing);
                        emitter.onNext(chbFactor);
                    }
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            });
        });
    }


    private Observable<CHBFactor> updateCHBFactor(String projectId, CHBFactor chbFactor) {
        Observable<CHBFactor> chbFactorObservable = updateCHB(projectId, chbFactor);
        Observable<List<CHBMortarClass>> chbMortarClassObservable = updateCHBMortarClasses(projectId, chbFactor.getCHB(), chbFactor.getConcreteMortarClasses());
        Observable<List<VerticalSpacing>> verticalSpacingObservable = updateCHBVerticalSpacingObservable(projectId, chbFactor.getChbSpacing().getVerticalSpacings());
        Observable<List<HorizontalSpacing>> horizontalSpacingObservable = updateCHBHorizontalSpacingObservable(projectId, chbFactor.getChbSpacing().getHorizontalSpacings());
        Observable<List<TieWireRebarSpacingFactor>> tieWireRebarSpacingFactorObservable = updateCHBTieWireRebarSpacingFactorObservable(projectId, chbFactor.getChbSpacing().getTieWireRebarSpacingFactors());

        return Observable.zip(chbFactorObservable, chbMortarClassObservable, verticalSpacingObservable, horizontalSpacingObservable, tieWireRebarSpacingFactorObservable,
                (chbFactor1, chbMortarClasses, verticalSpacings, horizontalSpacings, tieWireRebarSpacingFactors) -> {
                    CHBSpacing chbSpacing = new CHBSpacing();
                    chbSpacing.setVerticalSpacings(verticalSpacings);
                    chbSpacing.setHorizontalSpacings(horizontalSpacings);
                    chbSpacing.setTieWireRebarSpacingFactors(tieWireRebarSpacingFactors);

                    chbFactor.setChbSpacing(chbSpacing);
                    chbFactor.setConcreteMortarClasses(chbMortarClasses);
                    return chbFactor;
                });
    }

    public Observable<CHBFactor> updateCHB(String projectId, CHBFactor chbFactor) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_LAYING_FACTOR);

        }).flatMap(chbRef -> setValue(chbRef, chbFactor));
    }


    @Override
    public Observable<List<CHBMortarClass>> updateCHBMortarClasses(String projectId, double chb, List<CHBMortarClass> chbMortarClasses) {

        DatabaseReference chbLayingFactorRef = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_CHB_LAYING_FACTOR);

        DatabaseReference mortartRefs = chbLayingFactorRef
                .child(FIREBASE_CHB_CONCRETE_MORTAR_CLASSES);

        /*
         * Set chb size value then update the chb mortar classes.
         */
        return Completable.fromCallable(() -> {
            chbLayingFactorRef
                    .child("chb")
                    .setValue(chb);
            return Model.IRRELEVANT_VALUE;
        }).andThen(Observable.fromIterable(chbMortarClasses)
                .flatMap(chbMortarClass -> {
                            DatabaseReference mortartRef = mortartRefs.child(chbMortarClass.getKey());
                            return setValue(mortartRef, chbMortarClass)
                                    .flatMap(updatedClass -> Observable.fromIterable(updatedClass.getChbSizeFactors()))
                                    .flatMap(chbSizeFactor -> setValue(mortartRef.child(chbSizeFactor.getKey()), chbSizeFactor))
                                    .toList().map(chbSizeFactors -> {
                                        chbMortarClass.setChbSizeFactors(chbSizeFactors);
                                        return chbMortarClass;
                                    }).toObservable();
                        }
                ).toList().toObservable());
    }

    @Override
    public Observable<List<VerticalSpacing>> updateCHBVerticalSpacingObservable(String projectId, List<VerticalSpacing> verticalSpacings) {
        DatabaseReference verticalSpacingRefs = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_CHB_LAYING_FACTOR)
                .child(FIREBASE_CHB_SPACING)
                .child(FIREBASE_CHB_VERTICAL);
        return Observable.fromIterable(verticalSpacings)
                .flatMap(verticalSpacing -> setValue(verticalSpacingRefs.child(verticalSpacing.getKey()), verticalSpacing))
                .toList().toObservable();
    }

    @Override
    public Observable<List<HorizontalSpacing>> updateCHBHorizontalSpacingObservable(String projectId, List<HorizontalSpacing> horizontalSpacings) {
        DatabaseReference horizontalSpacingRefs = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_CHB_LAYING_FACTOR)
                .child(FIREBASE_CHB_SPACING)
                .child(FIREBASE_CHB_HORIZONTAL);

        return Observable.fromIterable(horizontalSpacings)
                .flatMap(horizontalSpacing -> setValue(horizontalSpacingRefs.child(horizontalSpacing.getKey()), horizontalSpacing))
                .toList().toObservable();
    }

    @Override
    public Observable<List<TieWireRebarSpacingFactor>> updateCHBTieWireRebarSpacingFactorObservable(String projectId, List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors) {
        DatabaseReference tieWireRefs = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_CHB_LAYING_FACTOR)
                .child(FIREBASE_CHB_SPACING)
                .child(FIREBASE_CHB_TIE_WIRE_REBAR_SPACING);
        return Observable.fromIterable(tieWireRebarSpacingFactors)
                .flatMap(tieWireRebarSpacingFactor -> setValue(tieWireRefs.child(tieWireRebarSpacingFactor.getKey()), tieWireRebarSpacingFactor))
                .toList().toObservable();
    }


    @Override
    public Observable<CHBInput> createCHBInput(String projectId, String scopeOfWorkId, CHBInput chbInput) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(databaseReference -> push(databaseReference, chbInput));
    }

    @Override
    public Observable<CHBInput> updateCHBInput(String projectId, String scopeOfWorkId, String itemId, CHBInput chbInput) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference, chbInput));
    }

    @Override
    public Observable<CHBResult> saveCHBInputResult(String projectId, String scopeOfWorkId, String itemId, CHBResult result) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId)
                    .child(FIREBASE_RESULT);
        }).flatMap(databaseReference -> setValue(databaseReference, result));
    }

    @Override
    public Observable<List<CHBInput>> getCHBInputs(String projectId, String scopeOfWorkId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(query -> queryListValue(query, CHBInput.class));
    }

    @Override
    public Observable<CHBInput> getCHBInput(String projectId, String scopeOfWorkId, String itemId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(query -> get(query, CHBInput.class));
    }
}
