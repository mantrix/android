package com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.tiletype;

import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface TileTypeFactorContract {

    interface View {

        void displayList(List<TileType> tileTypes);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(List<TileType> tileTypes);

        void createTileType(String name, double w1, double w2);
    }
}
