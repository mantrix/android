package com.android.estimateapp.cleanarchitecture.presentation.launch;

import android.util.Log;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.AccountRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.UserSessionRepository;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class LaunchScreenPresenter implements LaunchScreenContract.UserActionListener {
    private static final String TAG = LaunchScreenPresenter.class.getSimpleName();

    private LaunchScreenContract.View view;
    private AccountRepository accountRepository;
    private UserSessionRepository userSessionRepository;
    private ThreadExecutorProvider threadExecutorProvider;
    private PostExecutionThread postExecutionThread;

    @Inject
    public LaunchScreenPresenter(AccountRepository accountRepository,
                                 UserSessionRepository userSessionRepository,
                                 ThreadExecutorProvider threadExecutorProvider,
                                 PostExecutionThread postExecutionThread){
        this.accountRepository = accountRepository;
        this.userSessionRepository = userSessionRepository;
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }

    @Override
    public void setView(LaunchScreenContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {

        accountRepository.hasActiveUser()
                .subscribeOn(threadExecutorProvider.ioScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(hasActiveuser -> {
                    userSessionRepository.setHasActiveUser(hasActiveuser);
                    view.navigateToWelcomeScreen();

                    setActiveAccountToUserSession();
                },throwable -> {
                    throwable.printStackTrace();
                    view.navigateToWelcomeScreen();
                });
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void setActiveAccountToUserSession(){
        accountRepository.getLoggedInUser()
                .observeOn(threadExecutorProvider.ioScheduler())
                .subscribe(account -> {
                    Log.d(TAG, "Active User Name: " + account.getName());
                    userSessionRepository.setCurrentActiveUser(account);
                },Throwable::printStackTrace);

    }
}
