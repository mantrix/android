package com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.works.StirrupsOutput;

public class ColumnRebarOutput extends Model {

    double lClear;

    RSBValuePair rebar1;
    RSBValuePair rebar2;

    RSBValuePair rebarBendAtFooting1;
    RSBValuePair rebarBendAtFooting2;

    RSBValuePair rebarBendAtColumnEnd1;
    RSBValuePair rebarBendAtColumnEnd2;

    RSBValuePair possibleSplicing1;
    RSBValuePair possibleSplicing2;

    StirrupsOutput stirrupsOutput;

    public ColumnRebarOutput(){

    }

    public double getlClear() {
        return lClear;
    }

    public void setlClear(double lClear) {
        this.lClear = lClear;
    }

    public RSBValuePair getRebar1() {
        return rebar1;
    }

    public void setRebar1(RSBValuePair rebar1) {
        this.rebar1 = rebar1;
    }

    public RSBValuePair getRebar2() {
        return rebar2;
    }

    public void setRebar2(RSBValuePair rebar2) {
        this.rebar2 = rebar2;
    }

    public RSBValuePair getRebarBendAtFooting1() {
        return rebarBendAtFooting1;
    }

    public void setRebarBendAtFooting1(RSBValuePair rebarBendAtFooting1) {
        this.rebarBendAtFooting1 = rebarBendAtFooting1;
    }

    public RSBValuePair getRebarBendAtFooting2() {
        return rebarBendAtFooting2;
    }

    public void setRebarBendAtFooting2(RSBValuePair rebarBendAtFooting2) {
        this.rebarBendAtFooting2 = rebarBendAtFooting2;
    }

    public RSBValuePair getRebarBendAtColumnEnd1() {
        return rebarBendAtColumnEnd1;
    }

    public void setRebarBendAtColumnEnd1(RSBValuePair rebarBendAtColumnEnd1) {
        this.rebarBendAtColumnEnd1 = rebarBendAtColumnEnd1;
    }

    public RSBValuePair getRebarBendAtColumnEnd2() {
        return rebarBendAtColumnEnd2;
    }

    public void setRebarBendAtColumnEnd2(RSBValuePair rebarBendAtColumnEnd2) {
        this.rebarBendAtColumnEnd2 = rebarBendAtColumnEnd2;
    }

    public RSBValuePair getPossibleSplicing1() {
        return possibleSplicing1;
    }

    public void setPossibleSplicing1(RSBValuePair possibleSplicing1) {
        this.possibleSplicing1 = possibleSplicing1;
    }

    public RSBValuePair getPossibleSplicing2() {
        return possibleSplicing2;
    }

    public void setPossibleSplicing2(RSBValuePair possibleSplicing2) {
        this.possibleSplicing2 = possibleSplicing2;
    }

    public StirrupsOutput getStirrupsOutput() {
        return stirrupsOutput;
    }

    public void setStirrupsOutput(StirrupsOutput stirrupsOutput) {
        this.stirrupsOutput = stirrupsOutput;
    }
}