package com.android.estimateapp.cleanarchitecture.domain.repository;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;

import java.util.List;

import io.reactivex.Observable;

public interface ProjectPropertiesRepository {

    Observable<MemberSectionProperty> createMemberSectionProperty(String projectId, MemberSectionProperty memberSectionProperty);

    Observable<List<MemberSectionProperty>> getMemberSectionProperties(String projectId);

    Observable<List<MemberSectionProperty>> updateMemberSectionProperty(String projectId, List<MemberSectionProperty> memberSectionProperties);

    Observable<BeamSectionProperties> createBeamSectionProperty(String projectId, BeamSectionProperties properties);

    Observable<List<BeamSectionProperties>> getBeamSectionProperties(String projectId);

    Observable<BeamSectionProperties> updateBeamSectionProperty(String projectId, String itemId, BeamSectionProperties properties);

    Observable<BeamSectionProperties> getBeamSectionProperty(String projectId, String itemId);

    Observable<ColumnRebarProperties> createColumnProperty(String projectId, ColumnRebarProperties properties);

    Observable<List<ColumnRebarProperties>> getColumnProperties(String projectId);

    Observable<ColumnRebarProperties> updateColumnProperty(String projectId, String itemId , ColumnRebarProperties properties);

    Observable<ColumnRebarProperties> getColumnProperty(String projectId, String itemId);

}
