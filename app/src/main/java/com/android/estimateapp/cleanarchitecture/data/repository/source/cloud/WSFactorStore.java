package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import android.content.Context;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudFactorDataStore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class WSFactorStore extends BaseStore implements CloudFactorDataStore {

    private Context context;

    @Inject
    public WSFactorStore(Context context) {
        super();
        this.context = context;

    }

    @Override
    public Observable<RSBFactor> createProjectShareRSBFactors(String projectId, RSBFactor rsbFactor) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_RSB_FACTOR);
        }).flatMap(databaseReference -> push(databaseReference,rsbFactor));
    }


    @Override
    public Observable<List<RSBFactor>> getProjectRSBFactors(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_RSB_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, RSBFactor.class));
    }

    @Override
    public Observable<List<RSBFactor>> updateProjectRSBFactors(String projectId, List<RSBFactor> rsbFactors){
        DatabaseReference rsbRef = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_RSB_FACTOR);

        return Observable.fromIterable(rsbFactors)
                .flatMap(rsbFactor -> setValue(rsbRef.child(rsbFactor.getKey()), rsbFactor))
                .toList().toObservable();
    }
}
