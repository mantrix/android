package com.android.estimateapp.cleanarchitecture.presentation.properties.membersection;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.EdittextBaseOutputListener;
import com.android.estimateapp.utils.DisplayUtil;
import com.android.estimateapp.utils.NumberUtils;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberSectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<MemberSectionProperty>> {


    private List<MemberSectionProperty> list;

    @Inject
    public MemberSectionAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.member_section_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<MemberSectionProperty> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<MemberSectionProperty> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    public List<MemberSectionProperty> getData() {
        return list;
    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.et_item_num)
        EditText etItemNum;

        @BindView(R.id.et_name)
        EditText etName;

        @BindView(R.id.et_area)
        EditText etArea;

        @BindView(R.id.et_base)
        EditText etBase;

        @BindView(R.id.et_width)
        EditText etWidth;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(MemberSectionProperty property, int position) {



            etItemNum.setText(DisplayUtil.toPlainString(property.getItemNo()));
            etName.setText(property.getName());
            etArea.setText(DisplayUtil.toPlainString(property.getCrossSectionalArea()));
            etBase.setText(DisplayUtil.toPlainString(property.getBase()));
            etWidth.setText(DisplayUtil.toPlainString(property.getWidth()));


            etItemNum.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);
                    String input = etItemNum.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setItemNo(Integer.parseInt(etItemNum.getText().toString()));
                    }
                }
            });

            etName.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);
                    String input = etName.getText().toString();

                    if (!input.isEmpty()) {
                        list.get(position).setName(etName.getText().toString());
                    }
                }
            });

            etArea.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etArea.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setCrossSectionalArea(Double.parseDouble(etArea.getText().toString()));
                    }
                }
            });

            etBase.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etBase.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setBase(Double.parseDouble(etBase.getText().toString()));
                    }
                }
            });


            etWidth.addTextChangedListener(new EdittextBaseOutputListener() {
                @Override
                public void afterTextChanged(Editable s) {

                    String input = etWidth.getText().toString();

                    if (!input.isEmpty() && NumberUtils.convertibleToNumber(input)) {
                        list.get(position).setWidth(Double.parseDouble(etWidth.getText().toString()));
                    }
                }
            });
        }
    }
}
