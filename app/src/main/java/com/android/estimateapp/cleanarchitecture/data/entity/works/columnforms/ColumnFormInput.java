package com.android.estimateapp.cleanarchitecture.data.entity.works.columnforms;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.PlywoodSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RectangularConcreteSection;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.Node;

public class ColumnFormInput {

    Node nodeA;

    Node nodeB;

    double hCC;

    double offset1;

    double offset2;

    double hClear;

    PlywoodSize plywoodSize;

    RectangularConcreteSection rectangularConcreteSection;

    public Node getNodeA() {
        return nodeA;
    }

    public void setNodeA(Node nodeA) {
        this.nodeA = nodeA;
    }

    public Node getNodeB() {
        return nodeB;
    }

    public void setNodeB(Node nodeB) {
        this.nodeB = nodeB;
    }

    public double gethCC() {
        return hCC;
    }

    public void sethCC(double hCC) {
        this.hCC = hCC;
    }

    public double getOffset1() {
        return offset1;
    }

    public void setOffset1(double offset1) {
        this.offset1 = offset1;
    }

    public double getOffset2() {
        return offset2;
    }

    public void setOffset2(double offset2) {
        this.offset2 = offset2;
    }

    public double gethClear() {
        return hClear;
    }

    public void sethClear(double hClear) {
        this.hClear = hClear;
    }

    public PlywoodSize getPlywoodSize() {
        return plywoodSize;
    }

    public void setPlywoodSize(PlywoodSize plywoodSize) {
        this.plywoodSize = plywoodSize;
    }

    public RectangularConcreteSection getRectangularConcreteSection() {
        return rectangularConcreteSection;
    }

    public void setRectangularConcreteSection(RectangularConcreteSection rectangularConcreteSection) {
        this.rectangularConcreteSection = rectangularConcreteSection;
    }

    public double computeHCC(){
        return 0;
    }
}
