package com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class MemberSectionProperty extends Model {

    int itemNo;

    String name;

    double crossSectionalArea;

    double base;

    double width;

    public MemberSectionProperty(){

    }

    public MemberSectionProperty(int itemNo, String name, double area, double base, double width){
        setItemNo(itemNo);
        setName(name);
        setCrossSectionalArea(area);
        setBase(base);
        setWidth(width);
    }

    public int getItemNo() {
        return itemNo;
    }

    public void setItemNo(int itemNo) {
        this.itemNo = itemNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCrossSectionalArea() {
        return crossSectionalArea;
    }

    public void setCrossSectionalArea(double crossSectionalArea) {
        this.crossSectionalArea = crossSectionalArea;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
