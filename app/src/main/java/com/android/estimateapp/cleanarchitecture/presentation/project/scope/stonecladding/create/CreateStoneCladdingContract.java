package com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateStoneCladdingContract {

    interface View {

        void setInput(StoneCladdingInput input);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(StoneCladdingResult result);

        void inputSaved();

        void savingError();

        void requestForReCalculation();

        void setMortarClassPickerOptions(List<Grade> classes);

        void setStoneTypes(List<StoneType> stoneTypes);

        void showSavingDialog();

        void showSavingNotAllowedError();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(StoneCladdingInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
