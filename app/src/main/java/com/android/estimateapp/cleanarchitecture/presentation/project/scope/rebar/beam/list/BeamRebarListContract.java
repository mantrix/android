package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface BeamRebarListContract {

    interface View {

        void displayList(List<BeamRebarInput> inputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
