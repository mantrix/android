package com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnforms;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.FormworkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnforms.ColumnFormInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnforms.ColumnFormOutput;

public interface ColumnFormCalculator {

    ColumnFormOutput calculate(boolean autoCompute, ColumnFormInput input, FormworkFactor factor);

}
