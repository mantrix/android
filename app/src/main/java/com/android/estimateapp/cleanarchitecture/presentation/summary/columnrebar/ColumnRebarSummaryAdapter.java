package com.android.estimateapp.cleanarchitecture.presentation.summary.columnrebar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ColumnRebarMaterial;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ColumnRebarSummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<ColumnRebarMaterial>> {


    private List<ColumnRebarMaterial> list;
    private Context context;

    @Inject
    public ColumnRebarSummaryAdapter(Context context) {
        list = new ArrayList<>();
        this.context = context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.column_rebar_summary_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<ColumnRebarMaterial> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<ColumnRebarMaterial> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }


    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_rsb_size)
        TextView tvRsbSize;
        @BindView(R.id.tv_rebar_1_total)
        TextView tvRebar1Total;
        @BindView(R.id.tv_rebar_2_total)
        TextView tvRebar2Total;
        @BindView(R.id.tv_footing_total)
        TextView tvFootingTotal;
        @BindView(R.id.tv_column_total)
        TextView tvColumnTotal;
        @BindView(R.id.tv_splicing_total)
        TextView tvSplicingTotal;
        @BindView(R.id.tv_rsb_total)
        TextView tvRsbTotal;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(ColumnRebarMaterial material) {

            tvRsbSize.setText(String.format(context.getString(R.string.label_rebar_size), Integer.toString(material.rsbSize.getValue())));

            tvRebar1Total.setText(DisplayUtil.toDisplayFormat(material.rebar1));
            tvRebar2Total.setText(DisplayUtil.toDisplayFormat(material.rebar2));
            tvFootingTotal.setText(DisplayUtil.toDisplayFormat(material.bendAtFootings));
            tvColumnTotal.setText(DisplayUtil.toDisplayFormat(material.bendAtColumnEnds));
            tvSplicingTotal.setText(DisplayUtil.toDisplayFormat(material.splicings));

            tvRsbTotal.setText(String.format(context.getString(R.string.label_total), DisplayUtil.toDisplayFormat(material.getRebarTotal())));
        }
    }
}