package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.tiewire;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.CladdingFactorContract;

import java.util.List;

import javax.inject.Inject;

public class CHBTieWireFactorPresenter extends BasePresenter implements CHBTieWireFactorContract.UserActionListener {

    private CHBTieWireFactorContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeKey;

    @Inject
    public CHBTieWireFactorPresenter(ProjectRepository projectRepository,
                                  ThreadExecutorProvider threadExecutorProvider,
                                  PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }


    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(CHBTieWireFactorContract.View view) {
        this.view = view;
    }

    @Override
    public void updateFactors(List<TieWireRebarSpacingFactor> factors) {
        projectRepository.updateCHBTieWireRebarSpacingFactor(projectId, factors)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(updatedRsbFactors -> {
                    view.factorSaved();
                }, throwable -> {
                    view.factorSaved();
                    throwable.printStackTrace();
                });
    }


    @Override
    public void start() {
        projectRepository.getCHBFactor(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbFactor -> {
                    if (chbFactor.getChbSpacing() != null){
                        view.displayList(chbFactor.getChbSpacing().getTieWireRebarSpacingFactors());
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}