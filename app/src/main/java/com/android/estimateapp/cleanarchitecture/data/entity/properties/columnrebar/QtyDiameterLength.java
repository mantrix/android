package com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar;

public class QtyDiameterLength extends QtyLength {

    int diameter;

    public QtyDiameterLength(){

    }

    public QtyDiameterLength(double qty, int diameter, double length){
        quantity = qty;
        this.diameter = diameter;
        this.length = length;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }
}
