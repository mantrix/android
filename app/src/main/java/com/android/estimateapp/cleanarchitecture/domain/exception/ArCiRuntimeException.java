package com.android.estimateapp.cleanarchitecture.domain.exception;

import com.android.estimateapp.configuration.Constants;

public class ArCiRuntimeException extends Exception {

    private Constants.Code code;
    private String message;


    public ArCiRuntimeException(Constants.Code code) {
        this(code,code.getMatcher(), null);
    }

    public ArCiRuntimeException(Constants.Code code, String message) {
        this(code,message, null);
    }

    public ArCiRuntimeException(Constants.Code code,String message, Throwable throwable) {
        super(throwable);
        this.message = message;
        this.code = code;
    }

    public Constants.Code getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}