package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.list.PlasteringListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.list.PlasteringListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class PlasteringListModule {

    @Provides
    @PerActivity
    PlasteringListContract.UserActionListener providePlasteringListPresenter(PlasteringListPresenter presenter) {
        return presenter;
    }
}
