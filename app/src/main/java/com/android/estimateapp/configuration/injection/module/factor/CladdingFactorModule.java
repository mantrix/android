package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.CladdingFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.StoneCladdingPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class CladdingFactorModule {

    @Provides
    @PerFragment
    CladdingFactorContract.UserActionListener providesCladdingFactorPresenter(StoneCladdingPresenter presenter) {
        return presenter;
    }
}
