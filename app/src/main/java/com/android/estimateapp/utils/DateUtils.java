package com.android.estimateapp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    public static final String DATE_PATTERN_1 = "MM/dd/yyyy HH:mm:ss.SSS'Z'";

    public static final String DATE_PATTERN_2 = "MM-dd-yyyy";

    public static long nowInUtc() {
        Date currentTime = Calendar.getInstance().getTime();
        return currentTime.getTime();
    }

    public static Date convertToDate(String dateStr, String format) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.parse(dateStr);
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }
}
