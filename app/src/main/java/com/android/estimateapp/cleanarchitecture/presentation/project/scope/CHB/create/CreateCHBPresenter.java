package com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.create;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.HorizontalRSBpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.VerticalRSBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBSizeFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying.CHBLayingCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class CreateCHBPresenter extends CreateScopeInputBasePresenter implements CreateCHBContract.UserActionListener {

    private CreateCHBContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;
    private CHBLayingCalculator chbLayingCalculator;

    private Map<Grade, CHBMortarClass> chbMortarClassFactorMap = new HashMap<>();
    private Map<CHBSize, CHBSizeFactor> chbSizeFactorMap = new HashMap<>();
    private Map<VerticalRSBSpacing, VerticalSpacing> verticalSpacingFactorMap = new HashMap<>();
    private Map<HorizontalRSBpacing, HorizontalRSBpacing> horizontalSpacingFactorMap = new HashMap<>();
    private Map<RsbSize, RSBFactor> RSBFactorMap = new HashMap<>();

    private CHBFactor chbFactor;
    private List<RSBFactor> rsbFactors;
    private DoorsAndWindows doorsAndWindows;
    private CHBInput input;
    private CHBResult result;


    @Inject
    public CreateCHBPresenter(ProjectRepository projectRepository,
                              AccountSubscriptionUseCase accountSubscriptionUseCase,
                              CHBLayingCalculator chbLayingCalculator,
                              ThreadExecutorProvider threadExecutorProvider,
                              PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase,threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.chbLayingCalculator = chbLayingCalculator;

        chbFactor = new CHBFactor();
        rsbFactors = new ArrayList<>();
    }

    @Override
    public void setView(CreateCHBContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);


        setPickerOptions();
        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {
        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateCHBInput(projectId, scopeKey, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createCHBInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<CHBInput> chbInputObservable){
        view.showSavingDialog();
        chbInputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbInput -> {
                    itemKey = input.getKey();
                    input = chbInput;
                    saveResult();
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    private void saveResult(){
        if (result != null && itemKey != null) {
            projectRepository.saveCHBInputResult(projectId, scopeKey, itemKey,result)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(result -> {
                        this.result = result;
                    }, throwable -> {
                        throwable.printStackTrace();
                    });
        }
    }

    @Override
    public void calculate(CHBInput input) {
        input.setChbFactor(chbFactor);

        List<DoorWindowInputBase> doors = new ArrayList<>();
        List<DoorWindowInputBase> windows = new ArrayList<>();
        if (doorsAndWindows != null){
            doors.addAll(doorsAndWindows.getDoors());
            windows.addAll(doorsAndWindows.getWindows());
        }

        result = chbLayingCalculator.calculate(input, doors, windows, rsbFactors);

        this.input = input;
        view.displayResult(result);
    }

    private void setPickerOptions() {
        view.setMortarClassPickerOptions(Arrays.asList(Grade.A, Grade.B, Grade.C, Grade.D));
        view.setCHBSizePickerOptions(CHBSize.getCHBSizes());
        view.setVerticalSpacingPickerOptions(VerticalRSBSpacing.getAllVerticalRSBSpacings());
        view.setHorizontalSpacingPickerOptions(HorizontalRSBpacing.getAllHorizontalSpacings());
        view.setRSBSizePickerOptions(Arrays.asList(RsbSize.TEN_MM, RsbSize.TWELVE_MM));
    }

    private void loadCHBInput(){
        projectRepository.getCHBInput(projectId, scopeKey,itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbInput -> {
                    this.input = chbInput;

                    view.setCHBInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate) {
        Observable.zip(projectRepository.getCHBFactor(projectId).observeOn(postExecutionThread.getScheduler()),
                projectRepository.getProjectRSBFactors(projectId).observeOn(postExecutionThread.getScheduler()),
                loadDoorsAndWindows(), (chbFactor, rsbFactors, doorsAndWindows) -> {
                    return new CHBFactors() {
                        @Override
                        public DoorsAndWindows getDoorsAndWindows() {
                            return doorsAndWindows;
                        }

                        @Override
                        public CHBFactor getChbFactor() {
                            return chbFactor;
                        }

                        @Override
                        public List<RSBFactor> getRsbFactors() {
                            return rsbFactors;
                        }
                    };
                }).subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(data -> {
                    this.chbFactor = data.getChbFactor();
                    this.rsbFactors = data.getRsbFactors();
                    this.doorsAndWindows = data.getDoorsAndWindows();

                    view.setDoorOptions(doorsAndWindows.getDoors());
                    view.setWindowOptions(doorsAndWindows.getWindows());

                    if (!factorUpdate) {
                        view.enabledCalculateBtn(true);

                        if (itemKey != null) {
                            loadCHBInput();
                        }
                    } else {
                        view.requestForReCalculation();
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }

    private Observable<DoorsAndWindows> loadDoorsAndWindows() {
        return projectRepository.getProjectScopesOfWorks(projectId)
                .flatMap(works -> {

                    Observable<List<DoorWindowInputBase>> doorsObs = Observable.just(new ArrayList<>());
                    Observable<List<DoorWindowInputBase>> windowsObs = Observable.just(new ArrayList<>());
                    for (Work work : works) {
                        if (ScopeOfWork.DOORS.getId() == work.getScopeID()) {
                            doorsObs = projectRepository.getDoorAndWindowInputs(projectId, work.getKey());
                        } else if (ScopeOfWork.WINDOWS.getId() == work.getScopeID()) {
                            windowsObs = projectRepository.getDoorAndWindowInputs(projectId, work.getKey());
                        }
                    }
                    return Observable.zip(doorsObs, windowsObs, (doors, windows) -> new DoorsAndWindows() {

                        @Override
                        public List<DoorWindowInputBase> getDoors() {
                            return doors;
                        }

                        @Override
                        public List<DoorWindowInputBase> getWindows() {
                            return windows;
                        }


                    });
                });
    }

    interface CHBFactors{
        DoorsAndWindows getDoorsAndWindows();
        CHBFactor getChbFactor();
        List<RSBFactor> getRsbFactors();
    }

    interface DoorsAndWindows {
        List<DoorWindowInputBase> getDoors();
        List<DoorWindowInputBase> getWindows();
    }



}
