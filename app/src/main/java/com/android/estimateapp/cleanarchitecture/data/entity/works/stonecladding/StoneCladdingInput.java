package com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class  StoneCladdingInput extends Model {

    String wall;

    int sets;

    Dimension dimension;

    double openings;

    String stoneTypeId;

    StoneType stoneType;

    double wastage;

    int mortarClass;

    public StoneCladdingInput(){

    }

    public String getWall() {
        return wall;
    }

    public void setWall(String wall) {
        this.wall = wall;
    }

    @Exclude
    public Grade getMortarClassEnum() {
        return Grade.parseInt(mortarClass);
    }

    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public double getOpenings() {
        return openings;
    }

    public void setOpenings(double openings) {
        this.openings = openings;
    }

    public String getStoneTypeId() {
        return stoneTypeId;
    }

    public void setStoneTypeId(String stoneTypeId) {
        this.stoneTypeId = stoneTypeId;
    }

    @Exclude
    public StoneType getStoneType() {
        return stoneType;
    }

    @Exclude
    public void setStoneType(StoneType stoneType) {
        this.stoneType = stoneType;
    }

    public double getWastage() {
        return wastage;
    }

    public void setWastage(double wastage) {
        this.wastage = wastage;
    }
}
