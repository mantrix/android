package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.add.AddLaborDialogFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LaborCostBuilderModule {


    @PerFragment
    @ContributesAndroidInjector(modules = AddLaborCostModule.class)
    abstract AddLaborDialogFragment bindAddLaborDialogFrament();
}
