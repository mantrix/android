package com.android.estimateapp.cleanarchitecture.presentation.cost.labor;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerEntity;
import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.utils.DisplayUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LaborCostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<WorkerGroup>> {

    private List<WorkerGroup> list;
    private Activity context;

    private ListItemClickLister<WorkerGroup> listItemClickLister;

    @Inject
    public LaborCostAdapter() {
        list = new ArrayList<>();
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    public void setListItemClickLister(ListItemClickLister<WorkerGroup> listItemClickLister) {
        this.listItemClickLister = listItemClickLister;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.worker_group_layout, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<WorkerGroup> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<WorkerGroup> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    public List<WorkerGroup> getData() {
        return list;
    }

    public void deleteItem(String id){
        Integer position = null;
        for (int i = 0 ; i < list.size() ; i++){
            if (list.get(i).getKey().equalsIgnoreCase(id)){
                position = i;
                list.remove(i);
                break;
            }
        }
        if (position != null){
            notifyItemRemoved(position);
        }
    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.et_worker_designation_1)
        TextInputEditText etWorkerDesignation1;
        @BindView(R.id.til_worker_designation_1)
        TextInputLayout tilWorkerDesignation1;
        @BindView(R.id.et_hourly_rate_1)
        TextInputEditText etHourlyRate1;
        @BindView(R.id.til_worker_hourly_rate_1)
        TextInputLayout tilWorkerHourlyRate1;
        @BindView(R.id.et_worker_designation_2)
        TextInputEditText etWorkerDesignation2;
        @BindView(R.id.til_worker_designation_2)
        TextInputLayout tilWorkerDesignation2;
        @BindView(R.id.et_hourly_rate_2)
        TextInputEditText etHourlyRate2;
        @BindView(R.id.til_worker_hourly_rate_2)
        TextInputLayout tilWorkerHourlyRate2;
        @BindView(R.id.et_worker_designation_3)
        TextInputEditText etWorkerDesignation3;
        @BindView(R.id.til_worker_designation_3)
        TextInputLayout tilWorkerDesignation3;
        @BindView(R.id.et_hourly_rate_3)
        TextInputEditText etHourlyRate3;
        @BindView(R.id.til_worker_hourly_rate_3)
        TextInputLayout tilWorkerHourlyRate3;
        @BindView(R.id.btn_add)
        Button btnAdd;

        @BindView(R.id.cv_inputs)
        CardView cvItem;


        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(WorkerGroup group, int position) {

            etWorkerDesignation1.setEnabled(false);
            etWorkerDesignation2.setEnabled(false);
            etWorkerDesignation3.setEnabled(false);

            etHourlyRate1.setEnabled(false);
            etHourlyRate2.setEnabled(false);
            etHourlyRate3.setEnabled(false);

            btnAdd.setText(context.getString(R.string.edit));
            btnAdd.setOnClickListener(view -> {
                listItemClickLister.itemClick(group);
            });

            cvItem.setOnLongClickListener(view -> {
                listItemClickLister.onLongClick(group);
                return false;
            });

            if (group.getWorkers() != null) {

                for (int i = 0; i < group.getWorkers().size(); i++) {
                    WorkerEntity worker = group.getWorkers().get(i);

                    switch (i) {
                        case 0:
                            etWorkerDesignation1.setText(worker.getDesignation() != null ? worker.getDesignation() : "");
                            etHourlyRate1.setText(DisplayUtil.toDisplayFormat(worker.getHourlyRate()));
                            break;
                        case 1:
                            etWorkerDesignation2.setText(worker.getDesignation() != null ? worker.getDesignation() : "");
                            etHourlyRate2.setText(DisplayUtil.toDisplayFormat(worker.getHourlyRate()));
                            break;
                        case 2:
                            etWorkerDesignation3.setText(worker.getDesignation() != null ? worker.getDesignation() : "");
                            etHourlyRate3.setText(DisplayUtil.toDisplayFormat(worker.getHourlyRate()));
                            break;
                    }
                }
            }
        }
    }


}