package com.android.estimateapp.cleanarchitecture.presentation.project.create;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.detail.ProjectDetailActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.OnClick;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateProjectActivity extends BaseActivity implements CreateProjectContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.til_project_name)
    TextInputLayout tilProjectName;

    @BindView(R.id.et_project_name)
    TextInputEditText etProjectName;

    @BindView(R.id.til_project_description)
    TextInputLayout tilProjectDescription;


    @BindView(R.id.et_project_description)
    TextInputEditText etProjectDescription;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.et_owner)
    TextInputEditText etOwner;

    @BindView(R.id.til_project_owner)
    TextInputLayout tilProjectOwner;

    @BindView(R.id.et_location)
    TextInputEditText etLocation;

    @BindView(R.id.til_project_location)
    TextInputLayout tilProjectLocation;

    @Inject
    CreateProjectContract.UserActionListener presenter;



    @Override
    protected int getLayoutResource() {
        return R.layout.create_project_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        setToolbarTitle(R.string.create_project);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        presenter.setView(this);
        presenter.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_save)
    public void onSaveClick() {
        String name = etProjectName.getText().toString().trim();
        String owner = etOwner.getText().toString().trim();
        String location = etLocation.getText().toString().trim();

        String description = etProjectDescription.getText().toString().trim();

        boolean allRequiredInputsAreProvided = true;

        if (name.isEmpty()) {
            tilProjectName.setError(getString(R.string.required_error));
            allRequiredInputsAreProvided = false;
        } else {
            tilProjectName.setError(null);
        }

        if (owner.isEmpty()) {
            tilProjectOwner.setError(getString(R.string.required_error));
            allRequiredInputsAreProvided = false;
        } else {
            tilProjectOwner.setError(null);
        }

        if (location.isEmpty()) {
            tilProjectLocation.setError(getString(R.string.required_error));
            allRequiredInputsAreProvided = false;
        } else {
            tilProjectLocation.setError(null);
        }


        if (allRequiredInputsAreProvided){
            presenter.createProject(name, owner,location, description);
        }

    }

    @Override
    public void launchProjectDetail(String projectId) {
        Intent intent = ProjectDetailActivity.createIntent(this, projectId);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void displayCreateProjectLoader() {
        progressBar.setVisibility(View.VISIBLE);
        btnSave.setEnabled(false);
        btnSave.setVisibility(View.INVISIBLE);
    }

    @Override
    public void dismissCreateProjectLoader() {
        btnSave.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        btnSave.setVisibility(View.VISIBLE);
    }

    @Override
    public void projectCreationError() {
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_CODE_BACK_BUTTON:
                    setResult(RESULT_CONTENT_UPDATE);
                    finish();
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }
}
