package com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.PlasteringSummary;

import java.util.List;

public interface PlasteringWorkCalculator {

    PlasteringResult calculate(PlasteringMortarFactor factor, PlasteringInput input);

    PlasteringSummary generateSummary(List<PlasteringResult> results);

}
