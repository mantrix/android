package com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.Node;
import com.google.firebase.database.Exclude;

public class BeamRebarInput extends Model {

    Node nodeA;

    Node nodeB;

    String storey;

    String grid;

    String groupSpanMember;

    int memberNumber;

    double lengthCC;

    double offset;

    double offset2;

    double wastage;

    String mainRebarPropertyKey;

    String propertyReferenceName;

    ComputationInclusions inclusions;

    public BeamRebarInput(){

    }

    @Exclude
    public Node getNodeA() {
        return nodeA;
    }

    public void setNodeA(Node nodeA) {
        this.nodeA = nodeA;
    }

    @Exclude
    public Node getNodeB() {
        return nodeB;
    }

    public void setNodeB(Node nodeB) {
        this.nodeB = nodeB;
    }

    public String getStorey() {
        return storey;
    }

    public void setStorey(String storey) {
        this.storey = storey;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getGroupSpanMember() {
        return groupSpanMember;
    }

    public void setGroupSpanMember(String groupSpanMember) {
        this.groupSpanMember = groupSpanMember;
    }

    public String getPropertyReferenceName() {
        return propertyReferenceName;
    }

    public void setPropertyReferenceName(String propertyReferenceName) {
        this.propertyReferenceName = propertyReferenceName;
    }

    public int getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(int memberNumber) {
        this.memberNumber = memberNumber;
    }

    public double getLengthCC() {
        return lengthCC;
    }

    public void setLengthCC(double lengthCC) {
        this.lengthCC = lengthCC;
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public double getOffset2() {
        return offset2;
    }

    public void setOffset2(double offset2) {
        this.offset2 = offset2;
    }

    public double getWastage() {
        return wastage;
    }

    public void setWastage(double wastage) {
        this.wastage = wastage;
    }

    public String getMainRebarPropertyKey() {
        return mainRebarPropertyKey;
    }

    public void setMainRebarPropertyKey(String mainRebarPropertyKey) {
        this.mainRebarPropertyKey = mainRebarPropertyKey;
    }

    public ComputationInclusions getInclusions() {
        return inclusions;
    }

    public void setInclusions(ComputationInclusions inclusions) {
        this.inclusions = inclusions;
    }

    /**
     * @return computed Length CC based on Nodes
     */
    public double computeLengthCCBasedOnNodes(){

        if (nodeA != null && nodeB != null){
            double x = Math.pow(nodeA.getX() - nodeB.getX(), 2);
            double y = Math.pow(nodeA.getY() - nodeB.getY(), 2);
            double z = Math.pow(nodeA.getZ() - nodeB.getZ(), 2);

            setLengthCC(Math.sqrt(x + y + z));
        }
        return lengthCC;
    }
}
