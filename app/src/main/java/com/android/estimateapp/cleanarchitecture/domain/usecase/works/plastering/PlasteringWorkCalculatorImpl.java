package com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.PlasteringSummary;

import java.util.List;

import javax.inject.Inject;

public class PlasteringWorkCalculatorImpl extends BaseCalculator implements PlasteringWorkCalculator {

    @Inject
    public PlasteringWorkCalculatorImpl(){

    }

    @Override
    public PlasteringResult calculate(PlasteringMortarFactor factor, PlasteringInput input) {

        Dimension dimension = input.getDimension();

        double area = 0;
        double areaWithWastage = 0;
        if (dimension != null){
            area = ((dimension.getHeight() * dimension.getWidth()) - input.getOpenings());
            areaWithWastage = area *  (1 + toDoublePercentage(input.getWastage()));
        }

        PlasteringResult result = new PlasteringResult();
        result.setArea(roundHalfUp(area * input.getSides()));

        if (factor != null){
            result.setCement(roundHalfUp(areaWithWastage * input.getThickness() * factor.getCement() * input.getSides()));
            result.setSand(roundHalfUp(areaWithWastage * input.getThickness() * factor.getSand() * input.getSides()));
        }

        return result;
    }

    @Override
    public PlasteringSummary generateSummary(List<PlasteringResult> results){
        double totalArea = 0;
        double totalCement = 0;
        double totalSand = 0;

        for (PlasteringResult result : results){
            totalArea += result.getArea();
            totalCement += result.getCement();
            totalSand += result.getSand();
        }

        return new PlasteringSummary(totalArea,roundUpToNearestWholeNumber(totalCement),roundUpToNearestWholeNumber(totalSand));
    }
}
