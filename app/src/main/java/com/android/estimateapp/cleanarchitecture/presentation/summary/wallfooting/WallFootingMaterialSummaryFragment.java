package com.android.estimateapp.cleanarchitecture.presentation.summary.wallfooting;

import android.os.Bundle;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;

public class WallFootingMaterialSummaryFragment extends BaseFragment implements WallFootingSummaryContract.View {


    @Inject
    WallFootingSummaryContract.UserActionListener presenter;


    @BindView(R.id.tv_ten_mm_label)
    TextView tvTenMmLabel;
    @BindView(R.id.tv_ten_mm)
    TextView tvTenMm;
    @BindView(R.id.tv_twelve_mm_label)
    TextView tvTwelveMmLabel;
    @BindView(R.id.tv_twelve_mm)
    TextView tvTwelveMm;
    @BindView(R.id.tv_tie_wire)
    TextView tvTieWire;


    public static WallFootingMaterialSummaryFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        WallFootingMaterialSummaryFragment fragment = new WallFootingMaterialSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String projectId;
    private String scopeKey;

    @Override
    protected int getLayoutResource() {
        return R.layout.wall_footing_summary_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        extractExtras();
        presenter.setData(projectId, scopeKey);
        presenter.setView(this);
        presenter.start();

        tvTenMmLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 10));
        tvTwelveMmLabel.setText(String.format(getString(R.string.diameter_rsb_label_mm), 12));
    }


    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
    }

    @Override
    public void displaySummary(RebarMaterialSummary summary) {

        BaseActivity baseActivity = (BaseActivity) getActivity();

        tvTenMm.setText(baseActivity.toDisplayFormat(summary.getTenRsb()));
        tvTwelveMm.setText(baseActivity.toDisplayFormat(summary.getTwelveRsb()));
        tvTieWire.setText(baseActivity.toDisplayFormat(summary.getGiTieWire()));
    }

}