package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.plastering.PlasteringMaterialContract;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.plastering.PlasteringMaterialPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class PlasteringMaterialModule {

    @Provides
    @PerFragment
    PlasteringMaterialContract.UserActionListener providePlasteringkMaterialPresenter(PlasteringMaterialPresenter presenter) {
        return presenter;
    }
}
