package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum SurfaceType {

    ROUGH(1,"Rough"),
    SMOOTH(2,"Smooth");


    int id;
    String name;

    SurfaceType(int id,String name){
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    public static List<SurfaceType> allSurfaceTypes(){
        List<SurfaceType> list = new ArrayList<>();

        list.add(ROUGH);
        list.add(SMOOTH);

        return list;
    }

    public static SurfaceType parseString(String value){
        if (value != null){
            for (SurfaceType surfaceType : allSurfaceTypes()){
                if (value.equalsIgnoreCase(surfaceType.toString())){
                    return surfaceType;
                }
            }
        }
        return null;
    }
}

