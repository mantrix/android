package com.android.estimateapp.cleanarchitecture.presentation.home;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

public interface HomeContract {

    interface View {

        void showProjectList();

        void setName(String name);

        void displayProfilePhoto(String url);

        void onLogout();

    }

    interface UserActionListener extends Presenter<View> {
        void logout();
    }
}
