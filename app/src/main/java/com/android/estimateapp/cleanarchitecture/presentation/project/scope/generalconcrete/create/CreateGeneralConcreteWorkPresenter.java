package com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class CreateGeneralConcreteWorkPresenter extends CreateScopeInputBasePresenter implements CreateGeneralConcreteWorkContract.UserActionListener {

    private CreateGeneralConcreteWorkContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;
    private ConcreteWorkCalculator concreteWorkCalculator;
    private GeneralConcreteWorksResult result;


    private List<ConcreteFactor> concreteFactors;
    private ConcreteWorksInput input;


    @Inject
    public CreateGeneralConcreteWorkPresenter(ProjectRepository projectRepository,
                                              AccountSubscriptionUseCase accountSubscriptionUseCase,
                                              ConcreteWorkCalculator concreteWorkCalculator,
                                              ThreadExecutorProvider threadExecutorProvider,
                                              PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase,threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.concreteWorkCalculator = concreteWorkCalculator;

        concreteFactors = new ArrayList<>();
    }

    @Override
    public void setView(CreateGeneralConcreteWorkContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        setPickerOptions();
        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {
        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateConcreteWorkInput(projectId, scopeKey, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createConcreteWorkInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<ConcreteWorksInput> inputObservable) {
        view.showSavingDialog();

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    saveResult();
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    private void saveResult(){
        if (result != null && itemKey != null) {
            projectRepository.saveConcreteWorkInputResult(projectId, scopeKey, itemKey, result)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(result -> {
                        this.result = result;
                    }, throwable -> {
                        throwable.printStackTrace();
                        view.savingError();
                    });
        }
    }

    @Override
    public void calculate(ConcreteWorksInput input) {
        result = concreteWorkCalculator.calculateGeneralConcrete(getConcreteFactor(input.getConcreteClassEnum()), input);

        this.input = input;
        view.displayResult(result);
    }

    private ConcreteFactor getConcreteFactor(Grade grade) {

        if (grade != null) {
            for (ConcreteFactor concreteFactor : concreteFactors) {
                if (concreteFactor.getMortarClass() == grade.getId()) {
                    return concreteFactor;
                }
            }
        }
        return null;
    }

    private void setPickerOptions() {
        view.setCategoryOptions(Category.getCategories());
        view.setMortarClassPickerOptions(Arrays.asList(Grade.AA, Grade.A, Grade.B, Grade.C, Grade.D, Grade.SPECIAL_CASE_ONE, Grade.SPECIAL_CASE_TWO, Grade.SPECIAL_CASE_THREE));
        view.setGravelSizesOptions(GravelSize.getGravelSizes());
    }

    private void loadPlasteringInput() {
        projectRepository.getConcreteWorkInput(projectId, scopeKey, itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    this.input = input;

                    view.setInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate) {
        projectRepository.getConcreteWorkFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(concreteFactors -> {
                    this.concreteFactors = concreteFactors;

                    if (!factorUpdate) {
                        view.enabledCalculateBtn(true);

                        if (itemKey != null) {
                            loadPlasteringInput();
                        }
                    } else {
                        view.requestForReCalculation();
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }

}