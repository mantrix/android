package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar.ColumnRebarWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar.ColumnRebarWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.columnrebar.ColumnRebarSummaryContract;
import com.android.estimateapp.cleanarchitecture.presentation.summary.columnrebar.ColumnRebarSummaryPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class ColumnRebarSummaryModule {


    @Provides
    @PerFragment
    ColumnRebarWorkCalculator provideColumnRebarWorkCalculator(ColumnRebarWorkCalculatorImpl calculator) {
        return calculator;
    }

    @Provides
    @PerFragment
    ColumnRebarSummaryContract.UserActionListener provideColumnRebarSummaryPresenter(ColumnRebarSummaryPresenter presenter) {

        return presenter;
    }
}
