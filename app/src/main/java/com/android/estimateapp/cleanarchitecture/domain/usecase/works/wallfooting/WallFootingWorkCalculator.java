package com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;

import java.util.List;

public interface WallFootingWorkCalculator {

    RebarMaterialSummary generateSummary(List<WallFootingRebarResult> results);

    WallFootingRebarResult calculate(int lengthOfTieWirePerKilo, WallFootingRebarInput input, List<RSBFactor> rsbFactorFactors);
}
