package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

public class CHBSummaryPerLocData extends CHBSummaryPerSizeData {

    public double chbFourInch;

    public double chbFiveInch;

    public double chbSixInch;

    public CHBSummaryPerLocData(){

    }

    public double getChbFourInch() {
        return chbFourInch;
    }

    public void setChbFourInch(double chbFourInch) {
        this.chbFourInch = chbFourInch;
    }

    public double getChbFiveInch() {
        return chbFiveInch;
    }

    public void setChbFiveInch(double chbFiveInch) {
        this.chbFiveInch = chbFiveInch;
    }

    public double getChbSixInch() {
        return chbSixInch;
    }

    public void setChbSixInch(double chbSixInch) {
        this.chbSixInch = chbSixInch;
    }
}
