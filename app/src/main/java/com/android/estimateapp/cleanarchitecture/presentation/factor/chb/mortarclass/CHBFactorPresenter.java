package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.mortarclass;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class CHBFactorPresenter extends BasePresenter implements CHBFactorContract.UserActionListener {

    private CHBFactorContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeKey;

    @Inject
    public CHBFactorPresenter(ProjectRepository projectRepository,
                                  ThreadExecutorProvider threadExecutorProvider,
                                  PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }


    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(CHBFactorContract.View view) {
        this.view = view;
    }

    @Override
    public void updateFactors(double chb, List<CHBMortarClass> factors) {
        projectRepository.updateCHBMortarClasses(projectId,chb, factors)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbMortarClasses -> {
                    view.factorSaved();
                }, throwable -> {
                    view.factorSaved();
                    throwable.printStackTrace();
                });
    }


    @Override
    public void start() {
        projectRepository.getCHBFactor(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbFactor -> {
                    view.displayList(chbFactor.getCHB(),chbFactor.getConcreteMortarClasses());
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}