package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.generalconcrete.GeneralConcreteWorkSummaryContract;
import com.android.estimateapp.cleanarchitecture.presentation.summary.generalconcrete.ConcreteWorkSummaryPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class ConcreteWorkSummaryModule {

    @Provides
    @PerFragment
    ConcreteWorkCalculator provideConcreteWorkCalculator(ConcreteWorkCalculatorImpl calculator){
        return calculator;
    }

    @Provides
    @PerFragment
    GeneralConcreteWorkSummaryContract.UserActionListener provideConcreteWorkPresenter(ConcreteWorkSummaryPresenter presenter){
        return presenter;
    }
}
