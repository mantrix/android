package com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;

import java.util.List;

import javax.inject.Inject;

public class ColumnFootingWorkCalculatorImpl extends BaseCalculator implements ColumnFootingWorkCalculator {

    @Inject
    public ColumnFootingWorkCalculatorImpl() {

    }


    @Override
    public RebarMaterialSummary generateSummary(List<ColumnFootingRebarResult> results) {
        double totalTenMm = 0;
        double totalTwelveMm = 0;
        double totalSixteenMm = 0;
        double totalTwentyMm = 0;
        double totalTwentyFiveMm = 0;
        double totalGiTieWire = 0;

        for (ColumnFootingRebarResult result : results) {
            totalTenMm += result.getTenRsb();
            totalTwelveMm += result.getTwelveRsb();
            totalSixteenMm += result.getSixteenRsb();
            totalTwentyMm += result.getTwentyRsb();
            totalTwentyFiveMm += result.getTwentyFiveRsb();
            totalGiTieWire += result.getSixteenGiTieWire();
        }

        RebarMaterialSummary summary =  new RebarMaterialSummary();
        summary.setTenRsb(totalTenMm);
        summary.setTwelveRsb(totalTwelveMm);
        summary.setSixteenRsb(totalSixteenMm);
        summary.setTwentyRsb(totalTwentyMm);
        summary.setTwentyFiveRsb(totalTwentyFiveMm);
        summary.setGiTieWire(totalGiTieWire);

        return summary;
    }


    @Override
    public ColumnFootingRebarResult calculate(int lengthOfTieWirePerKilo, ColumnFootingRebarInput input, List<RSBFactor> rsbFactorList) {

        int qtyOfL = input.getSets() * input.getNumberOfBarsAtL();
        int qtyOfW = input.getSets() * input.getNumberOfBarsAtW();

        double gITieWire = (input.getSets() * input.getNumberOfBarsAtL() * input.getNumberOfBarsAtW() * input.getLengthOfTieWire()) / lengthOfTieWirePerKilo;

        RSBFactor rsbFactorAtL = getRSB(rsbFactorList, input.getDiameterAtL());

        double rsbAtLResult = 0;

        if (rsbFactorAtL != null) {
            rsbAtLResult = computeDiameterOfRsb(rsbFactorAtL, qtyOfL, input.getLengthAtL());
        }

        RSBFactor rsbFactorAtW = getRSB(rsbFactorList, input.getDiameterAtW());

        double rsbAtWResult = 0;

        if (rsbFactorAtW != null) {
            rsbAtWResult = computeDiameterOfRsb(rsbFactorAtW, qtyOfW, input.getLengthAtW());
        }

        ColumnFootingRebarResult output = new ColumnFootingRebarResult();
        output.setQtyOfL(qtyOfL);
        output.setQtyOfW(qtyOfW);
        output.setSixteenGiTieWire(roundHalfUp(gITieWire));


        if (rsbFactorAtL != null && rsbFactorAtW != null && rsbFactorAtL.getSize() == rsbFactorAtW.getSize()) {
            output = setRSBValue(output, rsbFactorAtL.getSize(), (rsbAtLResult + rsbAtWResult));
        } else {
            if (rsbFactorAtL != null){
                output = setRSBValue(output, rsbFactorAtL.getSize(), rsbAtLResult);
            }

            if (rsbFactorAtW != null){
                output = setRSBValue(output, rsbFactorAtW.getSize(), rsbAtWResult);
            }
        }

        return output;
    }

    private double computeDiameterOfRsb(RSBFactor rsbFactor, int qty, double lengthOfCut) {
        return qty * lengthOfCut * rsbFactor.getWeight();
    }

    private ColumnFootingRebarResult setRSBValue(ColumnFootingRebarResult output, int size, double result) {

        result = roundHalfUp(result);

        switch (RsbSize.parseInt(size)) {
            case TEN_MM:
                output.setTenRsb(result);
                break;
            case TWELVE_MM:
                output.setTwelveRsb(result);
                break;
            case SIXTEEN_MM:
                output.setSixteenRsb(result);
                break;
            case TWENTY_MM:
                output.setTwentyRsb(result);
                break;
            case TWENTY_FIVE_MM:
                output.setTwentyFiveRsb(result);
                break;
            default:
                throw new RuntimeException(size + " Rsb size is not supported in ColumnFootingWork");
        }

        return output;
    }


}
