package com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface CreateColumnSecionPropertyContract {

    interface View {

        void setInput(ColumnRebarProperties properties);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(int splicingTotalQty, double area);

        void inputSaved();

        void savingError();

        void requestForReCalculation();

        void setRSBPickerOptions(List<RsbSize> sizes);

        void showSavingDialog();

        void showSavingNotAllowedError();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String itemKey);

        void calculate(ColumnRebarProperties input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
