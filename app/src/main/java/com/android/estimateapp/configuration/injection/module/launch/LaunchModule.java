package com.android.estimateapp.configuration.injection.module.launch;

import com.android.estimateapp.cleanarchitecture.presentation.launch.LaunchScreenContract;
import com.android.estimateapp.cleanarchitecture.presentation.launch.LaunchScreenPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class LaunchModule {


    @Provides
    @PerActivity
    LaunchScreenContract.UserActionListener providesLaunchScreenPresenter(LaunchScreenPresenter presenter) {
        return presenter;
    }
}
