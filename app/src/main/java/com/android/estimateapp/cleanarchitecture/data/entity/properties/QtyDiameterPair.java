package com.android.estimateapp.cleanarchitecture.data.entity.properties;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class QtyDiameterPair extends Model {

    int quantity;

    int diameter;

    public QtyDiameterPair(int qty, int diameter){
        quantity = qty;
        this.diameter = diameter;
    }


    public QtyDiameterPair(){

    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }
}
