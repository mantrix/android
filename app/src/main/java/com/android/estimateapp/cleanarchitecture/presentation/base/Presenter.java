package com.android.estimateapp.cleanarchitecture.presentation.base;


/**
 * A base class for Presenters
 * See <a href="https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter">MVP Pattern</a>
 *
 * @param <V> Android {@link android.view.View} that will be attached to the {@link Presenter}
 */
public interface Presenter<V> {

    /**
     * Use for setting Android {@link android.view.View} reference to {@link Presenter}.
     * Usually, this should be called first before calling {@link Presenter#start()} or any method on the Presenter.
     *
     * @param v a class that will be a reference of the {@link Presenter} to an Android {@link android.view.View}.
     */
    void setView(V v);

    /**
     * Lifecycle method for initializing/starting any operations inside the {@link Presenter}.
     */
    void start();

    /**
     * Lifecycle method for notifying the {@link Presenter} that the {@link android.view.View}
     * has been resumed and currently displayed.
     */
    void resume();


    /**
     * Lifecycle method for notifying the {@link Presenter} that
     * the {@link android.view.View} has been paused and not currently displayed.
     */
    void pause();

    /**
     * Lifecycle method for notifying the {@link Presenter} that the {@link android.view.View} attached to it is
     * going to be destroyed.
     *
     * Note: It is important that this method is called before destroying a {@link android.view.View}
     * to make sure that all running operations are cancelled and will not cause an {@link Exception}
     */
    void destroy();
}
