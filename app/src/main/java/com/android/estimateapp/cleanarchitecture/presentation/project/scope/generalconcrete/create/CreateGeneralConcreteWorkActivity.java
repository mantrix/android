package com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.create;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateGeneralConcreteWorkActivity extends CreateScopeInputBaseActivity implements CreateGeneralConcreteWorkContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_category)
    TextInputEditText etCategory;
    @BindView(R.id.til_category)
    TextInputLayout tilCategory;
    @BindView(R.id.et_description)
    TextInputEditText etDescription;
    @BindView(R.id.til_description)
    TextInputLayout tilDescription;
    @BindView(R.id.et_height)
    TextInputEditText etHeight;
    @BindView(R.id.til_height)
    TextInputLayout tilHeight;
    @BindView(R.id.et_width)
    TextInputEditText etWidth;
    @BindView(R.id.til_width)
    TextInputLayout tilWidth;
    @BindView(R.id.et_thickness)
    TextInputEditText etThickness;
    @BindView(R.id.til_thickness)
    TextInputLayout tilThickness;
    @BindView(R.id.et_sets)
    TextInputEditText etSets;
    @BindView(R.id.til_sets)
    TextInputLayout tilSets;
    @BindView(R.id.et_wastage)
    TextInputEditText etWastage;
    @BindView(R.id.til_wastage)
    TextInputLayout tilWastage;
    @BindView(R.id.et_concrete_class)
    TextInputEditText etConcreteClass;
    @BindView(R.id.til_concrete_class)
    TextInputLayout tilConcreteClass;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.tv_volume)
    TextView tvVolume;
    @BindView(R.id.tv_cement)
    TextView tvCement;
    @BindView(R.id.tv_sand)
    TextView tvSand;
    @BindView(R.id.tv_three_fourths)
    TextView tvThreeFourths;
    @BindView(R.id.tv_g1)
    TextView tvG1;
    @BindView(R.id.cv_results)
    CardView cvResults;

    @BindView(R.id.et_gravel_size)
    TextInputEditText etGravelSize;
    @BindView(R.id.til_gravel_size)
    TextInputLayout tilGravelSize;

    @Inject
    CreateGeneralConcreteWorkContract.UserActionListener presenter;

    private String projectId;
    private String scopeKey;
    private String itemKey;

    private ConcreteWorksInput input;
    private Dimension dimension;

    private ProgressDialog saveProgressDialog;
    private boolean updateContent;

    AlertDialog categoryPickerDialog;
    AlertDialog mortarClassPickerDialog;
    AlertDialog gravelSizePickerDialog;

    public static Intent createIntent(Context context, String projectId, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateGeneralConcreteWorkActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.create_general_concrete_work_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.CONCRETE_WORKS.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        input = new ConcreteWorksInput();
        dimension = new Dimension();

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.CONCRETE_WORKS.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void setInput(ConcreteWorksInput input) {
        this.input = input;
        if (input.getDescription() != null) {
            etDescription.setText(input.getDescription());
        }

        if (input.getCategory() != null) {
            etCategory.setText(input.getCategory());
        }

        if (input.getGravelSizeType() != null) {
            etGravelSize.setText(input.getGravelSizeType().toString());
        }

        if (input.getDimension() != null) {
            dimension = input.getDimension();
            if (dimension.getHeight() != 0) {
                etHeight.setText(toDisplayFormat(dimension.getHeight()));
            }

            if (dimension.getWidth() != 0) {
                etWidth.setText(toDisplayFormat(dimension.getWidth()));
            }
        }

        if (input.getThickness() != 0) {
            etThickness.setText(toDisplayFormat(input.getThickness()));
        }

        if (input.getSets() != 0) {
            etSets.setText(toDisplayFormat(input.getSets()));
        }

        if (input.getWastage() != 0) {
            etWastage.setText(toDisplayFormat(input.getWastage()));
        }


        if (input.getMortarClass() != 0) {
            etConcreteClass.setText(Grade.parseInt(input.getMortarClass()).toString());
        }
    }


    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayResult(GeneralConcreteWorksResult result) {
        tvVolume.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getVolume())));
        tvCement.setText(String.format(getString(R.string.bag_result), toDisplayFormat(result.getCement())));
        tvSand.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getSand())));
        tvThreeFourths.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getThreeFourthGravel())));
        tvG1.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getG1Gravel())));

        cvResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }


    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }


    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    @OnClick(R.id.et_category)
    protected void categoryClick() {
        if (categoryPickerDialog != null) {
            categoryPickerDialog.show();
        }
    }

    @Override
    public void setCategoryOptions(List<Category> categories) {
        String[] options = new String[categories.size()];
        for (int i = 0; i < categories.size(); i++) {
            options[i] = categories.get(i).toString();
        }

        categoryPickerDialog = createChooser(getString(R.string.category), options, (dialog, which) -> {
            etCategory.setText(options[which]);
            input.setCategory(categories.get(which).toString());
            dialog.dismiss();
        });
    }

    @OnClick(R.id.et_concrete_class)
    protected void concreteClassClick() {
        if (mortarClassPickerDialog != null) {
            mortarClassPickerDialog.show();
        }
    }

    @Override
    public void setMortarClassPickerOptions(List<Grade> classes) {
        String[] options = new String[classes.size()];
        for (int i = 0; i < classes.size(); i++) {
            options[i] = classes.get(i).toString();
        }

        mortarClassPickerDialog = createChooser(getString(R.string.concrete_mortar_class), options, (dialog, which) -> {
            etConcreteClass.setText(options[which]);
            input.setMortarClass(classes.get(which).getId());
            dialog.dismiss();
        });
    }

    @OnClick(R.id.et_gravel_size)
    protected void gravelSizeClick() {
        if (gravelSizePickerDialog != null) {
            gravelSizePickerDialog.show();
        }
    }

    @Override
    public void setGravelSizesOptions(List<GravelSize> gravelSizes) {
        String[] options = new String[gravelSizes.size()];
        for (int i = 0; i < gravelSizes.size(); i++) {
            options[i] = gravelSizes.get(i).toString();
        }

        gravelSizePickerDialog = createChooser(getString(R.string.gravel_sizes), options, (dialog, which) -> {
            etGravelSize.setText(options[which]);
            input.setGravelSizeType(gravelSizes.get(which).toString());
            dialog.dismiss();
        });
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setDescription(etDescription.getText().toString());

        String height = etHeight.getText().toString();
        if (height.isEmpty()) {
            dimension.setHeight(0);
        } else {
            dimension.setHeight(Double.parseDouble(height));
        }

        String width = etWidth.getText().toString();
        if (width.isEmpty()) {
            dimension.setWidth(0);
        } else {
            dimension.setWidth(Double.parseDouble(width));
        }

        input.setDimension(dimension);

        String thickness = etThickness.getText().toString();
        if (thickness.isEmpty()) {
            input.setThickness(0);
        } else {
            input.setThickness(Double.parseDouble(thickness));
        }

        String sides = etSets.getText().toString();
        if (sides.isEmpty()) {
            input.setSets(0);
        } else {
            input.setSets(Integer.parseInt(sides));
        }

        String wastage = etWastage.getText().toString();
        if (wastage.isEmpty()) {
            input.setWastage(0);
        } else {
            input.setWastage(Integer.parseInt(wastage));
        }

        presenter.calculate(input);
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etCategory.getText().toString().isEmpty()) {
            tilCategory.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilCategory.setError(null);
        }

        if (etDescription.getText().toString().isEmpty()) {
            tilDescription.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilDescription.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
