package com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ColumnRebarSummary;

import java.util.List;

public interface ColumnRebarWorkCalculator {

    ColumnRebarSummary generateSummary(List<ColumnRebarOutput> results);

    ColumnRebarOutput calculate(boolean autoCompute, ColumnRebarInput input, ColumnRebarProperties properties, List<RSBFactor> rsbFactors);
}

