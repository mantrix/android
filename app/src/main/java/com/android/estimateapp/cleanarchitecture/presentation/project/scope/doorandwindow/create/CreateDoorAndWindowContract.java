package com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import io.reactivex.annotations.Nullable;

public interface CreateDoorAndWindowContract {

    interface View {

        void setInput(DoorWindowInputBase input, int totalSets);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(DoorWindowOutputBase result);

        void inputSaved();

        void showSavingDialog();

        void savingError();

        void showSavingNotAllowedError();

        void requestForReCalculation();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey, ScopeOfWork scopeOfWork);

        void calculate(DoorWindowInputBase input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
