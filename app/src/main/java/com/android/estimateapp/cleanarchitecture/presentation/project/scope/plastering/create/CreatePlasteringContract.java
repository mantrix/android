package com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.PlasteringLocation;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.SurfaceType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreatePlasteringContract {

    interface View {

        void setInput(PlasteringInput input);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(PlasteringResult result);

        void inputSaved();

        void showSavingDialog();

        void savingError();

        void showSavingNotAllowedError();

        void requestForReCalculation();

        void setMortarClassPickerOptions(List<Grade> classes);

        void setPlasteringLocationOptions(List<PlasteringLocation> locations);

        void setSurfaceTypeOptions(List<SurfaceType> surfaceTypes);

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(PlasteringInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
