package com.android.estimateapp.cleanarchitecture.data.clients;

import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class FirebaseDatabaseClient implements DatabaseClient {

    @Inject
    public FirebaseDatabaseClient(){

    }

    @Override
    public void setup() {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
