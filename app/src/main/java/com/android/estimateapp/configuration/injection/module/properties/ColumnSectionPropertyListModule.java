package com.android.estimateapp.configuration.injection.module.properties;

import com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.list.ColumnSectionPropertiesListContract;
import com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.list.ColumnSectionPropertiesListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ColumnSectionPropertyListModule {

    @Provides
    @PerActivity
    ColumnSectionPropertiesListContract.UserActionListener provideColumnSectionPresenter(ColumnSectionPropertiesListPresenter presenter) {
        return presenter;
    }
}
