package com.android.estimateapp.configuration.service;

import com.android.estimateapp.cleanarchitecture.data.entity.CreateUserPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.SignupPayload;

import io.reactivex.Completable;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface WebService {

    @FormUrlEncoded
    @POST("login")
    Observable<Response<Object>> loginUser(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("auth/restrictions")
    Observable<Object> authorizeUser(@Header("Authorization") String token, @Field("uid") String uid, @Field("platform") String platform, @Field("deviceId") String deviceId);

    @POST("user/create")
    Completable createUser(@Header("Authorization") String token, @Body CreateUserPayload payload);

    @POST("user/signup")
    Completable registerUser(@Body SignupPayload payload);

}
