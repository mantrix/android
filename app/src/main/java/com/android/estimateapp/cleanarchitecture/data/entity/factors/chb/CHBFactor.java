package com.android.estimateapp.cleanarchitecture.data.entity.factors.chb;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

@IgnoreExtraProperties
public class CHBFactor extends Model{

   double CHB;

   List<CHBMortarClass> concreteMortarClasses;

   CHBSpacing chbSpacing;

   public CHBFactor(){

   }

    public CHBFactor(double chb){
        this.CHB = chb;
    }

    public double getCHB() {
        return CHB;
    }

    public void setCHB(double CHB) {
        this.CHB = CHB;
    }

    @Exclude
    public List<CHBMortarClass> getConcreteMortarClasses() {
        return concreteMortarClasses;
    }

    public void setConcreteMortarClasses(List<CHBMortarClass> concreteMortarClasses) {
        this.concreteMortarClasses = concreteMortarClasses;
    }

    @Exclude
    public CHBSpacing getChbSpacing() {
        return chbSpacing;
    }

    public void setChbSpacing(CHBSpacing chbSpacing) {
        this.chbSpacing = chbSpacing;
    }
}
