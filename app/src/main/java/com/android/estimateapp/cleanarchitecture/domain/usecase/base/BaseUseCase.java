package com.android.estimateapp.cleanarchitecture.domain.usecase.base;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class BaseUseCase {
    protected CompositeDisposable compositeDisposables = new CompositeDisposable();

    protected ThreadExecutorProvider threadExecutorProvider;
    protected PostExecutionThread postExecutionThread;

    public BaseUseCase() {
    }

    public BaseUseCase(ThreadExecutorProvider threadExecutorProvider, PostExecutionThread postExecutionThread) {
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }


    public void clear() {
        compositeDisposables.clear();
    }

    protected  <T> List<List<T>> getPages(Collection<T> c, Integer pageSize) {
        if (c == null)
            return Collections.emptyList();
        List<T> list = new ArrayList<T>(c);
        if (pageSize == null || pageSize <= 0 || pageSize > list.size())
            pageSize = list.size();
        int numPages = (int) Math.ceil((double)list.size() / (double)pageSize);
        List<List<T>> pages = new ArrayList<List<T>>(numPages);
        for (int pageNum = 0; pageNum < numPages;)
            pages.add(list.subList(pageNum * pageSize, Math.min(++pageNum * pageSize, list.size())));
        return pages;
    }
}
