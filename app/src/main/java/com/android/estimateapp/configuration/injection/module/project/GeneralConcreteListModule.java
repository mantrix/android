package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.list.GeneralConcreteWorkListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.list.GeneralConcreteWorkListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class GeneralConcreteListModule {

    @Provides
    @PerActivity
    GeneralConcreteWorkListContract.UserActionListener provideGeneralConreteListPresenter(GeneralConcreteWorkListPresenter presenter) {
        return presenter;
    }
}
