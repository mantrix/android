package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.LumberSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.FormworkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.slabformworks.SlabFormInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.slabformworks.SlabFormOutput;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.slabform.SlabFormworksCalculatorImpl;

import org.junit.Before;
import org.junit.Test;

public class SlabFormComputationTest extends BaseTest {

    SlabFormworksCalculatorImpl slabFormworksCalculator;

    @Before
    public void setup() {
        slabFormworksCalculator = new SlabFormworksCalculatorImpl();
        initRSBSizes();
    }

    @Test
    public void slabFormTest() {

        SlabFormInput input = new SlabFormInput();
        input.setSets(2);
        input.setLength(12);
        input.setWidth(12);
        input.setSpacingX(0.1);
        input.setSpacingY(0.1);
        input.setLumberSizeAtX(LumberSize.TWO_BY_TWO );
        input.setLumberSizeAtY(LumberSize.TWO_BY_THREE);
        input.setPermiterBrace(LumberSize.TWO_BY_FOUR);

        FormworkFactor factor = new FormworkFactor();
        factor.setEffectiveArea(1.6);
        factor.setNail(0.1);

        SlabFormOutput result = slabFormworksCalculator.calculate(input,factor);

        System.out.println("Area: " + result.getArea());
        System.out.println("Plywood: " + result.getPlywood());
        System.out.println("2x2: " + result.getTwoByTwo());
        System.out.println("2x3: " + result.getTwoByThree());
        System.out.println("2x4: " + result.getTwoByFour());
        System.out.println("Nails: " + result.getNails());

    }
}
