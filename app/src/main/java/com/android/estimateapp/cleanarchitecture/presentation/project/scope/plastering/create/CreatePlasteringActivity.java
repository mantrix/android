package com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.PlasteringLocation;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.SurfaceType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreatePlasteringActivity extends CreateScopeInputBaseActivity implements CreatePlasteringContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_description)
    TextInputEditText etDescription;

    @BindView(R.id.til_description)
    TextInputLayout tilDescription;

    @BindView(R.id.et_height)
    TextInputEditText etHeight;

    @BindView(R.id.til_height)
    TextInputLayout tilHeight;

    @BindView(R.id.et_width)
    TextInputEditText etWidth;

    @BindView(R.id.til_width)
    TextInputLayout tilWidth;

    @BindView(R.id.et_thickness)
    TextInputEditText etThickness;

    @BindView(R.id.til_thickness)
    TextInputLayout tilThickness;

    @BindView(R.id.et_location)
    TextInputEditText etLocation;

    @BindView(R.id.til_location)
    TextInputLayout tilLocation;

    @BindView(R.id.et_surface_type)
    TextInputEditText etSurfaceType;

    @BindView(R.id.til_surface_type)
    TextInputLayout tilSurfaceType;

    @BindView(R.id.et_opening)
    TextInputEditText etOpening;

    @BindView(R.id.til_opening)
    TextInputLayout tilOpening;

    @BindView(R.id.et_sides)
    TextInputEditText etSides;

    @BindView(R.id.til_sides)
    TextInputLayout tilSides;

    @BindView(R.id.et_wastage)
    TextInputEditText etWastage;

    @BindView(R.id.til_wastage)
    TextInputLayout tilWastage;

    @BindView(R.id.et_mortar_class)
    TextInputEditText etMortarClass;

    @BindView(R.id.til_mortar_class)
    TextInputLayout tilMortarClass;

    @BindView(R.id.cv_inputs)
    CardView cvInputs;

    @BindView(R.id.btn_calculate)
    Button btnCalculate;

    @BindView(R.id.tv_area)
    TextView tvArea;

    @BindView(R.id.tv_cement)
    TextView tvCement;

    @BindView(R.id.tv_sand)
    TextView tvSand;

    @BindView(R.id.cv_results)
    CardView cvResults;

    @Inject
    CreatePlasteringContract.UserActionListener presenter;

    private String projectId;
    private String scopeKey;
    private String itemKey;

    private PlasteringInput input;
    private Dimension dimension;

    private ProgressDialog saveProgressDialog;


    private boolean updateContent;

    AlertDialog mortarClassPickerDialog;
    AlertDialog plasteringLocationPickerDialog;
    AlertDialog surfaceTypePickerDialog;

    public static Intent createIntent(Context context, String projectId, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreatePlasteringActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.create_chb_plastering_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.CHB_PLASTERING.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        input = new PlasteringInput();
        dimension = new Dimension();

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.CHB_PLASTERING.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }

    @Override
    public void setMortarClassPickerOptions(List<Grade> classes) {
        String[] options = new String[classes.size()];
        for (int i = 0; i < classes.size(); i++) {
            options[i] = classes.get(i).toString();
        }

        mortarClassPickerDialog = createChooser(getString(R.string.concrete_mortar_class), options, (dialog, which) -> {
            etMortarClass.setText(options[which]);
            input.setMortarClass(classes.get(which).getId());
            dialog.dismiss();
        });
    }

    public void setPlasteringLocationOptions(List<PlasteringLocation> locations) {
        String[] options = new String[locations.size()];
        for (int i = 0; i < locations.size(); i++) {
            options[i] = locations.get(i).toString();
        }

        plasteringLocationPickerDialog = createChooser(getString(R.string.location), options, (dialog, which) -> {
            etLocation.setText(options[which]);
            input.setLocation(locations.get(which).toString());
            dialog.dismiss();
        });
    }

    public void setSurfaceTypeOptions(List<SurfaceType> surfaceTypes) {
        String[] options = new String[surfaceTypes.size()];
        for (int i = 0; i < surfaceTypes.size(); i++) {
            options[i] = surfaceTypes.get(i).toString();
        }

        surfaceTypePickerDialog = createChooser(getString(R.string.surface_type), options, (dialog, which) -> {
            etSurfaceType.setText(options[which]);
            input.setSurfaceType(surfaceTypes.get(which).toString());
            dialog.dismiss();
        });
    }

    @OnClick(R.id.et_mortar_class)
    protected void mortarClassClick() {
        if (mortarClassPickerDialog != null) {
            mortarClassPickerDialog.show();
        }
    }

    @OnClick(R.id.et_location)
    protected void locationClick() {
        if (plasteringLocationPickerDialog != null) {
            plasteringLocationPickerDialog.show();
        }
    }

    @OnClick(R.id.et_surface_type)
    protected void surfaceTypeClick() {
        if (surfaceTypePickerDialog != null) {
            surfaceTypePickerDialog.show();
        }
    }

    @Override
    public void setInput(PlasteringInput input) {
        this.input = input;
        if (input.getDescription() != null){
            etDescription.setText(input.getDescription());
        }

        if (input.getLocationEnum() != null){
            etLocation.setText(input.getLocationEnum().toString());
        }

        if (input.getSurfaceTypeEnum() != null){
            etSurfaceType.setText(input.getSurfaceTypeEnum().toString());
        }

        if (input.getDimension() != null){
            dimension = input.getDimension();
            if (dimension.getHeight() != 0){
                etHeight.setText(toDisplayFormat(dimension.getHeight()));
            }

            if (dimension.getWidth() != 0){
                etWidth.setText(toDisplayFormat(dimension.getWidth()));
            }
        }

        if (input.getThickness() != 0){
            etThickness.setText(toDisplayFormat(input.getThickness()));
        }

        if (input.getSides() != 0){
            etSides.setText(toDisplayFormat(input.getSides()));
        }

        if (input.getWastage() != 0){
            etWastage.setText(toDisplayFormat(input.getWastage()));
        }

        if (input.getOpenings() != 0){
            etOpening.setText(toDisplayFormat(input.getOpenings()));
        }


        if (input.getMortarClass() != 0){
            etMortarClass.setText(Grade.parseInt(input.getMortarClass()).toString());
        }
    }

    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayResult(PlasteringResult result) {

        tvArea.setText(String.format(getString(R.string.sqm_result),toDisplayFormat(result.getArea())));
        tvCement.setText(String.format(getString(R.string.bag_result),toDisplayFormat(result.getCement())));
        tvSand.setText(String.format(getString(R.string.cum_result),toDisplayFormat(result.getSand())));

        cvResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }

    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setDescription(etDescription.getText().toString());
        input.setSurfaceType(etSurfaceType.getText().toString());
        input.setLocation(etLocation.getText().toString());

        String height = etHeight.getText().toString();
        if (height.isEmpty()){
            dimension.setHeight(0);
        } else {
            dimension.setHeight(Double.parseDouble(height));
        }

        String width = etWidth.getText().toString();
        if (width.isEmpty()){
            dimension.setWidth(0);
        } else {
            dimension.setWidth(Double.parseDouble(width));
        }

        input.setDimension(dimension);

        String thickness = etThickness.getText().toString();
        if (thickness.isEmpty()){
            input.setThickness(0);
        } else {
            input.setThickness(Double.parseDouble(thickness));
        }

        String opening = etOpening.getText().toString();
        if (opening.isEmpty()){
            input.setOpenings(0);
        } else {
            input.setOpenings(Double.parseDouble(opening));
        }

        String sides = etSides.getText().toString();
        if (sides.isEmpty()){
            input.setSides(0);
        } else {
            input.setSides(Integer.parseInt(sides));
        }

        String wastage = etWastage.getText().toString();
        if (wastage.isEmpty()){
            input.setWastage(0);
        } else {
            input.setWastage(Integer.parseInt(wastage));
        }

        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etLocation.getText().toString().isEmpty()){
            tilLocation.setError(getString(R.string.required_error));
            valid =  false;
        } else {
            tilLocation.setError(null);
        }

        if (etDescription.getText().toString().isEmpty()){
            tilDescription.setError(getString(R.string.required_error));
            valid =  false;
        } else {
            tilDescription.setError(null);
        }

        if (etSurfaceType.getText().toString().isEmpty()){
            tilSurfaceType.setError(getString(R.string.required_error));
            valid =  false;
        } else {
            tilSurfaceType.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
