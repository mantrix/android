package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering.PlasteringWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering.PlasteringWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.plastering.PlasteringSummaryContract;
import com.android.estimateapp.cleanarchitecture.presentation.summary.plastering.PlasteringSummaryPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class PlasteringSummaryModule {

    @Provides
    @PerFragment
    PlasteringWorkCalculator providePlasteringWorkCalculator(PlasteringWorkCalculatorImpl calculator) {
        return calculator;
    }

    @Provides
    @PerFragment
    PlasteringSummaryContract.UserActionListener providePlasteringSummaryPresenter(PlasteringSummaryPresenter presenter) {
        return presenter;
    }
}
