package com.android.estimateapp.cleanarchitecture.domain.executors;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;


@Singleton
public class SchedulerProvider implements ThreadExecutorProvider {


    @Inject
    public SchedulerProvider(){

    }
    @Override
    public Scheduler ioScheduler() {
        return Schedulers.io();
    }

    @Override
    public Scheduler computationScheduler() {
        return Schedulers.computation();
    }
}
