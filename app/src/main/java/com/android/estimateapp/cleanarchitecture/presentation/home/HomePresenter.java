package com.android.estimateapp.cleanarchitecture.presentation.home;

import android.util.Log;

import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Name;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.AccountRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.UserSessionRepository;

import javax.inject.Inject;

public class HomePresenter implements HomeContract.UserActionListener {
    private static final String TAG = HomePresenter.class.getSimpleName();

    private HomeContract.View view;
    private UserSessionRepository userSessionRepository;
    private AccountRepository accountRepository;
    private Account account;
    private ThreadExecutorProvider threadExecutorProvider;
    private PostExecutionThread postExecutionThread;
    private ProjectRepository projectRepository;

    @Inject
    public HomePresenter(UserSessionRepository userSessionRepository,
                         AccountRepository accountRepository,
                         ProjectRepository projectRepository,
                         ThreadExecutorProvider threadExecutorProvider,
                         PostExecutionThread postExecutionThread) {
        this.userSessionRepository = userSessionRepository;
        this.accountRepository = accountRepository;
        this.projectRepository = projectRepository;
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }

    @Override
    public void setView(HomeContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        view.showProjectList();

        setAccountDetails();
    }

    @Override
    public void resume() {
        loadActiveAccount();
    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void logout() {
        accountRepository.logout()
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(() -> {
                    userSessionRepository.setCurrentActiveUser(null);
                    view.onLogout();
                }, throwable -> {
                    throwable.printStackTrace();
                    userSessionRepository.setCurrentActiveUser(null);
                    view.onLogout();
                });
    }


    private void setAccountDetails() {
        account = userSessionRepository.getCurrentActiveUser();
        if (account != null) {
            Name name = account.getName();

            if (name != null && !name.isEmpty()){
                view.setName(name.toString());
            }

            if (account.getPicURL() != null && !account.getPicURL().isEmpty()) {
                view.displayProfilePhoto(account.getPicURL());
            }

        } else {
            loadActiveAccount();
        }
    }

    private void loadActiveAccount() {
        accountRepository.getLoggedInUser()
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(account -> {
                    Log.d(TAG, "Active User Name: " + account.getName());
                    userSessionRepository.setCurrentActiveUser(account);
                    setAccountDetails();
                }, Throwable::printStackTrace);
    }


}
