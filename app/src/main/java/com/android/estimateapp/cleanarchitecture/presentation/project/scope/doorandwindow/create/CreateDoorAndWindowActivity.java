package com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.create;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateDoorAndWindowActivity extends CreateScopeInputBaseActivity implements CreateDoorAndWindowContract.View {


    @Inject
    CreateDoorAndWindowContract.UserActionListener presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_description)
    TextInputEditText etDescription;
    @BindView(R.id.til_description)
    TextInputLayout tilDescription;
    @BindView(R.id.et_code)
    TextInputEditText etCode;
    @BindView(R.id.til_code)
    TextInputLayout tilCode;
    @BindView(R.id.et_sets)
    TextInputEditText etSets;
    @BindView(R.id.til_sets)
    TextInputLayout tilSets;
    @BindView(R.id.et_length)
    TextInputEditText etLength;
    @BindView(R.id.til_length)
    TextInputLayout tilLength;
    @BindView(R.id.et_width)
    TextInputEditText etWidth;
    @BindView(R.id.til_width)
    TextInputLayout tilWidth;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.tv_area)
    TextView tvArea;
    @BindView(R.id.tv_perimeter)
    TextView tvPerimeter;
    @BindView(R.id.cv_results)
    CardView cvResults;

    private String projectId;
    private String scopeKey;
    private String itemKey;

    private DoorWindowInputBase input;

    private ProgressDialog saveProgressDialog;

    private boolean updateContent;
    private ScopeOfWork scopeOfWork;


    public static Intent createIntent(Context context, String projectId, String scopeKey,  int scopeOfWork, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateDoorAndWindowActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        intent.putExtra(Constants.SCOPE_OF_WORK, scopeOfWork);
        return intent;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.create_door_and_window_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(scopeOfWork.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        input = new DoorWindowInputBase();

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey, scopeOfWork);
        presenter.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_door_and_window_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
        scopeOfWork = ScopeOfWork.parseInt(intent.getIntExtra(Constants.SCOPE_OF_WORK, 0));
    }


    @Override
    public void setInput(DoorWindowInputBase input, int sets) {
        this.input = input;

        if (input.getName() != null) {
            etDescription.setText(input.getName());
        }

        if (input.getCode() != null) {
            etCode.setText(input.getCode());
        }

        if (input.getLength() != 0) {
            etLength.setText(toDisplayFormat(input.getLength()));
        }

        if (input.getWidth() != 0) {
            etWidth.setText(toDisplayFormat(input.getWidth()));
        }

        if (sets != 0) {
            etSets.setText(toDisplayFormat(sets));
        }
    }

    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayResult(DoorWindowOutputBase result) {

        tvArea.setText(String.format(getString(R.string.sqm_result), toDisplayFormat(result.getArea())));
        tvPerimeter.setText(String.format(getString(R.string.lm_result), toDisplayFormat(result.getPerimeter())));


        cvResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }

    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {

        input.setName(etDescription.getText().toString());
        input.setCode(etCode.getText().toString());

        String length = etLength.getText().toString();
        if (length.isEmpty()) {
            input.setLength(0);
        } else {
            input.setLength(Double.parseDouble(length));
        }

        String width = etWidth.getText().toString();
        if (width.isEmpty()) {
            input.setWidth(0);
        } else {
            input.setWidth(Double.parseDouble(width));
        }

        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etCode.getText().toString().isEmpty()) {
            etCode.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etCode.setError(null);
        }

        if (etDescription.getText().toString().isEmpty()) {
            tilDescription.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilDescription.setError(null);
        }


        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
