package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting.WallFootingRebarCalculatorImpl;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class WallFootingComputationTest extends BaseTest {

    WallFootingRebarCalculatorImpl wallFootingWorkCalculator;

    @Before
    public void setup() {
        wallFootingWorkCalculator = new WallFootingRebarCalculatorImpl();
        initRSBSizes();
    }

    @Test
    public void wallFootingTest() {

        WallFootingRebarInput input = new WallFootingRebarInput();
        input.setSets(2);
        input.setLength(10);
        input.setNumOfBarsAlongL(3);
        input.setSpacingAlongL(0.2);
        input.setRsbFactorLongitudinalBars(tenMM.getSize());
        input.setRsbFactorTransverseBars(twelveMM.getSize());
        input.setTieWireLength(0.25);
        input.setTransverseBarsUnitLength(0.4);


        WallFootingRebarResult result = wallFootingWorkCalculator.calculate(53,input, Arrays.asList(twelveMM,tenMM));

        System.out.println("# of Transverse bars: " + result.getTraverseBarCount());
        System.out.println("Total L of longitudinal bars: " + result.getLongitudinalBarsTotalLength());
        System.out.println("10 mm rsb: " + result.getTenRSB());
        System.out.println("12 mm rsb: " + result.getTwelveRSB());
        System.out.println("Tie Wire: " + result.getGiTieWire());
    }

}
