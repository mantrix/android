package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying.CHBLayingCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying.CHBLayingCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.create.CreateCHBContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.create.CreateCHBPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateCHBModule {

    @Provides
    @PerActivity
    CreateCHBContract.UserActionListener provideCreateCGBPresenter(CreateCHBPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    CHBLayingCalculator provideChbLayingCalculator(CHBLayingCalculatorImpl calculator){
        return calculator;
    }
}
