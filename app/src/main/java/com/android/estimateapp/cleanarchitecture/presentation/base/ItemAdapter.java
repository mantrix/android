package com.android.estimateapp.cleanarchitecture.presentation.base;

public interface ItemAdapter<E> {

    int LIST_ITEM = 1;
    int LIST_FOOTER = 2;
    int LIST_HEADER = 3;

    /**
     * Set a brand new list
     *
     * @param list
     */
    void setItems(E list);


    /**
     * Add a collection in the existing list
     *
     * @param list
     */
    void addItems(E list);


    /***
     * Show Footer View
     */
    void showFooter();


    /***
     * Remove Footer View
     */
    void removeFooter();

}
