package com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.QtyDiameterFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.QtyDiameterPair;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.ExtraBar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.VerticalBar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterLength;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Stirrups;
import com.android.estimateapp.cleanarchitecture.data.entity.works.StirrupsOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.ComputationInclusions;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.RSBValuePair;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarMaterial;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarSummary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class BeamRebarCalculatorImpl extends BaseCalculator implements BeamRebarCalculator {



    private List<RSBFactor> rsbFactors;
    private ComputationInclusions inclusions;

    @Inject
    public BeamRebarCalculatorImpl() {

    }

    enum Location {
        TOP_BAR,
        BOTTOM_BAR,
        LEFT_BAR,
        RIGHT_BAR,
        MID_BAR,
        SPLICING,
        HOOKES
    }


    @Override
    public BeamRebarSummary generateSummary(List<BeamRebarOutput> results){

        Map<Integer, BeamRebarMaterial> beamRebarMaterialMap = new HashMap<>();
        for (BeamRebarOutput result : results){
            // TOP
            applyData(beamRebarMaterialMap, result.getTopContinuousBars(),Location.TOP_BAR);
            applyData(beamRebarMaterialMap, result.getTopContinuousBars2(),Location.TOP_BAR);

            // BOTTOM
            applyData(beamRebarMaterialMap, result.getBottomContinuousBars(),Location.BOTTOM_BAR);
            applyData(beamRebarMaterialMap, result.getBottomContinuousBars2(),Location.BOTTOM_BAR);

            // LEFT
            applyData(beamRebarMaterialMap, result.getLeftSupport(),Location.LEFT_BAR);
            applyData(beamRebarMaterialMap, result.getLeftSupport2(),Location.LEFT_BAR);

            // RIGHT
            applyData(beamRebarMaterialMap, result.getRightSupport(),Location.RIGHT_BAR);
            applyData(beamRebarMaterialMap, result.getRightSupport2(),Location.RIGHT_BAR);

            // MID
            applyData(beamRebarMaterialMap, result.getExtraBarsMid(),Location.MID_BAR);
            applyData(beamRebarMaterialMap, result.getExtraBarsMid2(),Location.MID_BAR);

            // SPLICING
            applyData(beamRebarMaterialMap, result.getSplicing(),Location.SPLICING);
            applyData(beamRebarMaterialMap, result.getSplicing2(),Location.SPLICING);

            // HOOKES
            applyData(beamRebarMaterialMap, result.getHookes(),Location.HOOKES);
            applyData(beamRebarMaterialMap, result.getHookes2(),Location.HOOKES);
        }

        return new BeamRebarSummary(beamRebarMaterialMap);
    }


    private void applyData(Map<Integer, BeamRebarMaterial> beamRebarMaterialMap, RSBValuePair rsbValuePair, Location location) {
        if (rsbValuePair != null){
            int rebarSize = rsbValuePair.getRsbFactor().getSize();
            if (beamRebarMaterialMap.containsKey(rebarSize)) {
                // get previous data and add the result to it.
                beamRebarMaterialMap.put(rebarSize, addResult(beamRebarMaterialMap.get(rebarSize), rsbValuePair, location));
            } else {
                // create a new map entry and set result to it.
                BeamRebarMaterial material = new BeamRebarMaterial();
                material.setRsbSize(RsbSize.parseInt(rebarSize));
                beamRebarMaterialMap.put(rebarSize, addResult(material, rsbValuePair, location));
            }
        }
    }

    private BeamRebarMaterial addResult(BeamRebarMaterial material, RSBValuePair rsbValuePair, Location location) {

        double value = rsbValuePair.getValue();

        switch (location){
            case TOP_BAR:
                material.topBars += value;
                break;
            case BOTTOM_BAR:
                material.bottomBars += value;
                break;
            case LEFT_BAR:
                material.extraBarLeft += value;
                break;
            case RIGHT_BAR:
                material.extraBarRight += value;
                break;
            case MID_BAR:
                material.extraBarMid += value;
                break;
            case SPLICING:
                material.splicings += value;
                break;
            case HOOKES:
                material.hookes += value;
                break;
        }
        return material;
    } 
    

    @Override
    public BeamRebarOutput calculate(boolean autoCompute, BeamRebarInput input, BeamSectionProperties properties, List<RSBFactor> rsbFactors) {
        this.inclusions = input.getInclusions() != null ? input.getInclusions() : new ComputationInclusions();
        this.rsbFactors = rsbFactors;

        BeamRebarOutput output = new BeamRebarOutput();

        double lengthCC = input.getLengthCC();
        if (autoCompute) {
            lengthCC = input.computeLengthCCBasedOnNodes();
        }

        double lClear = roundHalfUp(lengthCC - input.getOffset() - input.getOffset2());

        double wastage = (1 + toDoublePercentage(input.getWastage()));

        if (properties != null) {
            setBarValues(output, lengthCC, properties.getTopBar(), wastage, Location.TOP_BAR);
            setBarValues(output, lengthCC, properties.getBottomBar(), wastage, Location.BOTTOM_BAR);

            setExtraBarValues(output, lengthCC, properties.getExtraBarAtLeftSupport(), wastage, Location.LEFT_BAR);
            setExtraBarValues(output, lengthCC, properties.getExtraBarAtRightSupport(), wastage, Location.RIGHT_BAR);
            setExtraBarValues(output, lengthCC, properties.getBottomExtraAtMid(), wastage, Location.MID_BAR);

            setQtyDiameterLength(output, properties.getSplicing(), properties.getSplicing2(), wastage, Location.SPLICING);
            setQtyDiameterLength(output, properties.getHookes(), properties.getHookes2(), wastage, Location.HOOKES);

            StirrupsOutput stirrupsOutput = createStirrupsOutput(properties.getStirrups(), lClear,wastage);
            output.setStirrupsOutput(stirrupsOutput);
        }

        output.setlClear(lClear);

        return output;
    }

    private void setQtyDiameterLength(BeamRebarOutput output, QtyDiameterLength qtyDiameterLength, QtyDiameterLength qtyDiameterLength2, double wastage, Location location) {

        if (qtyDiameterLength != null) {
            double val = qtyDiameterLength(qtyDiameterLength, wastage);
            double val2 = qtyDiameterLength(qtyDiameterLength2, wastage);
            double total = (val + val2);

            RSBValuePair rsbValuePair = null;
            RSBValuePair rsbValuePair2 = null;

            if (val != 0 && val2 != 0 &&
                    (qtyDiameterLength.getDiameter() == qtyDiameterLength2.getDiameter())) {
                // combine
                rsbValuePair = new RSBValuePair(getRSB(rsbFactors, qtyDiameterLength.getDiameter()), (val + val2));
            } else {
                if (qtyDiameterLength != null) {
                    // first
                    rsbValuePair = new RSBValuePair(getRSB(rsbFactors, qtyDiameterLength.getDiameter()), val);
                }

                if (qtyDiameterLength2 != null) {
                    // second
                    rsbValuePair2 = new RSBValuePair(getRSB(rsbFactors, qtyDiameterLength2.getDiameter()), val2);
                }
            }

            setOutputValues(output, location, total, rsbValuePair, rsbValuePair2);
        }

    }

    private StirrupsOutput createStirrupsOutput(Stirrups stirrups, double lClear,  double wastage) {
        double theRest = 0;
        double data1 = 0;
        double length = 0;
        RSBFactor gRsbFactor = null;

        if (stirrups != null) {
            data1 = stirrups.getData1();
            if (stirrups.getColumnTieTheRest() != null) {
                theRest = stirrups.getColumnTieTheRest().getSpacing();
            }

            if (stirrups.getColumnTieG() != null) {
                length = stirrups.getColumnTieG().getLength();
                gRsbFactor = getRSB(rsbFactors, stirrups.getColumnTieG().getDiameter());
            }
        }

        double qty = (lClear - data1) / theRest;

        StirrupsOutput stirrupsOutput = new StirrupsOutput();

        if (gRsbFactor != null) {
            double rsbVal = qty * length * gRsbFactor.getWeight() * wastage;

            stirrupsOutput.setSelectedRsb(gRsbFactor.getSize());
            stirrupsOutput.setTotalWeight(rsbVal);
        }

        if (Double.isNaN(Math.floor(qty)) || Double.isInfinite(Math.floor(qty))){
            stirrupsOutput.setQuantity(0);
        } else {
            stirrupsOutput.setQuantity(Math.floor(qty));
        }

        stirrupsOutput.setLength(length);
        stirrupsOutput.setTotal(stirrupsOutput.getRSBTotalWeight());

        return stirrupsOutput;
    }


    private void setExtraBarValues(BeamRebarOutput output, double lengthCC, ExtraBar extraBar, double wastage, Location location) {

        if (extraBar != null) {
            double val = computeExtraBarValue(lengthCC, extraBar.getProperty1(), wastage);
            double val2 = computeExtraBarValue(lengthCC, extraBar.getProperty2(), wastage);

            double total = val + val2;
            RSBValuePair rsbValuePair = null;
            RSBValuePair rsbValuePair2 = null;

            if (val != 0 && val2 != 0 &&
                    (extraBar.getProperty1().getDiameter() == extraBar.getProperty2().getDiameter())) {
                // combine
                rsbValuePair = new RSBValuePair(getRSB(rsbFactors, extraBar.getProperty1().getDiameter()), (val + val2));
            } else {
                if (extraBar.getProperty1() != null) {
                    // first
                    rsbValuePair = new RSBValuePair(getRSB(rsbFactors, extraBar.getProperty1().getDiameter()), val);
                }

                if (extraBar.getProperty2() != null) {

                    // second
                    rsbValuePair2 = new RSBValuePair(getRSB(rsbFactors, extraBar.getProperty2().getDiameter()), val2);
                }
            }

            setOutputValues(output, location, total, rsbValuePair, rsbValuePair2);
        }
    }

    private void setBarValues(BeamRebarOutput output, double lengthCC, VerticalBar verticalBar, double wastage, Location location) {
        if (verticalBar != null) {

            double val = computeBarValue(lengthCC, verticalBar.getProperty1(), wastage);
            double val2 = computeBarValue(lengthCC, verticalBar.getProperty2(), wastage);

            double total = val + val2;
            RSBValuePair rsbValuePair = null;
            RSBValuePair rsbValuePair2 = null;


            if (val != 0 && val2 != 0 &&
                    (verticalBar.getProperty1().getDiameter() == verticalBar.getProperty2().getDiameter())) {
                // combine
                rsbValuePair = new RSBValuePair(getRSB(rsbFactors, verticalBar.getProperty1().getDiameter()), (val + val2));
            } else {

                if (verticalBar.getProperty1() != null) {
                    // first
                    rsbValuePair = new RSBValuePair(getRSB(rsbFactors, verticalBar.getProperty1().getDiameter()), val);
                }

                if (verticalBar.getProperty2() != null) {
                    // second
                    rsbValuePair2 = new RSBValuePair(getRSB(rsbFactors, verticalBar.getProperty2().getDiameter()), val2);
                }
            }

            setOutputValues(output, location, total, rsbValuePair, rsbValuePair2);
        }
    }


    private void setOutputValues(BeamRebarOutput output, Location location, double total, RSBValuePair rsbValuePair, RSBValuePair rsbValuePair2) {

        switch (location) {
            case TOP_BAR:
                if (inclusions.isComputeTopBar1()){
                    output.setTopContinuousBars(rsbValuePair);
                }
                if (inclusions.isComputeTopBar2()) {
                    output.setTopContinuousBars2(rsbValuePair2);
                }
                if (inclusions.isComputeTopBar1() || inclusions.isComputeTopBar2()){
                    output.setTopTotal(total);
                }
                break;
            case BOTTOM_BAR:
                if (inclusions.isComputeBottomBar1()) {
                    output.setBottomContinuousBars(rsbValuePair);
                }

                if (inclusions.isComputeBottomBar2()) {
                    output.setBottomContinuousBars2(rsbValuePair2);
                }

                if (inclusions.isComputeBottomBar1() || inclusions.isComputeBottomBar2()){
                    output.setBottomTotal(total);
                }
                break;
            case MID_BAR:
                output.setMidTotal(total);
                output.setExtraBarsMid(rsbValuePair);
                output.setExtraBarsMid2(rsbValuePair2);
                break;
            case LEFT_BAR:
                if (inclusions.isComputeExtraBarLeft1()) {
                    output.setLeftSupport(rsbValuePair);
                }

                if (inclusions.isComputeExtraBarLeft2()) {
                    output.setLeftSupport2(rsbValuePair2);
                }

                if (inclusions.isComputeExtraBarLeft1() || inclusions.isComputeExtraBarLeft2()) {
                    output.setLeftTotal(total);
                }
                break;
            case RIGHT_BAR:
                if (inclusions.isComputeExtraBarRight1()) {
                    output.setRightSupport(rsbValuePair);
                }

                if (inclusions.isComputeExtraBarRight2()) {
                    output.setRightSupport2(rsbValuePair2);
                }

                if (inclusions.isComputeExtraBarRight1() || inclusions.isComputeExtraBarRight2()) {
                    output.setRightTotal(total);
                }


            case SPLICING:
                if (inclusions.isComputeSplicing()) {
                    output.setSplicing(rsbValuePair);
                    output.setSplicin2(rsbValuePair2);
                    output.setSplicingTotal(total);
                }
                break;
            case HOOKES:
                output.setHookesTotal(total);
                output.setHookes(rsbValuePair);
                output.setHookes2(rsbValuePair2);
                break;
        }
    }


    private double computeBarValue(double lengthCC, QtyDiameterPair qtyDiameterPair, double wastage) {
        if (qtyDiameterPair != null ) {
            if (getRSB(rsbFactors, qtyDiameterPair.getDiameter()) != null){
                return (lengthCC * qtyDiameterPair.getQuantity() * getRSB(rsbFactors, qtyDiameterPair.getDiameter()).getWeight()) * wastage;
            }
        }
        return 0;
    }

    private double computeExtraBarValue(double lengthCC, QtyDiameterFactor qtyDiameterPairFactor, double wastage) {
        if (qtyDiameterPairFactor != null &&  getRSB(rsbFactors, qtyDiameterPairFactor.getDiameter()) != null) {
            return (lengthCC * qtyDiameterPairFactor.getQuantity() * qtyDiameterPairFactor.getLengthFactor() * getRSB(rsbFactors, qtyDiameterPairFactor.getDiameter()).getWeight()) * wastage;
        }
        return 0;
    }

    private double qtyDiameterLength(QtyDiameterLength qtyDiameterLength, double wastage) {
        if (qtyDiameterLength != null &&  getRSB(rsbFactors, qtyDiameterLength.getDiameter()) != null) {
            return (qtyDiameterLength.getQuantity() * qtyDiameterLength.getLength() * getRSB(rsbFactors, qtyDiameterLength.getDiameter()).getWeight()) * wastage;
        }
        return 0;
    }
}
