package com.android.estimateapp.cleanarchitecture.presentation.summary.generalconcrete;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.GeneralConcreteWorkSummary;

public interface GeneralConcreteWorkSummaryContract {


    interface View {
        void displayGeneralSummary(GeneralConcreteWorkSummary<Category> summary);

        void displaySummary(GeneralConcreteWorkSummary<String> summary);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, ScopeOfWork scopeOfWork);

    }
}
