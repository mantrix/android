package com.android.estimateapp.configuration.injection.module.app;


import com.android.estimateapp.cleanarchitecture.domain.executors.SchedulerProvider;
import com.android.estimateapp.cleanarchitecture.domain.executors.UIThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.presentation.application.ApplicationContract;
import com.android.estimateapp.cleanarchitecture.presentation.application.ApplicationPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    @Provides
    @Singleton
    ThreadExecutorProvider provideThreadExecutorProvider(SchedulerProvider schedulerProvider) {
        return schedulerProvider;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }


    @Provides
    @Singleton
    ApplicationContract.UserActionListener provideApplicationPresenter(ApplicationPresenter presenter){
        return presenter;
    }
}
