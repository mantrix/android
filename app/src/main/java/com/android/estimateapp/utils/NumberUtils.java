package com.android.estimateapp.utils;

public class NumberUtils {

    public static boolean convertibleToNumber(String str){
        try
        {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

}
