package com.android.estimateapp.cleanarchitecture.presentation.summary.stonecladding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.StoneCladdingMaterial;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StoneCladdingSummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<StoneCladdingMaterial>> {


    private List<StoneCladdingMaterial> list;

    @Inject
    public StoneCladdingSummaryAdapter() {
        list = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.tile_work_summary_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<StoneCladdingMaterial> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<StoneCladdingMaterial> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }


    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_tile_name)
        TextView tvStoneType;

        @BindView(R.id.tv_sand)
        TextView tvSand;

        @BindView(R.id.tv_cement)
        TextView tvCement;

        @BindView(R.id.tv_adhesive)
        TextView tvAdhesive;

        @BindView(R.id.tv_area)
        TextView tvArea;

        @BindView(R.id.tv_tiles)
        TextView tvQuantity;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(StoneCladdingMaterial materials, int position) {

            tvStoneType.setText(materials.stoneType.getName());
            tvArea.setText(DisplayUtil.toPlainString(materials.area));
            tvSand.setText(DisplayUtil.toPlainString(materials.sand));
            tvCement.setText(DisplayUtil.toPlainString(materials.cement));
            tvAdhesive.setText(DisplayUtil.toPlainString(materials.adhesive));
            tvQuantity.setText(DisplayUtil.toPlainString(materials.quantity));
        }
    }
}