package com.android.estimateapp.configuration.injection.module.factor;


import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.tiewire.CHBTieWireFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.tiewire.CHBTieWireFactorPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class TieWireFactorModule {

    @Provides
    @PerFragment
    CHBTieWireFactorContract.UserActionListener providesCHBTieWireFactorPresenter(CHBTieWireFactorPresenter presenter) {
        return presenter;
    }
}
