package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum PlasteringLocation {

    EXTERIOR(1,"Exterior"),
    INTERIOR(2,"Interior");


    int id;
    String name;

    PlasteringLocation(int id,String name){
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    public static List<PlasteringLocation> allPlasteringLocation(){
        List<PlasteringLocation> list = new ArrayList<>();

        list.add(EXTERIOR);
        list.add(INTERIOR);

        return list;
    }

    public static PlasteringLocation parseString(String value){
        if (value != null){
            for (PlasteringLocation location : allPlasteringLocation()){
                if (value.equalsIgnoreCase(location.toString())){
                    return location;
                }
            }
        }
       return null;
    }
}
