package com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.list;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface ColumnSectionPropertiesListContract {

    interface View {

        void displayList(List<ColumnRebarProperties> inputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
