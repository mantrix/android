package com.android.estimateapp.cleanarchitecture.domain.exception;

public class NoConnectionException extends Exception {

    public NoConnectionException(Throwable throwable){
        super(throwable);
    }
}
