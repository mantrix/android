package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.material.tiles.TileWorkMaterialContract;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.tiles.TileWorkMaterialDialogPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class TileWorkMaterialModule {

    @Provides
    @PerFragment
    TileWorkMaterialContract.UserActionListener provideTileWorkMaterialPresenter(TileWorkMaterialDialogPresenter presenter) {
        return presenter;
    }
}
