package com.android.estimateapp.cleanarchitecture.data.entity.works.plastering;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.PlasteringLocation;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.SurfaceType;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class PlasteringInput extends Model {

    Dimension dimension;

    double thickness;

    double openings;

    int sides;

    double wastage;

    int mortarClass;

    String location;

    String surfaceType;

    String description;

    public PlasteringInput(){

    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    public double getOpenings() {
        return openings;
    }

    public void setOpenings(double openings) {
        this.openings = openings;
    }

    public int getSides() {
        return sides;
    }

    public void setSides(int sides) {
        this.sides = sides;
    }

    public double getWastage() {
        return wastage;
    }

    public void setWastage(double wastage) {
        this.wastage = wastage;
    }

    @Exclude
    public Grade getMortarClassEnum() {
        return Grade.parseInt(mortarClass);
    }

    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int concreteMortarClass) {
        this.mortarClass = concreteMortarClass;
    }

    @Exclude
    public PlasteringLocation getLocationEnum() {
        return PlasteringLocation.parseString(location);
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Exclude
    public SurfaceType getSurfaceTypeEnum() {
        return SurfaceType.parseString(surfaceType);
    }

    public String getSurfaceType() {
        return surfaceType;
    }

    public void setSurfaceType(String surfaceType) {
        this.surfaceType = surfaceType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
