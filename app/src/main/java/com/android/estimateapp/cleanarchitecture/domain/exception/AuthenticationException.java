package com.android.estimateapp.cleanarchitecture.domain.exception;

import com.android.estimateapp.configuration.Constants;

public class AuthenticationException extends Exception {

    private Constants.Code code;

    public AuthenticationException(Constants.Code code){
        this.code = code;
    }


    public AuthenticationException(Constants.Code code, Throwable throwable){
        super(throwable);
        this.code = code;
    }

    public Constants.Code getCode() {
        return code;
    }
}
