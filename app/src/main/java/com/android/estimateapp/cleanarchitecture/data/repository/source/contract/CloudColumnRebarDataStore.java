package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;

import java.util.List;

import io.reactivex.Observable;

public interface CloudColumnRebarDataStore {

    Observable<ColumnRebarInput> createColumnRebarInput(String projectId, String scopeOfWorkId, ColumnRebarInput input);

    Observable<ColumnRebarInput> updateColumnRebarInput(String projectId, String scopeOfWorkId, String itemId, ColumnRebarInput input);

    Observable<List<ColumnRebarInput>> getColumnRebarInputs(String projectId, String scopeOfWorkId);

    Observable<ColumnRebarInput> getColumnRebarInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<ColumnRebarOutput> saveColumnRebarInputResult(String projectId, String scopeOfWorkId, String itemId, ColumnRebarOutput output);
}
