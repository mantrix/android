package com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PlasteringListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<PlasteringInput>> {


    private List<PlasteringInput> list;

    ListItemClickLister<PlasteringInput> listItemClickLister;

    @Inject
    public PlasteringListAdapter() {
        list = new ArrayList<>();
    }

    public void setListItemClickLister(ListItemClickLister<PlasteringInput> listItemClickLister) {
        this.listItemClickLister = listItemClickLister;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.plastering_input_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public void setItems(List<PlasteringInput> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<PlasteringInput> list) {

    }

    public void deleteItem(String id){
        Integer position = null;
        for (int i = 0 ; i < list.size() ; i++){
            if (list.get(i).getKey().equalsIgnoreCase(id)){
                position = i;
                list.remove(i);
                break;
            }
        }
        if (position != null){
            notifyItemRemoved(position);
        }
    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv_item)
        CardView cvItem;

        @BindView(R.id.tv_location)
        TextView tvLocation;

        @BindView(R.id.tv_description)
        TextView tvDescription;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(PlasteringInput plasteringInput) {

            StringBuilder stringBuilder = new StringBuilder();

            if (plasteringInput.getLocationEnum() != null && !plasteringInput.getLocationEnum().toString().isEmpty()){
                stringBuilder.append(plasteringInput.getLocationEnum().toString());
            }

            if (plasteringInput.getSurfaceTypeEnum() != null && !plasteringInput.getSurfaceTypeEnum().toString().isEmpty()){
                stringBuilder.append( " - " + plasteringInput.getSurfaceTypeEnum().toString());
            }

            if (!stringBuilder.toString().isEmpty()){
                tvLocation.setText(stringBuilder.toString());
            }

            if (plasteringInput.getDescription() != null){
                tvDescription.setText(plasteringInput.getDescription());
            } else {
                tvDescription.setText("");
            }

            cvItem.setOnClickListener(v -> {
                listItemClickLister.itemClick(plasteringInput);
            });

            cvItem.setOnLongClickListener(v -> {
                listItemClickLister.onLongClick(plasteringInput);
                return false;
            });
        }
    }
}