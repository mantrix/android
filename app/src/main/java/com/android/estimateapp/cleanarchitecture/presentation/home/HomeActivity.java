package com.android.estimateapp.cleanarchitecture.presentation.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.listing.ProjectListFragment;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.mantrixengineering.estimateapp.R;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends BaseActivity implements HomeContract.View, NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.fab)
    FloatingActionButton addProjectFab;

    TextView tvName;

    LinearLayout llProfile;

    CircleImageView ivProfilePhoto;

    @Inject
    HomeContract.UserActionListener presenter;


    @Override
    protected int getLayoutResource() {
        return R.layout.home_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(Constants.RESULT_CODE_HOME_BACK_BUTTON);

        setSupportActionBar(toolbar);
        setToolbarTitle(R.string.projects);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer_menu);

        View header = navigationView.getHeaderView(0);
        tvName = header.findViewById(R.id.tv_name);
        llProfile = header.findViewById(R.id.ll_profile);
        ivProfilePhoto = header.findViewById(R.id.iv_profile_image);
        llProfile.setOnClickListener(v -> {
            drawerLayout.closeDrawers();
            startActivity(ProfileActivity.createIntent(this));
        });

        presenter.setView(this);
        presenter.start();

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // close drawer when item is tapped
        drawerLayout.closeDrawers();

        switch (menuItem.getItemId()){
            case R.id.nav_logout:
                presenter.logout();
                break;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void displayProfilePhoto(String url) {
        Picasso.get().load(url).into(ivProfilePhoto);
    }

    @Override
    public void showProjectList() {
        showFragment(R.id.rl_fragment_container, ProjectListFragment.newInstance(), false);

    }

    @Override
    public void setName(String name) {
        tvName.setText(name);
    }


    @Override
    public void onLogout() {
        setResult(Constants.RESULT_LOGOUT);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    protected void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
