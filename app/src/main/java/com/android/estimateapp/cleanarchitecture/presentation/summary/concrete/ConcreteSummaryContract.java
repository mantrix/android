package com.android.estimateapp.cleanarchitecture.presentation.summary.concrete;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.GeneralConcreteWorkSummary;

public interface ConcreteSummaryContract {

    interface View extends SummaryView<GeneralConcreteWorkSummary> {

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
