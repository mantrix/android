package com.android.estimateapp.cleanarchitecture.data.repository;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.data.entity.project.WorkerGroup;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudBeamRebarStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudCHBStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudColumnFootingRebarDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudColumnRebarDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudConcreteWorkDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudDoorAndWindowDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudFactorDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudPlasteringStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudScopeWorkDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudStoneCladdingDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudTileWorkStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudWallFootingRebarDataStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.project.CloudProjectDataStore;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;

@Singleton
public class ProjectDataRepository implements ProjectRepository {

    private CloudProjectDataStore cloudProjectDataStore;
    private CloudScopeWorkDataStore cloudScopeWorkDataStore;
    private CloudFactorDataStore cloudFactorDataStore;
    private CloudCHBStore cloudCHBStore;
    private CloudPlasteringStore cloudPlasteringStore;
    private CloudConcreteWorkDataStore cloudConcreteWorkDataStore;
    private CloudTileWorkStore cloudTileWorkStore;
    private CloudStoneCladdingDataStore cloudStoneCladdingDataStore;
    private CloudDoorAndWindowDataStore cloudDoorAndWindowDataStore;
    private CloudWallFootingRebarDataStore cloudWallFootingRebarDataStore;
    private CloudColumnFootingRebarDataStore cloudColumnFootingRebarDataStore;
    private CloudBeamRebarStore cloudBeamRebarStore;
    private CloudColumnRebarDataStore cloudColumnRebarDataStore;


    @Inject
    public ProjectDataRepository(CloudProjectDataStore cloudProjectDataStore,
                                 CloudScopeWorkDataStore cloudScopeWorkDataStore,
                                 CloudFactorDataStore cloudFactorDataStore,
                                 CloudCHBStore cloudCHBStore,
                                 CloudPlasteringStore cloudPlasteringStore,
                                 CloudConcreteWorkDataStore cloudConcreteWorkDataStore,
                                 CloudTileWorkStore cloudTileWorkStore,
                                 CloudStoneCladdingDataStore cloudStoneCladdingDataStore,
                                 CloudDoorAndWindowDataStore cloudDoorAndWindowDataStore,
                                 CloudWallFootingRebarDataStore cloudWallFootingRebarDataStore,
                                 CloudColumnFootingRebarDataStore cloudColumnFootingRebarDataStore,
                                 CloudBeamRebarStore cloudBeamRebarStore,
                                 CloudColumnRebarDataStore cloudColumnRebarDataStore) {
        this.cloudProjectDataStore = cloudProjectDataStore;
        this.cloudScopeWorkDataStore = cloudScopeWorkDataStore;
        this.cloudFactorDataStore = cloudFactorDataStore;
        this.cloudCHBStore = cloudCHBStore;
        this.cloudPlasteringStore = cloudPlasteringStore;
        this.cloudConcreteWorkDataStore = cloudConcreteWorkDataStore;
        this.cloudTileWorkStore = cloudTileWorkStore;
        this.cloudStoneCladdingDataStore = cloudStoneCladdingDataStore;
        this.cloudDoorAndWindowDataStore = cloudDoorAndWindowDataStore;
        this.cloudWallFootingRebarDataStore = cloudWallFootingRebarDataStore;
        this.cloudColumnFootingRebarDataStore = cloudColumnFootingRebarDataStore;
        this.cloudBeamRebarStore = cloudBeamRebarStore;
        this.cloudColumnRebarDataStore = cloudColumnRebarDataStore;
    }

    // PROJECTS

    @Override
    public Observable<String> createProject(Project project) {
        return cloudProjectDataStore.createProject(project);
    }

    @Override
    public Observable<List<String>> getUserProjectsIds() {
        return cloudProjectDataStore.getUserProjectIds();
    }

    @Override
    public Observable<Project> getProject(String id) {
        return cloudProjectDataStore.getProject(id);
    }

    @Override
    public Observable<List<Project>> getUserProjects() {
        return cloudProjectDataStore.getUserProjects()
                .flatMap(Observable::fromIterable)
                .toSortedList((project, project2) -> Long.compare(project2.getCreatedAt(), project.getCreatedAt()))
                .toObservable();
    }

    @Override
    public Completable deleteProject(String id) {
        return cloudProjectDataStore.deleteProject(id);
    }

    // SCOPES

    @Override
    public Observable<List<Work>> getProjectScopesOfWorks(String projectId) {
        return cloudScopeWorkDataStore.getProjectScopes(projectId);
    }

    @Override
    public Observable<Work> createProjectScopeOfWork(String projectId, Work work) {
        return cloudScopeWorkDataStore.createProjectScope(projectId, work);
    }

    @Override
    public Completable deleteScopeInput(String projectId, String scopeOfWorkId, String inputId) {
        return cloudScopeWorkDataStore.deleteScopeInput(projectId, scopeOfWorkId, inputId);
    }

    // FACTORS

    @Override
    public Observable<RSBFactor> createRSBFactor(String projectId, RSBFactor rsbFactor) {
        return cloudFactorDataStore.createProjectShareRSBFactors(projectId, rsbFactor);
    }

    @Override
    public Observable<List<RSBFactor>> getProjectRSBFactors(String projectId) {
        return cloudFactorDataStore.getProjectRSBFactors(projectId);
    }

    @Override
    public Observable<List<RSBFactor>> updateProjectRSBFactors(String projectId, List<RSBFactor> rsbFactors) {
        return cloudFactorDataStore.updateProjectRSBFactors(projectId, rsbFactors);
    }

    // SPECIFIC FACTORS

    @Override
    public Observable<CHBFactor> createCHBFactor(String projectId, CHBFactor chbFactor) {
        return cloudCHBStore.createCHBFactor(projectId, chbFactor);
    }

    @Override
    public Observable<CHBFactor> getCHBFactor(String projectId) {
        return cloudCHBStore.getCHBFactor(projectId);
    }


    @Override
    public Observable<List<CHBMortarClass>> updateCHBMortarClasses(String projectId, double chb, List<CHBMortarClass> chbMortarClasses) {
        return cloudCHBStore.updateCHBMortarClasses(projectId, chb, chbMortarClasses);
    }

    @Override
    public Observable<List<VerticalSpacing>> updateCHBVerticalSpacing(String projectId, List<VerticalSpacing> verticalSpacings) {
        return cloudCHBStore.updateCHBVerticalSpacingObservable(projectId, verticalSpacings);
    }

    @Override
    public Observable<List<HorizontalSpacing>> updateCHBHorizontalSpacing(String projectId, List<HorizontalSpacing> horizontalSpacings) {
        return cloudCHBStore.updateCHBHorizontalSpacingObservable(projectId, horizontalSpacings);
    }

    @Override
    public Observable<List<TieWireRebarSpacingFactor>> updateCHBTieWireRebarSpacingFactor(String projectId, List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors) {
        return cloudCHBStore.updateCHBTieWireRebarSpacingFactorObservable(projectId, tieWireRebarSpacingFactors);
    }

    // CHB

    @Override
    public Observable<CHBInput> createCHBInput(String projectId, String scopeOfWorkId, CHBInput chbInput) {
        return cloudCHBStore.createCHBInput(projectId, scopeOfWorkId, chbInput);
    }

    @Override
    public Observable<CHBInput> updateCHBInput(String projectId, String scopeOfWorkId, String itemId, CHBInput chbInput) {
        return cloudCHBStore.updateCHBInput(projectId, scopeOfWorkId, itemId, chbInput);
    }

    @Override
    public Observable<CHBResult> saveCHBInputResult(String projectId, String scopeOfWorkId, String itemId, CHBResult result) {
        return cloudCHBStore.saveCHBInputResult(projectId, scopeOfWorkId, itemId, result);
    }

    @Override
    public Observable<List<CHBInput>> getCHBInputs(String projectId, String scopeOfWorkId) {
        return cloudCHBStore.getCHBInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<CHBInput> getCHBInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudCHBStore.getCHBInput(projectId, scopeOfWorkId, itemId);
    }

    // PLASTERING

    @Override
    public Observable<PlasteringInput> createPlasteringInput(String projectId, String scopeOfWorkId, PlasteringInput plasteringInput) {
        return cloudPlasteringStore.createPlasteringInput(projectId, scopeOfWorkId, plasteringInput);
    }

    @Override
    public Observable<PlasteringInput> updatePlasteringInput(String projectId, String scopeOfWorkId, String itemId, PlasteringInput plasteringInput) {
        return cloudPlasteringStore.updatePlasteringInput(projectId, scopeOfWorkId, itemId, plasteringInput);
    }

    @Override
    public Observable<PlasteringResult> savePlasteringInputResult(String projectId, String scopeOfWorkId, String itemId, PlasteringResult result) {
        return cloudPlasteringStore.savePlasteringInputResult(projectId, scopeOfWorkId, itemId, result);
    }

    @Override
    public Observable<List<PlasteringInput>> getPlasteringInputs(String projectId, String scopeOfWorkId) {
        return cloudPlasteringStore.getPlasteringInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<PlasteringInput> getPlasteringInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudPlasteringStore.getPlasteringInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<List<PlasteringMortarFactor>> createPlasteringMortarFactors(String projectId, List<PlasteringMortarFactor> plasteringMortarFactors) {
        return cloudPlasteringStore.createPlasteringMortarFactors(projectId, plasteringMortarFactors);
    }

    @Override
    public Observable<List<PlasteringMortarFactor>> getPlasteringMortarFactors(String projectId) {
        return cloudPlasteringStore.getPlasteringMortarFactors(projectId);
    }

    @Override
    public Observable<List<PlasteringMortarFactor>> updatePlasteringMortarFactors(String projectId, List<PlasteringMortarFactor> plasteringMortarFactors) {
        return cloudPlasteringStore.updatePlasteringMortarFactors(projectId, plasteringMortarFactors);
    }


    // CONCRETE WORK


    @Override
    public Observable<ConcreteWorksInput> createConcreteWorkInput(String projectId, String scopeOfWorkId, ConcreteWorksInput input) {
        return cloudConcreteWorkDataStore.createConcreteWorkInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<ConcreteWorksInput> updateConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId, ConcreteWorksInput input) {
        return cloudConcreteWorkDataStore.updateConcreteWorkInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<List<ConcreteWorksInput>> getConcreteWorkInputs(String projectId, String scopeOfWorkId) {
        return cloudConcreteWorkDataStore.getConcreteWorkInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<ConcreteWorksInput> getConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudConcreteWorkDataStore.getConcreteWorkInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<List<ConcreteFactor>> updateConcreteWorkFactors(String projectId, List<ConcreteFactor> concreteFactors) {
        return cloudConcreteWorkDataStore.updateConcreteWorkFactors(projectId, concreteFactors);
    }

    @Override
    public Observable<List<ConcreteFactor>> getConcreteWorkFactors(String projectId) {
        return cloudConcreteWorkDataStore.getConcreteWorkFactors(projectId);
    }

    @Override
    public Observable<List<ConcreteFactor>> createConcreteWorksFactors(String projectId, List<ConcreteFactor> concreteFactors) {
        return cloudConcreteWorkDataStore.createConcreteWorksFactors(projectId, concreteFactors);
    }

    @Override
    public Observable<RectangularConcreteInput> createRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, RectangularConcreteInput input) {
        return cloudConcreteWorkDataStore.createRectangularConcreteWorkInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<RectangularConcreteInput> updateRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId, RectangularConcreteInput input) {
        return cloudConcreteWorkDataStore.updateRectangularConcreteWorkInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<List<RectangularConcreteInput>> getRectangularConcreteWorkInputs(String projectId, String scopeOfWorkId) {
        return cloudConcreteWorkDataStore.getRectangularConcreteWorkInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<RectangularConcreteInput> getRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudConcreteWorkDataStore.getRectangularConcreteWorkInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<GeneralConcreteWorksResult> saveConcreteWorkInputResult(String projectId, String scopeOfWorkId, String itemId, GeneralConcreteWorksResult result) {
        return cloudConcreteWorkDataStore.saveConcreteWorkInputResult(projectId, scopeOfWorkId, itemId, result);
    }

    @Override
    public Observable<RectangularGeneralConcreteResult> saveRectangularConcreteWorkInputResult(String projectId, String scopeOfWorkId, String itemId, RectangularGeneralConcreteResult result) {
        return cloudConcreteWorkDataStore.saveRectangularConcreteWorkInputResult(projectId, scopeOfWorkId, itemId, result);
    }

    // TILE WORK

    @Override
    public Observable<TileWorkInput> getTileWorkInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudTileWorkStore.getTileWorkInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<List<TileWorkInput>> getTileWorkInputs(String projectId, String scopeOfWorkId) {
        return cloudTileWorkStore.getTileWorkInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<TileWorkInput> updateTileWorkInput(String projectId, String scopeOfWorkId, String itemId, TileWorkInput input) {
        return cloudTileWorkStore.updateTileWorkInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<TileWorkInput> createTileWorkInput(String projectId, String scopeOfWorkId, TileWorkInput input) {
        return cloudTileWorkStore.createTileWorkInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<List<TileWorkFactor>> updateTileWorkFactors(String projectId, List<TileWorkFactor> tileWorkFactors) {
        return cloudTileWorkStore.updateTileWorkFactors(projectId, tileWorkFactors);
    }

    @Override
    public Observable<List<TileWorkFactor>> getTileWorkFactors(String projectId) {
        return cloudTileWorkStore.getTileWorkFactors(projectId);
    }

    @Override
    public Observable<List<TileWorkFactor>> createTileWorkFactors(String projectId, List<TileWorkFactor> factors) {
        return cloudTileWorkStore.createTileWorkFactors(projectId, factors);
    }

    @Override
    public Observable<TileType> createTileTypeFactor(String projectId, TileType tileType) {
        return cloudTileWorkStore.createTileTypeFactor(projectId, tileType);
    }

    @Override
    public Observable<List<TileType>> getTileTypeFactors(String projectId) {
        return cloudTileWorkStore.getTileTypeFactors(projectId);
    }

    @Override
    public Observable<List<TileType>> updateTileTypeFactors(String projectId, List<TileType> factors) {
        return cloudTileWorkStore.updateTileTypeFactors(projectId, factors);
    }

    // STONE CLADDING

    @Override
    public Observable<StoneCladdingInput> createStoneCladdingInput(String projectId, String scopeOfWorkId, StoneCladdingInput input) {
        return cloudStoneCladdingDataStore.createStoneCladdingInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<StoneCladdingInput> updateStoneCladdingInput(String projectId, String scopeOfWorkId, String itemId, StoneCladdingInput input) {
        return cloudStoneCladdingDataStore.updateStoneCladdingInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<StoneCladdingResult> saveStoneCladdingInputResult(String projectId, String scopeOfWorkId, String itemId, StoneCladdingResult result) {
        return cloudStoneCladdingDataStore.saveStoneCladdingInputResult(projectId, scopeOfWorkId, itemId, result);
    }

    @Override
    public Observable<List<StoneCladdingInput>> getStoneCladdingInputs(String projectId, String scopeOfWorkId) {
        return cloudStoneCladdingDataStore.getStoneCladdingInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<StoneCladdingInput> getStoneCladdingInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudStoneCladdingDataStore.getStoneCladdingInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<List<StoneCladdingFactor>> createStoneCladdingFactors(String projectId, List<StoneCladdingFactor> factors) {
        return cloudStoneCladdingDataStore.createStoneCladdingFactors(projectId, factors);
    }

    @Override
    public Observable<List<StoneCladdingFactor>> getStoneCladdingFactors(String projectId) {
        return cloudStoneCladdingDataStore.getStoneCladdingFactors(projectId);
    }

    @Override
    public Observable<List<StoneCladdingFactor>> updateStoneCladdingFactors(String projectId, List<StoneCladdingFactor> factors) {
        return cloudStoneCladdingDataStore.updateStoneCladdingFactors(projectId, factors);
    }


    @Override
    public Observable<StoneType> createStoneTypeFactor(String projectId, StoneType stoneType) {
        return cloudStoneCladdingDataStore.createStoneTypeFactor(projectId, stoneType);
    }

    @Override
    public Observable<List<StoneType>> getStoneTypeFactors(String projectId) {
        return cloudStoneCladdingDataStore.getStoneTypeFactors(projectId);
    }

    @Override
    public Observable<List<StoneType>> updateStoneTypeFactors(String projectId, List<StoneType> factors) {
        return cloudStoneCladdingDataStore.updateStoneTypeFactors(projectId, factors);
    }

    @Override
    public Observable<DoorWindowInputBase> createDoorAndWindowInput(String projectId, String scopeOfWorkId, DoorWindowInputBase input) {
        return cloudDoorAndWindowDataStore.createDoorAndWindowInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<DoorWindowInputBase> updateDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId, DoorWindowInputBase input) {
        return cloudDoorAndWindowDataStore.updateDoorAndWindowInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<DoorWindowOutputBase> saveDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId, DoorWindowOutputBase result) {
        return cloudDoorAndWindowDataStore.saveDoorAndWindowInput(projectId, scopeOfWorkId, itemId, result);
    }

    @Override
    public Observable<List<DoorWindowInputBase>> getDoorAndWindowInputs(String projectId, String scopeOfWorkId) {
        return cloudDoorAndWindowDataStore.getDoorAndWindowInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<DoorWindowInputBase> getDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudDoorAndWindowDataStore.getDoorAndWindowInput(projectId, scopeOfWorkId, itemId);
    }


    @Override
    public Observable<WallFootingRebarInput> createWallFootingInput(String projectId, String scopeOfWorkId, WallFootingRebarInput input) {
        return cloudWallFootingRebarDataStore.createWallFootingInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<WallFootingRebarInput> updateWallFootingInput(String projectId, String scopeOfWorkId, String itemId, WallFootingRebarInput input) {
        return cloudWallFootingRebarDataStore.updateWallFootingInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<List<WallFootingRebarInput>> getWallFootingInputs(String projectId, String scopeOfWorkId) {
        return cloudWallFootingRebarDataStore.getWallFootingInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<WallFootingRebarInput> getWallFootingInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudWallFootingRebarDataStore.getWallFootingInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<WallFootingRebarResult> saveWallFootingInputResult(String projectId, String scopeOfWorkId, String itemId, WallFootingRebarResult result) {
        return cloudWallFootingRebarDataStore.saveWallFootingInputResult(projectId, scopeOfWorkId, itemId, result);
    }

    @Override
    public Observable<ColumnFootingRebarResult> saveColumnFootingInputResult(String projectId, String scopeOfWorkId, String itemId, ColumnFootingRebarResult result) {
        return cloudColumnFootingRebarDataStore.saveColumnFootingInputResult(projectId, scopeOfWorkId, itemId, result);
    }

    @Override
    public Observable<ColumnFootingRebarInput> createColumnFootingInput(String projectId, String scopeOfWorkId, ColumnFootingRebarInput input) {
        return cloudColumnFootingRebarDataStore.createColumnFootingInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<ColumnFootingRebarInput> updateColumnFootingInput(String projectId, String scopeOfWorkId, String itemId, ColumnFootingRebarInput input) {
        return cloudColumnFootingRebarDataStore.updateColumnFootingInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<List<ColumnFootingRebarInput>> getColumnFootingInputs(String projectId, String scopeOfWorkId) {
        return cloudColumnFootingRebarDataStore.getColumnFootingInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<ColumnFootingRebarInput> getColumnFootingInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudColumnFootingRebarDataStore.getColumnFootingInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<BeamRebarInput> createBeamRebarInputInput(String projectId, String scopeOfWorkId, BeamRebarInput input) {
        return cloudBeamRebarStore.createBeamRebarInputInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<BeamRebarOutput> saveBeamRebarInputResult(String projectId, String scopeOfWorkId, String itemId, BeamRebarOutput result) {
        return cloudBeamRebarStore.saveBeamRebarInputResult(projectId, scopeOfWorkId, itemId, result);
    }

    @Override
    public Observable<BeamRebarInput> updateBeamRebarInputInput(String projectId, String scopeOfWorkId, String itemId, BeamRebarInput input) {
        return cloudBeamRebarStore.updateBeamRebarInputInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<List<BeamRebarInput>> getBeamRebarInputs(String projectId, String scopeOfWorkId) {
        return cloudBeamRebarStore.getBeamRebarInputInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<BeamRebarInput> getBeamRebarInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudBeamRebarStore.getBeamRebarInputInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<ColumnRebarInput> createColumnRebarInput(String projectId, String scopeOfWorkId, ColumnRebarInput input) {
        return cloudColumnRebarDataStore.createColumnRebarInput(projectId, scopeOfWorkId, input);
    }

    @Override
    public Observable<ColumnRebarInput> updateColumnRebarInput(String projectId, String scopeOfWorkId, String itemId, ColumnRebarInput input) {
        return cloudColumnRebarDataStore.updateColumnRebarInput(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<ColumnRebarOutput> saveColumnRebarInputResult(String projectId, String scopeOfWorkId, String itemId, ColumnRebarOutput output) {
        return cloudColumnRebarDataStore.saveColumnRebarInputResult(projectId, scopeOfWorkId, itemId, output);
    }

    @Override
    public Observable<List<ColumnRebarInput>> getColumnRebarInputs(String projectId, String scopeOfWorkId) {
        return cloudColumnRebarDataStore.getColumnRebarInputs(projectId, scopeOfWorkId);
    }

    @Override
    public Observable<ColumnRebarInput> getColumnRebarInput(String projectId, String scopeOfWorkId, String itemId) {
        return cloudColumnRebarDataStore.getColumnRebarInput(projectId, scopeOfWorkId, itemId);
    }

    @Override
    public Observable<WorkerGroup> createLaborCost(String projectId, String scopeOfWorkId, WorkerGroup group) {
        return cloudProjectDataStore.createLaborCost(projectId, scopeOfWorkId, group);
    }

    @Override
    public Observable<WorkerGroup> updateLaborCost(String projectId, String scopeOfWorkId, String itemId, WorkerGroup input) {
        return cloudProjectDataStore.updateLaborCost(projectId, scopeOfWorkId, itemId, input);
    }

    @Override
    public Observable<List<WorkerGroup>> getLaborCosts(String projectId, String scopeOfWorkId) {
        return cloudProjectDataStore.getLaborCosts(projectId, scopeOfWorkId);
    }

    @Override
    public Completable deleteLaborCost(String projectId, String scopeOfWorkId, String inputId) {
        return cloudProjectDataStore.deleteLaborCost(projectId, scopeOfWorkId, inputId);
    }

    @Override
    public Observable<WorkerGroup> getLaborCost(String projectId, String scopeOfWorkId, String inputId) {
        return cloudProjectDataStore.getLaborCost(projectId, scopeOfWorkId, inputId);
    }


    @Override
    public <T extends Model> Observable<T> createMaterial(String projectId, String scopeOfWorkId, T model) {
        return cloudProjectDataStore.createMaterial(projectId, scopeOfWorkId, model);
    }

    @Override
    public <T extends Model> Observable<T> getMaterial(String projectId, String scopeOfWorkId, Class<T> type) {
        return cloudProjectDataStore.getMaterial(projectId, scopeOfWorkId, type);
    }

    @Override
    public <T extends Model> Observable<T> updateMaterial(String projectId, String scopeOfWorkId, T input) {
        return cloudProjectDataStore.updateMaterial(projectId, scopeOfWorkId, input);
    }
}
