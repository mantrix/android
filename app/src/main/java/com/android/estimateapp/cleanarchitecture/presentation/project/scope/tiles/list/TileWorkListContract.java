package com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface TileWorkListContract {

    interface View {

        void displayList(List<TileWorkInput> tileWorkInputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
