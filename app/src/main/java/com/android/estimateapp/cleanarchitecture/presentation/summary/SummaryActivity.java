package com.android.estimateapp.cleanarchitecture.presentation.summary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.summary.beamrebar.BeamRebarSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.CHBSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.columnfooting.ColumnFootingMaterialSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.columnrebar.ColumnRebarSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.generalconcrete.ConcreteWorkSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.plastering.PlasteringSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.stonecladding.StoneCladdingSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.tilework.TileWorkSummaryFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.wallfooting.WallFootingMaterialSummaryFragment;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import butterknife.BindView;

public class SummaryActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ScopeOfWork scopeOfWork;

    private String projectId;

    private String scopeKey;

    public static Intent createIntent(Context context, String projectId, String scopeKey, int scopeOfWork) {
        Intent intent = new Intent(context, SummaryActivity.class);
        intent.putExtra(Constants.SCOPE_OF_WORK, scopeOfWork);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);

        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.summary_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        extractExtras();
        setToolbarTitle(getString(R.string.summary));

        displaySummary();
    }

    private void displaySummary(){
        Fragment fragment = null;

        switch (scopeOfWork){
            case CHB_LAYING:
                fragment = CHBSummaryFragment.newInstance(projectId,scopeKey);
                break;
            case CHB_PLASTERING:
                fragment = PlasteringSummaryFragment.newInstance(projectId,scopeKey);
                break;
            case CONCRETE_WORKS:
            case BEAM_CONCRETE:
            case COLUMN_CONCRETE:
                fragment = ConcreteWorkSummaryFragment.newInstance(projectId,scopeKey,scopeOfWork.getId());
                break;
            case TILE_WORKS:
                fragment = TileWorkSummaryFragment.newInstance(projectId,scopeKey);
                break;
            case STONE_CLADDING:
                fragment = StoneCladdingSummaryFragment.newInstance(projectId,scopeKey);
                break;
            case WALL_FOOTING_REBAR:
                fragment = WallFootingMaterialSummaryFragment.newInstance(projectId,scopeKey);
                break;
            case COLUMN_FOOTING_REBAR:
                fragment = ColumnFootingMaterialSummaryFragment.newInstance(projectId, scopeKey);
                break;
            case BEAM_REBAR:
                fragment = BeamRebarSummaryFragment.newInstance(projectId,scopeKey);
                break;
            case COLUMN_REBAR:
                fragment = ColumnRebarSummaryFragment.newInstance(projectId,scopeKey);
                break;
        }

        if (fragment != null){
            showFragment(R.id.rl_fragment_container, fragment, false);
        }
    }


    private void extractExtras() {
        Intent intent = getIntent();
        scopeOfWork = ScopeOfWork.parseInt(intent.getIntExtra(Constants.SCOPE_OF_WORK, 0));
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
    }

}
