package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum Category {

    EXCAVATION(1, "Excavation"),
    BACKFILL(2, "Backfill"),
    GRAVEL_BEDDING(3, "Gravel Bedding"),
    COLUMN_FOOTING(4, "Column Footing"),
    WALL_FOOTING(5, "Wall Footing"),
    COLUMN(6, "Column"),
    TIE_BEAM(7, "Tie Beam"),
    GRADE_BEAM(8, "Grade Beam"),
    BEAM(9, "Beam"),
    SLAB_ON_GRADE(10, "Slab on Grade"),
    SLAB(11, "Slab"),
    SUSPENDED_SLAB(12, "Suspended Slab"),
    STAIR(13, "Stair"),
    SHEAR_WALL(14, "Shear Wall"),
    PLAIN_CONCRETE(15, "Plain Concrete"),
    PILES(17, "Piles"),
    FLOOR_TILES(18, "Floor Tiles"),
    WALL_TILES(19, "Wall Tiles"),
    SINK_TILES(20, "Sink Tiles"),
    SLAB_ON_FILL(21, "Slab on Fill"),
    OTHERS(22, "Others");


    int id;
    String name;

    Category(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Category parseString(String value) {
        if (value != null){
            for (Category category : getCategories()) {
                if (value.equalsIgnoreCase(category.toString())) {
                    return category;
                }
            }
        }
        return null;
    }

    public static List<Category> getCategories() {
        List<Category> list = new ArrayList<>();
        list.add(COLUMN_FOOTING);
        list.add(WALL_FOOTING);
        list.add(TIE_BEAM);
        list.add(BEAM);
        list.add(COLUMN);
        list.add(SLAB_ON_FILL);
        list.add(SLAB);
        list.add(STAIR);
        list.add(OTHERS);
        return list;
    }
}
