package com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.google.firebase.database.Exclude;

public class RectangularGeneralConcreteResult extends GeneralConcreteWorksResult {

    @Exclude
    double lClear;

    @Exclude
    MemberSectionProperty memberSectionProperty;

    public RectangularGeneralConcreteResult(){

    }

    @Exclude
    public double getlClear() {
        return lClear;
    }

    public void setlClear(double lClear) {
        this.lClear = lClear;
    }

    @Exclude
    public MemberSectionProperty getMemberSectionProperty() {
        return memberSectionProperty;
    }

    public void setMemberSectionProperty(MemberSectionProperty memberSectionProperty) {
        this.memberSectionProperty = memberSectionProperty;
    }
}
