package com.android.estimateapp.cleanarchitecture.presentation.welcome;

import com.android.estimateapp.cleanarchitecture.domain.repository.UserSessionRepository;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.contract.WelcomeContract;

import javax.inject.Inject;

public class WelcomePresenter implements WelcomeContract.UserActionListener {

    private WelcomeContract.View view;

    private UserSessionRepository userSessionRepository;

    @Inject
    public WelcomePresenter(UserSessionRepository userSessionRepository){
        this.userSessionRepository = userSessionRepository;
    }

    @Override
    public void setView(WelcomeContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {

        view.showLoginScreen();

        if (userSessionRepository.hasActiveUser()){
            view.navigateToHomeScreen();
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onBackPressed(int backStack) {

    }
}
