package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum Shape {

    RECTANGLE(1, "RECTANGLE"),

    SQUARE(2, "SQUARE"),

    TRAPEZOID(3, "TRAPEZOID"),

    CIRCLE(4, "CIRCLE"),

    TAPPERED(5, "TAPPERED");

    int id;
    String name;

    Shape(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public static Shape parseString(String value){
        for (Shape shape : getShapes()){
            if (value.equalsIgnoreCase(shape.toString())){
                return shape;
            }
        }
        return null;
    }

    public static List<Shape> getShapes(){
        List<Shape> list = new ArrayList<>();
        list.add(RECTANGLE);
        list.add(TRAPEZOID);
        list.add(CIRCLE);
        return list;
    }


}
