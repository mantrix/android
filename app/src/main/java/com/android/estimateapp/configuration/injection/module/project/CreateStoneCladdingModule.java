package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding.StoneCladdingCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.stonecladding.StoneCladdingCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.create.CreateStoneCladdingContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.create.CreateStoneCladdingPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateStoneCladdingModule {

    @Provides
    @PerActivity
    CreateStoneCladdingContract.UserActionListener provideCreateStoneCladdingPresenter(CreateStoneCladdingPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    StoneCladdingCalculator provideStoneCladdingCalculator(StoneCladdingCalculatorImpl calculator) {
        return calculator;
    }
}
