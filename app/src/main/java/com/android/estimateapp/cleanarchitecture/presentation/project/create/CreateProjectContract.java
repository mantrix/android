package com.android.estimateapp.cleanarchitecture.presentation.project.create;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import io.reactivex.annotations.Nullable;

public interface CreateProjectContract {

    interface View {
        void launchProjectDetail(String projectId);

        void displayCreateProjectLoader();

        void dismissCreateProjectLoader();

        void projectCreationError();

    }

    interface UserActionListener extends Presenter<View> {

        void createProject(String name, String owner, String location, @Nullable String description);

    }
}
