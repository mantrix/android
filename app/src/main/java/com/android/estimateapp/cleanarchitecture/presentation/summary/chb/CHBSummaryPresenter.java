package com.android.estimateapp.cleanarchitecture.presentation.summary.chb;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying.CHBLayingCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class CHBSummaryPresenter extends BasePresenter implements CHBSummaryContract.UserActionListener {

    private CHBSummaryContract.View view;
    private ProjectRepository projectRepository;
    private CHBLayingCalculator chbLayingCalculator;
    private String projectId;
    private String scopeKey;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Map<RsbSize, RSBFactor> RSBFactorMap = new HashMap<>();

    private CHBFactor chbFactor;
    private List<RSBFactor> rsbFactors;
    private DoorsAndWindows doorsAndWindows;


    @Inject
    public CHBSummaryPresenter(ProjectRepository projectRepository,
                               CHBLayingCalculator chbLayingCalculator,
                               ThreadExecutorProvider threadExecutorProvider,
                               PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.chbLayingCalculator = chbLayingCalculator;
    }

    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(CHBSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadFactors();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void calculateSummary() {
        compositeDisposable.add(projectRepository.getCHBInputs(projectId, scopeKey)
                .map(inputs -> {
                    if (!inputs.isEmpty()) {
                        for (int i = 0; i < inputs.size(); i++) {
                            inputs.get(i).setChbFactor(chbFactor);
                        }
                    }
                    return chbLayingCalculator.generateSummary(generateResults(inputs));
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    view.displaySummary(summary);
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    private List<CHBResult> generateResults(List<CHBInput> inputs) {
        List<CHBResult> results = new ArrayList<>();


        List<DoorWindowInputBase> doors = new ArrayList<>();
        List<DoorWindowInputBase> windows = new ArrayList<>();
        if (doorsAndWindows != null){
            doors.addAll(doorsAndWindows.getDoors());
            windows.addAll(doorsAndWindows.getWindows());
        }



        for (CHBInput input : inputs) {
            results.add(chbLayingCalculator.calculate(input, doors, windows, rsbFactors));
        }
        return results;
    }



    public void loadFactors() {
        Observable.zip(projectRepository.getCHBFactor(projectId).observeOn(postExecutionThread.getScheduler()),
                projectRepository.getProjectRSBFactors(projectId).observeOn(postExecutionThread.getScheduler()),
                loadDoorsAndWindows(), (chbFactor, rsbFactors, doorsAndWindows) -> {
                    return new CHBFactors() {
                        @Override
                        public DoorsAndWindows getDoorsAndWindows() {
                            return doorsAndWindows;
                        }

                        @Override
                        public CHBFactor getChbFactor() {
                            return chbFactor;
                        }

                        @Override
                        public List<RSBFactor> getRsbFactors() {
                            return rsbFactors;
                        }
                    };
                }).subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(data -> {
                    this.chbFactor = data.getChbFactor();
                    this.rsbFactors = data.getRsbFactors();
                    this.doorsAndWindows = data.getDoorsAndWindows();

                    calculateSummary();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private Observable<DoorsAndWindows> loadDoorsAndWindows() {
        return projectRepository.getProjectScopesOfWorks(projectId)
                .flatMap(works -> {

                    Observable<List<DoorWindowInputBase>> doorsObs = Observable.just(new ArrayList<>());
                    Observable<List<DoorWindowInputBase>> windowsObs = Observable.just(new ArrayList<>());
                    for (Work work : works) {
                        if (ScopeOfWork.DOORS.getId() == work.getScopeID()) {
                            doorsObs = projectRepository.getDoorAndWindowInputs(projectId, work.getKey());
                        } else if (ScopeOfWork.WINDOWS.getId() == work.getScopeID()) {
                            windowsObs = projectRepository.getDoorAndWindowInputs(projectId, work.getKey());
                        }
                    }
                    return Observable.zip(doorsObs, windowsObs, (doors, windows) -> new DoorsAndWindows() {

                        @Override
                        public List<DoorWindowInputBase> getDoors() {
                            return doors;
                        }

                        @Override
                        public List<DoorWindowInputBase> getWindows() {
                            return windows;
                        }


                    });
                });
    }

    interface CHBFactors{
        DoorsAndWindows getDoorsAndWindows();
        CHBFactor getChbFactor();
        List<RSBFactor> getRsbFactors();
    }

    interface DoorsAndWindows {
        List<DoorWindowInputBase> getDoors();
        List<DoorWindowInputBase> getWindows();
    }



}