package com.android.estimateapp.cleanarchitecture.presentation.properties.membersection;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface MemberSectionPropertieContract {

    interface View {

        void displayProperties(List<MemberSectionProperty> properties);

        void saved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId);

        void update(List<MemberSectionProperty> properties);

        void createProperty(MemberSectionProperty property);
    }
}
