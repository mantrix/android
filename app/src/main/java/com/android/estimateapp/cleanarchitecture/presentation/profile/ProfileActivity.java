package com.android.estimateapp.cleanarchitecture.presentation.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileContract.FIELD_FIRST_NAME;
import static com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileContract.FIELD_LAST_NAME;
import static com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileContract.FIELD_MIDDLE_NAME;

public class ProfileActivity extends BaseActivity implements ProfileContract.View {

    private static final String TAG = ProfileActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.ll_input_field_container)
    LinearLayout llInputFields;

    @BindView(R.id.ll_text_container)
    LinearLayout llTextContainer;

    @BindView(R.id.btn_edit_profile_cta)
    Button btnEditProfileCta;


    @BindView(R.id.til_first_name)
    TextInputLayout tilFirstName;

    @BindView(R.id.et_first_name)
    EditText etFirstName;

    @BindView(R.id.til_middle_name)
    TextInputLayout tilMiddleName;

    @BindView(R.id.et_middle_name)
    EditText etMiddleName;

    @BindView(R.id.til_last_name)
    TextInputLayout tilLastName;

    @BindView(R.id.et_last_name)
    EditText etLastName;

    @BindView(R.id.tv_first_name)
    TextView tvFirstName;

    @BindView(R.id.tv_middle_name)
    TextView tvMiddleName;

    @BindView(R.id.tv_last_name)
    TextView tvLastName;

    @BindView(R.id.tv_email)
    TextView tvEmail;

    @BindView(R.id.tv_full_name)
    TextView tvFullName;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.iv_profile_image)
    CircleImageView ivProfileImage;

    @BindView(R.id.tv_subscription)
    TextView tvSubscription;

    @Inject
    ProfileContract.UserActionListener presenter;

    public static Intent createIntent(Context context){
        return new Intent(context, ProfileActivity.class);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.profile_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        setToolbarTitle(R.string.account);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        presenter.setView(this);
        presenter.start();
    }


    @OnClick(R.id.iv_profile_image)
    public void onProfileImageClick(){
        ImagePicker.create(this)
                .toolbarImageTitle(getString(R.string.select_profile_photo))
                .toolbarArrowColor(ContextCompat.getColor(this,R.color.white))
                .theme(R.style.AppImagePickerTheme)
                .showCamera(false)
                .single() // single
                .start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void displayProfilePhoto(String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get().load(url).into(ivProfileImage);
        }
    }

    @OnClick(R.id.btn_edit_profile_cta)
    public void editProfileClick(){
        if (llTextContainer.getVisibility() == View.VISIBLE){
            llTextContainer.setVisibility(View.GONE);
            llInputFields.setVisibility(View.VISIBLE);
            btnEditProfileCta.setText(getString(R.string.done));

        } else {

            String fName = etFirstName.getText().toString().trim();
            String mName = etMiddleName.getText().toString().trim();
            String lName = etLastName.getText().toString().trim();

            // validate inputs and update name
            if (presenter.validateNameInputs(fName, mName, lName)){
                presenter.updateName(fName, mName, lName);

                llInputFields.setVisibility(View.GONE);
                llTextContainer.setVisibility(View.VISIBLE);
                btnEditProfileCta.setText(getString(R.string.edit_profile));
            }
        }
    }

    @Override
    public void removedError(int field) {
        switch (field){
            case FIELD_FIRST_NAME:
                tilFirstName.setError(null);
                break;
            case FIELD_MIDDLE_NAME:
                tilMiddleName.setError(null);
                break;
            case FIELD_LAST_NAME:
                tilLastName.setError(null);
                break;
        }
    }

    @Override
    public void showNoConnectionError() {
        Toast.makeText(this, getString(R.string.no_connection_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUnhandledError() {
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayEmail(String email) {
        tvEmail.setText(email);
        etEmail.setText(email);
    }

    @Override
    public void populateNameFields(String fullName, String firstName, String middleName, String lastName) {
        tvFullName.setText(fullName);

        tvFirstName.setText(firstName);
        etFirstName.setText(firstName);

        tvMiddleName.setText(middleName);
        etMiddleName.setText(middleName);

        tvLastName.setText(lastName);
        etLastName.setText(lastName);

    }

    @Override
    public void displaySubscription(String subscription) {
        tvSubscription.setText(subscription);
    }

    @Override
    public void displayFieldRequiredError(int field) {
        switch (field){
            case FIELD_FIRST_NAME:
                tilFirstName.setError(getString(R.string.required_error));
                break;
            case FIELD_MIDDLE_NAME:
                tilMiddleName.setError(getString(R.string.required_error));
                break;
            case FIELD_LAST_NAME:
                tilLastName.setError(getString(R.string.required_error));
                break;
        }
    }

    @Override
    public void displayInvalidInputError(int field) {
        switch (field){
            case FIELD_FIRST_NAME:
                tilFirstName.setError(getString(R.string.invalid_input));
                break;
            case FIELD_MIDDLE_NAME:
                tilMiddleName.setError(getString(R.string.invalid_input));
                break;
            case FIELD_LAST_NAME:
                tilLastName.setError(getString(R.string.invalid_input));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            Image image = ImagePicker.getFirstImageOrNull(data);

            Log.d(TAG, "onActivityResult: " + image.getPath() + " " + image.getName());

            UCrop.of(Uri.fromFile(new File(image.getPath())), Uri.fromFile(new File(getCacheDir(), image.getName())))
                    .withAspectRatio(4, 4)
                    .start(this);

        } else  if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {

            final Uri resultUri = UCrop.getOutput(data);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                ivProfileImage.setImageBitmap(bitmap);

                presenter.uploadProfilePhoto(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {

            final Throwable cropError = UCrop.getError(data);
            cropError.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }




}
