package com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting.WallFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;

import java.util.List;

public interface ColumnFootingWorkCalculator {

    RebarMaterialSummary generateSummary(List<ColumnFootingRebarResult> results);

    ColumnFootingRebarResult calculate(int lengthOfTieWirePerKilo, ColumnFootingRebarInput input, List<RSBFactor> rsbFactorList);
}
