package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnfooting.ColumnFootingWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnfooting.ColumnFootingWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.columnfooting.ColumnFootingMaterialSummaryContract;
import com.android.estimateapp.cleanarchitecture.presentation.summary.columnfooting.ColumnFootingMaterialSummaryPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class ColumnFootingMaterialSummaryModule {

    @Provides
    @PerFragment
    ColumnFootingWorkCalculator provideColumnFootingWorkCalculator(ColumnFootingWorkCalculatorImpl calculator) {
        return calculator;
    }

    @Provides
    @PerFragment
    ColumnFootingMaterialSummaryContract.UserActionListener provideColummnFootingSummaryPresenter(ColumnFootingMaterialSummaryPresenter presenter) {
        return presenter;
    }
}
