package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.workandoor.DoorAndWindowCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.workandoor.DoorAndWindowCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.create.CreateDoorAndWindowContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.create.CreateDoorAndWindowPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateDoorAndWindowModule {


    @Provides
    @PerActivity
    CreateDoorAndWindowContract.UserActionListener provideCreateDoorAndWindowPresenter(CreateDoorAndWindowPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DoorAndWindowCalculator provideDoorAndWindowCalculator(DoorAndWindowCalculatorImpl calculator) {
        return calculator;
    }
}
