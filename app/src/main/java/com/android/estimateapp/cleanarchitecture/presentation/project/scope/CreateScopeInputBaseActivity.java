package com.android.estimateapp.cleanarchitecture.presentation.project.scope;

import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.mantrixengineering.estimateapp.R;

public abstract class CreateScopeInputBaseActivity extends BaseActivity {


    public void showSavingNotAllowedError(){
        Toast.makeText(this, getString(R.string.saving_not_allowed), Toast.LENGTH_SHORT).show();
    }

}
