package com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.PlasteringLocation;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.SurfaceType;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering.PlasteringWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;


@PerActivity
public class CreatePlasteringPresenter extends CreateScopeInputBasePresenter implements CreatePlasteringContract.UserActionListener {

    private static final String TAG = CreatePlasteringPresenter.class.getSimpleName();

    private CreatePlasteringContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;
    private PlasteringWorkCalculator plasteringWorkCalculator;
    private PlasteringResult result;


    private List<PlasteringMortarFactor> plasteringMortarFactors;
    private PlasteringInput input;


    @Inject
    public CreatePlasteringPresenter(ProjectRepository projectRepository,
                              PlasteringWorkCalculator plasteringWorkCalculator,
                              AccountSubscriptionUseCase accountSubscriptionUseCase,
                              ThreadExecutorProvider threadExecutorProvider,
                              PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase, threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.plasteringWorkCalculator = plasteringWorkCalculator;

        plasteringMortarFactors = new ArrayList<>();
    }

    @Override
    public void setView(CreatePlasteringContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        setPickerOptions();
        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {

        if (allowPaidExperience){
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updatePlasteringInput(projectId, scopeKey, itemKey,input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createPlasteringInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<PlasteringInput> inputObservable){

        view.showSavingDialog();

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    saveResult();
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    private void saveResult(){
        if (result != null && itemKey != null) {
            projectRepository.savePlasteringInputResult(projectId, scopeKey, itemKey, result)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(result -> {
                        this.result = result;
                    }, throwable -> {
                        throwable.printStackTrace();
                        view.savingError();
                    });
        }
    }

    @Override
    public void calculate(PlasteringInput input) {
        result = plasteringWorkCalculator.calculate(getPlasteringFactor(input.getMortarClassEnum()), input);

        this.input = input;
        view.displayResult(result);
    }

    private PlasteringMortarFactor getPlasteringFactor(Grade grade){

        if (grade != null){
            for (PlasteringMortarFactor plasteringMortarFactor : plasteringMortarFactors){
                if (plasteringMortarFactor.getMortarClass() == grade.getId()){
                    return plasteringMortarFactor;
                }
            }
        }
        return null;
    }

    private void setPickerOptions() {
        view.setMortarClassPickerOptions(Arrays.asList(Grade.A,Grade.B,Grade.C,Grade.D));
        view.setSurfaceTypeOptions(SurfaceType.allSurfaceTypes());
        view.setPlasteringLocationOptions(PlasteringLocation.allPlasteringLocation());
    }

    private void loadPlasteringInput(){
        projectRepository.getPlasteringInput(projectId, scopeKey,itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbInput -> {
                    this.input = chbInput;

                    view.setInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate){
        projectRepository.getPlasteringMortarFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(plasteringMortarFactors -> {
                    this.plasteringMortarFactors = plasteringMortarFactors;

                    if (!factorUpdate){
                        view.enabledCalculateBtn(true);

                        if (itemKey != null){
                            loadPlasteringInput();
                        }
                    } else {
                        view.requestForReCalculation();
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }

}
