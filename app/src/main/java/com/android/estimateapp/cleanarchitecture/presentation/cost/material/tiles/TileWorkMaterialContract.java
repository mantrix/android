package com.android.estimateapp.cleanarchitecture.presentation.cost.material.tiles;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkMaterials;

public interface TileWorkMaterialContract {

    interface View {

        void setInput(TileWorkMaterials materials);

        void inputSaved();

        void showSavingNotAllowedError();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void save(TileWorkMaterials materials);
    }
}
