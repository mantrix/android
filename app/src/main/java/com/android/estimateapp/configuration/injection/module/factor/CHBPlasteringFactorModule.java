package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.plastering.PlasteringMortarFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.plastering.PlasteringMortarFactorPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class CHBPlasteringFactorModule {

    @Provides
    @PerFragment
    PlasteringMortarFactorContract.UserActionListener providesCHBPlasteringFactorPresenter(PlasteringMortarFactorPresenter presenter) {
        return presenter;
    }
}
