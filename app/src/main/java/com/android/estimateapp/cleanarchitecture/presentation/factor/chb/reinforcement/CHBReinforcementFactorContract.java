package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface CHBReinforcementFactorContract {

    interface ReinforcementItem {

        VerticalSpacing getVerticalSpacing();

        HorizontalSpacing getHorizontalSpacing();

        void setVerticalSpacing(VerticalSpacing spacing);

        void setHorizontalSpacing(HorizontalSpacing spacing);
    }


    interface View {

        void displayList(List<ReinforcementItem> factors);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(List<ReinforcementItem> factors);
    }
}
