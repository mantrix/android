package com.android.estimateapp.configuration.injection.module.summary;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar.BeamRebarCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar.BeamRebarCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.beamrebar.BeamRebarSummaryPresenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.beamrebar.BeamSummaryContract;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class BeamRebarSummaryModule {

    @Provides
    @PerFragment
    BeamRebarCalculator provideBeamRebarCalculator(BeamRebarCalculatorImpl calculator) {
        return calculator;
    }

    @Provides
    @PerFragment
    BeamSummaryContract.UserActionListener provideBeamRebarSummaryPresenter(BeamRebarSummaryPresenter presenter) {
        return presenter;
    }
}
