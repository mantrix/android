package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.ProjectRetrieverUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.impl.ProjectRetrieverV2;
import com.android.estimateapp.cleanarchitecture.presentation.project.detail.ProjectDetailContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.detail.ProjectDetailPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ProjectDetailModule {

    @Provides
    @PerActivity
    ProjectDetailContract.UserActionListener provideProjectDetailPresenter(ProjectDetailPresenter presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    ProjectRetrieverUseCase provideProjectRetrieverUseCase(ProjectRetrieverV2 projectRetriever){
        return projectRetriever;
    }
}
