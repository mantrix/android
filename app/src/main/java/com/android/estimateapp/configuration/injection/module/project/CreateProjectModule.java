package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.CreateProjectUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.impl.CreateProjectHelper;
import com.android.estimateapp.cleanarchitecture.presentation.project.create.CreateProjectContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.create.CreateProjectPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateProjectModule {

    @Provides
    @PerActivity
    CreateProjectContract.UserActionListener provideCreateProjectPresenter(CreateProjectPresenter presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    CreateProjectUseCase provideCreateProjectUseCase(CreateProjectHelper helper){
        return helper;
    }
}
