package com.android.estimateapp.cleanarchitecture.presentation.welcome.create;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatDialogFragment;

public class CreateUserDialogFragment extends DaggerAppCompatDialogFragment implements CreateUserContract.View {

    private final static String UID = "uid";
    private final static String EMAIL = "email";
    private final static String MODE = "mode";
    public static final String TAG = CreateUserDialogFragment.class.getSimpleName();
    public static final int FAILED = 4000;


    public static CreateUserDialogFragment newInstance(String uid, String email, CreateUserContract.RegistrationMode registrationMode) {

        Bundle args = new Bundle();
        args.putString(UID, uid);
        args.putString(EMAIL, email);
        args.putSerializable(MODE, registrationMode);
        CreateUserDialogFragment fragment = new CreateUserDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.et_first_name)
    TextInputEditText etFirstName;
    @BindView(R.id.til_first_name)
    TextInputLayout tilFirstName;
    @BindView(R.id.et_middle_name)
    TextInputEditText etMiddleName;
    @BindView(R.id.til_middle_name)
    TextInputLayout tilMiddleName;
    @BindView(R.id.et_last_name)
    TextInputEditText etLastName;
    @BindView(R.id.til_last_name)
    TextInputLayout tilLastName;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.fl_login_using_cred)
    FrameLayout flLoginUsingCred;
    @BindView(R.id.et_mobile)
    TextInputEditText etMobile;
    @BindView(R.id.til_mobile)
    TextInputLayout tilMobile;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.et_password)
    TextInputEditText etPassword;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.ll_email_registration_container)
    LinearLayout llEmailRegistrationContainer;

    CreateUserContract.RegistrationMode registrationMode;

    private Unbinder unbinder;

    @Inject
    CreateUserContract.UserActionListener presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.create_user_dialog_fragment, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        registrationMode = (CreateUserContract.RegistrationMode) getArguments().getSerializable(MODE);
        presenter.setView(this);
        presenter.setMode(registrationMode);
        presenter.start();

        return rootView;
    }

    @Override
    public void invalidFirstName() {
        tilFirstName.setError(getString(R.string.required_error));
    }

    @Override
    public void invalidLastName() {
        tilLastName.setError(getString(R.string.required_error));
    }

    @Override
    public void invalidMiddleName() {
        tilMiddleName.setError(getString(R.string.required_error));
    }

    @Override
    public void invalidPhoneNumber() {
        tilMobile.setError(getString(R.string.invalid_input));
    }

    @Override
    public void registrationFailed() {
        Fragment fragment = getTargetFragment();
        if (fragment != null) {
            Intent intent = new Intent();
            fragment.onActivityResult(getTargetRequestCode(), FAILED, intent);
        }
        dismiss();
    }

    @Override
    public void registered() {
        Fragment fragment = getTargetFragment();
        if (fragment != null) {
            Intent intent = new Intent();
            fragment.onActivityResult(getTargetRequestCode(), getActivity().RESULT_OK, intent);
        }
        dismiss();
    }

    @Override
    public void startLoading() {
        progressBar.setVisibility(View.VISIBLE);
        btnRegister.setVisibility(View.INVISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBar.setVisibility(View.GONE);
        btnRegister.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_register)
    protected void onRegisterClick() {
        tilFirstName.setError(null);
        tilLastName.setError(null);
        tilMiddleName.setError(null);
        tilMobile.setError(null);
        presenter.create(getArguments().getString(UID),
                registrationMode == CreateUserContract.RegistrationMode.GOOGLE ? getArguments().getString(EMAIL) : etEmail.getText().toString(),
                etMobile.getText().toString(),
                etFirstName.getText().toString(),
                etMiddleName.getText().toString(),
                etLastName.getText().toString(),
                registrationMode == CreateUserContract.RegistrationMode.GOOGLE ? null : etPassword.getText().toString());
    }

    @Override
    public void invalidEmail() {
        tilEmail.setError(getString(R.string.invalid_email_error));
    }

    @Override
    public void invalidPassword() {
        tilPassword.setError(getString(R.string.invalid_password_error));
    }

    @Override
    public void showEmailMode() {
        llEmailRegistrationContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (unbinder != null)
            unbinder.unbind();
    }
}
