package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.StoneCladdingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudStoneCladdingDataStore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class WSStoneCladdingStore extends BaseStore implements CloudStoneCladdingDataStore {


    private DatabaseReference projectScopeOfWorksRef;

    @Inject
    public WSStoneCladdingStore() {
        super();
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
    }


    @Override
    public Observable<StoneCladdingInput> createStoneCladdingInput(String projectId, String scopeOfWorkId, StoneCladdingInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(databaseReference -> push(databaseReference, input));
    }


    @Override
    public Observable<StoneCladdingInput> updateStoneCladdingInput(String projectId, String scopeOfWorkId, String itemId, StoneCladdingInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference, input));
    }


    @Override
    public Observable<StoneCladdingResult> saveStoneCladdingInputResult(String projectId, String scopeOfWorkId, String itemId, StoneCladdingResult result) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId)
                    .child(FIREBASE_RESULT);
        }).flatMap(databaseReference -> setValue(databaseReference, result));
    }


    @Override
    public Observable<List<StoneCladdingInput>> getStoneCladdingInputs(String projectId, String scopeOfWorkId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(query -> queryListValue(query, StoneCladdingInput.class));
    }


    @Override
    public Observable<StoneCladdingInput> getStoneCladdingInput(String projectId, String scopeOfWorkId, String itemId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(query -> get(query, StoneCladdingInput.class));
    }


    @Override
    public Observable<List<StoneCladdingFactor>> createStoneCladdingFactors(String projectId, List<StoneCladdingFactor> factors) {

        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_STONE_CLADDING_FACTOR);

        return Observable.fromIterable(factors)
                .flatMap(factor -> push(databaseReference, factor))
                .toList().toObservable();
    }


    @Override
    public Observable<List<StoneCladdingFactor>> getStoneCladdingFactors(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_STONE_CLADDING_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, StoneCladdingFactor.class));
    }


    @Override
    public Observable<List<StoneCladdingFactor>> updateStoneCladdingFactors(String projectId, List<StoneCladdingFactor> factors) {
        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_STONE_CLADDING_FACTOR);

        return Observable.fromIterable(factors)
                .flatMap(factor -> setValue(databaseReference.child(factor.getKey()), factor))
                .toList().toObservable();
    }

    @Override
    public Observable<StoneType> createStoneTypeFactor(String projectId, StoneType stoneType) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_STONE_TYPE_FACTOR);
        }).flatMap(databaseReference -> push(databaseReference, stoneType));
    }

    @Override
    public Observable<List<StoneType>> getStoneTypeFactors(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_STONE_TYPE_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, StoneType.class));
    }

    @Override
    public Observable<List<StoneType>> updateStoneTypeFactors(String projectId, List<StoneType> factors) {
        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_STONE_TYPE_FACTOR);

        return Observable.fromIterable(factors)
                .flatMap(factor -> setValue(databaseReference.child(factor.getKey()), factor))
                .toList().toObservable();
    }

}

