package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudConcreteWorkDataStore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class WSConcreteWorkStore extends BaseStore implements CloudConcreteWorkDataStore {


    private DatabaseReference projectScopeOfWorksRef;

    @Inject
    public WSConcreteWorkStore() {
        super();
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
    }

    @Override
    public Observable<ConcreteWorksInput> createConcreteWorkInput(String projectId, String scopeOfWorkId, ConcreteWorksInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(databaseReference -> push(databaseReference, input));
    }

    @Override
    public Observable<ConcreteWorksInput> updateConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId, ConcreteWorksInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference, input));
    }

    @Override
    public Observable<GeneralConcreteWorksResult> saveConcreteWorkInputResult(String projectId, String scopeOfWorkId, String itemId, GeneralConcreteWorksResult result) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId)
                    .child(FIREBASE_RESULT);
        }).flatMap(databaseReference -> setValue(databaseReference, result));
    }


    @Override
    public Observable<List<ConcreteWorksInput>> getConcreteWorkInputs(String projectId, String scopeOfWorkId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(query -> queryListValue(query, ConcreteWorksInput.class));
    }


    @Override
    public Observable<ConcreteWorksInput> getConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(query -> get(query, ConcreteWorksInput.class));
    }


    @Override
    public Observable<List<ConcreteFactor>> createConcreteWorksFactors(String projectId, List<ConcreteFactor> concreteFactors) {

        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_CONCRETE_WORK_FACTOR);

        return Observable.fromIterable(concreteFactors)
                .flatMap(concreteFactor -> push(databaseReference, concreteFactor))
                .toList().toObservable();
    }


    @Override
    public Observable<List<ConcreteFactor>> getConcreteWorkFactors(String projectId) {
        return Observable.fromCallable(() -> {
            Query query =  projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CONCRETE_WORK_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, ConcreteFactor.class));
    }


    @Override
    public Observable<List<ConcreteFactor>> updateConcreteWorkFactors(String projectId, List<ConcreteFactor> concreteFactors) {
        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_CONCRETE_WORK_FACTOR);

        return Observable.fromIterable(concreteFactors)
                .flatMap(concreteFactor -> setValue(databaseReference.child(concreteFactor.getKey()), concreteFactor))
                .toList().toObservable();
    }

    /* Rectangular Concrete */

    @Override
    public Observable<RectangularConcreteInput> createRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, RectangularConcreteInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(databaseReference -> push(databaseReference, input));
    }

    @Override
    public Observable<RectangularConcreteInput> updateRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId, RectangularConcreteInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference, input));
    }

    @Override
    public Observable<RectangularGeneralConcreteResult> saveRectangularConcreteWorkInputResult(String projectId, String scopeOfWorkId, String itemId, RectangularGeneralConcreteResult result) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId)
                    .child(FIREBASE_RESULT);
        }).flatMap(databaseReference -> setValue(databaseReference, result));
    }


    @Override
    public Observable<List<RectangularConcreteInput>> getRectangularConcreteWorkInputs(String projectId, String scopeOfWorkId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(query -> queryListValue(query, RectangularConcreteInput.class));
    }


    @Override
    public Observable<RectangularConcreteInput> getRectangularConcreteWorkInput(String projectId, String scopeOfWorkId, String itemId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(query -> get(query, RectangularConcreteInput.class));
    }
}
