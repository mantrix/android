package com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.Node;
import com.google.firebase.database.Exclude;

public class ColumnRebarInput extends Model {

    Node nodeA;

    Node nodeB;

    String storey;

    String grid;

    String groupSpanMember;

    String propertyKey;

    String propertyReferenceName;

    double lengthCC;

    double offset1;

    double offset2;

    double wastage;

    public ColumnRebarInput(){

    }

    @Exclude
    public Node getNodeA() {
        return nodeA;
    }

    public void setNodeA(Node nodeA) {
        this.nodeA = nodeA;
    }

    @Exclude
    public Node getNodeB() {
        return nodeB;
    }

    public void setNodeB(Node nodeB) {
        this.nodeB = nodeB;
    }


    public double getLengthCC() {
        return lengthCC;
    }

    public void setLengthCC(double lengthCC) {
        this.lengthCC = lengthCC;
    }

    public double getOffset1() {
        return offset1;
    }

    public void setOffset1(double offset1) {
        this.offset1 = offset1;
    }

    public double getOffset2() {
        return offset2;
    }

    public void setOffset2(double offset2) {
        this.offset2 = offset2;
    }

    public double getWastage() {
        return wastage;
    }

    public void setWastage(double wastage) {
        this.wastage = wastage;
    }

    public String getGrid() {
        return grid;
    }

    public String getStorey() {
        return storey;
    }

    public void setStorey(String storey) {
        this.storey = storey;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getGroupSpanMember() {
        return groupSpanMember;
    }

    public void setGroupSpanMember(String groupSpanMember) {
        this.groupSpanMember = groupSpanMember;
    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public void setPropertyKey(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    public String getPropertyReferenceName() {
        return propertyReferenceName;
    }

    public void setPropertyReferenceName(String propertyReferenceName) {
        this.propertyReferenceName = propertyReferenceName;
    }

    /**
     * @return computed Length CC based on Nodes
     */
    public double computeLengthCCBasedOnNodes(){

       if (nodeA != null && nodeB != null){
           double x = Math.pow(nodeA.getX() - nodeB.getX(), 2);
           double y = Math.pow(nodeA.getY() - nodeB.getY(), 2);
           double z = Math.pow(nodeA.getZ() - nodeB.getZ(), 2);

           setLengthCC(Math.sqrt(x + y + z));
       }
        return lengthCC;
    }

}
