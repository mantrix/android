package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;

import java.util.List;

import io.reactivex.Observable;

public interface CloudDoorAndWindowDataStore {

    Observable<DoorWindowInputBase> createDoorAndWindowInput(String projectId, String scopeOfWorkId, DoorWindowInputBase input);

    Observable<DoorWindowInputBase> updateDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId, DoorWindowInputBase input);

    Observable<List<DoorWindowInputBase>> getDoorAndWindowInputs(String projectId, String scopeOfWorkId);

    Observable<DoorWindowInputBase> getDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId);

    Observable<DoorWindowOutputBase> saveDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId, DoorWindowOutputBase result);

}
