package com.android.estimateapp.cleanarchitecture.presentation.application;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

public interface ApplicationContract {

    interface View {

    }


    interface UserActionListener extends Presenter<View> {

    }
}
