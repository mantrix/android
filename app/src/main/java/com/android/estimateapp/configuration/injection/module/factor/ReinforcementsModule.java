package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement.CHBReinforcementFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement.CHBReinforcementPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class ReinforcementsModule {

    @Provides
    @PerFragment
    CHBReinforcementFactorContract.UserActionListener providesCHBReinforcementFactorPresenter(CHBReinforcementPresenter presenter) {
        return presenter;
    }
}
