package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;

public class CHBSummaryPerSizeData extends Model {

    @Exclude
    public CHBSize chbSize;
    @Exclude
    public String location;
    @Exclude
    public double chbCount = 0;


    public double cement = 0;
    public double sand = 0;
    public double tenRsb = 0;
    public double twelveRsb = 0;
    public double tieWire = 0;

    public CHBSummaryPerSizeData(){

    }

    public CHBSummaryPerSizeData(CHBSize chbSize){
        this.chbSize = chbSize;
    }

    @Exclude
    public CHBSize getChbSize() {
        return chbSize;
    }

    public void setChbSize(CHBSize chbSize) {
        this.chbSize = chbSize;
    }

    @Exclude
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Exclude
    public double getChbCount() {
        return chbCount;
    }

    public void setChbCount(double chbCount) {
        this.chbCount = chbCount;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    public double getTenRsb() {
        return tenRsb;
    }

    public void setTenRsb(double tenRsb) {
        this.tenRsb = tenRsb;
    }

    public double getTwelveRsb() {
        return twelveRsb;
    }

    public void setTwelveRsb(double twelveRsb) {
        this.twelveRsb = twelveRsb;
    }

    public double getTieWire() {
        return tieWire;
    }

    public void setTieWire(double tieWire) {
        this.tieWire = tieWire;
    }
}
