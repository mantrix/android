package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.QtyDiameterFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.QtyDiameterPair;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.VerticalBar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.ExtraBar;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterLength;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Stirrups;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar.BeamRebarCalculatorImpl;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class BeamRebarComputationTest extends BaseTest {
    BeamRebarCalculatorImpl beamRebarCalculator;

    @Before
    public void setup() {
        beamRebarCalculator = new BeamRebarCalculatorImpl();
        initRSBSizes();
    }

    @Test
    public void beamRebarTest() {

        BeamRebarInput input = new BeamRebarInput();
        BeamSectionProperties properties = new BeamSectionProperties();

        input.setLengthCC(1.2);
        input.setWastage(10);
        input.setOffset(0.1);
        input.setOffset2(0.1);

//        properties.setBase(0.38);
//        properties.setHeight(0.38);


        // TOP
        QtyDiameterPair topQtyDiameterPair = new QtyDiameterPair();
        topQtyDiameterPair.setDiameter(twelveMM.getSize());
        topQtyDiameterPair.setQuantity(3);
        VerticalBar topBar = new VerticalBar(topQtyDiameterPair,null);

        properties.setTopBar(topBar);

        // BOTTOM

        QtyDiameterPair bottomQtyDiameterPair = new QtyDiameterPair();
        bottomQtyDiameterPair.setDiameter(sixteenMM.getSize());
        bottomQtyDiameterPair.setQuantity(3);
        VerticalBar bottomBar = new VerticalBar(bottomQtyDiameterPair,null);

        properties.setBottomBar(bottomBar);


        // LEFT

        QtyDiameterFactor leftQtyDiameterFactor = new QtyDiameterFactor();
        leftQtyDiameterFactor.setLengthFactor(0.3);
        leftQtyDiameterFactor.setDiameter(twelveMM.getSize());
        leftQtyDiameterFactor.setQuantity(1);

        ExtraBar leftExtraBar = new ExtraBar();
        leftExtraBar.setProperty1(leftQtyDiameterFactor);

        properties.setExtraBarAtLeftSupport(leftExtraBar);


        // RIGHT

        QtyDiameterFactor rightQtyDiameterFactor = new QtyDiameterFactor();
        rightQtyDiameterFactor.setLengthFactor(0.3);
        rightQtyDiameterFactor.setDiameter(twelveMM.getSize());
        rightQtyDiameterFactor.setQuantity(1);

        ExtraBar rightExtraBar = new ExtraBar();
        rightExtraBar.setProperty1(rightQtyDiameterFactor);

        properties.setExtraBarAtRightSupport(rightExtraBar);


        // MID

        QtyDiameterFactor midQtyDiameterFactor = new QtyDiameterFactor();
        midQtyDiameterFactor.setLengthFactor(0.5);
        midQtyDiameterFactor.setDiameter(tenMM.getSize());
        midQtyDiameterFactor.setQuantity(2);

        ExtraBar midExtraBar = new ExtraBar();
        midExtraBar.setProperty1(midQtyDiameterFactor);
        properties.setBottomExtraAtMid(midExtraBar);

        // STIRRUPS
        Stirrups stirrups = new Stirrups();
        stirrups.setColumnTieA(new QtyDiameterSpacing(4,0.05));
        stirrups.setColumnTieB(new QtyDiameterSpacing(4,0.1));
        stirrups.setColumnTieC(new QtyDiameterSpacing(4,0.15));
        stirrups.setColumnTieTheRest(new QtyDiameterSpacing(0,0.2));
        stirrups.setColumnTieD(new QtyDiameterSpacing(4,0.15));
        stirrups.setColumnTieE(new QtyDiameterSpacing(4,0.1));
        stirrups.setColumnTieF(new QtyDiameterSpacing(4,0.05));

        QtyDiameterSpacing qtyDiameterSpacing = new QtyDiameterSpacing();
        qtyDiameterSpacing.setDiameter(tenMM.getSize());
        qtyDiameterSpacing.setLength(1.2);
        stirrups.setColumnTieG(qtyDiameterSpacing);

        stirrups.setData1(2.3);
        stirrups.setData2(24);
        stirrups.setTotalQuantity(18);

        properties.setStirrups(stirrups);

        QtyDiameterLength splicing = new QtyDiameterLength();
        splicing.setDiameter(twentyMM.getSize());
        splicing.setQuantity(2);
        splicing.setLength(0.3);

        QtyDiameterLength splicing2 = new QtyDiameterLength();
        splicing2.setDiameter(twentyMM.getSize());
        splicing2.setQuantity(3);
        splicing2.setLength(0.25);

        properties.setSplicing(splicing);
        properties.setSplicing2(splicing2);

        QtyDiameterLength hookes = new QtyDiameterLength();
        hookes.setDiameter(twentyMM.getSize());
        hookes.setQuantity(2);
        hookes.setLength(0.5);

        QtyDiameterLength hookes2 = new QtyDiameterLength();
        hookes2.setDiameter(tenMM.getSize());
        hookes2.setQuantity(2);
        hookes2.setLength(0.3);

        properties.setHookes(hookes);
        properties.setHookes2(hookes2);

        BeamRebarOutput result = beamRebarCalculator.calculate(false,input,properties, Arrays.asList(tenMM,twelveMM, sixteenMM, twentyMM));

        System.out.println("L CC: " + input.getLengthCC());
        System.out.println("L Clear: " + result.getlClear());
        System.out.println("Top " + result.getTopContinuousBars().getRsbFactor().getSize() + "  = " + result.getTopContinuousBars().getValue());
        System.out.println("Bottom " + result.getBottomContinuousBars().getRsbFactor().getSize() + "  = " + result.getBottomContinuousBars().getValue());
        System.out.println("Left " + result.getLeftSupport().getRsbFactor().getSize() + "  = " + result.getLeftSupport().getValue());
        System.out.println("Mid " + result.getExtraBarsMid().getRsbFactor().getSize() + "  = " + result.getExtraBarsMid().getValue());
        System.out.println("Right " + result.getRightSupport().getRsbFactor().getSize() + "  = " + result.getRightSupport().getValue());

        System.out.println("Qty: " + result.getStirrupsOutput().getQuantity());
        System.out.println("Length: " + result.getStirrupsOutput().getLength());
        System.out.println("10 mm rsb: " + result.getStirrupsOutput().getTenMM());
        System.out.println("12 mm rsb: " + result.getStirrupsOutput().getTwelveMM());
        System.out.println("Total: " + result.getStirrupsOutput().getTotal());

        System.out.println("Splicing " + result.getSplicing().getRsbFactor().getSize() + "  = " + result.getSplicing().getValue());
        System.out.println("Hookes " + result.getHookes().getRsbFactor().getSize() + "  = " + result.getHookes().getValue());


    }
}