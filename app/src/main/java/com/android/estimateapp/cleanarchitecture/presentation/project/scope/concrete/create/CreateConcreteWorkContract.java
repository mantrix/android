package com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateConcreteWorkContract {

    interface View {

        void setInput(RectangularConcreteInput input);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(RectangularGeneralConcreteResult result);

        void inputSaved();

        void savingError();

        void requestForReCalculation();

        void setPropertyReferenceOptions(List<MemberSectionProperty> properties);

        void setMortarClassPickerOptions(List<Grade> classes);

        void setGravelSizesOptions(List<GravelSize> gravelSizes);

        void showSavingDialog();

        void showSavingNotAllowedError();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(RectangularConcreteInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
