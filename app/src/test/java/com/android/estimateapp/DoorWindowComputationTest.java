package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.works.door.DoorWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.door.DoorWorkOutput;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.workandoor.DoorAndWindowCalculatorImpl;

import org.junit.Before;
import org.junit.Test;

public class DoorWindowComputationTest {

    DoorAndWindowCalculatorImpl doorAndWindowCalculator;

    @Before
    public void setup() {
        doorAndWindowCalculator = new DoorAndWindowCalculatorImpl();
    }

    @Test
    public void setDoorAndWindowTest() {

        DoorWorkInput input = new DoorWorkInput();
        input.setLength(1.2);
        input.setWidth(1.4);

        DoorWorkOutput result = (DoorWorkOutput) doorAndWindowCalculator.calculate(2,input);

        System.out.println("Area: " + result.getArea());
        System.out.println("Perimeter: " + result.getPerimeter());
    }
}
