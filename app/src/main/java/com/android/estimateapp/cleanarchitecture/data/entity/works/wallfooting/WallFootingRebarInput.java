package com.android.estimateapp.cleanarchitecture.data.entity.works.wallfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class WallFootingRebarInput extends Model {

    String description;

    double sets;

    double length;

    double numOfBarsAlongL;

    double spacingAlongL;

    int rsbFactorLongitudinalBars;

    int rsbFactorTransverseBars;

    double tieWireLength;

    double transverseBarsUnitLength;

    public WallFootingRebarInput(){

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getSets() {
        return sets;
    }

    public void setSets(double sets) {
        this.sets = sets;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getNumOfBarsAlongL() {
        return numOfBarsAlongL;
    }

    public void setNumOfBarsAlongL(double numOfBarsAlongL) {
        this.numOfBarsAlongL = numOfBarsAlongL;
    }

    public int getRsbFactorLongitudinalBars() {
        return rsbFactorLongitudinalBars;
    }

    public void setRsbFactorLongitudinalBars(int rsbFactorLongitudinalBars) {
        this.rsbFactorLongitudinalBars = rsbFactorLongitudinalBars;
    }

    public int getRsbFactorTransverseBars() {
        return rsbFactorTransverseBars;
    }

    public void setRsbFactorTransverseBars(int rsbFactorTransverseBars) {
        this.rsbFactorTransverseBars = rsbFactorTransverseBars;
    }

    public double getTieWireLength() {
        return tieWireLength;
    }

    public void setTieWireLength(double tieWireLength) {
        this.tieWireLength = tieWireLength;
    }

    public double getTransverseBarsUnitLength() {
        return transverseBarsUnitLength;
    }

    public void setTransverseBarsUnitLength(double transverseBarsUnitLength) {
        this.transverseBarsUnitLength = transverseBarsUnitLength;
    }

    public double getSpacingAlongL() {
        return spacingAlongL;
    }

    public void setSpacingAlongL(double spacingAlongL) {
        this.spacingAlongL = spacingAlongL;
    }

}
