package com.android.estimateapp.cleanarchitecture.presentation.factor.chb.tiewire;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface CHBTieWireFactorContract {

    interface View {

        void displayList(List<TieWireRebarSpacingFactor> factors);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(List<TieWireRebarSpacingFactor> factors);
    }
}
