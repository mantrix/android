package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.list.DoorAndWindowListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.list.DoorAndWindowListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class DoorAndWindowListModule {

    @Provides
    @PerActivity
    DoorAndWindowListContract.UserActionListener provideDoorAndWindowListPresenter(DoorAndWindowListPresenter presenter) {
        return presenter;
    }
}
