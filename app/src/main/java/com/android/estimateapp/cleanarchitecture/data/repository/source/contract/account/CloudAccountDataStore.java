package com.android.estimateapp.cleanarchitecture.data.repository.source.contract.account;

import com.android.estimateapp.cleanarchitecture.data.entity.CreateUserPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.SignupPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.AccountCredential;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Name;

import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;


public interface CloudAccountDataStore extends AccountDataStore {

    Observable<AccountCredential> login(String email, String password);

    Completable logout();

    Single<Boolean> hasActiveUser();

    Observable<Account> getAccount(String uid);

    Observable<Account> getLoggedInAccount();

    Observable<String> updateProfilePhoto();

    Observable<Name> updateAccountName(Name name);

    Observable<Object> authorizeUser(String iDtoken, String deviceId);

    Observable<AccountCredential> createUser(CreateUserPayload createUserPayload);

    Single<Boolean> userExist(String uid);

    Observable<AccountCredential> getIDToken(String uid);

    Observable<AccountCredential> registerUser(SignupPayload signupPayload);

}
