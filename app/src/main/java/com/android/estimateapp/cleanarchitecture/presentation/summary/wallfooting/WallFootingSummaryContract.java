package com.android.estimateapp.cleanarchitecture.presentation.summary.wallfooting;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;

public interface WallFootingSummaryContract {


    interface View extends SummaryView<RebarMaterialSummary> {

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
