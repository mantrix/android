package com.android.estimateapp.cleanarchitecture.presentation.profile;

import android.graphics.Bitmap;
import android.util.Log;

import com.android.estimateapp.cleanarchitecture.data.entity.NameEntity;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Name;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Subscription;
import com.android.estimateapp.cleanarchitecture.domain.exception.NoConnectionException;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.AccountRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.StorageRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.UserSessionRepository;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileContract.FIELD_FIRST_NAME;
import static com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileContract.FIELD_LAST_NAME;
import static com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileContract.FIELD_MIDDLE_NAME;

public class ProfilePresenter implements ProfileContract.UserActionListener {

    private static final String TAG = ProfilePresenter.class.getSimpleName();

    private ProfileContract.View view;

    private AccountRepository accountRepository;
    private PostExecutionThread postExecutionThread;
    private ThreadExecutorProvider threadExecutorProvider;
    private UserSessionRepository userSessionRepository;
    private Account account;
    private StorageRepository storageRepository;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public ProfilePresenter(AccountRepository accountRepository,
                            UserSessionRepository userSessionRepository,
                            StorageRepository storageRepository,
                            ThreadExecutorProvider threadExecutorProvider,
                            PostExecutionThread postExecutionThread){
        this.accountRepository = accountRepository;
        this.userSessionRepository = userSessionRepository;
        this.storageRepository = storageRepository;
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }

    @Override
    public void setView(ProfileContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        account = userSessionRepository.getCurrentActiveUser();
        view.displayEmail(account.getEmail());
        Name name = account.getName();
        view.populateNameFields(name.toString(),name.getFirstName(),name.getMiddleName(), name.getLastName());
        if (account.getPicURL() != null){
            view.displayProfilePhoto(account.getPicURL());
        }

        if (account.getSubscription() != null){
            Subscription subscription = account.getSubscription();
            if (subscription.getPlan() != null && !subscription.getPlan().isEmpty()){
                view.displaySubscription(subscription.getPlan().toUpperCase());
            }
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


    @Override
    public void uploadProfilePhoto(Bitmap bitmap) {
        storageRepository.uploadProfilePhoto(bitmap)
                .doOnNext(progress -> Log.d(TAG, "Upload Progress: " + progress))
                .toList()
                .flatMapObservable(doubles -> accountRepository.updateProfilePhoto())
                .subscribeOn(threadExecutorProvider.ioScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(url -> {
                    view.displayProfilePhoto(url);
                },throwable -> throwable.printStackTrace());
    }

    @Override
    public void updateName(String firstName, String middleName, String lastName) {
        Name name = new NameEntity(firstName,middleName,lastName);
        view.populateNameFields(name.toString(),name.getFirstName(),name.getMiddleName(), name.getLastName());

        accountRepository.updateName(name)
                .subscribeOn(threadExecutorProvider.ioScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(updatedName -> {
                    updateAccountInUserSession();
                }, throwable -> {
                    throwable.printStackTrace();

                    if (throwable instanceof NoConnectionException) {
                        view.showNoConnectionError();
                    } else {
                        view.showUnhandledError();
                    }
                });
    }

    @Override
    public boolean validateNameInputs(String firstName, String middleName, String lastName) {
        boolean validInputs = true;

        if (firstName.isEmpty()){
            view.displayFieldRequiredError(FIELD_FIRST_NAME);
            validInputs = false;
        } else if (!validateName(firstName)){
            validInputs = false;
            view.displayInvalidInputError(FIELD_FIRST_NAME);
        } else {
            view.removedError(FIELD_FIRST_NAME);
        }

        if (middleName.isEmpty()){
            validInputs = false;
            view.displayFieldRequiredError(FIELD_MIDDLE_NAME);
        } else if (!validateName(middleName)){
            validInputs = false;
            view.displayInvalidInputError(FIELD_MIDDLE_NAME);
        } else {
            view.removedError(FIELD_MIDDLE_NAME);
        }

        if (lastName.isEmpty()){
            validInputs = false;
            view.displayFieldRequiredError(FIELD_LAST_NAME);
        } else if (!validateName(lastName)){
            validInputs = false;
            view.displayInvalidInputError(FIELD_LAST_NAME);
        } else {
            view.removedError(FIELD_LAST_NAME);
        }

        if (validInputs){
            Log.d(TAG, "All Inputs are Valid!");
        }

        return validInputs;
    }

    public boolean validateName(String name) {
        return name.matches( "[a-zA-z]+([ '-][a-zA-Z]+)*" );
    }

    private void updateAccountInUserSession() {
        accountRepository.getLoggedInUser()
                .observeOn(threadExecutorProvider.ioScheduler())
                .subscribe(account -> {
                    Log.d(TAG, "Active User Name: " + account.getName());
                    this.account = account;
                    userSessionRepository.setCurrentActiveUser(account);
                }, Throwable::printStackTrace);
    }

}
