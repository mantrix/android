package com.android.estimateapp.cleanarchitecture.data.repository;

import com.android.estimateapp.cleanarchitecture.data.entity.CreateUserPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.SignupPayload;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.AccountCredential;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Name;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.account.CloudAccountDataStore;
import com.android.estimateapp.cleanarchitecture.domain.repository.AccountRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

@Singleton
public class AccountDataRepository implements AccountRepository {

    private CloudAccountDataStore cloudAccountDataStore;

    @Inject
    public AccountDataRepository(CloudAccountDataStore cloudAccountDataStore){
        this.cloudAccountDataStore = cloudAccountDataStore;
    }

    @Override
    public Observable<AccountCredential> login(String email, String password) {
        return cloudAccountDataStore.login(email,password);
    }

    @Override
    public Completable logout() {
        return cloudAccountDataStore.logout();
    }

    @Override
    public Single<Boolean> hasActiveUser() {
        return cloudAccountDataStore.hasActiveUser();
    }

    @Override
    public Observable<Account> getLoggedInUser() {
        return cloudAccountDataStore.getLoggedInAccount();
    }

    @Override
    public Observable<Account> getUser(String uid) {
        return cloudAccountDataStore.getAccount(uid);
    }

    @Override
    public Observable<String> updateProfilePhoto() {
        return cloudAccountDataStore.updateProfilePhoto();
    }

    @Override
    public Observable<Name> updateName(Name name) {
        return cloudAccountDataStore.updateAccountName(name);
    }

    @Override
    public Observable<Object> authorizeUser(String idToken, String deviceId) {
        return cloudAccountDataStore.authorizeUser(idToken, deviceId);
    }

    @Override
    public Observable<AccountCredential> createUser(CreateUserPayload createUserPayload) {
        return cloudAccountDataStore.createUser(createUserPayload);
    }

    @Override
    public Single<Boolean> userExist(String uid) {
        return cloudAccountDataStore.userExist(uid);
    }

    @Override
    public Observable<AccountCredential> getIDToken(String uid) {
        return cloudAccountDataStore.getIDToken(uid);
    }

    @Override
    public Observable<AccountCredential> registerUser(SignupPayload signupPayload) {
        return cloudAccountDataStore.registerUser(signupPayload);
    }
}
