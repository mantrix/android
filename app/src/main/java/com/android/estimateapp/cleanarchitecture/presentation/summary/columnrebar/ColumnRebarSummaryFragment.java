package com.android.estimateapp.cleanarchitecture.presentation.summary.columnrebar;

import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ColumnRebarMaterial;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ColumnRebarSummary;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.Observable;

public class ColumnRebarSummaryFragment extends BaseFragment implements ColumnRebarSummaryContract.View {

    @Inject
    ColumnRebarSummaryContract.UserActionListener presenter;

    @Inject
    ColumnRebarSummaryAdapter columnRebarSummaryAdapter;

    @BindView(R.id.rv_column_rebar_summary_list)
    RecyclerView rvRebarSummaryList;
    @BindView(R.id.tv_total)
    TextView tvTotal;


    private String projectId;
    private String scopeKey;

    NpaLinearLayoutManager npaLinearLayoutManager;

    public static ColumnRebarSummaryFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        ColumnRebarSummaryFragment fragment = new ColumnRebarSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.column_rebar_summary;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {

        extractExtras();
        initRecyclerView();
        presenter.setData(projectId, scopeKey);
        presenter.setView(this);
        presenter.start();
    }

    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
    }

    @Override
    public void displaySummary(ColumnRebarSummary columnRebarSummary) {
        Map<Integer, ColumnRebarMaterial> materialMap = columnRebarSummary.getColumnRebarMaterialMap();

        double total = 0;
        List<ColumnRebarMaterial> list = new ArrayList<>();
        for (Integer key : materialMap.keySet()) {
            ColumnRebarMaterial material = materialMap.get(key);
            total += material.getRebarTotal();
            list.add(material);
        }

        list = Observable.fromIterable(list)
                .toSortedList((o1, o2) -> Integer.compare(o1.rsbSize.getValue(), o2.rsbSize.getValue())).blockingGet();

        columnRebarSummaryAdapter.setItems(list);
        displayTotal(total);
    }

    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        rvRebarSummaryList.setLayoutManager(npaLinearLayoutManager);
        rvRebarSummaryList.setAdapter(columnRebarSummaryAdapter);
    }

    @Override
    public void displayTotal(double total) {
        tvTotal.setText(String.format(getString(R.string.label_total), DisplayUtil.toDisplayFormat(total)));
    }
}