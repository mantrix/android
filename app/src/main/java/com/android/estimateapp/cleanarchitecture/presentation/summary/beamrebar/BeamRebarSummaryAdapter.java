package com.android.estimateapp.cleanarchitecture.presentation.summary.beamrebar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.BeamRebarMaterial;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BeamRebarSummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<BeamRebarMaterial>> {


    private List<BeamRebarMaterial> list;
    private Context context;

    @Inject
    public BeamRebarSummaryAdapter(Context context) {
        list = new ArrayList<>();
        this.context = context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.beam_rebar_summary_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<BeamRebarMaterial> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<BeamRebarMaterial> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }


    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_rsb_size)
        TextView tvRsbSize;
        @BindView(R.id.tv_top_bar_total)
        TextView tvTopBarTotal;
        @BindView(R.id.tv_bottom_bar_total)
        TextView tvBottomBarTotal;
        @BindView(R.id.tv_left_bar_total)
        TextView tvLeftBarTotal;
        @BindView(R.id.tv_right_bar_total)
        TextView tvRightBarTotal;
        @BindView(R.id.tv_mid_bar_total)
        TextView tvMidBarTotal;
        @BindView(R.id.tv_splicing_total)
        TextView tvSplicingTotal;
        @BindView(R.id.tv_hookes_total)
        TextView tvHookesTotal;
        @BindView(R.id.tv_rsb_total)
        TextView tvRsbTotal;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(BeamRebarMaterial material) {

            tvRsbSize.setText(String.format(context.getString(R.string.label_rebar_size), Integer.toString(material.rsbSize.getValue())));

            tvTopBarTotal.setText(DisplayUtil.toDisplayFormat(material.topBars));
            tvBottomBarTotal.setText(DisplayUtil.toDisplayFormat(material.bottomBars));
            tvLeftBarTotal.setText(DisplayUtil.toDisplayFormat(material.extraBarLeft));
            tvRightBarTotal.setText(DisplayUtil.toDisplayFormat(material.extraBarRight));
            tvMidBarTotal.setText(DisplayUtil.toDisplayFormat(material.extraBarMid));
            tvSplicingTotal.setText(DisplayUtil.toDisplayFormat(material.splicings));
            tvHookesTotal.setText(DisplayUtil.toDisplayFormat(material.hookes));

            tvRsbTotal.setText(String.format(context.getString(R.string.label_total), DisplayUtil.toDisplayFormat(material.getRebarTotal())));
        }
    }
}