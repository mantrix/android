package com.android.estimateapp.configuration.injection.module.summary;


import com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting.WallFootingRebarCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting.WallFootingWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.wallfooting.WallFootingMaterialSummaryPresenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.wallfooting.WallFootingSummaryContract;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class WallFootingMaterialSummaryModule {

    @Provides
    @PerFragment
    WallFootingWorkCalculator provideWallFootingWorkCalculator(WallFootingRebarCalculatorImpl calculator) {
        return calculator;
    }

    @Provides
    @PerFragment
    WallFootingSummaryContract.UserActionListener provideWallFootingSummaryPresenter(WallFootingMaterialSummaryPresenter presenter) {
        return presenter;
    }
}
