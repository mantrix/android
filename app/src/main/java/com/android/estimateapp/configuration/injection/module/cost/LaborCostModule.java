package com.android.estimateapp.configuration.injection.module.cost;

import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.LaborCostInputContract;
import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.LaborCostPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class LaborCostModule {


    @Provides
    @PerActivity
    LaborCostInputContract.UserActionListener provideLaborCostPresenter(LaborCostPresenter presenter) {
        return presenter;
    }
}
