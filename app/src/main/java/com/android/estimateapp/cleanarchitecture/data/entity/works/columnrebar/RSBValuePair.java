package com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.google.firebase.database.Exclude;

public class RSBValuePair extends Model {

    @Exclude
    RSBFactor rsbFactor;

    double rsbSize;
    double value;

    public RSBValuePair(){

    }

    public RSBValuePair(RSBFactor rsbFactor, double value) {
       setRsbFactor(rsbFactor);
       setValue(value);
    }

    @Exclude
    public RSBFactor getRsbFactor() {
        return rsbFactor;
    }

    public void setRsbFactor(RSBFactor rsbFactor) {
        this.rsbFactor = rsbFactor;
        rsbSize = rsbFactor.getSize();
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getRsbSize() {
        return rsbSize;
    }
}
