package com.android.estimateapp.cleanarchitecture.domain.usecase.works.tiles;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkMaterials;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.TileWorkSummary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class TileWorkCalculatorImpl extends BaseCalculator implements TileWorkCalculator {

    @Inject
    public TileWorkCalculatorImpl() {

    }


    @Override
    public TileWorkSummary generateSummary(List<TileWorkResult> results){

        Map<String, TileWorkMaterials> materialPerTileTypeMap = new HashMap<>();
        TileWorkMaterials total = new TileWorkMaterials();

        for (TileWorkResult result : results) {
            if (result.getTileType() != null){

                // compute totals per category
                if (materialPerTileTypeMap.containsKey(result.getTileType().getName())) {
                    // get previous data and add the result to it.
                    materialPerTileTypeMap.put(result.getTileType().getName(), addResult(materialPerTileTypeMap.get(result.getTileType().getName()), result));
                } else {
                    // create a new map entry and set result to it.
                    materialPerTileTypeMap.put(result.getTileType().getName(), addResult(new TileWorkMaterials(), result));
                }

                // compute totals regardless of category
                total.area += result.getArea();
                total.cement += result.getCement();
                total.sand += result.getSand();
                total.tiles += result.getTiles();
                total.adhesive += result.getAdhesive();

            }
        }

        // normalize results
        for (String key : materialPerTileTypeMap.keySet()){
            TileWorkMaterials data = materialPerTileTypeMap.get(key);

            data.area = roundHalfUp(data.area);
            data.cement = roundHalfUp(data.cement);
            data.sand = roundHalfUp(data.sand);
            data.adhesive = roundHalfUp(data.adhesive);

            materialPerTileTypeMap.put(key,data);
        }

        // normalize results
        total.area = roundHalfUp(total.area);
        total.cement = roundUpToNearestWholeNumber(total.cement);
        total.sand = roundUpToNearestWholeNumber(total.sand);
        total.adhesive = roundUpToNearestWholeNumber(total.adhesive);


        return new TileWorkSummary(materialPerTileTypeMap,total);
    }


    private TileWorkMaterials addResult(TileWorkMaterials materials, TileWorkResult result) {
        materials.tileType = result.getTileType();
        materials.area += result.getArea();
        materials.cement += result.getCement();
        materials.sand += result.getSand();
        materials.tiles += result.getTiles();
        materials.adhesive += result.getAdhesive();
        return materials;
    }

    @Override
    public TileWorkResult calculate(TileWorkFactor factor, TileWorkInput input){

        double area = 0;

        if (input.getDimension() != null){
            area = input.getDimension().getLength() * input.getDimension().getWidth() * input.getSets();
        }

        TileType tileType = input.getTileType();
        double tiles = 0;
        if (tileType != null){
            tiles = (area / ((tileType.getLength() * tileType.getWidth()) / 10000)) *  (1 + toDoublePercentage(input.getWastage()));
        }

        double cement = 0;
        double sand = 0;
        double adhesive = 0;

        if (factor != null){
            cement = area * factor.getCement();

            sand = area * factor.getSand();

            adhesive = area * factor.getAdhesive();
        }

        TileWorkResult output = new TileWorkResult();

        output.setArea(roundHalfUp(area));
        output.setTiles(Math.ceil(tiles));
        output.setTileType(tileType);
        output.setCement(roundHalfUp(cement));
        output.setSand(roundHalfUp(sand));
        output.setAdhesive(roundHalfUp(adhesive));

        return output;
    }
}
