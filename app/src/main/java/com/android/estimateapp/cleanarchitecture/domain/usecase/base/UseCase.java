package com.android.estimateapp.cleanarchitecture.domain.usecase.base;

public interface UseCase {

    void clear();
}
