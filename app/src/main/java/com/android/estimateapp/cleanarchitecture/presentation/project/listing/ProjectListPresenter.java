package com.android.estimateapp.cleanarchitecture.presentation.project.listing;

import android.os.Handler;
import android.util.Log;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.ProjectRetrieverUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.utils.DateUtils;

import java.util.Date;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ProjectListPresenter extends BasePresenter implements ProjectListContract.UserActionListener {

    private static final String TAG = ProjectListPresenter.class.getSimpleName();

    private static final int PROJECT_LIMIT = 1;


    private ProjectListContract.View view;

    private ProjectRetrieverUseCase projectRetrieverUseCase;

    private ProjectRepository projectRepository;

    private int page = 0;
    private final static int PROJECT_LIST_PAGE_SIZE = 10;

    private boolean stop;

    private boolean onProgress;

    private AccountSubscriptionUseCase accountSubscription;

    private boolean allowPaidExperience;

    private boolean allowAddProject;

    private boolean fromPause;


    @Inject
    public ProjectListPresenter(ProjectRepository projectRepository,
                                AccountSubscriptionUseCase accountSubscription,
                                ProjectRetrieverUseCase projectRetrieverUseCase,
                                ThreadExecutorProvider threadExecutorProvider,
                                PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.projectRetrieverUseCase = projectRetrieverUseCase;
        this.accountSubscription = accountSubscription;
        this.threadExecutorProvider = threadExecutorProvider;
        this.postExecutionThread = postExecutionThread;
    }

    @Override
    public void setView(ProjectListContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadAccountEligibility();
        showSubscriptionStatus();
    }

    @Override
    public void resume() {
        if (fromPause) {
            fromPause = false;
            detemineAddProjectBtnBehavior();
        }
    }

    @Override
    public void pause() {
        fromPause = true;
    }

    @Override
    public void destroy() {

    }

    @Override
    public void addProjectClick() {
        if (allowAddProject){
            view.showAddProjectScreen();
        } else {
            view.displayUpgradeToAddProject();
        }
    }

    @Override
    public void reloadProjectList() {

        loadUserProjects(true);
    }

    @Override
    public void onListScrolled(int totalItemCount, int lastVisibleItem) {
        if (!stop && !onProgress && lastVisibleItem == totalItemCount - 1) {
//            page++;
//            loadUserProjects(false);
        }
    }

    private void loadAccountEligibility() {
        accountSubscription.isPayingUserExperience()
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(allowPaidExperience -> {
                    this.allowPaidExperience = allowPaidExperience;
                    detemineAddProjectBtnBehavior();
                    loadUserProjects(false);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void loadUserProjects(boolean reload) {

        if (reload) {
            // Reset page on reload
            page = 0;
        }

        onProgress = true;
        projectRetrieverUseCase.getUserProjects(reload, page, PROJECT_LIST_PAGE_SIZE)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .doOnTerminate(() -> {
                    dontAllowEndlessScrollingUntilAfter2Sec();
                })
                .subscribe(projects -> {

                    stop = projects.size() < PROJECT_LIST_PAGE_SIZE;

                    if (!projects.isEmpty()) {
                        if (reload) {
                            view.refreshProjects(projects);
                        } else {
                            view.displayProjects(projects);
                        }
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    stop = true;
                });
    }

    @Override
    public void deleteItem(String id) {
        if (allowPaidExperience){
            projectRepository.deleteProject(id)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(() -> {
                        view.removeProject(id);
                    }, throwable -> {
                        throwable.printStackTrace();
                    });
        } else {
            view.displayUpgradeAlert();
        }
    }

    private void detemineAddProjectBtnBehavior(){
        Observable.just(allowPaidExperience)
                .flatMap(allowPaidExperience -> {
                    Log.d(TAG, "allowPaidExperience: " + allowPaidExperience);
                    if (allowPaidExperience){
                        // always eligible for paying users or under trial period.
                        return Observable.just(allowPaidExperience);
                    }
                    // Basic user. Will be eligible only if the project count limit is not hit.
                    return projectRepository.getUserProjectsIds()
                            .map(ids ->  {
                                Log.d(TAG, "project count: " + ids.size());
                                return ids.size() < PROJECT_LIMIT;
                            });
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe( addProjectEligible -> {
                    Log.d(TAG, "detemineAddProjectBtnBehavior: " + addProjectEligible);
                    allowAddProject = addProjectEligible;
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private void showSubscriptionStatus(){
        accountSubscription.getSubscription()
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(subscription -> {
                    if (subscription.isSubscribed() && subscription.getSubscriptionEndTime() > 0){
                        boolean today = android.text.format.DateUtils.isToday(subscription.getSubscriptionEndTime());
                        if (today) {
                            view.subscriptionStatus(DateUtils.formatDate(new Date(subscription.getSubscriptionEndTime()), DateUtils.DATE_PATTERN_2),today);
                        }
                    } else if (subscription.isTrial() && subscription.getTrialEndAt() != null){
                        boolean today = android.text.format.DateUtils.isToday(
                                DateUtils.convertToDate(subscription.getTrialEndAt(),
                                        DateUtils.DATE_PATTERN_2).getTime());
                        view.trialStatus(subscription.getTrialEndAt(),today);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    private void dontAllowEndlessScrollingUntilAfter2Sec() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onProgress = false;
            }
        }, 2000);
    }
}
