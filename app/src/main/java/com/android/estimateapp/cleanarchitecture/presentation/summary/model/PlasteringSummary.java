package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.Exclude;

public class PlasteringSummary extends Model {

    @Exclude
    private double totalArea;

    private double cement;
    private double sand;

    public PlasteringSummary(){

    }

    public PlasteringSummary(double totalArea, double cement, double totalSand){
        setTotalArea(totalArea);
        setCement(cement);
        setSand(totalSand);
    }

    @Exclude
    public double getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(double totalArea) {
        this.totalArea = totalArea;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }
}
