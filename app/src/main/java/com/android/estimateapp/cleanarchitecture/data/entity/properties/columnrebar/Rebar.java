package com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Rebar extends Model {

    int quantity;

    int diameter;


    public Rebar(){

    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getBarSize() {
        return diameter;
    }

    public void setBarSize(int diameter) {
        this.diameter = diameter;
    }

}
