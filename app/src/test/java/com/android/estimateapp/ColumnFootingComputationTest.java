package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnfooting.ColumnFootingWorkCalculatorImpl;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class ColumnFootingComputationTest extends BaseTest{

    ColumnFootingWorkCalculatorImpl concreteWorkCalculator;

    @Before
    public void setup() {
        concreteWorkCalculator = new ColumnFootingWorkCalculatorImpl();
        initRSBSizes();
    }

    @Test
    public void columnFootingTest() {

        ColumnFootingRebarInput input = new ColumnFootingRebarInput();
        input.setSets(12);
        input.setNumberOfBarsAtL(7);
        input.setNumberOfBarsAtW(7);

        input.setDiameterAtL(twentyFiveMM.getSize());
        input.setDiameterAtW(sixteenMM.getSize());

        input.setLengthOfTieWire(0.35);
        input.setLengthAtL(1.2);
        input.setLengthAtW(1.2);

        ColumnFootingRebarResult result = concreteWorkCalculator.calculate(53,input, Arrays.asList(twentyFiveMM,sixteenMM));

        System.out.println("Qty of L: " + result.getQtyOfL());
        System.out.println("Qty of W: " + result.getQtyOfW());
        System.out.println("10 mm rsb: " + result.getTenRsb());
        System.out.println("12 mm rsb: " + result.getTwelveRsb());
        System.out.println("16 mm rsb: " + result.getSixteenRsb());
        System.out.println("20 mm rsb: " + result.getTwentyRsb());
        System.out.println("25 mm rsb: " + result.getTwentyFiveRsb());
        System.out.println("#16 GI Tie Wire: " + result.getSixteenGiTieWire());
    }
}

