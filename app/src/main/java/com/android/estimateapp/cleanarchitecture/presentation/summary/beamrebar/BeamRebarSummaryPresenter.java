package com.android.estimateapp.cleanarchitecture.presentation.summary.beamrebar;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar.BeamRebarCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.chblaying.CHBLayingCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.CHBSummaryContract;
import com.android.estimateapp.cleanarchitecture.presentation.summary.chb.CHBSummaryPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class BeamRebarSummaryPresenter  extends BasePresenter implements BeamSummaryContract.UserActionListener {

    private BeamSummaryContract.View view;
    private ProjectRepository projectRepository;
    private ProjectPropertiesRepository projectPropertiesRepository;
    private BeamRebarCalculator beamRebarCalculator;
    private String projectId;
    private String scopeKey;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private List<RSBFactor> rsbFactors;
    private List<BeamSectionProperties> beamSectionProperties;


    @Inject
    public BeamRebarSummaryPresenter(ProjectRepository projectRepository,
                                     ProjectPropertiesRepository projectPropertiesRepository,
                                     BeamRebarCalculator beamRebarCalculator,
                               ThreadExecutorProvider threadExecutorProvider,
                               PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.projectPropertiesRepository = projectPropertiesRepository;
        this.beamRebarCalculator = beamRebarCalculator;
    }

    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(BeamSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadFactors();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void calculateSummary() {
        compositeDisposable.add(projectRepository.getBeamRebarInputs(projectId, scopeKey)
                .map(beamRebarInputs -> beamRebarCalculator.generateSummary(generateResults(beamRebarInputs)))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    view.displaySummary(summary);
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }


    private List<BeamRebarOutput> generateResults(List<BeamRebarInput> inputs) {
        List<BeamRebarOutput> results = new ArrayList<>();

        for (BeamRebarInput input : inputs) {
            results.add(beamRebarCalculator.calculate(false, input, getProperties(input.getMainRebarPropertyKey()), rsbFactors));
        }

        return results;
    }

    private BeamSectionProperties getProperties(String key) {
        if (key != null) {
            for (BeamSectionProperties properties : beamSectionProperties) {
                if (key.equalsIgnoreCase(properties.getKey())) {
                    return properties;
                }
            }
        }
        return null;
    }



    public void loadFactors() {
        Observable.zip(projectPropertiesRepository.getBeamSectionProperties(projectId).observeOn(postExecutionThread.getScheduler()),
                projectRepository.getProjectRSBFactors(projectId).observeOn(postExecutionThread.getScheduler()), (properties, rsbFactors) -> {
                    return new Pair(properties, rsbFactors);
                }).subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {
                    this.beamSectionProperties = (List<BeamSectionProperties>) pair.first;
                    this.rsbFactors = (List<RSBFactor>) pair.second;

                    calculateSummary();

                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}
