package com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.create;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.HorizontalRSBpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.VerticalRSBSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.DoorWindowOpeningSet;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.RSBSpacing;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateCHBActivity extends CreateScopeInputBaseActivity implements CreateCHBContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.til_grid)
    TextInputLayout tilGrid;

    @BindView(R.id.et_grid)
    EditText etGrid;

    @BindView(R.id.til_storey)
    TextInputLayout tilStoreyLocation;

    @BindView(R.id.et_storey)
    EditText etStoreyLocation;

    @BindView(R.id.et_length)
    EditText etLength;

    @BindView(R.id.et_width)
    EditText etWidth;

    @BindView(R.id.et_tie_wire)
    EditText etTieWireLength;

    @BindView(R.id.et_chb_size)
    EditText etCHBSize;

    @BindView(R.id.et_vertical_spacing)
    EditText etVerticalSpacing;

    @BindView(R.id.et_horizontal_spacing)
    EditText etHorizontalSpacing;

    @BindView(R.id.et_rsb_vertical)
    EditText etRSBVerticalBar;

    @BindView(R.id.et_rsb_horizontal)
    EditText etRSBHorizontalBar;

    @BindView(R.id.et_mortar_class)
    EditText etMortarClass;

    @BindView(R.id.et_miscellaneous_opening)
    EditText etMiscellaneousOpening;

    @BindView(R.id.et_sets)
    EditText etSets;

    @BindView(R.id.btn_calculate)
    Button btnCalculate;

    @BindView(R.id.tv_openings)
    TextView tvOpeningsResult;

    @BindView(R.id.tv_area)
    TextView tvAreaResult;

    @BindView(R.id.tv_chb)
    TextView tvCHBResult;

    @BindView(R.id.chb_label)
    TextView tvCHBLabel;

    @BindView(R.id.tv_cement)
    TextView tvCementResult;

    @BindView(R.id.tv_sand)
    TextView tvSandResult;

    @BindView(R.id.tv_ten_rsb_result)
    TextView tvTenRSBResult;

    @BindView(R.id.tv_ten_rsb_label)
    TextView tvTenRSBLabel;

    @BindView(R.id.tv_twelve_rsb_result)
    TextView tvTwelveRSBResult;

    @BindView(R.id.tv_twelve_rsb_label)
    TextView tvTwelveRSBLabel;

    @BindView(R.id.tv_tie_wire)
    TextView tvTieWireResult;

    @BindView(R.id.cv_results)
    CardView cvResults;

    @BindView(R.id.chb_result_line)
    View chbResultLine;

    @BindView(R.id.ll_chb_result_holder)
    LinearLayout llCHBResultHolder;

    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.til_length)
    TextInputLayout tilLength;
    @BindView(R.id.til_width)
    TextInputLayout tilWidth;
    @BindView(R.id.til_chb_size)
    TextInputLayout tilChbSize;
    @BindView(R.id.til_sets)
    TextInputLayout tilSets;
    @BindView(R.id.til_miscellaneous_opening)
    TextInputLayout tilMiscellaneousOpening;
    @BindView(R.id.et_door1_code)
    TextInputEditText etDoor1Code;
    @BindView(R.id.et_door1_qty)
    TextInputEditText etDoor1Qty;
    @BindView(R.id.et_door2_code)
    TextInputEditText etDoor2Code;
    @BindView(R.id.et_door2_qty)
    TextInputEditText etDoor2Qty;
    @BindView(R.id.et_window1_code)
    TextInputEditText etWindow1Code;
    @BindView(R.id.et_window1_qty)
    TextInputEditText etWindow1Qty;
    @BindView(R.id.et_window2_code)
    TextInputEditText etWindow2Code;
    @BindView(R.id.et_window2_qty)
    TextInputEditText etWindow2Qty;
    @BindView(R.id.tv_spacing_label)
    TextView tvSpacingLabel;
    @BindView(R.id.til_horizontal_spacing)
    TextInputLayout tilHorizontalSpacing;
    @BindView(R.id.til_vertical_spacing)
    TextInputLayout tilVerticalSpacing;
    @BindView(R.id.tv_rsb_label)
    TextView tvRsbLabel;
    @BindView(R.id.til_rsb_horizontal)
    TextInputLayout tilRsbHorizontal;
    @BindView(R.id.til_rsb_vertical)
    TextInputLayout tilRsbVertical;
    @BindView(R.id.til_tie_wire)
    TextInputLayout tilTieWire;
    @BindView(R.id.til_mortar_class)
    TextInputLayout tilMortarClass;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.tv_rsb)
    TextView tvRsb;

    @Inject
    CreateCHBContract.UserActionListener presenter;

    AlertDialog mortarClassPickerDialog;
    AlertDialog chbSizePickerDialog;
    AlertDialog rsbSizePickerDialog;
    AlertDialog verticalSpacingPickerDialog;
    AlertDialog horizontalSpacingPickerDialog;
    AlertDialog doorPickerDialog;
    AlertDialog windowPickerDialog;

    private int doorIndex;
    private int windowIndex;

    private List<DoorWindowInputBase> windows;
    private List<DoorWindowInputBase> doors;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private boolean isVerticalBar;

    private CHBInput input;
    private RSBSpacing rsbSpacing;
    private Dimension dimension;

    private ProgressDialog saveProgressDialog;

    private boolean updateContent;

    public static Intent createIntent(Context context, String projectId, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateCHBActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.create_chb_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.CHB_LAYING.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        input = new CHBInput();
        rsbSpacing = new RSBSpacing();
        dimension = new Dimension();
        doors = new ArrayList<>();
        windows = new ArrayList<>();

        setDefaultDoorsAndWindowsValues();

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.CHB_LAYING.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }

    private void setDefaultDoorsAndWindowsValues(){
        DoorWindowOpeningSet defaultVal = new DoorWindowOpeningSet("",0);
        List<DoorWindowOpeningSet> defaultSets = new ArrayList<>();
        defaultSets.add(defaultVal);
        defaultSets.add(defaultVal);
        input.setWindows(defaultSets);
        input.setDoors(defaultSets);
    }

    private boolean validateInputs() {
        boolean valid = true;
        if (etGrid.getText().toString().isEmpty()) {
            tilGrid.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilGrid.setError(null);
        }

        if (etStoreyLocation.getText().toString().isEmpty()) {
            tilStoreyLocation.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilStoreyLocation.setError(null);
        }
        return valid;
    }

    @Override
    public void setDoorOptions(List<DoorWindowInputBase> doorOptions) {
        doors = doorOptions;
        String[] options = new String[doorOptions.size()];
        for (int i = 0; i < doorOptions.size(); i++) {
            options[i] = doorOptions.get(i).getCode();
        }

        doorPickerDialog = createChooser(getString(R.string.doors), options, (dialog, which) -> {

            EditText editText = null;
            if (doorIndex == 0){
                editText = etDoor1Code;
            } else if (doorIndex == 1){
                editText = etDoor2Code;
            }

            if (editText != null){
                editText.setText(doorOptions.get(which).getCode());
            }

            if (doorIndex < input.getDoors().size()){
                input.getDoors().get(doorIndex).setKey(doorOptions.get(which).getKey());
            }

            dialog.dismiss();
        });
    }


    @Override
    public void setWindowOptions(List<DoorWindowInputBase> windowOptions) {
        windows = windowOptions;
        String[] options = new String[windowOptions.size()];
        for (int i = 0; i < windowOptions.size(); i++) {
            options[i] = windowOptions.get(i).getCode();
        }

        windowPickerDialog = createChooser(getString(R.string.windows), options, (dialog, which) -> {
            EditText editText = null;
            if (windowIndex == 0){
                editText = etWindow1Code;
            } else if (windowIndex == 1){
                editText = etWindow2Code;
            }

            if (windowIndex < input.getWindows().size()){
                input.getWindows().get(windowIndex).setKey(windowOptions.get(which).getKey());
            }

            if (editText != null){
                editText.setText(windowOptions.get(which).getCode());
            }

            dialog.dismiss();
        });
    }

    @Override
    public void setMortarClassPickerOptions(List<Grade> classes) {
        String[] options = new String[classes.size()];
        for (int i = 0; i < classes.size(); i++) {
            options[i] = classes.get(i).toString();
        }

        mortarClassPickerDialog = createChooser(getString(R.string.concrete_mortar_class), options, (dialog, which) -> {
            etMortarClass.setText(options[which]);
            input.setMortarClass(classes.get(which).getId());
            dialog.dismiss();
        });
    }

    @Override
    public void setCHBSizePickerOptions(List<CHBSize> chbSizes) {
        String[] options = new String[chbSizes.size()];
        for (int i = 0; i < chbSizes.size(); i++) {
            options[i] = Integer.toString(chbSizes.get(i).getValue());
        }

        chbSizePickerDialog = createChooser(getString(R.string.chb_size), options, (dialog, which) -> {
            etCHBSize.setText(options[which]);
            input.setChbSize(chbSizes.get(which).getValue());
            dialog.dismiss();
        });
    }

    @Override
    public void setHorizontalSpacingPickerOptions(List<HorizontalRSBpacing> horizontalSpacingOptions) {
        String[] options = new String[horizontalSpacingOptions.size()];
        for (int i = 0; i < horizontalSpacingOptions.size(); i++) {
            options[i] = Integer.toString(horizontalSpacingOptions.get(i).getValue());
        }

        horizontalSpacingPickerDialog = createChooser(getString(R.string.horizontal), options, (dialog, which) -> {
            etHorizontalSpacing.setText(options[which]);
            rsbSpacing.setHorizontal(horizontalSpacingOptions.get(which).getValue());
            dialog.dismiss();
        });
    }

    @Override
    public void setVerticalSpacingPickerOptions(List<VerticalRSBSpacing> verticalSpacingOptions) {
        String[] options = new String[verticalSpacingOptions.size()];
        for (int i = 0; i < verticalSpacingOptions.size(); i++) {
            options[i] = Double.toString(verticalSpacingOptions.get(i).getValue());
        }

        verticalSpacingPickerDialog = createChooser(getString(R.string.vertical), options, (dialog, which) -> {
            etVerticalSpacing.setText(options[which]);
            rsbSpacing.setVertical(verticalSpacingOptions.get(which).getValue());
            dialog.dismiss();
        });
    }

    @Override
    public void setRSBSizePickerOptions(List<RsbSize> rsbSizeOptions) {

        String[] options = new String[rsbSizeOptions.size()];
        for (int i = 0; i < rsbSizeOptions.size(); i++) {
            options[i] = Integer.toString(rsbSizeOptions.get(i).getValue());
        }

        rsbSizePickerDialog = createChooser(getString(R.string.horizontal_bar), options, (dialog, which) -> {
            if (isVerticalBar) {
                etRSBVerticalBar.setText(options[which]);
                input.setDiameterOfRSBVerticalBar(rsbSizeOptions.get(which).getValue());
            } else {
                etRSBHorizontalBar.setText(options[which]);
                input.setDiameterOfRSBHorizontalBar(rsbSizeOptions.get(which).getValue());
            }
            dialog.dismiss();
        });
    }


    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }

    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setCHBInput(CHBInput input) {
        this.input = input;

        if (input.getGrid() != null) {
            etGrid.setText(input.getGrid());
        }

        if (input.getStoreyLocation() != null) {
            etStoreyLocation.setText(input.getStoreyLocation());
        }

        if (input.getDimension() != null) {
            dimension = input.getDimension();
            if (dimension.getLength() != 0) {
                etLength.setText(toDisplayFormat(dimension.getLength()));
            }

            if (dimension.getWidth() != 0) {
                etWidth.setText(toDisplayFormat(dimension.getWidth()));
            }
        }

        if (input.getSets() != 0) {
            etSets.setText(toDisplayFormat(input.getSets()));
        }

        if (input.getMiscellaneousOpenings() != 0) {
            etMiscellaneousOpening.setText(toDisplayFormat(input.getMiscellaneousOpenings()));
        }

        if (input.getTieWireLength() != 0) {
            etTieWireLength.setText(toDisplayFormat(input.getTieWireLength()));
        }


        if (input.getChbSize() != 0) {
            etCHBSize.setText(Integer.toString(input.getChbSize()));
        }

        if (input.getRsbSpacing() != null) {
            rsbSpacing = input.getRsbSpacing();
            if (rsbSpacing.getHorizontal() != 0) {
                etHorizontalSpacing.setText(Integer.toString(rsbSpacing.getHorizontal()));
            }

            if (rsbSpacing.getVertical() != 0) {
                etVerticalSpacing.setText(toDisplayFormat(rsbSpacing.getVertical()));
            }
        }

        if (input.getDiameterOfRSBVerticalBar() != 0) {
            etRSBVerticalBar.setText(Integer.toString(input.getDiameterOfRSBVerticalBar()));
        }

        if (input.getDiameterOfRSBHorizontalBar() != 0) {
            etRSBHorizontalBar.setText(Integer.toString(input.getDiameterOfRSBHorizontalBar()));
        }

        if (input.getMortarClass() != 0) {
            etMortarClass.setText(Grade.parseInt(input.getMortarClass()).toString());
        }

        for (int i = 0 ; i < input.getDoors().size() ; i++){

            DoorWindowInputBase doorWindowInputBase = getDoorWindowInput(input.getDoors().get(i).getKey(), true);

            if (i == 0){
                etDoor1Qty.setText(Integer.toString(input.getDoors().get(i).getSets()));
                if (doorWindowInputBase != null){
                    etDoor1Code.setText(doorWindowInputBase.getCode());
                }
            } else {
                etDoor2Qty.setText(Integer.toString(input.getDoors().get(i).getSets()));
                if (doorWindowInputBase != null){
                    etDoor2Code.setText(doorWindowInputBase.getCode());
                }
            }

        }

        for (int i = 0 ; i < input.getWindows().size() ; i++){

            DoorWindowInputBase doorWindowInputBase = getDoorWindowInput(input.getWindows().get(i).getKey(), false);

            if (i == 0){
                etWindow1Qty.setText(Integer.toString(input.getWindows().get(i).getSets()));
                if (doorWindowInputBase != null){
                    etWindow1Code.setText(doorWindowInputBase.getCode());
                }
            } else {
                etWindow2Qty.setText(Integer.toString(input.getWindows().get(i).getSets()));
                if (doorWindowInputBase != null){
                    etWindow2Code.setText(doorWindowInputBase.getCode());
                }
            }
        }
    }

    private DoorWindowInputBase getDoorWindowInput(String key, boolean isDoor){
        List<DoorWindowInputBase> inputs = new ArrayList<>();

        if (isDoor){
            inputs = doors;
        } else {
            inputs = windows;
        }

        for (DoorWindowInputBase input : inputs){
            if (key.equalsIgnoreCase(input.getKey())){
                return input;
            }
        }
        return null;
    }


    @Override
    public void displayResult(CHBResult output) {
        tvOpeningsResult.setText(String.format(getString(R.string.sqm_result), toDisplayFormat(output.getOpenings())));

        tvAreaResult.setText(String.format(getString(R.string.sqm_result), toDisplayFormat(output.getArea())));


        if (output.getChbSizeEnum() == null) {
            llCHBResultHolder.setVisibility(View.GONE);
            chbResultLine.setVisibility(View.GONE);
        } else {
            llCHBResultHolder.setVisibility(View.VISIBLE);
            chbResultLine.setVisibility(View.VISIBLE);
            tvCHBLabel.setText(String.format(getString(R.string.chb_label), Integer.toString(output.getChbSizeEnum().getValue())));
            tvCHBResult.setText(String.format(getString(R.string.pcs_result), toDisplayFormat(output.getChbCount())));
        }

        tvCementResult.setText(String.format(getString(R.string.bag_result), toDisplayFormat(output.getCement())));
        tvSandResult.setText(String.format(getString(R.string.cum_result), toDisplayFormat(output.getSand())));
        tvTieWireResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(output.getTieWire())));

        tvTenRSBLabel.setText(String.format(getString(R.string.rsb_label), Integer.toString(RsbSize.TEN_MM.getValue())));
        tvTenRSBResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(output.getTenRsb())));

        tvTwelveRSBLabel.setText(String.format(getString(R.string.rsb_label), Integer.toString(RsbSize.TWELVE_MM.getValue())));
        tvTwelveRSBResult.setText(String.format(getString(R.string.kg_result), toDisplayFormat(output.getTwelveRsb())));

        cvResults.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.et_door1_code)
    protected void doorCode1Click() {
        if (doorPickerDialog != null) {
            doorIndex = 0;
            doorPickerDialog.show();
        }
    }

    @OnClick(R.id.et_door2_code)
    protected void doorCode2Click() {
        if (doorPickerDialog != null) {
            doorIndex = 1;
            doorPickerDialog.show();
        }
    }

    @OnClick(R.id.et_window1_code)
    protected void windowCode1Click() {
        if (windowPickerDialog != null) {
            windowIndex = 0;
            windowPickerDialog.show();
        }
    }

    @OnClick(R.id.et_window2_code)
    protected void windowCode2Click() {
        if (windowPickerDialog != null) {
            windowIndex = 1;
            windowPickerDialog.show();
        }
    }


    @OnClick(R.id.et_mortar_class)
    protected void mortarClassClick() {
        if (mortarClassPickerDialog != null) {
            mortarClassPickerDialog.show();
        }
    }

    @OnClick(R.id.et_chb_size)
    protected void chbSizeClick() {
        if (chbSizePickerDialog != null) {
            chbSizePickerDialog.show();
        }
    }

    @OnClick(R.id.et_horizontal_spacing)
    protected void horizontalSpacingClick() {
        if (horizontalSpacingPickerDialog != null) {
            horizontalSpacingPickerDialog.show();
        }
    }

    @OnClick(R.id.et_vertical_spacing)
    protected void verticalSpacingClick() {
        if (verticalSpacingPickerDialog != null) {
            verticalSpacingPickerDialog.show();
        }
    }

    @OnClick(R.id.et_rsb_horizontal)
    protected void rsbHorizontalSpacingClick() {
        if (rsbSizePickerDialog != null) {
            isVerticalBar = false;
            rsbSizePickerDialog.setTitle(getString(R.string.horizontal_bar));
            rsbSizePickerDialog.show();
        }
    }

    @OnClick(R.id.et_rsb_vertical)
    protected void rsbVerticalSpacingClick() {
        if (rsbSizePickerDialog != null) {
            isVerticalBar = true;
            rsbSizePickerDialog.setTitle(getString(R.string.vertical_bar));
            rsbSizePickerDialog.show();
        }
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setGrid(etGrid.getText().toString());
        input.setStoreyLocation(etStoreyLocation.getText().toString());

        String length = etLength.getText().toString();
        if (length.isEmpty()) {
            dimension.setLength(0);
        } else {
            dimension.setLength(Double.parseDouble(length));
        }

        String width = etWidth.getText().toString();
        if (width.isEmpty()) {
            dimension.setWidth(0);
        } else {
            dimension.setWidth(Double.parseDouble(width));
        }
        input.setDimension(dimension);

        String tieWireLength = etTieWireLength.getText().toString();
        if (tieWireLength.isEmpty()) {
            input.setTieWireLength(0);
        } else {
            input.setTieWireLength(Double.parseDouble(tieWireLength));
        }


        String sets = etSets.getText().toString();
        if (sets.isEmpty()) {
            input.setSets(0);
        } else {
            input.setSets(Integer.parseInt(sets));
        }

        String openings = etMiscellaneousOpening.getText().toString();
        if (openings.isEmpty()) {
            input.setMiscellaneousOpenings(0);
        } else {
            input.setMiscellaneousOpenings(Double.parseDouble(openings));
        }

        String doorQty1 = etDoor1Qty.getText().toString();

        if (!doorQty1.isEmpty() && input.getDoors().size() > 0){
            input.getDoors().get(0).setSets(Integer.parseInt(doorQty1));
        }

        String doorQty2 = etDoor2Qty.getText().toString();

        if (!doorQty2.isEmpty() && input.getDoors().size() > 1){
            input.getDoors().get(1).setSets(Integer.parseInt(doorQty2));
        }

        String windowQty1 = etWindow1Qty.getText().toString();

        if (!windowQty1.isEmpty() && input.getWindows().size() > 0){
            input.getWindows().get(0).setSets(Integer.parseInt(windowQty1));
        }

        String windowQty2 = etWindow2Qty.getText().toString();

        if (!windowQty2.isEmpty() && input.getWindows().size() > 1){
            input.getWindows().get(1).setSets(Integer.parseInt(windowQty2));
        }

        input.setRsbSpacing(rsbSpacing);

        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
