package com.android.estimateapp.cleanarchitecture.data.entity.factors;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class ConcreteFactor extends Model {

    int mortarClass;

    private double cement;

    private double sand;

    private double threeFourths;

    private double g1;


    public ConcreteFactor() {

    }

    public ConcreteFactor(int mortarClass,double cement, double sand, double threeFourths, double g1) {
        setMortarClass(mortarClass);
        setCement(cement);
        setSand(sand);
        setThreeFourths(threeFourths);
        setG1(g1);
    }

    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    public double getThreeFourths() {
        return threeFourths;
    }

    public void setThreeFourths(double threeFourths) {
        this.threeFourths = threeFourths;
    }

    public double getG1() {
        return g1;
    }

    public void setG1(double g1) {
        this.g1 = g1;
    }
}
