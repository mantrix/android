package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;

import java.util.HashMap;
import java.util.Map;

public class BeamRebarSummary {

    Map<Integer, BeamRebarMaterial> beamRebarMaterialMap = new HashMap<>();

    public BeamRebarSummary(){

    }

    public BeamRebarSummary(Map<Integer, BeamRebarMaterial> beamRebarMaterialMap){
        this.beamRebarMaterialMap = beamRebarMaterialMap;
    }

    public Map<Integer, BeamRebarMaterial> getBeamRebarMaterialMap() {
        return beamRebarMaterialMap;
    }

    public void setBeamRebarMaterialMap(Map<Integer, BeamRebarMaterial> beamRebarMaterialMap) {
        this.beamRebarMaterialMap = beamRebarMaterialMap;
    }
}
