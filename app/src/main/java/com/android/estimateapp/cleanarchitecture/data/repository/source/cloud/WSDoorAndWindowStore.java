package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudDoorAndWindowDataStore;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class WSDoorAndWindowStore extends BaseStore implements CloudDoorAndWindowDataStore {


    private DatabaseReference projectScopeOfWorksRef;

    @Inject
    public WSDoorAndWindowStore() {
        super();
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
    }

    @Override
    public Observable<DoorWindowInputBase> createDoorAndWindowInput(String projectId, String scopeOfWorkId, DoorWindowInputBase input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(databaseReference -> push(databaseReference, input));
    }

    @Override
    public Observable<DoorWindowInputBase> updateDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId, DoorWindowInputBase input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference, input));
    }

    @Override
    public Observable<DoorWindowOutputBase> saveDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId, DoorWindowOutputBase result) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId)
                    .child(FIREBASE_RESULT);
        }).flatMap(databaseReference -> setValue(databaseReference, result));
    }


    @Override
    public Observable<List<DoorWindowInputBase>> getDoorAndWindowInputs(String projectId, String scopeOfWorkId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(query -> queryListValue(query, DoorWindowInputBase.class));
    }

    @Override
    public Observable<DoorWindowInputBase> getDoorAndWindowInput(String projectId, String scopeOfWorkId, String itemId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(query -> get(query, DoorWindowInputBase.class));
    }
}
