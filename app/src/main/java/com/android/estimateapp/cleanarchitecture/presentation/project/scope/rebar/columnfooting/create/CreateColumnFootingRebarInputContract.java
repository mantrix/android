package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.create;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateColumnFootingRebarInputContract {

    interface View {

        void setInput(ColumnFootingRebarInput input);

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(ColumnFootingRebarResult result);

        void inputSaved();

        void savingError();

        void requestForReCalculation();

        void setRSBPickerOptions(List<RsbSize> sizes);

        void showSavingDialog();

        void showSavingNotAllowedError();

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(ColumnFootingRebarInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
