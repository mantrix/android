package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ScopeOfWork {

    EARTH_WORKS(1,"Earth Works"),
    CONCRETE_WORKS(2,"Concrete Works"),
    REBAR_WORKS(3,"Rebar Works"),
    FORM_WORKS(4,"Form Works"),
    TILE_WORKS(5,"Tile Works"),
    FLAT_CEILING_WORKS(6,"Flat Ceiling Works"),
    MASONRY_PAINTING_WORKS(7,"Masonry Painting Works"),
    CHB_PLASTERING(8,"CHB Plastering"),
    CHB_LAYING(9,"CHB Laying"),
    DOORS(10,"Doors"),
    STONE_CLADDING(11,"Stone Cladding"),
    WINDOWS(12,"Windows"),
    SCAFFOLDINGS(13,"Scaffoldings"),
    BEAM_CONCRETE(14,"Beam Concrete"),
    COLUMN_CONCRETE(15,"Column Concrete"),
    WALL_FOOTING_REBAR(16,"Wall Footing Rebar"),
    COLUMN_FOOTING_REBAR(17,"Column Footing Rebar"),
    BEAM_REBAR(18,"Beam Rebar"),
    COLUMN_REBAR(19,"Column Rebar");


    int id;
    String name;

    ScopeOfWork(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    public static List<ScopeOfWork> allScopeOfWorks(){
        List<ScopeOfWork> scopeOfWorks = new ArrayList<>();

//      Phase 1
        scopeOfWorks.add(CONCRETE_WORKS);
        scopeOfWorks.add(TILE_WORKS);
        scopeOfWorks.add(CHB_PLASTERING);
        scopeOfWorks.add(CHB_LAYING);
        scopeOfWorks.add(STONE_CLADDING);

        /* Additional */
        scopeOfWorks.add(BEAM_CONCRETE);
        scopeOfWorks.add(COLUMN_CONCRETE);
        scopeOfWorks.add(DOORS);
        scopeOfWorks.add(WINDOWS);

        /* Rebars */

        scopeOfWorks.add(WALL_FOOTING_REBAR);
        scopeOfWorks.add(COLUMN_FOOTING_REBAR);
        scopeOfWorks.add(BEAM_REBAR);
        scopeOfWorks.add(COLUMN_REBAR);

        return scopeOfWorks;
    }

    public static Map<Integer,ScopeOfWork> allScopeOfMap(){
        Map<Integer,ScopeOfWork> map = new HashMap<>();

//      Phase 1
        map.put(CONCRETE_WORKS.getId(),CONCRETE_WORKS);
        map.put(TILE_WORKS.getId(),TILE_WORKS);
        map.put(CHB_PLASTERING.getId(),CHB_PLASTERING);
        map.put(CHB_LAYING.getId(),CHB_LAYING);
        map.put(STONE_CLADDING.getId(),STONE_CLADDING);

        /* Additional */
        map.put(BEAM_CONCRETE.getId(),BEAM_CONCRETE);
        map.put(COLUMN_CONCRETE.getId(),COLUMN_CONCRETE);
        map.put(DOORS.getId(),DOORS);
        map.put(WINDOWS.getId(),WINDOWS);

        map.put(WALL_FOOTING_REBAR.getId(),WALL_FOOTING_REBAR);
        map.put(COLUMN_FOOTING_REBAR.getId(),COLUMN_FOOTING_REBAR);
        map.put(BEAM_REBAR.getId(),BEAM_REBAR);
        map.put(COLUMN_REBAR.getId(),COLUMN_REBAR);

        return map;
    }


    public static ScopeOfWork parseInt(int value){
        for (ScopeOfWork work : allScopeOfWorks()){
            if (value == work.getId()){
                return work;
            }
        }
        throw new Error(value + " not supported Scope Of Work!");
    }
}
