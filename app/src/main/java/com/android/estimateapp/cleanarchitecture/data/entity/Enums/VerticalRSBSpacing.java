package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum VerticalRSBSpacing {

    FOUR_TENTHS(0.4),
    SIX_TENTHS(0.6),
    EIGHT_TENTHS(0.8);

    double value;

    VerticalRSBSpacing(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public static List<VerticalRSBSpacing> getAllVerticalRSBSpacings(){
        List<VerticalRSBSpacing> list = new ArrayList<>();
        list.add(FOUR_TENTHS);
        list.add(SIX_TENTHS);
        list.add(EIGHT_TENTHS);
        return list;
    }
}
