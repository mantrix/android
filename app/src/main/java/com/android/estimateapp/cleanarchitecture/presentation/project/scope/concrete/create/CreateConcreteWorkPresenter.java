package com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.create;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class CreateConcreteWorkPresenter  extends CreateScopeInputBasePresenter implements CreateConcreteWorkContract.UserActionListener {

    private CreateConcreteWorkContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;
    private ConcreteWorkCalculator concreteWorkCalculator;
    private RectangularGeneralConcreteResult result;


    private List<ConcreteFactor> concreteFactors;
    private List<MemberSectionProperty> memberSectionProperties;
    private RectangularConcreteInput input;

    private ProjectPropertiesRepository projectPropertiesRepository;


    @Inject
    public CreateConcreteWorkPresenter(ProjectRepository projectRepository,
                                              ProjectPropertiesRepository projectPropertiesRepository,
                                              AccountSubscriptionUseCase accountSubscriptionUseCase,
                                              ConcreteWorkCalculator concreteWorkCalculator,
                                              ThreadExecutorProvider threadExecutorProvider,
                                              PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase,threadExecutorProvider, postExecutionThread);
        this.projectPropertiesRepository = projectPropertiesRepository;
        this.projectRepository = projectRepository;
        this.concreteWorkCalculator = concreteWorkCalculator;

        concreteFactors = new ArrayList<>();
    }

    @Override
    public void setView(CreateConcreteWorkContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        setPickerOptions();
        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {
        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateRectangularConcreteWorkInput(projectId, scopeKey, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createRectangularConcreteWorkInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<RectangularConcreteInput> inputObservable) {
        view.showSavingDialog();

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    itemKey = input.getKey();
                    this.input = input;
                    saveResult();
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    private void saveResult(){
        if (result != null && itemKey != null) {
            projectRepository.saveRectangularConcreteWorkInputResult(projectId, scopeKey, itemKey, result)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(result -> {
                        this.result = result;
                    }, throwable -> {
                        throwable.printStackTrace();
                        view.savingError();
                    });
        }
    }

    @Override
    public void calculate(RectangularConcreteInput input) {
        MemberSectionProperty property = getMemberSection(input.getPropertyReferenceId());
        input.setPropertyReference(property);

        result = concreteWorkCalculator.calculateRectangularConcrete(getConcreteFactor(input.getConcreteClassEnum()), input);

        this.input = input;
        view.displayResult(result);
    }

    private ConcreteFactor getConcreteFactor(Grade grade) {

        if (grade != null) {
            for (ConcreteFactor concreteFactor : concreteFactors) {
                if (concreteFactor.getMortarClass() == grade.getId()) {
                    return concreteFactor;
                }
            }
        }
        return null;
    }

    private void setPickerOptions() {
        view.setMortarClassPickerOptions(Arrays.asList(Grade.AA, Grade.A, Grade.B, Grade.C, Grade.D, Grade.SPECIAL_CASE_ONE, Grade.SPECIAL_CASE_TWO, Grade.SPECIAL_CASE_THREE));
        view.setGravelSizesOptions(GravelSize.getGravelSizes());
    }

    private void loadConcreteInput() {
        projectRepository.getRectangularConcreteWorkInput(projectId, scopeKey, itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {

                    MemberSectionProperty property = getMemberSection(input.getPropertyReferenceId());
                    input.setPropertyReference(property);

                    this.input = input;

                    view.setInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate) {
        Observable.zip(projectRepository.getConcreteWorkFactors(projectId),
                projectPropertiesRepository.getMemberSectionProperties(projectId),(concreteFactors, memberSectionProperties) -> new Pair<List<ConcreteFactor>,List<MemberSectionProperty>>(concreteFactors, memberSectionProperties))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {
                    this.concreteFactors = pair.first;
                    this.memberSectionProperties = pair.second;

                    if (!memberSectionProperties.isEmpty()) {
                        view.setPropertyReferenceOptions(memberSectionProperties);
                    }

                    if (!factorUpdate) {
                        view.enabledCalculateBtn(true);

                        if (itemKey != null) {
                            loadConcreteInput();
                        }
                    } else {
                        if (itemKey != null) {
                            input.setPropertyReference(getMemberSection(input.getPropertyReferenceId()));

                            view.setInput(input);

                            view.requestForReCalculation();
                        }

                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }

    private MemberSectionProperty getMemberSection(String key) {

        if (key != null && key != null) {
            for (MemberSectionProperty property : memberSectionProperties) {
                if (key.equalsIgnoreCase(property.getKey())) {
                    return property;
                }
            }
        }
        return null;
    }


}