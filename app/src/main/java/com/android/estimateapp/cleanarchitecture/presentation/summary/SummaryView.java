package com.android.estimateapp.cleanarchitecture.presentation.summary;

public interface SummaryView<T> {

    void displaySummary(T t);
}