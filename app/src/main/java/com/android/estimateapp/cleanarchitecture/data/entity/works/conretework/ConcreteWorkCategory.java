package com.android.estimateapp.cleanarchitecture.data.entity.works.conretework;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Shape;

public class ConcreteWorkCategory {

    ScopeOfWork scopeOfWork;

    Category category;

    Shape shape;

    ConcreteWorksInput input;

    GeneralConcreteWorksResult result;

    public ConcreteWorkCategory(){
        this.scopeOfWork = ScopeOfWork.CONCRETE_WORKS;
    }

    public ScopeOfWork getScopeOfWork() {
        return scopeOfWork;
    }


    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public ConcreteWorksInput getInput() {
        return input;
    }

    public void setInput(ConcreteWorksInput input) {
        this.input = input;
    }

    public GeneralConcreteWorksResult getResult() {
        return result;
    }

    public void setResult(GeneralConcreteWorksResult result) {
        this.result = result;
    }
}
