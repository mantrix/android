package com.android.estimateapp.cleanarchitecture.domain.usecase.validator;

import com.android.estimateapp.cleanarchitecture.domain.usecase.base.UseCase;

public interface InputValidatorUseCase extends UseCase {
    boolean isValidEmail(String email);
    boolean isValidPassword(String password);
}
