package com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class ColumnFootingRebarResult extends Model {

    double qtyOfL;

    double qtyOfW;

    double tenRsb;

    double twelveRsb;

    double sixteenRsb;

    double twentyRsb;

    double twentyFiveRsb;

    double sixteenGiTieWire;

    public ColumnFootingRebarResult(){

    }

    public double getQtyOfL() {
        return qtyOfL;
    }

    public void setQtyOfL(double qtyOfL) {
        this.qtyOfL = qtyOfL;
    }

    public double getQtyOfW() {
        return qtyOfW;
    }

    public void setQtyOfW(double qtyOfW) {
        this.qtyOfW = qtyOfW;
    }

    public double getTenRsb() {
        return tenRsb;
    }

    public void setTenRsb(double tenRsb) {
        this.tenRsb = tenRsb;
    }

    public double getTwelveRsb() {
        return twelveRsb;
    }

    public void setTwelveRsb(double twelveRsb) {
        this.twelveRsb = twelveRsb;
    }

    public double getSixteenRsb() {
        return sixteenRsb;
    }

    public void setSixteenRsb(double sixteenRsb) {
        this.sixteenRsb = sixteenRsb;
    }

    public double getTwentyRsb() {
        return twentyRsb;
    }

    public void setTwentyRsb(double twentyRsb) {
        this.twentyRsb = twentyRsb;
    }

    public double getTwentyFiveRsb() {
        return twentyFiveRsb;
    }

    public void setTwentyFiveRsb(double twentyFiveRsb) {
        this.twentyFiveRsb = twentyFiveRsb;
    }

    public double getSixteenGiTieWire() {
        return sixteenGiTieWire;
    }

    public void setSixteenGiTieWire(double sixteenGiTieWire) {
        this.sixteenGiTieWire = sixteenGiTieWire;
    }
}
