package com.android.estimateapp.cleanarchitecture.data.entity.works.chb;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;

import java.util.ArrayList;
import java.util.List;

@IgnoreExtraProperties
public class CHBInput extends Model {

    public String key;

    String grid;

    String storeyLocation;

    Dimension dimension;

    List<DoorWindowOpeningSet> windows;

    List<DoorWindowOpeningSet> doors;

    int chbSize;

    double sets;

    double miscellaneousOpenings;

    RSBSpacing rsbSpacing;

    int diameterOfRSBVerticalBar;

    int diameterOfRSBHorizontalBar;

    double tieWireLength;

    int mortarClass;

    CHBFactor chbFactor;

    public CHBInput(){
        windows = new ArrayList<>();
        doors = new ArrayList<>();
    }

    @Exclude
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getStoreyLocation() {
        return storeyLocation;
    }

    public void setStoreyLocation(String storeyLocation) {
        this.storeyLocation = storeyLocation;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    @PropertyName("chbSize")
    public int getChbSize() {
        return chbSize;
    }

    public void setChbSize(int chbSize) {
        this.chbSize = chbSize;
    }

    public double getSets() {
        return sets;
    }

    public void setSets(double sets) {
        this.sets = sets;
    }

    public double getMiscellaneousOpenings() {
        return miscellaneousOpenings;
    }

    public void setMiscellaneousOpenings(double miscellaneousOpenings) {
        this.miscellaneousOpenings = miscellaneousOpenings;
    }

    public RSBSpacing getRsbSpacing() {
        return rsbSpacing;
    }

    public void setRsbSpacing(RSBSpacing rsbSpacing) {
        this.rsbSpacing = rsbSpacing;
    }

    public int getDiameterOfRSBVerticalBar() {
        return diameterOfRSBVerticalBar;
    }

    public void setDiameterOfRSBVerticalBar(int diameterOfRSBVerticalBar) {
        this.diameterOfRSBVerticalBar = diameterOfRSBVerticalBar;
    }

    public int getDiameterOfRSBHorizontalBar() {
        return diameterOfRSBHorizontalBar;
    }

    public void setDiameterOfRSBHorizontalBar(int diameterOfRSBHorizontalBar) {
        this.diameterOfRSBHorizontalBar = diameterOfRSBHorizontalBar;
    }

    public double getTieWireLength() {
        return tieWireLength;
    }

    public void setTieWireLength(double tieWireLength) {
        this.tieWireLength = tieWireLength;
    }

    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    @Exclude
    public CHBFactor getChbFactor() {
        return chbFactor;
    }

    public void setChbFactor(CHBFactor chbFactor) {
        this.chbFactor = chbFactor;
    }


    public List<DoorWindowOpeningSet> getDoors() {
        return doors;
    }

    public void setDoors(List<DoorWindowOpeningSet> doors) {
        this.doors = doors;
    }

    public List<DoorWindowOpeningSet> getWindows() {
        return windows;
    }

    public void setWindows(List<DoorWindowOpeningSet> windows) {
        this.windows = windows;
    }
}
