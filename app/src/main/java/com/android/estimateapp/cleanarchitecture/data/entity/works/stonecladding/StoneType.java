package com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class StoneType extends Model{

    String name;

    double length;

    double width;

    public StoneType(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
