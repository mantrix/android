package com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.create.CreateBeamSectionPropertyActivity;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

import static com.android.estimateapp.configuration.Constants.RESULT_FACTOR_UPDATE;

public class BeamSectionPropertyListActivity extends BaseActivity implements BeamSectionPropertiesListContract.View, ListItemClickLister<BeamSectionProperties> {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    BeamSectionPropertiesListContract.UserActionListener presenter;

    @Inject
    BeamSectionPropertiesAdapter adapter;

    String projectId;

    NpaLinearLayoutManager linearLayoutManager;

    public static Intent createIntent(Context context, String projectId) {
        Intent intent = new Intent(context, BeamSectionPropertyListActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.scope_list_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(getString(R.string.label_beam_section_properties));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        initRecyclerView();

        presenter.setView(this);
        presenter.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        presenter.setData(projectId);
    }


    @Override
    public void itemClick(BeamSectionProperties input) {
        Intent intent = CreateBeamSectionPropertyActivity.createIntent(this, projectId, input.getKey());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onLongClick(BeamSectionProperties input) {
//        AlertDialog alertDialog = createAlertDialog(getString(R.string.delete),
//                getString(R.string.delete_input_msg),
//                getString(R.string.delete),
//                getString(R.string.cancel), (dialog, which) -> {
//                    presenter.deleteItem(input.getKey());
//                    dialog.dismiss();
//                }, (dialog, which) -> {
//                    dialog.dismiss();
//                });
//        alertDialog.show();
    }

    @Override
    public void removeItem(String inputId) {
        adapter.deleteItem(inputId);
    }

    @Override
    public void displayList(List<BeamSectionProperties> inputs) {
        adapter.setItems(inputs);
    }

    private void initRecyclerView() {
        linearLayoutManager = new NpaLinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setListItemClickLister(this);
    }


    @OnClick(R.id.fab)
    public void addItemClick() {
        Intent intent = CreateBeamSectionPropertyActivity.createIntent(this, projectId, null);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_CONTENT_UPDATE:
                    presenter.reloadList();
                    break;
            }
        }
    }


    @Override
    public void onBackPressed() {
        setResult(RESULT_FACTOR_UPDATE);
        finish();
    }
}