package com.android.estimateapp.cleanarchitecture.data.entity.works.slabformworks;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.LumberSize;

public class SlabFormInput {

    int sets;

    double length;

    double width;

    double spacingX;

    double spacingY;

    LumberSize lumberSizeAtX;

    LumberSize lumberSizeAtY;

    LumberSize permiterBrace;


    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getSpacingX() {
        return spacingX;
    }

    public void setSpacingX(double spacingX) {
        this.spacingX = spacingX;
    }

    public double getSpacingY() {
        return spacingY;
    }

    public void setSpacingY(double spacingY) {
        this.spacingY = spacingY;
    }

    public LumberSize getLumberSizeAtX() {
        return lumberSizeAtX;
    }

    public void setLumberSizeAtX(LumberSize lumberSizeAtX) {
        this.lumberSizeAtX = lumberSizeAtX;
    }

    public LumberSize getLumberSizeAtY() {
        return lumberSizeAtY;
    }

    public void setLumberSizeAtY(LumberSize lumberSizeAtY) {
        this.lumberSizeAtY = lumberSizeAtY;
    }

    public LumberSize getPermiterBrace() {
        return permiterBrace;
    }

    public void setPermiterBrace(LumberSize permiterBrace) {
        this.permiterBrace = permiterBrace;
    }

}
