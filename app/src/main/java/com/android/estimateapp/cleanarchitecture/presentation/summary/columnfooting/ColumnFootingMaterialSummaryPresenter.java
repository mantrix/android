package com.android.estimateapp.cleanarchitecture.presentation.summary.columnfooting;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnfooting.ColumnFootingRebarResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnfooting.ColumnFootingWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ColumnFootingMaterialSummaryPresenter extends BasePresenter implements ColumnFootingMaterialSummaryContract.UserActionListener {

    private ColumnFootingMaterialSummaryContract.View view;
    private ProjectRepository projectRepository;
    private ColumnFootingWorkCalculator columnFootingWorkCalculator;
    private String projectId;
    private String scopeOfWorkId;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<RSBFactor> rsbFactors;

    @Inject
    public ColumnFootingMaterialSummaryPresenter(ProjectRepository projectRepository,
                                                 ColumnFootingWorkCalculator columnFootingWorkCalculator,
                                                 ThreadExecutorProvider threadExecutorProvider,
                                                 PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.columnFootingWorkCalculator = columnFootingWorkCalculator;
    }

    @Override
    public void setData(String projectId, String scopeOfWorkId) {
        this.projectId = projectId;
        this.scopeOfWorkId = scopeOfWorkId;
    }

    @Override
    public void setView(ColumnFootingMaterialSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadFactors();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void calculateSummary() {
        compositeDisposable.add(projectRepository.getColumnFootingInputs(projectId, scopeOfWorkId)
                .map(inputs -> {
                    return columnFootingWorkCalculator.generateSummary(generateResults(inputs));
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    view.displaySummary(summary);
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    private List<ColumnFootingRebarResult> generateResults(List<ColumnFootingRebarInput> inputs) {
        List<ColumnFootingRebarResult> results = new ArrayList<>();

        for (ColumnFootingRebarInput input : inputs) {
            results.add(columnFootingWorkCalculator.calculate(53, input, rsbFactors));
        }

        return results;
    }


    public void loadFactors() {
        projectRepository.getProjectRSBFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(plasteringMortarFactors -> {
                    this.rsbFactors = plasteringMortarFactors;

                    calculateSummary();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}