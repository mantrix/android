package com.android.estimateapp.cleanarchitecture.presentation.summary.generalconcrete;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.GeneralConcreteWorkSummary;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class ConcreteWorkSummaryPresenter extends BasePresenter implements GeneralConcreteWorkSummaryContract.UserActionListener {

    private GeneralConcreteWorkSummaryContract.View view;
    private ProjectRepository projectRepository;
    private ConcreteWorkCalculator concreteWorkCalculator;
    private String projectId;
    private String scopeKey;
    private ScopeOfWork scopeOfWork;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<ConcreteFactor> concreteFactors;
    private List<MemberSectionProperty> memberSectionProperties;
    private ProjectPropertiesRepository projectPropertiesRepository;

    @Inject
    public ConcreteWorkSummaryPresenter(ProjectRepository projectRepository,
                                        ProjectPropertiesRepository projectPropertiesRepository,
                                        ConcreteWorkCalculator concreteWorkCalculator,
                                        ThreadExecutorProvider threadExecutorProvider,
                                        PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.concreteWorkCalculator = concreteWorkCalculator;
        this.projectPropertiesRepository = projectPropertiesRepository;
    }

    @Override
    public void setData(String projectId, String scopeKey, ScopeOfWork scopeOfWork) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.scopeOfWork = scopeOfWork;
    }

    @Override
    public void setView(GeneralConcreteWorkSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {

        if (scopeOfWork == ScopeOfWork.CONCRETE_WORKS){
            loadGeneralFactors();
        } else {
            loadConcreteFactors();
        }

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private Observable<GeneralConcreteWorkSummary> getGeneralConcreteWorkSummaryObservable(){
        if (scopeOfWork == ScopeOfWork.CONCRETE_WORKS){
            return projectRepository.getConcreteWorkInputs(projectId, scopeKey)
                    .map(concreteWorksInputs -> concreteWorkCalculator.generateGeneralConcreteSummary(generateGeneralConcreteResults(concreteWorksInputs)));
        } else {
            return projectRepository.getRectangularConcreteWorkInputs(projectId, scopeKey)
                    .map(concreteWorksInputs -> concreteWorkCalculator.generateConcreteSummary(generateConcreteResults(concreteWorksInputs)));
        }
    }

    private void calculateSummary() {
        compositeDisposable.add(getGeneralConcreteWorkSummaryObservable()
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    if (scopeOfWork == ScopeOfWork.CONCRETE_WORKS){
                        view.displayGeneralSummary(summary);
                    } else {
                        view.displaySummary(summary);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    private List<GeneralConcreteWorksResult> generateGeneralConcreteResults(List<ConcreteWorksInput> inputs) {
        List<GeneralConcreteWorksResult> results = new ArrayList<>();

        for (ConcreteWorksInput input : inputs) {
            ConcreteFactor factor = getConcreteFactor(input.getConcreteClassEnum());
            results.add(concreteWorkCalculator.calculateGeneralConcrete(factor, input));
        }

        return results;
    }

    private List<RectangularGeneralConcreteResult> generateConcreteResults(List<RectangularConcreteInput> inputs) {
        List<RectangularGeneralConcreteResult> results = new ArrayList<>();

        for (RectangularConcreteInput input : inputs) {
            ConcreteFactor factor = getConcreteFactor(input.getConcreteClassEnum());
            if (input.getPropertyReferenceId() != null){
                input.setPropertyReference(getMemberSection(input.getPropertyReferenceId()));
            }
            results.add(concreteWorkCalculator.calculateRectangularConcrete(factor, input));
        }

        return results;
    }


    public void loadGeneralFactors() {
        projectRepository.getConcreteWorkFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(concreteFactors -> {
                    this.concreteFactors = concreteFactors;
                    calculateSummary();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    public void loadConcreteFactors() {
        Observable.zip(projectRepository.getConcreteWorkFactors(projectId),
                projectPropertiesRepository.getMemberSectionProperties(projectId),(concreteFactors, memberSectionProperties) -> new Pair<List<ConcreteFactor>,List<MemberSectionProperty>>(concreteFactors, memberSectionProperties))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {

                    this.concreteFactors = pair.first;
                    this.memberSectionProperties = pair.second;

                    calculateSummary();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private ConcreteFactor getConcreteFactor(Grade grade) {

        if (grade != null) {
            for (ConcreteFactor concreteFactor : concreteFactors) {
                if (concreteFactor.getMortarClass() == grade.getId()) {
                    return concreteFactor;
                }
            }
        }
        return null;
    }
    private MemberSectionProperty getMemberSection(String key) {

        if (key != null && key != null) {
            for (MemberSectionProperty property : memberSectionProperties) {
                if (key.equalsIgnoreCase(property.getKey())) {

                    return property;
                }
            }
        }
        return null;
    }

}
