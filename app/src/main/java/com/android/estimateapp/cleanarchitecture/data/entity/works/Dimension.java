package com.android.estimateapp.cleanarchitecture.data.entity.works;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Dimension {

    double width;

    /*
     * width 2
     *
     * Use for Trapezoid dimension
     */
    double lx2;

    double length;
    double height;


    public double getLx2() {
        return lx2;
    }

    public void setLx2(double lx2) {
        this.lx2 = lx2;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
