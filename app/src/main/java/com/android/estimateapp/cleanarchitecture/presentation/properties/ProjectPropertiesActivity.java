package com.android.estimateapp.cleanarchitecture.presentation.properties;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.membersection.MemberSectionPropertiesFragment;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;

import static com.android.estimateapp.configuration.Constants.RESULT_PROPERTY_UPDATE;

public class ProjectPropertiesActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String projectId;

    public static Intent createIntent(Context context, String projectId) {
        Intent intent = new Intent(context, ProjectPropertiesActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.project_properties_activity_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        extractExtras();
        setToolbarTitle(getString(R.string.properties));

        showFragment(R.id.rl_fragment_container, MemberSectionPropertiesFragment.newInstance(projectId), false);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_PROPERTY_UPDATE);
        finish();
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
    }

}