package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

public enum  PlywoodSize {

    ONE_FOURTH_X_THREE_BY_SIX(1,"1/4x3x6"),
    THREE_EIGHT_X_THREE_BY_SIX(2,"3/8x3x6"),
    ONE_HALF_X_THREE_BY_SIX(3,"1/2x3x6"),
    THREE_FOURTHS_X_THREE_BY_SIX(4,"3/4x3x6"),
    ONE_FOURTH_X_FOUR_BY_EIGHT(5,"1/4x4x8"),
    THREE_EIGHT_X_FOUR_BY_EIGHT(6,"3/8x4x8"),
    TONE_HALF_X_FOUR_BY_EIGHT(7,"1/2x4x8"),
    THREE_FOURTHS_X_FOUR_BY_EIGHT(8,"3/4x4x8");

    String name;
    int id;

    PlywoodSize(int id,String name) {
        this.id = id;
        this.name = name;
    }
}
