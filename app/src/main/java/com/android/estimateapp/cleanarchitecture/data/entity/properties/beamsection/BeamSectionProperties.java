package com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.QtyDiameterLength;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.Stirrups;
import com.google.firebase.database.Exclude;

public class BeamSectionProperties extends Model {

    String stirrupsKey;

    String description;

    VerticalBar topBar;

    VerticalBar bottomBar;

    ExtraBar extraBarAtLeftSupport;

    ExtraBar bottomExtraAtMid;

    ExtraBar extraBarAtRightSupport;

    Stirrups stirrups;

    QtyDiameterLength splicing;

    QtyDiameterLength splicing2;

    QtyDiameterLength hookes;

    QtyDiameterLength hookes2;

    double base;

    double height;

    public BeamSectionProperties(){

    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public VerticalBar getTopBar() {
        return topBar;
    }

    public void setTopBar(VerticalBar topBar) {
        this.topBar = topBar;
    }

    public VerticalBar getBottomBar() {
        return bottomBar;
    }

    public void setBottomBar(VerticalBar bottomBar) {
        this.bottomBar = bottomBar;
    }

    public ExtraBar getExtraBarAtLeftSupport() {
        return extraBarAtLeftSupport;
    }

    public void setExtraBarAtLeftSupport(ExtraBar extraBarAtLeftSupport) {
        this.extraBarAtLeftSupport = extraBarAtLeftSupport;
    }

    public ExtraBar getBottomExtraAtMid() {
        return bottomExtraAtMid;
    }

    public void setBottomExtraAtMid(ExtraBar bottomExtraAtMid) {
        this.bottomExtraAtMid = bottomExtraAtMid;
    }

    public ExtraBar getExtraBarAtRightSupport() {
        return extraBarAtRightSupport;
    }

    public void setExtraBarAtRightSupport(ExtraBar extraBarAtRightSupport) {
        this.extraBarAtRightSupport = extraBarAtRightSupport;
    }

    public Stirrups getStirrups() {
        return stirrups;
    }

    public void setStirrups(Stirrups stirrups) {
        this.stirrups = stirrups;
    }

    public QtyDiameterLength getSplicing() {
        return splicing;
    }

    public void setSplicing(QtyDiameterLength splicing) {
        this.splicing = splicing;
    }

    public QtyDiameterLength getSplicing2() {
        return splicing2;
    }

    public void setSplicing2(QtyDiameterLength splicing2) {
        this.splicing2 = splicing2;
    }

    public QtyDiameterLength getHookes() {
        return hookes;
    }

    public void setHookes(QtyDiameterLength hookes) {
        this.hookes = hookes;
    }

    public QtyDiameterLength getHookes2() {
        return hookes2;
    }

    public void setHookes2(QtyDiameterLength hookes2) {
        this.hookes2 = hookes2;
    }

    @Exclude
    public double getArea(){
        return base * height;
    }

}
