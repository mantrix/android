package com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.create;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.ProjectPropertiesActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateConcreteWorkActivity extends CreateScopeInputBaseActivity implements CreateConcreteWorkContract.View {


    @Inject
    CreateConcreteWorkContract.UserActionListener presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_pouring_batch)
    TextInputEditText etPouringBatch;
    @BindView(R.id.til_pouring_batch)
    TextInputLayout tilPouringBatch;
    @BindView(R.id.et_member_number)
    TextInputEditText etMemberNumber;
    @BindView(R.id.til_member_number)
    TextInputLayout tilMemberNumber;
    @BindView(R.id.et_property_ref)
    TextInputEditText etPropertyRef;
    @BindView(R.id.til_property_ref)
    TextInputLayout tilPropertyRef;
    @BindView(R.id.et_length_cc)
    TextInputEditText etLengthCc;
    @BindView(R.id.til_length_cc)
    TextInputLayout tilLengthCc;
    @BindView(R.id.et_offset)
    TextInputEditText etOffset;
    @BindView(R.id.til_offset)
    TextInputLayout tilOffset;
    @BindView(R.id.et_pouring_cut)
    TextInputEditText etPouringCut;
    @BindView(R.id.til_pouring_cut)
    TextInputLayout tilPouringCut;
    @BindView(R.id.et_sets)
    TextInputEditText etSets;
    @BindView(R.id.til_sets)
    TextInputLayout tilSets;
    @BindView(R.id.et_wastage)
    TextInputEditText etWastage;
    @BindView(R.id.til_wastage)
    TextInputLayout tilWastage;
    @BindView(R.id.et_gravel_size)
    TextInputEditText etGravelSize;
    @BindView(R.id.til_gravel_size)
    TextInputLayout tilGravelSize;
    @BindView(R.id.et_concrete_class)
    TextInputEditText etConcreteClass;
    @BindView(R.id.til_concrete_class)
    TextInputLayout tilConcreteClass;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.tv_volume)
    TextView tvVolume;
    @BindView(R.id.tv_cement)
    TextView tvCement;
    @BindView(R.id.tv_sand)
    TextView tvSand;
    @BindView(R.id.tv_three_fourths)
    TextView tvThreeFourths;
    @BindView(R.id.tv_g1)
    TextView tvG1;
    @BindView(R.id.cv_results)
    CardView cvResults;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private int scopeID;
    private ScopeOfWork scopeOfWork;

    private RectangularConcreteInput input;

    private ProgressDialog saveProgressDialog;
    private boolean updateContent;

    AlertDialog propertyRefPickerDialog;
    AlertDialog mortarClassPickerDialog;
    AlertDialog gravelSizePickerDialog;

    public static Intent createIntent(Context context, String projectId, int scopeID, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateConcreteWorkActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.SCOPE_OF_WORK, scopeID);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.create_concrete_work_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(scopeOfWork.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        input = new RectangularConcreteInput();

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        scopeID = intent.getIntExtra(Constants.SCOPE_OF_WORK, 0);
        scopeOfWork = ScopeOfWork.parseInt(scopeID);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_w_property_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, scopeOfWork.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            case R.id.menu_property:
                startActivityForResult(ProjectPropertiesActivity.createIntent(this, projectId), Constants.REQUEST_CODE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void setInput(RectangularConcreteInput input) {
        this.input = input;

        if (input.getPropertyReference() != null) {
            etPropertyRef.setText(input.getPropertyReference().getName());
        }

        if (input.getPouringBatchNum() != 0) {
            etPouringBatch.setText(toDisplayFormat(input.getPouringBatchNum()));
        }

        if (input.getMemberNum() != 0) {
            etMemberNumber.setText(toDisplayFormat(input.getMemberNum()));
        }

        if (input.getLengthCC() != 0) {
            etLengthCc.setText(toDisplayFormat(input.getLengthCC()));
        }

        if (input.getOffset() != 0) {
            etOffset.setText(toDisplayFormat(input.getOffset()));
        }

        if (input.getPouringCut() != 0) {
            etPouringCut.setText(toDisplayFormat(input.getPouringCut()));
        }

        if (input.getSets() != 0) {
            etSets.setText(toDisplayFormat(input.getSets()));
        }

        if (input.getWastage() != 0) {
            etWastage.setText(toDisplayFormat(input.getWastage()));
        }


        if (input.getMortarClass() != 0) {
            etConcreteClass.setText(Grade.parseInt(input.getMortarClass()).toString());
        }

        if (input.getGravelSizeType() != null) {
            etGravelSize.setText(input.getGravelSizeType());
        }
    }


    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayResult(RectangularGeneralConcreteResult result) {
        tvVolume.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getVolume())));
        tvCement.setText(String.format(getString(R.string.bag_result), toDisplayFormat(result.getCement())));
        tvSand.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getSand())));
        tvThreeFourths.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getThreeFourthGravel())));
        tvG1.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getG1Gravel())));

        cvResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }


    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }


    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    @OnClick(R.id.et_property_ref)
    protected void propertyReferenceClick() {
        if (propertyRefPickerDialog != null) {
            propertyRefPickerDialog.show();
        } else {
            Intent intent = ProjectPropertiesActivity.createIntent(this, projectId);
            startActivityForResult(intent, Constants.REQUEST_CODE);
        }
    }


    @OnClick(R.id.et_concrete_class)
    protected void concreteClassClick() {
        if (mortarClassPickerDialog != null) {
            mortarClassPickerDialog.show();
        }
    }


    @Override
    public void setMortarClassPickerOptions(List<Grade> classes) {
        String[] options = new String[classes.size()];
        for (int i = 0; i < classes.size(); i++) {
            options[i] = classes.get(i).toString();
        }

        mortarClassPickerDialog = createChooser(getString(R.string.concrete_mortar_class), options, (dialog, which) -> {
            etConcreteClass.setText(options[which]);
            input.setMortarClass(classes.get(which).getId());
            dialog.dismiss();
        });
    }

    @Override
    public void setPropertyReferenceOptions(List<MemberSectionProperty> properties) {
        String[] options = new String[properties.size()];
        for (int i = 0; i < properties.size(); i++) {
            options[i] = properties.get(i).getName();
        }

        propertyRefPickerDialog = createChooser(getString(R.string.property_reference), options, (dialog, which) -> {
            etPropertyRef.setText(options[which]);
            input.setPropertyReference(properties.get(which));
            input.setPropertyReferenceId(properties.get(which).getKey());
            input.setPropertyReferenceName(String.format("%1$s::%2$s",properties.get(which).getKey(),properties.get(which).getName()));
            dialog.dismiss();
        });
    }

    @OnClick(R.id.et_gravel_size)
    protected void gravelSizeClick() {
        if (gravelSizePickerDialog != null) {
            gravelSizePickerDialog.show();
        }
    }

    @Override
    public void setGravelSizesOptions(List<GravelSize> gravelSizes) {
        String[] options = new String[gravelSizes.size()];
        for (int i = 0; i < gravelSizes.size(); i++) {
            options[i] = gravelSizes.get(i).toString();
        }

        gravelSizePickerDialog = createChooser(getString(R.string.gravel_sizes), options, (dialog, which) -> {
            etGravelSize.setText(options[which]);
            input.setGravelSizeType(gravelSizes.get(which).toString());
            dialog.dismiss();
        });
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {

        String pouringBatch = etPouringBatch.getText().toString();
        if (pouringBatch.isEmpty()) {
            input.setPouringBatchNum(0);
        } else {
            input.setPouringBatchNum(Integer.parseInt(pouringBatch));
        }

        String pouringMember = etMemberNumber.getText().toString();
        if (pouringMember.isEmpty()) {
            input.setMemberNum(0);
        } else {
            input.setMemberNum(Integer.parseInt(pouringMember));
        }

        String lengthCC = etLengthCc.getText().toString();
        if (lengthCC.isEmpty()) {
            input.setLengthCC(0);
        } else {
            input.setLengthCC(Double.parseDouble(lengthCC));
        }

        String offset = etOffset.getText().toString();
        if (offset.isEmpty()) {
            input.setOffset(0);
        } else {
            input.setOffset(Double.parseDouble(offset));
        }

        String pouringCut = etPouringCut.getText().toString();
        if (pouringCut.isEmpty()) {
            input.setPouringCut(0);
        } else {
            input.setPouringCut(Double.parseDouble(pouringCut));
        }


        String sets = etSets.getText().toString();
        if (sets.isEmpty()) {
            input.setSets(0);
        } else {
            input.setSets(Integer.parseInt(sets));
        }

        String wastage = etWastage.getText().toString();
        if (wastage.isEmpty()) {
            input.setWastage(0);
        } else {
            input.setWastage(Integer.parseInt(wastage));
        }

        presenter.calculate(input);
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etPouringBatch.getText().toString().isEmpty()) {
            tilPouringBatch.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilPouringBatch.setError(null);
        }

        if (etMemberNumber.getText().toString().isEmpty()) {
            tilMemberNumber.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilMemberNumber.setError(null);
        }

        if (etPropertyRef.getText().toString().isEmpty()) {
            etPropertyRef.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etPropertyRef.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                case Constants.RESULT_PROPERTY_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
