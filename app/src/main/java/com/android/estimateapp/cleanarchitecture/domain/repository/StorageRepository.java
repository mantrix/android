package com.android.estimateapp.cleanarchitecture.domain.repository;

import android.graphics.Bitmap;

import io.reactivex.Flowable;

public interface StorageRepository {


    Flowable<Double> uploadProfilePhoto(Bitmap bitmap);

}
