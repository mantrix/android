package com.android.estimateapp.cleanarchitecture.presentation.welcome.contract;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

public interface WelcomeContract {

    interface View {
        void showLoginScreen();

        void navigateToHomeScreen();
    }

    interface UserActionListener extends Presenter<View> {
        void onBackPressed(int backStack);
    }
}
