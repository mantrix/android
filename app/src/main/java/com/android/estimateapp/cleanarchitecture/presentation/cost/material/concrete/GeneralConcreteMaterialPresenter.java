package com.android.estimateapp.cleanarchitecture.presentation.cost.material.concrete;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.tiles.TileWorkMaterialDialogPresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ConcreteWorkMaterials;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GeneralConcreteMaterialPresenter extends CreateScopeInputBasePresenter implements GeneralConcreteMaterialContract.UserActionListener {

    private static final String TAG = TileWorkMaterialDialogPresenter.class.getSimpleName();

    private GeneralConcreteMaterialContract.View view;

    private String projectId;
    private String scopeKey;
    private ProjectRepository projectRepository;

    private ConcreteWorkMaterials input;

    private String itemKey;

    @Inject
    public GeneralConcreteMaterialPresenter(ProjectRepository projectRepository,
                                           AccountSubscriptionUseCase accountSubscriptionUseCase,
                                           ThreadExecutorProvider threadExecutorProvider,
                                           PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase, threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;

    }

    @Override
    public void setView(GeneralConcreteMaterialContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void start() {
        loadInput();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save(ConcreteWorkMaterials materials) {
        this.input = materials;

        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateMaterial(projectId, scopeKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createMaterial(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<ConcreteWorkMaterials> inputObservable) {

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    this.input = input;
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    private void loadInput() {
        projectRepository.getMaterial(projectId, scopeKey, ConcreteWorkMaterials.class)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(materials -> {
                    itemKey = materials.getKey();
                    this.input = materials;

                    view.setInput(input);

                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

}
