package com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.QtyDiameterFactor;

public class ExtraBar extends Model {

    QtyDiameterFactor property1;

    QtyDiameterFactor property2;

    public ExtraBar(){

    }

    public QtyDiameterFactor getProperty1() {
        return property1;
    }

    public void setProperty1(QtyDiameterFactor property1) {
        this.property1 = property1;
    }

    public QtyDiameterFactor getProperty2() {
        return property2;
    }

    public void setProperty2(QtyDiameterFactor property2) {
        this.property2 = property2;
    }
}
