package com.android.estimateapp.cleanarchitecture.presentation.summary.plastering;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.PlasteringSummary;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import javax.inject.Inject;

import butterknife.BindView;

public class PlasteringSummaryFragment extends BaseFragment implements PlasteringSummaryContract.View {


    @Inject
    PlasteringSummaryContract.UserActionListener presenter;

    @BindView(R.id.tv_cement)
    TextView tvCement;
    @BindView(R.id.tv_sand)
    TextView tvSand;


    public static PlasteringSummaryFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();

        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        PlasteringSummaryFragment fragment = new PlasteringSummaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String projectId;
    private String scopeKey;

    @Override
    protected int getLayoutResource() {
        return R.layout.plastering_summary_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        extractExtras();
        presenter.setData(projectId, scopeKey);
        presenter.setView(this);
        presenter.start();

    }


    private void extractExtras() {
        Bundle bundle = getArguments();
        projectId = bundle.getString(Constants.PROJECT_ID);
        scopeKey = bundle.getString(Constants.SCOPE_KEY);
    }

    private static final String TAG = PlasteringSummaryFragment.class.getSimpleName();

    @Override
    public void displaySummary(PlasteringSummary summary) {

        BaseActivity baseActivity = (BaseActivity) getActivity();

        Log.d(TAG, "displaySummary: " + summary.getCement() + " " + summary.getSand());

        tvCement.setText(baseActivity.toDisplayFormat(summary.getCement()));
        tvSand.setText(baseActivity.toDisplayFormat(summary.getSand()));
    }

}