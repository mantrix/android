package com.android.estimateapp.cleanarchitecture.presentation.factor.shared.rsb;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface RSBFactorContract {

    interface View {
        void displayList(List<RSBFactor> rsbFactors);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId);

        void updateFactors(List<RSBFactor> rsbFactors);
    }
}
