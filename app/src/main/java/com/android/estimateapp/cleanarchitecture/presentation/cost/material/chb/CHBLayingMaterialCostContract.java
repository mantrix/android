package com.android.estimateapp.cleanarchitecture.presentation.cost.material.chb;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerLocData;

public interface CHBLayingMaterialCostContract {

    interface View {

        void setInput(CHBSummaryPerLocData materials);

        void inputSaved();

        void showSavingNotAllowedError();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void save(CHBSummaryPerLocData materials);
    }
}
