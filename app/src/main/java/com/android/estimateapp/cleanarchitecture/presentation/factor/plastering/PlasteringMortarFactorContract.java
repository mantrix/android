package com.android.estimateapp.cleanarchitecture.presentation.factor.plastering;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface PlasteringMortarFactorContract {


    interface View {
        void displayList(List<PlasteringMortarFactor> factors);

        void factorSaved();
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

        void updateFactors(List<PlasteringMortarFactor> plasteringMortarFactors);
    }
}
