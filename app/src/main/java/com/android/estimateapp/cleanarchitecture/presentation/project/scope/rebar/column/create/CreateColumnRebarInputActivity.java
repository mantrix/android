package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.create;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.StirrupsOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.RSBValuePair;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.list.ColumnSectionPropertyListActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateColumnRebarInputActivity extends CreateScopeInputBaseActivity implements CreateColumnRebarInputContract.View {


    @Inject
    CreateColumnRebarInputContract.UserActionListener presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_storey)
    TextInputEditText etStorey;
    @BindView(R.id.til_storey)
    TextInputLayout tilStorey;
    @BindView(R.id.et_span)
    TextInputEditText etSpan;
    @BindView(R.id.til_span)
    TextInputLayout tilSpan;
    @BindView(R.id.et_length_cc)
    TextInputEditText etLengthCc;
    @BindView(R.id.til_length_cc)
    TextInputLayout tilLengthCc;
    @BindView(R.id.et_property_ref)
    TextInputEditText etPropertyRef;
    @BindView(R.id.til_property_ref)
    TextInputLayout tilPropertyRef;
    @BindView(R.id.et_offset_1)
    TextInputEditText etOffset1;
    @BindView(R.id.til_offset_1)
    TextInputLayout tilOffset1;
    @BindView(R.id.et_offset_2)
    TextInputEditText etOffset2;
    @BindView(R.id.til_offset_2)
    TextInputLayout tilOffset2;
    @BindView(R.id.et_wastage)
    TextInputEditText etWastage;
    @BindView(R.id.til_wastage)
    TextInputLayout tilWastage;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.tv_lclear)
    TextView tvLclear;
    @BindView(R.id.ll_header)
    LinearLayout llHeader;
    @BindView(R.id.tv_rsb_rebar_1)
    TextView tvRsbRebar1;
    @BindView(R.id.tv_qty_rebar_1)
    TextView tvQtyRebar1;
    @BindView(R.id.tv_rsb_rebar_2)
    TextView tvRsbRebar2;
    @BindView(R.id.tv_qty_rebar_2)
    TextView tvQtyRebar2;
    @BindView(R.id.tv_rsb_footing_1)
    TextView tvRsbFooting1;
    @BindView(R.id.tv_qty_footing_1)
    TextView tvQtyFooting1;
    @BindView(R.id.tv_rsb_footing_2)
    TextView tvRsbFooting2;
    @BindView(R.id.tv_qty_footing_2)
    TextView tvQtyFooting2;
    @BindView(R.id.tv_rsb_column_1)
    TextView tvRsbColumn1;
    @BindView(R.id.tv_qty_column_1)
    TextView tvQtyColumn1;
    @BindView(R.id.tv_rsb_column_2)
    TextView tvRsbColumn2;
    @BindView(R.id.tv_qty_column_2)
    TextView tvQtyColumn2;
    @BindView(R.id.tv_rsb_splicing_1)
    TextView tvRsbSplicing1;
    @BindView(R.id.tv_qty_splicing_1)
    TextView tvQtySplicing1;
    @BindView(R.id.tv_rsb_splicing_2)
    TextView tvRsbSplicing2;
    @BindView(R.id.tv_qty_splicing_2)
    TextView tvQtySplicing2;
    @BindView(R.id.tv_stirrups_qty_pc)
    TextView tvStirrupsQtyPc;
    @BindView(R.id.tv_stirrups_length)
    TextView tvStirrupsLength;
    @BindView(R.id.tv_stirrups_rsb_size)
    TextView tvStirrupsRsbSize;
    @BindView(R.id.tv_stirrups_qty_kg)
    TextView tvStirrupsQtyKg;
    @BindView(R.id.cv_results)
    CardView cvResults;


    private List<ColumnRebarProperties> columnSectionProperties;

    private String projectId;
    private String scopeKey;
    private String itemKey;

    private ColumnRebarInput input;

    private ProgressDialog saveProgressDialog;

    private AlertDialog sectionPropertyDialog;

    private boolean updateContent;

    public static Intent createIntent(Context context, String projectId, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateColumnRebarInputActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.create_column_rebar_input_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.COLUMN_REBAR.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        input = new ColumnRebarInput();
        columnSectionProperties = new ArrayList<>();

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_w_property_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.COLUMN_REBAR.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            case R.id.menu_property:
                startActivityForResult(ColumnSectionPropertyListActivity.createIntent(this, projectId), Constants.REQUEST_CODE);
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }


    private boolean validateInputs() {
        boolean valid = true;
        if (etSpan.getText().toString().isEmpty()) {
            tilSpan.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilSpan.setError(null);
        }

        if (etStorey.getText().toString().isEmpty()) {
            tilStorey.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilStorey.setError(null);
        }
        return valid;
    }

    @Override
    public void setPropertiesOption(List<ColumnRebarProperties> propertiesOption) {
        columnSectionProperties = propertiesOption;
        String[] options = new String[propertiesOption.size()];
        for (int i = 0; i < propertiesOption.size(); i++) {
            options[i] = propertiesOption.get(i).getDescription();
        }

        sectionPropertyDialog = createChooser(getString(R.string.label_beam_section_properties), options, (dialog, which) -> {
            etPropertyRef.setText(propertiesOption.get(which).getDescription());
            input.setPropertyKey(propertiesOption.get(which).getKey());
            input.setPropertyReferenceName(String.format("%1$s::%2$s",propertiesOption.get(which).getKey(),propertiesOption.get(which).getDescription()));
            dialog.dismiss();
        });
    }


    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }

    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setInput(ColumnRebarInput input) {
        this.input = input;

        if (input.getStorey() != null) {
            etStorey.setText(input.getStorey());
        }

        if (input.getGroupSpanMember() != null) {
            etSpan.setText(input.getGroupSpanMember());
        }

        if (input.getLengthCC() != 0) {
            etLengthCc.setText(toDisplayFormat(input.getLengthCC()));
        }

        if (input.getOffset1() != 0) {
            etOffset1.setText(toDisplayFormat(input.getOffset1()));
        }

        if (input.getOffset2() != 0) {
            etOffset2.setText(toDisplayFormat(input.getOffset2()));
        }

        if (input.getWastage() != 0) {
            etWastage.setText(toDisplayFormat(input.getWastage()));
        }

        if (input.getPropertyKey() != null && columnSectionProperties != null) {
            for (ColumnRebarProperties properties : columnSectionProperties) {
                if (input.getPropertyKey().equalsIgnoreCase(properties.getKey())) {
                    etPropertyRef.setText(properties.getDescription());
                    break;
                }
            }
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void displayResult(ColumnRebarOutput result) {

        tvLclear.setText("L (clear span): " + result.getlClear());

        // REBAR 1
        if (result.getRebar1() != null) {
            RSBValuePair rebar1 = result.getRebar1();
            if (rebar1.getRsbFactor() != null) {
                tvRsbRebar1.setText(Double.toString(rebar1.getRsbFactor().getSize()));
                tvQtyRebar1.setText(toDisplayFormat(rebar1.getValue()));
            }
        }

        // REBAR 2
        if (result.getRebar2() != null) {
            RSBValuePair rebar2 = result.getRebar2();
            if (rebar2.getRsbFactor() != null) {
                tvRsbRebar2.setText(Double.toString(rebar2.getRsbFactor().getSize()));
                tvQtyRebar2.setText(toDisplayFormat(rebar2.getValue()));
            }
        }

        // FOOTING
        if (result.getRebarBendAtFooting1() != null) {
            RSBValuePair footing1 = result.getRebarBendAtFooting1();
            if (footing1.getRsbFactor() != null) {
                tvRsbFooting1.setText(Double.toString(footing1.getRsbFactor().getSize()));
                tvQtyFooting1.setText(toDisplayFormat(footing1.getValue()));
            }
        }

        if (result.getRebarBendAtFooting2() != null) {
            RSBValuePair footing2 = result.getRebarBendAtFooting2();
            if (footing2.getRsbFactor() != null) {
                tvRsbFooting2.setText(Double.toString(footing2.getRsbFactor().getSize()));
                tvQtyFooting2.setText(toDisplayFormat(footing2.getValue()));
            }
        }

        // COLUMN END
        if (result.getRebarBendAtColumnEnd1() != null) {
            RSBValuePair column1 = result.getRebarBendAtColumnEnd1();
            if (column1.getRsbFactor() != null) {
                tvRsbColumn1.setText(Double.toString(column1.getRsbFactor().getSize()));
                tvQtyColumn1.setText(toDisplayFormat(column1.getValue()));
            }
        }

        if (result.getRebarBendAtColumnEnd2() != null) {
            RSBValuePair column2 = result.getRebarBendAtColumnEnd2();
            if (column2.getRsbFactor() != null) {
                tvRsbColumn2.setText(Double.toString(column2.getRsbFactor().getSize()));
                tvQtyColumn2.setText(toDisplayFormat(column2.getValue()));
            }
        }




        // SPLICING
        if (result.getPossibleSplicing1() != null) {
            RSBValuePair splicing1 = result.getPossibleSplicing1();
            if (splicing1.getRsbFactor() != null) {
                tvRsbSplicing1.setText(Double.toString(splicing1.getRsbFactor().getSize()));
                tvQtySplicing1.setText(toDisplayFormat(splicing1.getValue()));
            }
        }

        if (result.getPossibleSplicing2() != null) {
            RSBValuePair splicing2 = result.getPossibleSplicing2();
            if (splicing2.getRsbFactor() != null) {
                tvRsbSplicing2.setText(Double.toString(splicing2.getRsbFactor().getSize()));
                tvQtySplicing2.setText(toDisplayFormat(splicing2.getValue()));
            }
        }


        // STIRRUPS
        if (result.getStirrupsOutput() != null) {
            StirrupsOutput stirrupsOutput = result.getStirrupsOutput();

            if (stirrupsOutput.getQuantity() != 0) {
                tvStirrupsQtyPc.setText(Double.toString(stirrupsOutput.getQuantity()));
            }
            if (stirrupsOutput.getLength() != 0) {
                tvStirrupsLength.setText(Double.toString(stirrupsOutput.getLength()));
            }

            if (stirrupsOutput.getTotal() != 0) {
                tvStirrupsQtyKg.setText(toDisplayFormat(stirrupsOutput.getTotal()));
            }

            if (stirrupsOutput.getSelectedRsb() != 0) {
                tvStirrupsRsbSize.setText(Integer.toString(stirrupsOutput.getSelectedRsb()));
            }
        }

        cvResults.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.et_property_ref)
    protected void mainRebarClick() {
        if (columnSectionProperties != null && !columnSectionProperties.isEmpty() && sectionPropertyDialog != null) {
            sectionPropertyDialog.show();
        } else {
            startActivityForResult(ColumnSectionPropertyListActivity.createIntent(this, projectId), Constants.REQUEST_CODE);
        }
    }


    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setStorey(etStorey.getText().toString());
        input.setGroupSpanMember(etSpan.getText().toString());

        String length = etLengthCc.getText().toString();
        if (length.isEmpty()) {
            input.setLengthCC(0);
        } else {
            input.setLengthCC(Double.parseDouble(length));
        }

        String offset = etOffset1.getText().toString();
        if (offset.isEmpty()) {
            input.setOffset1(0);
        } else {
            input.setOffset1(Double.parseDouble(offset));
        }

        String offset2 = etOffset2.getText().toString();
        if (offset2.isEmpty()) {
            input.setOffset2(0);
        } else {
            input.setOffset2(Double.parseDouble(offset2));
        }

        String wastage = etWastage.getText().toString();
        if (wastage.isEmpty()) {
            input.setWastage(0);
        } else {
            input.setWastage(Integer.parseInt(wastage));
        }

        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}
