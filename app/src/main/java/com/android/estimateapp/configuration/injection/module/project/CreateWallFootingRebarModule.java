package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting.WallFootingRebarCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.wallfooting.WallFootingWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.create.CreateWallFootingInputPresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.create.CreateWallFootingRebarContract;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateWallFootingRebarModule {

    @Provides
    @PerActivity
    CreateWallFootingRebarContract.UserActionListener provideCreateWallFootingPresenter(CreateWallFootingInputPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    WallFootingWorkCalculator provideWallFootingCalculator(WallFootingRebarCalculatorImpl calculator) {
        return calculator;
    }
}
