package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.CHBMortarClass;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.HorizontalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.TieWireRebarSpacingFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.chb.VerticalSpacing;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.chb.CHBResult;

import java.util.List;

import io.reactivex.Observable;

public interface CloudCHBStore {

    Observable<CHBFactor> createCHBFactor(String projectId, CHBFactor chbFactor);

    Observable<CHBFactor> getCHBFactor(String projectId);

    Observable<CHBInput> createCHBInput(String projectId, String scopeOfWorkId, CHBInput chbInput);

    Observable<CHBInput> updateCHBInput(String projectId, String scopeOfWorkId,String  itemId,CHBInput chbInput);

    Observable<List<CHBInput>> getCHBInputs(String projectId, String scopeOfWorkId);

    Observable<CHBInput> getCHBInput(String projectId, String scopeOfWorkId,String itemId);

    Observable<List<CHBMortarClass>> updateCHBMortarClasses(String projectId, double chb, List<CHBMortarClass> chbMortarClasses);

    Observable<List<VerticalSpacing>> updateCHBVerticalSpacingObservable(String projectId,  List<VerticalSpacing> verticalSpacings);

    Observable<List<HorizontalSpacing>> updateCHBHorizontalSpacingObservable(String projectId, List<HorizontalSpacing> horizontalSpacings);

    Observable<List<TieWireRebarSpacingFactor>> updateCHBTieWireRebarSpacingFactorObservable(String projectId, List<TieWireRebarSpacingFactor> tieWireRebarSpacingFactors);

    Observable<CHBResult> saveCHBInputResult(String projectId, String scopeOfWorkId, String itemId, CHBResult result);
}
