package com.android.estimateapp.cleanarchitecture.presentation.summary.columnrebar;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar.ColumnRebarWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class ColumnRebarSummaryPresenter extends BasePresenter implements ColumnRebarSummaryContract.UserActionListener {

    private ColumnRebarSummaryContract.View view;
    private ProjectRepository projectRepository;
    private ProjectPropertiesRepository projectPropertiesRepository;
    private ColumnRebarWorkCalculator columnRebarWorkCalculator;
    private String projectId;
    private String scopeKey;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<RSBFactor> rsbFactors;
    private List<ColumnRebarProperties> columnSectionProperties;


    @Inject
    public ColumnRebarSummaryPresenter(ProjectRepository projectRepository,
                                       ProjectPropertiesRepository projectPropertiesRepository,
                                       ColumnRebarWorkCalculator columnRebarWorkCalculator,
                                       ThreadExecutorProvider threadExecutorProvider,
                                       PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.projectPropertiesRepository = projectPropertiesRepository;
        this.columnRebarWorkCalculator = columnRebarWorkCalculator;
    }

    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void setView(ColumnRebarSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadFactors();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void calculateSummary() {
        compositeDisposable.add(projectRepository.getColumnRebarInputs(projectId, scopeKey)
                .map(beamRebarInputs -> columnRebarWorkCalculator.generateSummary(generateResults(beamRebarInputs)))
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    view.displaySummary(summary);
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }


    private List<ColumnRebarOutput> generateResults(List<ColumnRebarInput> inputs) {
        List<ColumnRebarOutput> results = new ArrayList<>();

        for (ColumnRebarInput input : inputs) {
            results.add(columnRebarWorkCalculator.calculate(false, input, getProperties(input.getPropertyKey()), rsbFactors));
        }

        return results;
    }

    private ColumnRebarProperties getProperties(String key) {
        if (key != null) {
            for (ColumnRebarProperties properties : columnSectionProperties) {
                if (key.equalsIgnoreCase(properties.getKey())) {
                    return properties;
                }
            }
        }
        return null;
    }


    public void loadFactors() {
        Observable.zip(projectPropertiesRepository.getColumnProperties(projectId).observeOn(postExecutionThread.getScheduler()),
                projectRepository.getProjectRSBFactors(projectId).observeOn(postExecutionThread.getScheduler()), (properties, rsbFactors) -> {
                    return new Pair(properties, rsbFactors);
                }).subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {
                    this.columnSectionProperties = (List<ColumnRebarProperties>) pair.first;
                    this.rsbFactors = (List<RSBFactor>) pair.second;

                    calculateSummary();

                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}
