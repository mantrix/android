package com.android.estimateapp.cleanarchitecture.data.entity;


import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;

@IgnoreExtraProperties
public abstract class Model {

    public static String IRRELEVANT_VALUE = "";

    @IntDef({InstanceType.NULL, InstanceType.DUMMY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface InstanceType {
        int NULL = 0;
        int DUMMY = 1;
    }

    private boolean isNull;

    String key;

    public Model(){}

    public Model(@InstanceType int instanceType) {
        this.isNull = (instanceType == InstanceType.NULL);
    }

    @Exclude
    public boolean isEmpty() {
        return isNull;
    }

    @Exclude
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Exclude
    public boolean isNull() {
        return isNull;
    }
}