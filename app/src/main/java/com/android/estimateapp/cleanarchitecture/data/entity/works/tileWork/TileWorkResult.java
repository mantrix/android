package com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork;

public class TileWorkResult {

    double area;

    TileType tileType;

    double tiles;

    double sand;

    double cement;

    double adhesive;

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }


    public TileType getTileType() {
        return tileType;
    }

    public void setTileType(TileType tileType) {
        this.tileType = tileType;
    }

    public double getTiles() {
        return tiles;
    }

    public void setTiles(double tiles) {
        this.tiles = tiles;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getAdhesive() {
        return adhesive;
    }

    public void setAdhesive(double adhesive) {
        this.adhesive = adhesive;
    }
}
