package com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ConcreteWorkListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<RectangularConcreteInput>> {

    private Context context;
    private List<RectangularConcreteInput> list;

    ListItemClickLister<RectangularConcreteInput> listItemClickLister;

    @Inject
    public ConcreteWorkListAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    public void setListItemClickLister(ListItemClickLister<RectangularConcreteInput> listItemClickLister) {
        this.listItemClickLister = listItemClickLister;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.concrete_work_input_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public void setItems(List<RectangularConcreteInput> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<RectangularConcreteInput> list) {

    }

    public void deleteItem(String id) {
        Integer position = null;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getKey().equalsIgnoreCase(id)) {
                position = i;
                list.remove(i);
                break;
            }
        }
        if (position != null) {
            notifyItemRemoved(position);
        }
    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }

    protected class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv_item)
        CardView cvItem;

        @BindView(R.id.tv_category)
        TextView tvCategory;

        @BindView(R.id.tv_description)
        TextView tvDescription;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(RectangularConcreteInput input) {

            tvCategory.setText(String.format(context.getString(R.string.pouring_batch_num_list_label), Integer.toString(input.getPouringBatchNum())));
            tvDescription.setText(String.format(context.getString(R.string.member_num_list_label), Integer.toString(input.getMemberNum())));

            cvItem.setOnClickListener(v -> {
                listItemClickLister.itemClick(input);
            });

            cvItem.setOnLongClickListener(v -> {
                listItemClickLister.onLongClick(input);
                return false;
            });
        }
    }
}