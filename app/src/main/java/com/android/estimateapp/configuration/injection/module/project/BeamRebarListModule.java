package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.list.BeamRebarListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.list.BeamRebarListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class BeamRebarListModule {


    @Provides
    @PerActivity
    BeamRebarListContract.UserActionListener provideBeamRebarPresenter(BeamRebarListPresenter presenter) {
        return presenter;
    }
}
