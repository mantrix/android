package com.android.estimateapp;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Shape;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ConcreteWorkMaterials;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.GeneralConcreteWorkSummary;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class ConcreteWorkComputationTest extends BaseTest {

    ConcreteWorkCalculatorImpl concreteWorkCalculator;

    @Before
    public void setup() {
        concreteWorkCalculator = new ConcreteWorkCalculatorImpl();
    }

    @Test
    public void rectangularComputationTest() {
        ConcreteWorksInput input = new ConcreteWorksInput();

        input.setMortarClass(Grade.A.getId());
        Dimension dimension = new Dimension();
        dimension.setWidth(5);
        dimension.setHeight(5);
        input.setDimension(dimension);
        input.setThickness(2);
        input.setSets(2);
        input.setWastage(5);

        ConcreteFactor factor = defaultFactorFactory.getConcreteFactor(input.getConcreteClassEnum());

        input.setGravelSizeType(GravelSize.THREE_FOURTHS.toString());

        GeneralConcreteWorksResult result = concreteWorkCalculator.calculateByShape(factor, input, Shape.RECTANGLE);

        System.out.println("---------- Rectangular -----------");
        System.out.println("Volume: " + result.getVolume());
        System.out.println("Cement: " + result.getCement());
        System.out.println("FineAggregates: " + result.getSand());
        System.out.println("3/4: " + result.getThreeFourthGravel());
        System.out.println("G1: " + result.getG1Gravel());

//        assertEquals(100, result.getVolume(), DELTA);
//        assertEquals(11.55, result.getCement(), DELTA);
//        assertEquals(0.44, result.getSand(), DELTA);
//
//        if (input.getGravelSizeType() == GravelSize.THREE_FOURTHS) {
//            assertEquals(0.89, result.getThreeFourthGravel(), DELTA);
//        } else {
//            assertEquals(1.05, result.getG1Gravel(), DELTA);
//        }

    }


    @Test
    public void circularComputationTest() {
        ConcreteWorksInput input = new ConcreteWorksInput();

        input.setMortarClass(Grade.AA.getId());
        input.setDiameter(1);
        Dimension dimension = new Dimension();
        dimension.setLength(1);
        input.setDimension(dimension);
        input.setSets(1);
        input.setWastage(5);

        input.setGravelSizeType(GravelSize.THREE_FOURTHS.toString());

        ConcreteFactor factor = defaultFactorFactory.getConcreteFactor(input.getConcreteClassEnum());
        GeneralConcreteWorksResult result = concreteWorkCalculator.calculateByShape(factor, input, Shape.CIRCLE);

        System.out.println("---------- Circular -----------");
        System.out.println("Volume: " + result.getVolume());
        System.out.println("Cement: " + result.getCement());
        System.out.println("FineAggregates: " + result.getSand());
        System.out.println("3/4: " + result.getThreeFourthGravel());
        System.out.println("G1: " + result.getG1Gravel());

        assertEquals(0.79, result.getVolume(), DELTA);
        assertEquals(9.07, result.getCement(), DELTA);
        assertEquals(0.35, result.getSand(), DELTA);

        if (input.getGravelSizeTypeEnum() == GravelSize.THREE_FOURTHS) {
            assertEquals(0.70, result.getThreeFourthGravel(), DELTA);
        } else {
            assertEquals(0.82, result.getG1Gravel(), DELTA);
        }
    }

    @Test
    public void trapezoidalComputationTest() {
        ConcreteWorksInput input = new ConcreteWorksInput();

        input.setMortarClass(Grade.AA.getId());
        Dimension dimension = new Dimension();
        dimension.setWidth(0.5);
        dimension.setLx2(1);
        dimension.setHeight(1);
        input.setDimension(dimension);
        input.setThickness(1);
        input.setSets(1);
        input.setWastage(5);

        input.setGravelSizeType(GravelSize.G1.toString());

        ConcreteFactor factor = defaultFactorFactory.getConcreteFactor(input.getConcreteClassEnum());
        GeneralConcreteWorksResult result = concreteWorkCalculator.calculateByShape(factor, input, Shape.TRAPEZOID);

        System.out.println("---------- Trapezoidal -----------");
        System.out.println("Volume: " + result.getVolume());
        System.out.println("Cement: " + result.getCement());
        System.out.println("FineAggregates: " + result.getSand());
        System.out.println("3/4: " + result.getThreeFourthGravel());
        System.out.println("G1: " + result.getG1Gravel());

        assertEquals(0.75, result.getVolume(), DELTA);
        assertEquals(8.66, result.getCement(), DELTA);
        assertEquals(0.33, result.getSand(), DELTA);

        if (input.getGravelSizeTypeEnum() == GravelSize.THREE_FOURTHS) {
            assertEquals(0.67, result.getThreeFourthGravel(), DELTA);
        } else {
            assertEquals(0.79, result.getG1Gravel(), DELTA);
        }
    }

    @Test
    public void generateSummaryTest() {

        GeneralConcreteWorksResult wallFooting1 = new GeneralConcreteWorksResult();
        wallFooting1.setCategory(Category.WALL_FOOTING);
        wallFooting1.setVolume(100);
        wallFooting1.setCement(824.25);
        wallFooting1.setSand(44.10);
        wallFooting1.setThreeFourthGravel(0);
        wallFooting1.setG1Gravel(105);

        GeneralConcreteWorksResult wallFooting2 = new GeneralConcreteWorksResult();
        wallFooting2.setCategory(Category.WALL_FOOTING);
        wallFooting2.setVolume(3.79);
        wallFooting2.setCement(32.76);
        wallFooting2.setSand(1.75);
        wallFooting2.setThreeFourthGravel(3.51);
        wallFooting2.setG1Gravel(0);

        GeneralConcreteWorksResult wallFooting3 = new GeneralConcreteWorksResult();
        wallFooting3.setCategory(Category.WALL_FOOTING);
        wallFooting3.setVolume(0.64);
        wallFooting3.setCement(4.57);
        wallFooting3.setSand(0.31);
        wallFooting3.setThreeFourthGravel(0.61);
        wallFooting3.setG1Gravel(0);

        GeneralConcreteWorksResult column1 = new GeneralConcreteWorksResult();
        column1.setCategory(Category.COLUMN);
        column1.setVolume(4.70);
        column1.setCement(38.77);
        column1.setSand(2.07);
        column1.setThreeFourthGravel(4.15);
        column1.setG1Gravel(0);

        GeneralConcreteWorksResult column2 = new GeneralConcreteWorksResult();
        column2.setCategory(Category.COLUMN);
        column2.setVolume(5.21);
        column2.setCement(42.93);
        column2.setSand(2.30);
        column2.setThreeFourthGravel(4.59);
        column2.setG1Gravel(0);

        GeneralConcreteWorkSummary summary = concreteWorkCalculator.generateGeneralConcreteSummary(Arrays.asList(wallFooting1, wallFooting2, wallFooting3,column1,column2));

//        for (Category category : summary.getMaterialPerCategory().keySet()){
//            ConcreteWorkMaterials data = summary.getMaterialPerCategory().get(category);
//
//            System.out.println("---------------------------------------------------------");
//
//            System.out.println("Category: " + category.toString());
//            System.out.println("Quantity: " + data.quantity);
//            System.out.println("Cement: " + data.cement);
//            System.out.println("Sand: " + data.sand);
//            System.out.println("3/4: " + data.threeFourthGravel);
//            System.out.println("G1: " + data.g1Gravel);
//        }

        System.out.println("---------------------------------------------------------");

        ConcreteWorkMaterials data = summary.getTotalMaterials();
        System.out.println("Total Cement: " + data.cement);
        System.out.println("Total Sand: " + data.sand);
        System.out.println("Total 3/4: " + data.threeFourthGravel);
        System.out.println("Total G1: " + data.g1Gravel);
    }
}
