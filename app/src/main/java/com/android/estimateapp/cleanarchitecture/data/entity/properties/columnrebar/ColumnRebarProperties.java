package com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class ColumnRebarProperties extends Model {

    double base;

    double height;

    String description;

    Rebar rebar1;

    Rebar rebar2;

    RebarBend rebarBendAtFooting;

    RebarBend rebarBendAtColumnEnd;

    Stirrups stirrups;

    QtyDiameterLength splicing;

    QtyDiameterLength splicing2;

    public ColumnRebarProperties(){

    }



    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Rebar getRebar1() {
        return rebar1;
    }

    public void setRebar1(Rebar rebar1) {
        this.rebar1 = rebar1;
    }

    public Rebar getRebar2() {
        return rebar2;
    }

    public void setRebar2(Rebar rebar2) {
        this.rebar2 = rebar2;
    }

    public Stirrups getStirrups() {
        return stirrups;
    }

    public void setStirrups(Stirrups stirrups) {
        this.stirrups = stirrups;
    }

    public double getArea(){
        return base * height;
    }

    public RebarBend getRebarBendAtFooting() {
        return rebarBendAtFooting;
    }

    public void setRebarBendAtFooting(RebarBend rebarBendAtFooting) {
        this.rebarBendAtFooting = rebarBendAtFooting;
    }

    public RebarBend getRebarBendAtColumnEnd() {
        return rebarBendAtColumnEnd;
    }

    public void setRebarBendAtColumnEnd(RebarBend rebarBendAtColumnEnd) {
        this.rebarBendAtColumnEnd = rebarBendAtColumnEnd;
    }

    public QtyDiameterLength getSplicing() {
        return splicing;
    }

    public void setSplicing(QtyDiameterLength splicing) {
        this.splicing = splicing;
    }

    public QtyDiameterLength getSplicing2() {
        return splicing2;
    }

    public void setSplicing2(QtyDiameterLength splicing2) {
        this.splicing2 = splicing2;
    }
}
