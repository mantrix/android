package com.android.estimateapp.cleanarchitecture.data.entity.contract;

public interface Subscription extends NullChecker {

    boolean isPaid();

    boolean isTrial();

    boolean isSubscribed();

    String getPlan();

    String getTrialEndAt();

    long getSubscriptionStartTime();

    long getSubscriptionEndTime();
}
