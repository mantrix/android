package com.android.estimateapp.cleanarchitecture.presentation.application;

import android.content.Context;
import android.util.Log;


import com.android.estimateapp.configuration.injection.DaggerAppComponent;

import java.io.InterruptedIOException;
import java.net.SocketException;

import javax.inject.Inject;

import androidx.multidex.MultiDex;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import io.reactivex.plugins.RxJavaPlugins;

public class ArCiApplication extends DaggerApplication implements ApplicationContract.View{

    private static final String TAG = ArCiApplication.class.getSimpleName();

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent
                .builder()
                .application(this)
                .context(this)
                .build();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Inject
    ApplicationContract.UserActionListener presenter;

    @Override
    public void onCreate() {
        super.onCreate();

        /*
         * Suppress extra exceptions that may happen after disposing/unSubscribing Observable/Flowable.
         *
         * onError(), can't catch the extra errors after the source has been cancelled/disposed.
         */

        RxJavaPlugins.setErrorHandler(throwable -> {
            if (throwable instanceof InterruptedException) {
                Log.w(TAG, "Thread interrupted!", throwable);
            } else if (throwable instanceof InterruptedIOException) {
                Log.w(TAG, "IO interrupted!", throwable);
            } else if (throwable instanceof SocketException) {
                Log.w(TAG, "Socket Error!", throwable);
            }
        });

        presenter.setView(this);
        presenter.start();
    }

}
