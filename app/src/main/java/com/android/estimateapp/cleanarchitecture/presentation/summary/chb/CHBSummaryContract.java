package com.android.estimateapp.cleanarchitecture.presentation.summary.chb;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBLayingSummary;

public interface CHBSummaryContract {

    interface View extends SummaryView<CHBLayingSummary> {

    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
