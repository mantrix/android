package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;

public class ColumnRebarMaterial {

    public RsbSize rsbSize;

    public double rebar1;

    public double rebar2;

    public double bendAtFootings;

    public double bendAtColumnEnds;

    public double splicings;



    public double getRebarTotal() {
        return rebar1
                + rebar2
                + bendAtFootings
                + bendAtColumnEnds
                + splicings;
    }

    public void setRsbSize(RsbSize rsbSize) {
        this.rsbSize = rsbSize;
    }
}
