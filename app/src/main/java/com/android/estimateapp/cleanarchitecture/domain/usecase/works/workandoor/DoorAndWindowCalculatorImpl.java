package com.android.estimateapp.cleanarchitecture.domain.usecase.works.workandoor;

import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowInputBase;
import com.android.estimateapp.cleanarchitecture.data.entity.works.DoorWindowOutputBase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;

import javax.inject.Inject;

public class DoorAndWindowCalculatorImpl extends BaseCalculator implements DoorAndWindowCalculator {

    @Inject
    public DoorAndWindowCalculatorImpl(){

    }

    @Override
    public DoorWindowOutputBase calculate(int sets, DoorWindowInputBase input){

        double area = input.getLength() * input.getWidth();
        double perimeter = (input.getLength() * 2) + (input.getWidth() * 2);

        DoorWindowOutputBase output = new DoorWindowOutputBase();

       output.setArea(roundHalfUp(area));
       output.setPerimeter(roundHalfUp(perimeter));

       output.setKey(input.getKey());

       output.setSets(sets);

       return output;
    }
}
