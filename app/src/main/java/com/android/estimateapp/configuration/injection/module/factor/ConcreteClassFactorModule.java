package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.concrete.ConcreteClassFactorContract;
import com.android.estimateapp.cleanarchitecture.presentation.factor.concrete.ConcreteFactorPresenter;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class ConcreteClassFactorModule {

    @Provides
    @PerFragment
    ConcreteClassFactorContract.UserActionListener providesConcreteClassFactorPresenter(ConcreteFactorPresenter presenter) {
        return presenter;
    }
}
