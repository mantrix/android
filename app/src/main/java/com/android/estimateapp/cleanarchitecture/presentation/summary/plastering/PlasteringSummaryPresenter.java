package com.android.estimateapp.cleanarchitecture.presentation.summary.plastering;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkResult;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.plastering.PlasteringWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class PlasteringSummaryPresenter  extends BasePresenter implements PlasteringSummaryContract.UserActionListener {

    private PlasteringSummaryContract.View view;
    private ProjectRepository projectRepository;
    private PlasteringWorkCalculator plasteringWorkCalculator;
    private String projectId;
    private String scopeOfWorkId;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<PlasteringMortarFactor> plasteringMortarFactors;

    @Inject
    public PlasteringSummaryPresenter(ProjectRepository projectRepository,
                                    PlasteringWorkCalculator plasteringWorkCalculator,
                                    ThreadExecutorProvider threadExecutorProvider,
                                    PostExecutionThread postExecutionThread) {
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.plasteringWorkCalculator = plasteringWorkCalculator;
    }

    @Override
    public void setData(String projectId, String scopeOfWorkId) {
        this.projectId = projectId;
        this.scopeOfWorkId = scopeOfWorkId;
    }

    @Override
    public void setView(PlasteringSummaryContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadFactors();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void calculateSummary() {
        compositeDisposable.add(projectRepository.getPlasteringInputs(projectId, scopeOfWorkId)
                .map(inputs -> {
                    return plasteringWorkCalculator.generateSummary(generateResults(inputs));
                })
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(summary -> {
                    view.displaySummary(summary);
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    private List<PlasteringResult> generateResults(List<PlasteringInput> inputs){
        List<PlasteringResult> results = new ArrayList<>();

        for (PlasteringInput input : inputs){
            PlasteringMortarFactor factor = getPlasteringFactor(input.getMortarClassEnum());
            results.add(plasteringWorkCalculator.calculate(factor,input));
        }

        return results;
    }


    public void loadFactors(){
        projectRepository.getPlasteringMortarFactors(projectId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(plasteringMortarFactors -> {
                    this.plasteringMortarFactors = plasteringMortarFactors;

                    calculateSummary();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    private PlasteringMortarFactor getPlasteringFactor(Grade grade){

        if (grade != null){
            for (PlasteringMortarFactor plasteringMortarFactor : plasteringMortarFactors){
                if (plasteringMortarFactor.getMortarClass() == grade.getId()){
                    return plasteringMortarFactor;
                }
            }
        }
        return null;
    }

}