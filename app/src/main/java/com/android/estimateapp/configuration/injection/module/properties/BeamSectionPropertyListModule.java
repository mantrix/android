package com.android.estimateapp.configuration.injection.module.properties;

import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.list.BeamSectionPropertiesListContract;
import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.list.BeamSectionPropertiesListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;


@Module
public class BeamSectionPropertyListModule {

    @Provides
    @PerActivity
    BeamSectionPropertiesListContract.UserActionListener provideBeamSectionPresenter(BeamSectionPropertiesListPresenter presenter) {
        return presenter;
    }
}
