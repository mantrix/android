package com.android.estimateapp.cleanarchitecture.presentation.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.home.HomeActivity;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.contract.WelcomeContract;
import com.android.estimateapp.configuration.Constants;
import com.android.estimateapp.utils.DateUtils;
import com.mantrixengineering.estimateapp.R;

import java.util.Date;

import javax.inject.Inject;

public class WelcomeActivity extends BaseActivity implements WelcomeContract.View {

    private static final String TAG = WelcomeActivity.class.getSimpleName();

    @Inject
    WelcomeContract.UserActionListener presenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.welcome_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolbarTitle(R.string.login);

        presenter.setView(this);
        presenter.start();

        Log.d("Date", DateUtils.formatDate(new Date(System.currentTimeMillis()), DateUtils.DATE_PATTERN_1));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    protected void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @Override
    public void showLoginScreen() {
        showFragment(R.id.rl_fragment_container, new LoginFragment(), false);
    }

    @Override
    public void navigateToHomeScreen() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d(TAG, "Request Code: " + requestCode + " ResultCode: " + resultCode);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_CODE_HOME_BACK_BUTTON:
                    finish();
                    break;
                case Constants.RESULT_LOGOUT:
                    break;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
