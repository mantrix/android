package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;

public class RebarMaterialSummary extends Model {

    private double tenRsb;
    private double twelveRsb;
    private double sixteenRsb;
    private double twentyRsb;
    private double twentyFiveRsb;
    private double giTieWire;

    public RebarMaterialSummary(){

    }

    public RebarMaterialSummary(double tenRsb, double twelveRsb, double giTieWire) {
        this.tenRsb = tenRsb;
        this.twelveRsb = twelveRsb;
        this.giTieWire = giTieWire;
    }

    public double getTenRsb() {
        return tenRsb;
    }

    public void setTenRsb(double tenRsb) {
        this.tenRsb = tenRsb;
    }

    public double getTwelveRsb() {
        return twelveRsb;
    }


    public void setGiTieWire(double giTieWire) {
        this.giTieWire = giTieWire;
    }


    public double getSixteenRsb() {
        return sixteenRsb;
    }

    public void setSixteenRsb(double sixteenRsb) {
        this.sixteenRsb = sixteenRsb;
    }

    public double getTwentyRsb() {
        return twentyRsb;
    }

    public void setTwentyRsb(double twentyRsb) {
        this.twentyRsb = twentyRsb;
    }

    public double getTwentyFiveRsb() {
        return twentyFiveRsb;
    }

    public void setTwentyFiveRsb(double twentyFiveRsb) {
        this.twentyFiveRsb = twentyFiveRsb;
    }

    public void setTwelveRsb(double twelveRsb) {
        this.twelveRsb = twelveRsb;
    }

    public double getGiTieWire() {
        return giTieWire;
    }
}
