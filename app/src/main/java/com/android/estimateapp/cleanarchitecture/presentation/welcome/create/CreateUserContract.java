package com.android.estimateapp.cleanarchitecture.presentation.welcome.create;

import androidx.annotation.Nullable;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

public interface CreateUserContract {

    enum RegistrationMode {
        EMAIL,
        GOOGLE
    }

    interface View {

        void invalidFirstName();

        void invalidLastName();

        void invalidMiddleName();

        void invalidPhoneNumber();

        void invalidEmail();

        void invalidPassword();

        void registered();

        void registrationFailed();

        void startLoading();

        void stopLoading();

        void showEmailMode();
    }

    interface UserActionListener extends Presenter<View> {

        void setMode(RegistrationMode mode);

        void create(String uid,
                    String email,
                    String mobileNum,
                    String firstName,
                    String middleName,
                    String lastName,
                    @Nullable String password);
    }
}
