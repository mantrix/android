package com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Shape;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.GeneralConcreteWorkSummary;

import java.util.List;


public interface ConcreteWorkCalculator {

    GeneralConcreteWorksResult calculateByShape(ConcreteFactor concreteFactor, ConcreteWorksInput input, Shape shape);

    RectangularGeneralConcreteResult calculateRectangularConcrete(ConcreteFactor concreteFactor, RectangularConcreteInput input);

    GeneralConcreteWorksResult calculateGeneralConcrete(ConcreteFactor concreteFactor, ConcreteWorksInput input);

    GeneralConcreteWorkSummary generateGeneralConcreteSummary(List<GeneralConcreteWorksResult> results);

    GeneralConcreteWorkSummary generateConcreteSummary(List<RectangularGeneralConcreteResult> results);
}
