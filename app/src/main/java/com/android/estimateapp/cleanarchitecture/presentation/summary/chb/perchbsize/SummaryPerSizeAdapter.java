package com.android.estimateapp.cleanarchitecture.presentation.summary.chb.perchbsize;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.RsbSize;
import com.android.estimateapp.cleanarchitecture.presentation.base.ItemAdapter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerSizeData;
import com.android.estimateapp.utils.DisplayUtil;
import com.mantrixengineering.estimateapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryPerSizeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemAdapter<List<CHBSummaryPerSizeData>> {


    private List<CHBSummaryPerSizeData> list;
    private Context context;

    @Inject
    public SummaryPerSizeAdapter(Context context) {
        list = new ArrayList<>();
        this.context = context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LIST_ITEM:
                View item = inflater.inflate(R.layout.chb_summary_per_size_item, parent, false);
                item.setClickable(true);
                holder = new ItemHolder(item);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_ITEM:
                ((ItemHolder) holder).bind(list.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) != null) {
            return LIST_ITEM;
        }
        return LIST_FOOTER;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void setItems(List<CHBSummaryPerSizeData> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<CHBSummaryPerSizeData> list) {

    }

    @Override
    public void showFooter() {

    }

    @Override
    public void removeFooter() {

    }


    protected class ItemHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_chb_label)
        TextView tvCHBLabel;
        @BindView(R.id.tv_chb)
        TextView tvCHB;
        @BindView(R.id.tv_cement)
        TextView tvCement;
        @BindView(R.id.tv_sand)
        TextView tvSand;

        @BindView(R.id.tv_ten_rsb_label)
        TextView tvTenRsbLabel;
        @BindView(R.id.tv_ten_rsb_result)
        TextView tvTenRsbResult;
        @BindView(R.id.tv_twelve_rsb_label)
        TextView tvTwelveRsbLabel;
        @BindView(R.id.tv_twelve_rsb_result)
        TextView tvTwelveRsbResult;
        @BindView(R.id.tv_tie_wire)
        TextView tvTieWire;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(CHBSummaryPerSizeData materials, int position) {

            tvCHBLabel.setText(String.format(context.getString(R.string.chb_label), Integer.toString(materials.chbSize.getValue())));

            tvCHB.setText(String.format(context.getString(R.string.pcs_result), DisplayUtil.toPlainString(materials.chbCount)));

            tvCement.setText(String.format(context.getString(R.string.bag_result), DisplayUtil.toPlainString(materials.cement)));
            tvSand.setText(String.format(context.getString(R.string.cum_result), DisplayUtil.toPlainString(materials.sand)));
            tvTieWire.setText(String.format(context.getString(R.string.kg_result), DisplayUtil.toPlainString(materials.tieWire)));

            tvTenRsbLabel.setText(String.format(context.getString(R.string.rsb_label), Integer.toString(RsbSize.TEN_MM.getValue())));
            tvTenRsbResult.setText(String.format(context.getString(R.string.kg_result), DisplayUtil.toPlainString(materials.tenRsb)));

            tvTwelveRsbLabel.setText(String.format(context.getString(R.string.rsb_label), Integer.toString(RsbSize.TWELVE_MM.getValue())));
            tvTwelveRsbResult.setText(String.format(context.getString(R.string.kg_result), DisplayUtil.toPlainString(materials.twelveRsb)));
        }
    }
}