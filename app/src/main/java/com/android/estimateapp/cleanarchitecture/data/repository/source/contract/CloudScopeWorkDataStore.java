package com.android.estimateapp.cleanarchitecture.data.repository.source.contract;

import com.android.estimateapp.cleanarchitecture.data.entity.works.Work;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface CloudScopeWorkDataStore {

    Observable<List<Work>> getProjectScopes(String projectId);

    Observable<Work> createProjectScope(String projectId, Work work);

    Completable deleteScopeInput(String projectId, String scopeOfWorkId, String inputId);
}
