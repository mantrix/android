package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.google.firebase.database.Exclude;

public class ConcreteWorkMaterials extends Model {

    @Exclude
    public Category category;
    @Exclude
    public MemberSectionProperty memberSectionProperty;

    @Exclude
    public double quantity;

    public double cement;
    public double sand;
    public double threeFourthGravel;
    public double g1Gravel;

    public ConcreteWorkMaterials(){

    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    public double getThreeFourthGravel() {
        return threeFourthGravel;
    }

    public void setThreeFourthGravel(double threeFourthGravel) {
        this.threeFourthGravel = threeFourthGravel;
    }

    public double getG1Gravel() {
        return g1Gravel;
    }

    public void setG1Gravel(double g1Gravel) {
        this.g1Gravel = g1Gravel;
    }
}

