package com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.create;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneCladdingResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBaseActivity;
import com.android.estimateapp.configuration.Constants;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

import static com.android.estimateapp.configuration.Constants.RESULT_CONTENT_UPDATE;

public class CreateStoneCladdingActivity extends CreateScopeInputBaseActivity implements CreateStoneCladdingContract.View {


    @Inject
    CreateStoneCladdingContract.UserActionListener presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.et_wall)
    TextInputEditText etWall;
    @BindView(R.id.til_wall)
    TextInputLayout tilWall;
    @BindView(R.id.et_sets)
    TextInputEditText etSets;
    @BindView(R.id.til_sets)
    TextInputLayout tilSets;
    @BindView(R.id.et_length)
    TextInputEditText etLength;
    @BindView(R.id.til_length)
    TextInputLayout tilLength;
    @BindView(R.id.et_width)
    TextInputEditText etWidth;
    @BindView(R.id.til_width)
    TextInputLayout tilWidth;
    @BindView(R.id.et_opening)
    TextInputEditText etOpening;
    @BindView(R.id.til_opening)
    TextInputLayout tilOpening;
    @BindView(R.id.et_cladding)
    TextInputEditText etCladding;
    @BindView(R.id.til_cladding)
    TextInputLayout tilCladding;
    @BindView(R.id.tv_cladding_size)
    TextView tvCladdingSize;
    @BindView(R.id.et_w1)
    TextInputEditText etW1;
    @BindView(R.id.et_w2)
    TextInputEditText etW2;
    @BindView(R.id.et_wastage)
    TextInputEditText etWastage;
    @BindView(R.id.til_wastage)
    TextInputLayout tilWastage;
    @BindView(R.id.et_mortar_class)
    TextInputEditText etMortarClass;
    @BindView(R.id.til_mortar_class)
    TextInputLayout tilMortarClass;
    @BindView(R.id.cv_inputs)
    CardView cvInputs;
    @BindView(R.id.btn_calculate)
    Button btnCalculate;
    @BindView(R.id.tv_area)
    TextView tvArea;
    @BindView(R.id.tv_cladding_count)
    TextView tvCladdingCount;
    @BindView(R.id.chb_result_line)
    View chbResultLine;
    @BindView(R.id.tv_sand)
    TextView tvSand;
    @BindView(R.id.tv_cement)
    TextView tvCement;
    @BindView(R.id.tv_adhesive)
    TextView tvAdhesive;
    @BindView(R.id.cv_results)
    CardView cvResults;

    private String projectId;
    private String scopeKey;
    private String itemKey;

    private StoneCladdingInput input;
    private Dimension dimension;
    private StoneType stoneType;

    private ProgressDialog saveProgressDialog;

    private boolean updateContent;

    AlertDialog mortarClassPickerDialog;
    AlertDialog stoneTypePickerDialog;

    public static Intent createIntent(Context context, String projectId, String scopeKey, @Nullable String itemKey) {
        Intent intent = new Intent(context, CreateStoneCladdingActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        intent.putExtra(Constants.ITEM_KEY, itemKey);
        return intent;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.create_stone_cladding_input_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.STONE_CLADDING.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

        input = new StoneCladdingInput();
        dimension = new Dimension();

        presenter.setView(this);
        presenter.setData(projectId, scopeKey, itemKey);
        presenter.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_work_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_content_save:
                if (validateInputs()) {
                    calculateClick();
                    presenter.save();
                }
                break;
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.STONE_CLADDING.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        itemKey = intent.getStringExtra(Constants.ITEM_KEY);
    }

    @Override
    public void setMortarClassPickerOptions(List<Grade> classes) {
        String[] options = new String[classes.size()];
        for (int i = 0; i < classes.size(); i++) {
            options[i] = classes.get(i).toString();
        }

        mortarClassPickerDialog = createChooser(getString(R.string.concrete_mortar_class), options, (dialog, which) -> {
            etMortarClass.setText(options[which]);
            input.setMortarClass(classes.get(which).getId());
            dialog.dismiss();
        });
    }


    @OnClick(R.id.et_mortar_class)
    protected void mortarClassClick() {
        if (mortarClassPickerDialog != null) {
            mortarClassPickerDialog.show();
        }
    }

    @Override
    public void setStoneTypes(List<StoneType> stoneTypes) {
        String[] options = new String[stoneTypes.size()];
        for (int i = 0; i < stoneTypes.size(); i++) {
            options[i] = stoneTypes.get(i).getName();
        }

        stoneTypePickerDialog = createChooser(getString(R.string.tile_type), options, (dialog, which) -> {

            if (stoneTypes.get(which) != null) {
                stoneType = stoneTypes.get(which);
                etCladding.setText(stoneType.getName());
                etW1.setText(toDisplayFormat(stoneType.getLength()));
                etW2.setText(toDisplayFormat(stoneType.getWidth()));
                input.setStoneTypeId(stoneType.getKey());
            }

            dialog.dismiss();
        });
    }

    @OnClick(R.id.et_cladding)
    protected void claddingClick() {
        if (stoneTypePickerDialog != null) {
            stoneTypePickerDialog.show();
        } else {
            // Launch Factors, preselect Stone Type Factor tab
            Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.STONE_CLADDING.getId(),1);
            startActivityForResult(intent, Constants.REQUEST_CODE);
        }
    }


    @Override
    public void setInput(StoneCladdingInput input) {
        this.input = input;
        if (input.getWall() != null) {
            etWall.setText(input.getWall());
        }

        stoneType = input.getStoneType();
        if (stoneType != null) {
            etCladding.setText(stoneType.getName());
            etW1.setText(toDisplayFormat(stoneType.getLength()));
            etW2.setText(toDisplayFormat(stoneType.getWidth()));
        }


        if (input.getDimension() != null) {
            dimension = input.getDimension();
            if (dimension.getLength() != 0) {
                etLength.setText(toDisplayFormat(dimension.getLength()));
            }

            if (dimension.getWidth() != 0) {
                etWidth.setText(toDisplayFormat(dimension.getWidth()));
            }
        }


        if (input.getSets() != 0) {
            etSets.setText(toDisplayFormat(input.getSets()));
        }

        if (input.getOpenings() != 0){
            etOpening.setText(toDisplayFormat(input.getOpenings()));
        }

        if (input.getWastage() != 0) {
            etWastage.setText(toDisplayFormat(input.getWastage()));
        }

        if (input.getMortarClass() != 0) {
            etMortarClass.setText(Grade.parseInt(input.getMortarClass()).toString());
        }
    }

    @Override
    public void enabledCalculateBtn(boolean enabled) {
        btnCalculate.setEnabled(enabled);
    }

    @Override
    public void failedRetrievingFactorError() {
        Toast.makeText(this, getString(R.string.failed_retrieving_factors), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayResult(StoneCladdingResult result) {

        tvCladdingCount.setText(String.format(getString(R.string.pcs_result),toDisplayFormat(result.getStoneCladdingPcs())));
        tvArea.setText(String.format(getString(R.string.sqm_result), toDisplayFormat(result.getArea())));
        tvCement.setText(String.format(getString(R.string.bag_result), toDisplayFormat(result.getCement())));
        tvSand.setText(String.format(getString(R.string.cum_result), toDisplayFormat(result.getSand())));
        tvAdhesive.setText(String.format(getString(R.string.bag_result), toDisplayFormat(result.getAdhesive())));

        cvResults.setVisibility(View.VISIBLE);
    }

    @Override
    public void inputSaved() {
        updateContent = true;
        saveProgressDialog.dismiss();
    }

    @Override
    public void showSavingDialog() {
        saveProgressDialog.show();
    }

    @Override
    public void savingError() {
        saveProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.something_went_wrong_error), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_calculate)
    protected void calculateClick() {
        input.setWall(etWall.getText().toString());


        String length = etLength.getText().toString();
        if (length.isEmpty()) {
            dimension.setLength(0);
        } else {
            dimension.setLength(Double.parseDouble(length));
        }

        String width = etWidth.getText().toString();
        if (width.isEmpty()) {
            dimension.setWidth(0);
        } else {
            dimension.setWidth(Double.parseDouble(width));
        }

        input.setDimension(dimension);



        String sets = etSets.getText().toString();
        if (sets.isEmpty()) {
            input.setSets(0);
        } else {
            input.setSets(Integer.parseInt(sets));
        }

        String openings = etOpening.getText().toString();
        if (openings.isEmpty()) {
            input.setOpenings(0);
        } else {
            input.setOpenings(Double.parseDouble(openings));
        }

        String wastage = etWastage.getText().toString();
        if (wastage.isEmpty()) {
            input.setWastage(0);
        } else {
            input.setWastage(Integer.parseInt(wastage));
        }

        presenter.calculate(input);
    }

    @Override
    public void requestForReCalculation() {
        calculateClick();
    }

    private boolean validateInputs() {
        boolean valid = true;

        if (etWall.getText().toString().isEmpty()) {
            tilWall.setError(getString(R.string.required_error));
            valid = false;
        } else {
            tilWall.setError(null);
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        if (updateContent) {
            setResult(RESULT_CONTENT_UPDATE);
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_FACTOR_UPDATE:
                    presenter.loadFactors(true);
                    break;
            }
        }
    }
}

