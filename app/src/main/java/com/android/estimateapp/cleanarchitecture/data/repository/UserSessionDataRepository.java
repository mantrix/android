package com.android.estimateapp.cleanarchitecture.data.repository;

import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.domain.repository.UserSessionRepository;

import javax.inject.Inject;

public class UserSessionDataRepository implements UserSessionRepository {


    private Account activeUser;
    private boolean hasActiveUser;

    @Inject
    public UserSessionDataRepository(){

    }

    @Override
    public void setCurrentActiveUser(Account activeUser) {
        this.activeUser = activeUser;

        setHasActiveUser(activeUser != null && !activeUser.isEmpty());
    }

    @Override
    public Account getCurrentActiveUser() {
        return activeUser;
    }

    @Override
    public boolean hasActiveUser() {
        return hasActiveUser;
    }

    @Override
    public void setHasActiveUser(boolean hasActiveUser) {
        this.hasActiveUser = hasActiveUser;
    }
}
