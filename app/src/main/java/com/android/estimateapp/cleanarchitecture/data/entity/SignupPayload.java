package com.android.estimateapp.cleanarchitecture.data.entity;

import com.google.gson.annotations.SerializedName;

/**
 * {
 *           email: '',
 *           password: '',
 *           name: {
 *             prefix: '',
 *             firstName: '',
 *             middleName: '',
 *             lastName: ''
 *           },
 *           mobileNo: '',
 *           subscription: {
 *             plan: 'free',
 *             paid: false
 *           }
 *        }
 */
public class SignupPayload {

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

    @SerializedName("mobileNo")
    public String mobileNo;

    @SerializedName("subscription")
    public SubscriptionPayload subscriptionPayload;

    @SerializedName("name")
    NamePayload nameEntity;

    public SignupPayload(String email, String password, String mobileNo){
        this.email = email;
        this.password = password;
        this.mobileNo = mobileNo;
    }

    public void setNameEntity(NamePayload nameEntity) {
        this.nameEntity = nameEntity;
    }

    public void setSubscriptionPayload(SubscriptionPayload subscriptionPayload) {
        this.subscriptionPayload = subscriptionPayload;
    }
}
