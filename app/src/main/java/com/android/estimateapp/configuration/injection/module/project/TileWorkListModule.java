package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.list.TileWorkListContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.list.TileWorkListPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class TileWorkListModule {

    @Provides
    @PerActivity
    TileWorkListContract.UserActionListener provideTileWorkListPresenter(TileWorkListPresenter presenter) {
        return presenter;
    }
}
