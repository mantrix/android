package com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.ScopeOfWork;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseActivity;
import com.android.estimateapp.cleanarchitecture.presentation.base.ListItemClickLister;
import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.LaborCostActivity;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.plastering.PlasteringMaterialDialogFragment;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.tiles.TileWorkMaterialDialogFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create.CreatePlasteringActivity;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryActivity;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class PlasteringListActivity extends BaseActivity implements PlasteringListContract.View, ListItemClickLister<PlasteringInput> {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    PlasteringListContract.UserActionListener presenter;

    @Inject
    PlasteringListAdapter adapter;

    String projectId;
    String scopeKey;

    NpaLinearLayoutManager linearLayoutManager;

    public static Intent createIntent(Context context, String projectId, String scopeKey) {
        Intent intent = new Intent(context, PlasteringListActivity.class);
        intent.putExtra(Constants.PROJECT_ID, projectId);
        intent.putExtra(Constants.SCOPE_KEY, scopeKey);
        return intent;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.scope_list_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        extractExtras();
        setToolbarTitle(ScopeOfWork.CHB_PLASTERING.toString());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        initRecyclerView();

        presenter.setView(this);
        presenter.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.work_list_w_cost, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_factor:
                Intent intent = FactorActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.CHB_PLASTERING.getId());
                startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            case R.id.menu_summary:
                startActivityForResult(SummaryActivity.createIntent(this, projectId, scopeKey, ScopeOfWork.CHB_PLASTERING.getId()), Constants.REQUEST_CODE);
                break;
            case R.id.menu_labor:
                startActivityForResult(LaborCostActivity.createIntent(this,projectId,scopeKey), Constants.REQUEST_CODE);
                break;
            case R.id.menu_material:
                PlasteringMaterialDialogFragment dialogFragment = PlasteringMaterialDialogFragment.newInstance(projectId,scopeKey);
                FragmentManager fragmentManager = getSupportFragmentManager();
                dialogFragment.show(fragmentManager, "");
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void extractExtras() {
        Intent intent = getIntent();
        projectId = intent.getStringExtra(Constants.PROJECT_ID);
        scopeKey = intent.getStringExtra(Constants.SCOPE_KEY);
        presenter.setData(projectId,scopeKey);
    }


    @Override
    public void itemClick(PlasteringInput plasteringInput) {
        Intent intent = CreatePlasteringActivity.createIntent(this, projectId, scopeKey, plasteringInput.getKey());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onLongClick(PlasteringInput input) {
        AlertDialog alertDialog = createAlertDialog(getString(R.string.delete),
                getString(R.string.delete_input_msg),
                getString(R.string.delete),
                getString(R.string.cancel), (dialog, which) -> {
                    presenter.deleteItem(input.getKey());
                    dialog.dismiss();
                }, (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    @Override
    public void removeItem(String inputId) {
        adapter.deleteItem(inputId);
    }


    @Override
    public void displayList(List<PlasteringInput> plasteringInputs) {
        adapter.setItems(plasteringInputs);
    }



    private void initRecyclerView() {
        linearLayoutManager = new NpaLinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setListItemClickLister(this);
    }


    @OnClick(R.id.fab)
    public void addItemClick() {
        Intent intent = CreatePlasteringActivity.createIntent(this, projectId, scopeKey, null);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE) {
            switch (resultCode) {
                case Constants.RESULT_CONTENT_UPDATE:
                    presenter.reloadList();
                    break;
            }
        }
    }
}
