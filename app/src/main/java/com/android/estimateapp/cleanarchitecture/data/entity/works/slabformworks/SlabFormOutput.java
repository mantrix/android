package com.android.estimateapp.cleanarchitecture.data.entity.works.slabformworks;

public class SlabFormOutput {

    double area;

    double plywood;

    double twoByTwo;

    double twoByThree;

    double twoByFour;

    double nails;

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPlywood() {
        return plywood;
    }

    public void setPlywood(double plywood) {
        this.plywood = plywood;
    }

    public double getTwoByTwo() {
        return twoByTwo;
    }

    public void setTwoByTwo(double twoByTwo) {
        this.twoByTwo = twoByTwo;
    }

    public double getTwoByThree() {
        return twoByThree;
    }

    public void setTwoByThree(double twoByThree) {
        this.twoByThree = twoByThree;
    }

    public double getTwoByFour() {
        return twoByFour;
    }

    public void setTwoByFour(double twoByFour) {
        this.twoByFour = twoByFour;
    }

    public double getNails() {
        return nails;
    }

    public void setNails(double nails) {
        this.nails = nails;
    }
}
