package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import android.content.Context;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.rectangularconcrete.MemberSectionProperty;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudProjectPropertiesStore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class WSProjectPropertiesStore extends BaseStore implements CloudProjectPropertiesStore {

    private Context context;

    @Inject
    public WSProjectPropertiesStore(Context context) {
        super();
        this.context = context;

    }

    @Override
    public Observable<MemberSectionProperty> createMemberSectionProperty(String projectId, MemberSectionProperty memberSectionProperty) {
        return Observable.fromCallable(() -> {
            return projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_MEMBER_SECTION_FACTOR);
        }).flatMap(databaseReference -> push(databaseReference, memberSectionProperty));
    }


    @Override
    public Observable<List<MemberSectionProperty>> getMemberSectionProperty(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_MEMBER_SECTION_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, MemberSectionProperty.class));
    }

    @Override
    public Observable<List<MemberSectionProperty>> updateMemberSectionProperty(String projectId, List<MemberSectionProperty> memberSectionProperties) {
        DatabaseReference rsbRef = projectPropertiesReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_MEMBER_SECTION_FACTOR);

        return Observable.fromIterable(memberSectionProperties)
                .flatMap(rsbFactor -> setValue(rsbRef.child(rsbFactor.getKey()), rsbFactor))
                .toList().toObservable();
    }

    @Override
    public Observable<BeamSectionProperties> createBeamSectionProperty(String projectId, BeamSectionProperties properties) {
        return Observable.fromCallable(() -> {
            return projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_BEAM_SECTION_FACTOR);
        }).flatMap(databaseReference -> push(databaseReference, properties));
    }


    @Override
    public Observable<List<BeamSectionProperties>> getBeamSectionProperties(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_BEAM_SECTION_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, BeamSectionProperties.class));
    }

    @Override
    public Observable<BeamSectionProperties> updateBeamSectionProperty(String projectId, String itemId ,BeamSectionProperties properties){
        return Observable.fromCallable(() -> {
            return projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_BEAM_SECTION_FACTOR)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference,properties));
    }


    @Override
    public Observable<BeamSectionProperties> getBeamSectionProperty(String projectId, String itemId){
        return Observable.fromCallable(() -> {
            return projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_BEAM_SECTION_FACTOR)
                    .child(itemId);
        }).flatMap(query -> get(query,BeamSectionProperties.class));
    }


    @Override
    public Observable<ColumnRebarProperties> createColumnProperties(String projectId, ColumnRebarProperties properties) {
        return Observable.fromCallable(() -> {
            return projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_COLUMN_SECTION_FACTOR);
        }).flatMap(databaseReference -> push(databaseReference, properties));
    }


    @Override
    public Observable<List<ColumnRebarProperties>> getColumnProperties(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_COLUMN_SECTION_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, ColumnRebarProperties.class));
    }

    @Override
    public Observable<ColumnRebarProperties> updateColumnProperties(String projectId, String itemId ,ColumnRebarProperties properties){
        return Observable.fromCallable(() -> {
            return projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_COLUMN_SECTION_FACTOR)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference,properties));
    }


    @Override
    public Observable<ColumnRebarProperties> getColumnProperties(String projectId, String itemId){
        return Observable.fromCallable(() -> {
            return projectPropertiesReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_COLUMN_SECTION_FACTOR)
                    .child(itemId);
        }).flatMap(query -> get(query,ColumnRebarProperties.class));
    }
}



