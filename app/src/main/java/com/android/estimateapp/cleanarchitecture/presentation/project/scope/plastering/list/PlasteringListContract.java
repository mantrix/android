package com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface PlasteringListContract {

    interface View {

        void displayList(List<PlasteringInput> plasteringInputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void reloadList();

        void deleteItem(String inputId);
    }
}
