package com.android.estimateapp.cleanarchitecture.presentation.project.listing;

import com.android.estimateapp.cleanarchitecture.data.entity.project.Project;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface ProjectListContract {

    interface View {

        void displayProjects(List<Project> projects);

        void refreshProjects(List<Project> projects);

        void removeProject(String key);

        void displayUpgradeAlert();

        void showAddProjectScreen();

        void displayUpgradeToAddProject();

        void subscriptionStatus(String dateStr,  boolean today);

        void trialStatus(String dateStr, boolean today);
    }

    interface UserActionListener extends Presenter<View> {

        void reloadProjectList();

        void onListScrolled(int totalItemCount, int lastVisibleItem);

        void deleteItem(String id);

        void addProjectClick();
    }
}
