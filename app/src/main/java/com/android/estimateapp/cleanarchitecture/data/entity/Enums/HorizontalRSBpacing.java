package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

import java.util.ArrayList;
import java.util.List;

public enum HorizontalRSBpacing {

    TWO(2),
    THREE(3),
    FOUR(4);

    int value;

    HorizontalRSBpacing(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static List<HorizontalRSBpacing> getAllHorizontalSpacings() {
        List<HorizontalRSBpacing> list = new ArrayList<>();
        list.add(TWO);
        list.add(THREE);
        list.add(FOUR);
        return list;
    }
}
