package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.create;

import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

import io.reactivex.annotations.Nullable;

public interface CreateColumnRebarInputContract {

    interface View {

        void enabledCalculateBtn(boolean enabled);

        void failedRetrievingFactorError();

        void displayResult(ColumnRebarOutput output);

        void inputSaved();

        void savingError();

        void setInput(ColumnRebarInput input);

        void requestForReCalculation();

        void showSavingDialog();

        void showSavingNotAllowedError();

        void setPropertiesOption(List<ColumnRebarProperties> propertiesOption);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey, @Nullable String itemKey);

        void calculate(ColumnRebarInput input);

        void save();

        void loadFactors(boolean factorUpdate);
    }
}
