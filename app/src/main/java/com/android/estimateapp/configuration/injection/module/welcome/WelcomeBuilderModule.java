package com.android.estimateapp.configuration.injection.module.welcome;

import com.android.estimateapp.cleanarchitecture.presentation.welcome.LoginFragment;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.create.CreateUserDialogFragment;
import com.android.estimateapp.configuration.injection.scope.PerChildFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class WelcomeBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {LoginModule.class})
    abstract LoginFragment bindLoginFragment();


    @PerFragment
    @ContributesAndroidInjector(modules = {CreateUserModule.class})
    abstract CreateUserDialogFragment bindCreateUserDialogFragment();
}
