package com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.list;

import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

import java.util.List;

public interface ConcreteWorkListContract {

    interface View {

        void displayList(List<RectangularConcreteInput> inputs);

        void removeItem(String inputId);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeOfWorkId);

        void deleteItem(String inputId);

        void reloadList();
    }
}
