package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.TileWorkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileType;
import com.android.estimateapp.cleanarchitecture.data.entity.works.tileWork.TileWorkInput;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudTileWorkStore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;


@Singleton
public class WSTileWorkStore extends BaseStore implements CloudTileWorkStore {


    private DatabaseReference projectScopeOfWorksRef;

    @Inject
    public WSTileWorkStore() {
        super();
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
    }


    @Override
    public Observable<TileWorkInput> createTileWorkInput(String projectId, String scopeOfWorkId, TileWorkInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(databaseReference -> push(databaseReference, input));
    }

    @Override
    public Observable<TileWorkInput> updateTileWorkInput(String projectId, String scopeOfWorkId, String itemId, TileWorkInput input) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference, input));
    }


    @Override
    public Observable<List<TileWorkInput>> getTileWorkInputs(String projectId, String scopeOfWorkId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(query -> queryListValue(query, TileWorkInput.class));
    }



    @Override
    public Observable<TileWorkInput> getTileWorkInput(String projectId, String scopeOfWorkId, String itemId) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(query -> get(query, TileWorkInput.class));
    }


    @Override
    public Observable<List<TileWorkFactor>> createTileWorkFactors(String projectId, List<TileWorkFactor> factors) {

        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_TILE_WORK_FACTOR);

        return Observable.fromIterable(factors)
                .flatMap(tileWorkFactor -> push(databaseReference, tileWorkFactor))
                .toList().toObservable();
    }


    @Override
    public Observable<List<TileWorkFactor>> getTileWorkFactors(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_TILE_WORK_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, TileWorkFactor.class));
    }


    @Override
    public Observable<List<TileWorkFactor>> updateTileWorkFactors(String projectId,  List<TileWorkFactor> tileWorkFactors) {
        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_TILE_WORK_FACTOR);

        return Observable.fromIterable(tileWorkFactors)
                .flatMap(tileWorkFactor -> setValue(databaseReference.child(tileWorkFactor.getKey()), tileWorkFactor))
                .toList().toObservable();
    }

    @Override
    public Observable<TileType> createTileTypeFactor(String projectId, TileType tileType) {
        return Observable.fromCallable(() -> {
            return projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_TILE_TYPE_FACTOR);
        }).flatMap(databaseReference -> push(databaseReference, tileType));
    }

    @Override
    public Observable<List<TileType>> getTileTypeFactors(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_TILE_TYPE_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, TileType.class));
    }

    @Override
    public Observable<List<TileType>> updateTileTypeFactors(String projectId, List<TileType> factors) {
        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_TILE_TYPE_FACTOR);

        return Observable.fromIterable(factors)
                .flatMap(factor -> setValue(databaseReference.child(factor.getKey()), factor))
                .toList().toObservable();
    }

}
