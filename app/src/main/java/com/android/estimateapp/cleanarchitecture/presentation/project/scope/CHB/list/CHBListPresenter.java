package com.android.estimateapp.cleanarchitecture.presentation.project.CHB.list;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.presentation.base.BasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.list.CHBListContract;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import javax.inject.Inject;

@PerActivity
public class CHBListPresenter extends BasePresenter implements CHBListContract.UserActionListener {


    private CHBListContract.View view;
    private ProjectRepository projectRepository;
    private String projectId;
    private String scopeOfWorkId;

    @Inject
    public CHBListPresenter(ProjectRepository projectRepository,
                            ThreadExecutorProvider threadExecutorProvider, PostExecutionThread postExecutionThread){
        super(threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
    }


    @Override
    public void setView(CHBListContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeOfWorkId) {
        this.projectId = projectId;
        this.scopeOfWorkId = scopeOfWorkId;
    }

    @Override
    public void start() {
        loadInputs();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void reloadList() {
        loadInputs();
    }

    private void loadInputs(){
        projectRepository.getCHBInputs(projectId,scopeOfWorkId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(chbInputs -> {
                    if (!chbInputs.isEmpty()){
                        view.displayList(chbInputs);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Override
    public void deleteItem(String inputId) {
        projectRepository.deleteScopeInput(projectId, scopeOfWorkId, inputId)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(() -> {
                    view.removeItem(inputId);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }
}
