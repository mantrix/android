package com.android.estimateapp.cleanarchitecture.data.entity.factors;

import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class TileWorkFactor extends Model{


    int mortarClass;

    double sand;

    double cement;

    double adhesive;

    public TileWorkFactor(){

    }

    public TileWorkFactor(int mortarClass, double sand, double cement, double adhesive) {
        this.mortarClass = mortarClass;
        this.sand = sand;
        this.cement = cement;
        this.adhesive = adhesive;
    }

    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    public double getSand() {
        return sand;
    }

    public void setSand(double sand) {
        this.sand = sand;
    }

    public double getCement() {
        return cement;
    }

    public void setCement(double cement) {
        this.cement = cement;
    }

    public double getAdhesive() {
        return adhesive;
    }

    public void setAdhesive(double adhesive) {
        this.adhesive = adhesive;
    }
}
