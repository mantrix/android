package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import java.util.HashMap;
import java.util.Map;

public class TileWorkSummary {

    Map<String, TileWorkMaterials> materialPerTileType = new HashMap<>();

    TileWorkMaterials total;

    public TileWorkSummary(Map<String, TileWorkMaterials> materialPerTileType, TileWorkMaterials total) {
        this.materialPerTileType = materialPerTileType;
        this.total = total;
    }

    public Map<String, TileWorkMaterials> getMaterialPerTileType() {
        return materialPerTileType;
    }

    public TileWorkMaterials getTotal() {
        return total;
    }
}
