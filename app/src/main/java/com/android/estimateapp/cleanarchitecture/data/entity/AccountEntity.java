package com.android.estimateapp.cleanarchitecture.data.entity;

import com.android.estimateapp.cleanarchitecture.data.entity.contract.Account;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Name;
import com.android.estimateapp.cleanarchitecture.data.entity.contract.Subscription;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class AccountEntity extends Model implements Account {


    String uid;

    String email;

    String mobileNo;

    Long createdAt;

    String picURL;

    NameEntity name;

    SubscriptionEntity subscription;

    public AccountEntity(@InstanceType int type) {
        super(type);
    }

    public AccountEntity() {

    }

    @Override
    public String getUid() {
        return uid;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public Long getCreatedAt() {
        return createdAt;
    }

    @Override
    public String mobileNo() {
        return mobileNo;
    }

    @Override
    public Name getName() {
        return name;
    }

    @Override
    public String getPicURL() {
        return picURL;
    }

    @Override
    public Subscription getSubscription() {
        return subscription;
    }
}
