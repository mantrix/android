package com.android.estimateapp.cleanarchitecture.presentation.summary.columnrebar;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryView;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ColumnRebarSummary;

public interface ColumnRebarSummaryContract {


    interface View extends SummaryView<ColumnRebarSummary> {
        void displayTotal(double total);
    }

    interface UserActionListener extends Presenter<View> {

        void setData(String projectId, String scopeKey);

    }
}
