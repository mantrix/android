package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete.ConcreteWorkCalculatorImpl;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.create.CreateGeneralConcreteWorkContract;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.create.CreateGeneralConcreteWorkPresenter;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateGeneralConcreteWorkModule {


    @Provides
    @PerActivity
    CreateGeneralConcreteWorkContract.UserActionListener provideCreateGeneralConcretePresenter(CreateGeneralConcreteWorkPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ConcreteWorkCalculator provideConcreteWorkCalculator(ConcreteWorkCalculatorImpl calculator) {
        return calculator;
    }
}
