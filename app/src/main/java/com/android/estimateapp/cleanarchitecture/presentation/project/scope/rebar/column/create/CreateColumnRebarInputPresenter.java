package com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.create;

import android.util.Pair;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.beamsection.BeamSectionProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.properties.columnrebar.ColumnRebarProperties;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.beamrebar.BeamRebarOutput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnrebar.ColumnRebarOutput;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectPropertiesRepository;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.beamrebar.BeamRebarCalculator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnrebar.ColumnRebarWorkCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.create.CreateBeamRebarInputContract;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class CreateColumnRebarInputPresenter extends CreateScopeInputBasePresenter implements CreateColumnRebarInputContract.UserActionListener {

    private CreateColumnRebarInputContract.View view;

    private String projectId;
    private String scopeKey;
    private String itemKey;
    private ProjectRepository projectRepository;
    private ProjectPropertiesRepository projectPropertiesRepository;
    private ColumnRebarWorkCalculator columnRebarCalculator;

    private List<RSBFactor> rsbFactors;
    private List<ColumnRebarProperties> columnSectionProperties;
    private ColumnRebarInput input;
    private ColumnRebarOutput result;


    @Inject
    public CreateColumnRebarInputPresenter(ProjectRepository projectRepository,
                                         AccountSubscriptionUseCase accountSubscriptionUseCase,
                                           ColumnRebarWorkCalculator columnRebarCalculator,
                                         ProjectPropertiesRepository projectPropertiesRepository,
                                         ThreadExecutorProvider threadExecutorProvider,
                                         PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase, threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;
        this.columnRebarCalculator = columnRebarCalculator;
        this.projectPropertiesRepository = projectPropertiesRepository;

        rsbFactors = new ArrayList<>();
    }

    @Override
    public void setView(CreateColumnRebarInputContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey, @Nullable String itemKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
        this.itemKey = itemKey;
    }

    @Override
    public void start() {
        view.enabledCalculateBtn(false);

        loadFactors(false);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save() {
        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateColumnRebarInput(projectId, scopeKey, itemKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createColumnRebarInput(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<ColumnRebarInput> inputObservable) {
        view.showSavingDialog();
        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(result -> {
                    itemKey = input.getKey();
                    this.input = result;
                    saveResult();
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                    view.savingError();
                });
    }

    private void saveResult(){
        if (result != null && itemKey != null) {
            projectRepository.saveColumnRebarInputResult(projectId, scopeKey, itemKey,result)
                    .subscribeOn(threadExecutorProvider.computationScheduler())
                    .observeOn(postExecutionThread.getScheduler())
                    .subscribe(result -> {
                        this.result = result;
                    }, throwable -> {
                        throwable.printStackTrace();
                    });
        }
    }

    @Override
    public void calculate(ColumnRebarInput input) {
        result = columnRebarCalculator.calculate(false, input, getProperties(input.getPropertyKey()), rsbFactors);
        this.input = input;
        view.displayResult(result);
    }

    private ColumnRebarProperties getProperties(String key) {
        if (key != null) {
            for (ColumnRebarProperties properties : columnSectionProperties) {
                if (key.equalsIgnoreCase(properties.getKey())) {
                    return properties;
                }
            }
        }
        return null;
    }

    private void loadInput() {
        projectRepository.getColumnRebarInput(projectId, scopeKey, itemKey)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    this.input = input;

                    view.setInput(input);

                    // auto-calculate
                    calculate(input);
                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

    @Override
    public void loadFactors(boolean factorUpdate) {
        Observable.zip(projectPropertiesRepository.getColumnProperties(projectId).observeOn(postExecutionThread.getScheduler()),
                projectRepository.getProjectRSBFactors(projectId).observeOn(postExecutionThread.getScheduler()), (properties, rsbFactors) -> {
                    return new Pair(properties, rsbFactors);
                }).subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(pair -> {
                    this.columnSectionProperties = (List<ColumnRebarProperties>) pair.first;
                    this.rsbFactors = (List<RSBFactor>) pair.second;

                    view.setPropertiesOption(columnSectionProperties);

                    if (!factorUpdate) {
                        view.enabledCalculateBtn(true);

                        if (itemKey != null) {
                            loadInput();
                        }
                    } else {
                        view.requestForReCalculation();
                    }

                }, throwable -> {
                    throwable.printStackTrace();
                    view.enabledCalculateBtn(false);
                    view.failedRetrievingFactorError();
                });
    }
}