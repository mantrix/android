package com.android.estimateapp.cleanarchitecture.data.entity.Enums;

public enum Depth {


    TWENTY_HUNDREDTHS(1, "0.20"),
    TWENTY_FIVE_HUNDREDTHS(2, "0.25"),
    THIRTY_HUNDREDTHS(3, "0.30"),
    THIRTY_FIVE_HUNDREDTHS(4, "0.35"),
    FORTY_HUNDREDTHS(5, "0.40"),
    FIFTY_HUNDREDTHS(6, "0.50"),
    SIXTY_HUNDREDTHS(7, "0.60"),
    SEVENTY_HUNDREDTHS(8, "0.70"),
    EIGHTY_HUNDREDTHS(9, "0.80");

    String name;
    int id;

    Depth(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
