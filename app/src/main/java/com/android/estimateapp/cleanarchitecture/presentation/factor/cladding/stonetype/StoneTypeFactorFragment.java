package com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.stonetype;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.android.estimateapp.cleanarchitecture.data.entity.works.stonecladding.StoneType;
import com.android.estimateapp.cleanarchitecture.presentation.base.BaseFragment;
import com.android.estimateapp.cleanarchitecture.presentation.widget.NpaLinearLayoutManager;
import com.android.estimateapp.configuration.Constants;
import com.mantrixengineering.estimateapp.R;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class StoneTypeFactorFragment extends BaseFragment implements StoneTypeFactorContract.View {


    public static StoneTypeFactorFragment newInstance(String projectId, String scopeKey) {

        Bundle args = new Bundle();
        args.putString(Constants.PROJECT_ID, projectId);
        args.putString(Constants.SCOPE_KEY, scopeKey);
        StoneTypeFactorFragment fragment = new StoneTypeFactorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.tv_name_label)
    TextView tvNameLabel;

    @BindView(R.id.et_tile)
    EditText etStone;
    @BindView(R.id.et_length)
    EditText etLength;
    @BindView(R.id.et_width)
    EditText etWidth;

    @Inject
    StoneTypeAdapter adapter;

    @Inject
    StoneTypeFactorContract.UserActionListener presenter;

    NpaLinearLayoutManager npaLinearLayoutManager;

    private ProgressDialog saveProgressDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.tile_type_factor_layout;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        initRecyclerView();

        presenter.setData(getArguments().getString(Constants.PROJECT_ID), getArguments().getString(Constants.SCOPE_KEY));
        presenter.setView(this);
        presenter.start();

        etStone.setHint(getString(R.string.stone_type));
        tvNameLabel.setText(getString(R.string.stones));

        saveProgressDialog = createLoadingAlert(null, getString(R.string.saving));
        saveProgressDialog.setCancelable(false);

    }


    private void initRecyclerView() {
        npaLinearLayoutManager = new NpaLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(npaLinearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void displayList(List<StoneType> factors) {
        adapter.setItems(factors);
    }

    @OnClick(R.id.rl_save)
    public void onSaveClick() {
        if (saveProgressDialog != null) {
            saveProgressDialog.show();
        }
        presenter.updateFactors(adapter.getData());
    }

    @Override
    public void factorSaved() {
        if (saveProgressDialog != null) {
            saveProgressDialog.dismiss();
        }
    }

    @OnClick(R.id.iv_add)
    public void onAddStoneTypeClick() {
        boolean valid = true;

        if (etStone.getText().toString().isEmpty()) {
            etStone.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etStone.setError(null);
        }

        if (etLength.getText().toString().isEmpty()) {
            etLength.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etLength.setError(null);
        }

        if (etWidth.getText().toString().isEmpty()) {
            etWidth.setError(getString(R.string.required_error));
            valid = false;
        } else {
            etWidth.setError(null);
        }

        if (valid) {
            String name = etStone.getText().toString().trim();
            String length = etLength.getText().toString().trim();
            String width = etWidth.getText().toString().trim();

            presenter.createStoneType(name,Double.parseDouble(length),Double.parseDouble(width));

            etStone.setText("");
            etLength.setText("");
            etWidth.setText("");
        }
    }


}