package com.android.estimateapp.cleanarchitecture.presentation.summary.model;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.CHBSize;

import java.util.HashMap;
import java.util.Map;

public class CHBLayingSummary {

    Map<String,CHBSummaryPerLocData> summaryPerStorey = new HashMap<>();
    Map<CHBSize,CHBSummaryPerSizeData> summaryPerCHBSize = new HashMap<>();

    public CHBLayingSummary(Map<CHBSize,CHBSummaryPerSizeData> summaryPerCHBSize, Map<String,CHBSummaryPerLocData> summaryPerStorey){
        this.summaryPerCHBSize = summaryPerCHBSize;
        this.summaryPerStorey = summaryPerStorey;
    }

    public Map<CHBSize, CHBSummaryPerSizeData> getSummaryPerCHBSize() {
        return summaryPerCHBSize;
    }

    public Map<String, CHBSummaryPerLocData> getSummaryPerStorey() {
        return summaryPerStorey;
    }
}
