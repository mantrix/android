package com.android.estimateapp.configuration.injection.module.app;

import com.android.estimateapp.cleanarchitecture.presentation.cost.labor.LaborCostActivity;
import com.android.estimateapp.cleanarchitecture.presentation.factor.FactorActivity;
import com.android.estimateapp.cleanarchitecture.presentation.home.HomeActivity;
import com.android.estimateapp.cleanarchitecture.presentation.launch.LaunchScreenActivity;
import com.android.estimateapp.cleanarchitecture.presentation.profile.ProfileActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.create.CreateProjectActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.detail.ProjectDetailActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.create.CreateCHBActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.list.CHBListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.create.CreateConcreteWorkActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.concrete.list.ConcreteWorkListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.create.CreateDoorAndWindowActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.doorandwindow.list.DoorAndWindowListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.create.CreateGeneralConcreteWorkActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.generalconcrete.list.GeneralConcreteWorkListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.create.CreatePlasteringActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.plastering.list.PlasteringListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.create.CreateBeamRebarInputActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.beam.list.BeamRebarListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.create.CreateColumnRebarInputActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.column.list.ColumnRebarInputListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.create.CreateColumnFootingInputActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.columnfooting.list.ColumnFootingRebarListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.create.CreateWallFootingInputActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.rebar.wallfooting.list.WallFootingRebarListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.create.CreateStoneCladdingActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.stonecladding.list.StoneCladdingListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.create.CreateTileWorkActivity;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.tiles.list.TileWorkListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.ProjectPropertiesActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.create.CreateBeamSectionPropertyActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.beamsection.list.BeamSectionPropertyListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.create.CreateColumnSectionPropertyActivity;
import com.android.estimateapp.cleanarchitecture.presentation.properties.columnsection.list.ColumnSectionPropertyListActivity;
import com.android.estimateapp.cleanarchitecture.presentation.summary.SummaryActivity;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.WelcomeActivity;
import com.android.estimateapp.configuration.injection.module.cost.CHBLayingBuilderModule;
import com.android.estimateapp.configuration.injection.module.cost.ColumnAndWallMaterialCostBuilderModule;
import com.android.estimateapp.configuration.injection.module.cost.GeneralConcreteMaterialBuilderModule;
import com.android.estimateapp.configuration.injection.module.cost.LaborCostBuilderModule;
import com.android.estimateapp.configuration.injection.module.cost.LaborCostModule;
import com.android.estimateapp.configuration.injection.module.cost.PlasteringBuilderModule;
import com.android.estimateapp.configuration.injection.module.cost.TileWorkBuilderModule;
import com.android.estimateapp.configuration.injection.module.factor.FactorBuilderModule;
import com.android.estimateapp.configuration.injection.module.home.HomeBuilderModule;
import com.android.estimateapp.configuration.injection.module.home.HomeModule;
import com.android.estimateapp.configuration.injection.module.launch.LaunchModule;
import com.android.estimateapp.configuration.injection.module.profile.ProfileModule;
import com.android.estimateapp.configuration.injection.module.project.BeamRebarListModule;
import com.android.estimateapp.configuration.injection.module.project.CHBListModule;
import com.android.estimateapp.configuration.injection.module.project.ColumnFootingRebarListModule;
import com.android.estimateapp.configuration.injection.module.project.ColumnRebarListModule;
import com.android.estimateapp.configuration.injection.module.project.ConcreteListModule;
import com.android.estimateapp.configuration.injection.module.project.CreateBeamRebarModule;
import com.android.estimateapp.configuration.injection.module.project.CreateCHBModule;
import com.android.estimateapp.configuration.injection.module.project.CreateColumnFootingModule;
import com.android.estimateapp.configuration.injection.module.project.CreateColumnRebarModule;
import com.android.estimateapp.configuration.injection.module.project.CreateConcreteWorkModule;
import com.android.estimateapp.configuration.injection.module.project.CreateDoorAndWindowModule;
import com.android.estimateapp.configuration.injection.module.project.CreateGeneralConcreteWorkModule;
import com.android.estimateapp.configuration.injection.module.project.CreatePlasteringModule;
import com.android.estimateapp.configuration.injection.module.project.CreateProjectModule;
import com.android.estimateapp.configuration.injection.module.project.CreateStoneCladdingModule;
import com.android.estimateapp.configuration.injection.module.project.CreateTileWorkModule;
import com.android.estimateapp.configuration.injection.module.project.CreateWallFootingRebarModule;
import com.android.estimateapp.configuration.injection.module.project.DoorAndWindowListModule;
import com.android.estimateapp.configuration.injection.module.project.GeneralConcreteListModule;
import com.android.estimateapp.configuration.injection.module.project.PlasteringListModule;
import com.android.estimateapp.configuration.injection.module.project.ProjectDetailModule;
import com.android.estimateapp.configuration.injection.module.project.StoneCladdingListModule;
import com.android.estimateapp.configuration.injection.module.project.TileWorkListModule;
import com.android.estimateapp.configuration.injection.module.project.WallFootingRebarListModule;
import com.android.estimateapp.configuration.injection.module.properties.BeamSectionPropertyListModule;
import com.android.estimateapp.configuration.injection.module.properties.ColumnSectionPropertyListModule;
import com.android.estimateapp.configuration.injection.module.properties.CreateBeamPropertyModule;
import com.android.estimateapp.configuration.injection.module.properties.CreateColumnSectionPropertyModule;
import com.android.estimateapp.configuration.injection.module.properties.ProjectPropertiesBuilderModule;
import com.android.estimateapp.configuration.injection.module.summary.SummaryActivityBuilderModule;
import com.android.estimateapp.configuration.injection.module.welcome.WelcomeBuilderModule;
import com.android.estimateapp.configuration.injection.module.welcome.WelcomeModule;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * A module that map all our activities. Dagger will know our Activities in Compile time.
 */
@Module
public abstract class BuilderModule {

    @PerActivity
    @ContributesAndroidInjector(modules = {LaunchModule.class})
    abstract LaunchScreenActivity bindLaunchScreen();

    @PerActivity
    @ContributesAndroidInjector(modules = {WelcomeModule.class, WelcomeBuilderModule.class})
    abstract WelcomeActivity bindWelcomeActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {HomeModule.class, HomeBuilderModule.class})
    abstract HomeActivity bindHomeActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {ProfileModule.class})
    abstract ProfileActivity bindProfileActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {ProjectDetailModule.class})
    abstract ProjectDetailActivity bindProjectDetailActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {CreateProjectModule.class})
    abstract CreateProjectActivity bindCreateProjectActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {CHBListModule.class, CHBLayingBuilderModule.class})
    abstract CHBListActivity bindChbListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CreateCHBModule.class)
    abstract CreateCHBActivity bindCreateCHBActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {FactorBuilderModule.class})
    abstract FactorActivity bindFactorActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {PlasteringListModule.class, PlasteringBuilderModule.class})
    abstract PlasteringListActivity bindPlasteringListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {CreatePlasteringModule.class})
    abstract CreatePlasteringActivity bindCreatePlasteringActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {GeneralConcreteListModule.class, GeneralConcreteMaterialBuilderModule.class})
    abstract GeneralConcreteWorkListActivity bindGeneralConcreteWorkListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {CreateGeneralConcreteWorkModule.class})
    abstract CreateGeneralConcreteWorkActivity bindCreateGeneralConcreteWorkActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {ConcreteListModule.class})
    abstract ConcreteWorkListActivity bindConcreteWorkListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {CreateConcreteWorkModule.class})
    abstract CreateConcreteWorkActivity bindCreateConcreteWorkActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {CreateTileWorkModule.class})
    abstract CreateTileWorkActivity bindCreateTileWorkActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {TileWorkListModule.class, TileWorkBuilderModule.class})
    abstract TileWorkListActivity bindTileWorkListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {StoneCladdingListModule.class})
    abstract StoneCladdingListActivity bindStoneCladdingListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {CreateStoneCladdingModule.class})
    abstract CreateStoneCladdingActivity bindCreateStoneCladdingActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = SummaryActivityBuilderModule.class)
    abstract SummaryActivity bindSummaryActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = ProjectPropertiesBuilderModule.class)
    abstract ProjectPropertiesActivity bindProjectPropertiesActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = DoorAndWindowListModule.class)
    abstract DoorAndWindowListActivity bindDoorAndWindowListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CreateDoorAndWindowModule.class)
    abstract CreateDoorAndWindowActivity bindCreateDoorAndWindowActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {WallFootingRebarListModule.class, ColumnAndWallMaterialCostBuilderModule.class})
    abstract WallFootingRebarListActivity bindWallFootingRebarListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CreateWallFootingRebarModule.class)
    abstract CreateWallFootingInputActivity bindCreateWallFootingInputActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {ColumnFootingRebarListModule.class, ColumnAndWallMaterialCostBuilderModule.class})
    abstract ColumnFootingRebarListActivity bindColumnFootingRebarListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CreateColumnFootingModule.class)
    abstract CreateColumnFootingInputActivity bindCreateColumnFootingInputActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = BeamRebarListModule.class)
    abstract BeamRebarListActivity bindBeamRebarListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = BeamSectionPropertyListModule.class)
    abstract BeamSectionPropertyListActivity bindBeamSectionPropertyListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CreateBeamPropertyModule.class)
    abstract CreateBeamSectionPropertyActivity bindBeamSectionPropertyActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CreateBeamRebarModule.class)
    abstract CreateBeamRebarInputActivity bindCreateBeamRebarInputActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = ColumnRebarListModule.class)
    abstract ColumnRebarInputListActivity bindColumnRebarInputListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = ColumnSectionPropertyListModule.class)
    abstract ColumnSectionPropertyListActivity bindColumnSectionPropertyListActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CreateColumnSectionPropertyModule.class)
    abstract CreateColumnSectionPropertyActivity bindCreateColumnSectionPropertyActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CreateColumnRebarModule.class)
    abstract CreateColumnRebarInputActivity bindCreateColumnRebarInputActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = {LaborCostBuilderModule.class, LaborCostModule.class})
    abstract LaborCostActivity bindLaborCostActivity();
}
