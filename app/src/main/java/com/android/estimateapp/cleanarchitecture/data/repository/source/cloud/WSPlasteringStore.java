package com.android.estimateapp.cleanarchitecture.data.repository.source.cloud;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.PlasteringMortarFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RSBFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.plastering.PlasteringResult;
import com.android.estimateapp.cleanarchitecture.data.repository.source.BaseStore;
import com.android.estimateapp.cleanarchitecture.data.repository.source.contract.CloudPlasteringStore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class WSPlasteringStore extends BaseStore implements CloudPlasteringStore {

    private DatabaseReference projectScopeOfWorksRef;

    @Inject
    public WSPlasteringStore() {
        super();
        projectScopeOfWorksRef = baseReference.child(FIREBASE_PROJECT_SCOPE_OF_WORKS_REF);
    }

    @Override
    public Observable<PlasteringInput> createPlasteringInput(String projectId, String scopeOfWorkId, PlasteringInput plasteringInput) {
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(databaseReference -> push(databaseReference,plasteringInput));
    }

    @Override
    public Observable<PlasteringInput> updatePlasteringInput(String projectId, String scopeOfWorkId, String itemId, PlasteringInput plasteringInput){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(databaseReference -> setValue(databaseReference,plasteringInput));
    }

    @Override
    public Observable<PlasteringResult> savePlasteringInputResult(String projectId, String scopeOfWorkId, String itemId, PlasteringResult result){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId)
                    .child(FIREBASE_RESULT);
        }).flatMap(databaseReference -> setValue(databaseReference,result));
    }

    @Override
    public Observable<List<PlasteringInput>> getPlasteringInputs(String projectId, String scopeOfWorkId){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS);
        }).flatMap(query -> queryListValue(query,PlasteringInput.class));
    }

    @Override
    public Observable<PlasteringInput> getPlasteringInput(String projectId, String scopeOfWorkId, String itemId){
        return Observable.fromCallable(() -> {
            return projectScopeOfWorksRef.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(scopeOfWorkId)
                    .child(FIREBASE_INPUTS)
                    .child(itemId);
        }).flatMap(query -> get(query,PlasteringInput.class));
    }

    @Override
    public Observable<List<PlasteringMortarFactor>> createPlasteringMortarFactors(String projectId, List<PlasteringMortarFactor> plasteringMortarFactors){

        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_CHB_PLASTERING_MORTAR_FACTOR);

        return Observable.fromIterable(plasteringMortarFactors)
                .flatMap(plasteringMortarFactor -> push(databaseReference, plasteringMortarFactor))
                .toList().toObservable();
    }

    @Override
    public Observable<List<PlasteringMortarFactor>> getPlasteringMortarFactors(String projectId) {
        return Observable.fromCallable(() -> {
            Query query = projectFactorReference.child(projectId)
                    .child(FIREBASE_DATA_REF)
                    .child(FIREBASE_CHB_PLASTERING_MORTAR_FACTOR);

            query.keepSynced(true);
            return query;
        }).flatMap(query -> queryListValue(query, PlasteringMortarFactor.class));
    }


    @Override
    public Observable<List<PlasteringMortarFactor>> updatePlasteringMortarFactors(String projectId, List<PlasteringMortarFactor> plasteringMortarFactors){
        DatabaseReference databaseReference = projectFactorReference.child(projectId)
                .child(FIREBASE_DATA_REF)
                .child(FIREBASE_CHB_PLASTERING_MORTAR_FACTOR);

        return Observable.fromIterable(plasteringMortarFactors)
                .flatMap(plasteringMortarFactor -> setValue(databaseReference.child(plasteringMortarFactor.getKey()), plasteringMortarFactor))
                .toList().toObservable();
    }

}