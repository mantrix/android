package com.android.estimateapp.configuration.injection.module.factor;

import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.mortarclass.CHBConcreteClassFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.reinforcement.CHBReinforcementFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.chb.tiewire.CHBTieWireFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.StoneCladdingFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.cladding.stonetype.StoneTypeFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.concrete.ConcreteClassFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.plastering.PlasteringMortarFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.shared.rsb.RSBFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.TileFactorFragment;
import com.android.estimateapp.cleanarchitecture.presentation.factor.tilework.tiletype.TileTypeFactorFragment;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FactorBuilderModule {

    @PerFragment
    @ContributesAndroidInjector(modules = {RSBFactorModule.class})
    abstract RSBFactorFragment bindRsbFactorFragment();


    @PerFragment
    @ContributesAndroidInjector(modules = {CHBFactorModule.class})
    abstract CHBConcreteClassFactorFragment bindChbFactorFragment();


    @PerFragment
    @ContributesAndroidInjector(modules = {CHBPlasteringFactorModule.class})
    abstract PlasteringMortarFactorFragment bindPlasteringMortarFactorFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = {ConcreteClassFactorModule.class})
    abstract ConcreteClassFactorFragment bindConcreteClassFactorFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = {TileWorkFactorModule.class})
    abstract TileFactorFragment bindTileWorkFactorFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = {TileTypeFactorModule.class})
    abstract TileTypeFactorFragment bindTileTypeFactorFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = {CladdingFactorModule.class})
    abstract StoneCladdingFactorFragment bindStoneCladdingFactorFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = {StoneTypeFactorModule.class})
    abstract StoneTypeFactorFragment bindStoneTypeFactorFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = ReinforcementsModule.class)
    abstract CHBReinforcementFactorFragment bindChbReinforcementFactorFragment();


    @PerFragment
    @ContributesAndroidInjector(modules = TieWireFactorModule.class)
    abstract CHBTieWireFactorFragment bindChbTieWireFactorFragment();


}
