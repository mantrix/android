package com.android.estimateapp.cleanarchitecture.domain.usecase.works.concrete;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Shape;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.ConcreteFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.ConcreteWorksInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.conretework.GeneralConcreteWorksResult;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularConcreteInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.rectangularconcrete.RectangularGeneralConcreteResult;
import com.android.estimateapp.cleanarchitecture.domain.usecase.base.ComputationConstants;
import com.android.estimateapp.cleanarchitecture.domain.usecase.works.BaseCalculator;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.ConcreteWorkMaterials;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.GeneralConcreteWorkSummary;

import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class ConcreteWorkCalculatorImpl extends BaseCalculator implements ConcreteWorkCalculator {


    private ConcreteFactor factor;

    @Inject
    public ConcreteWorkCalculatorImpl() {

    }

    @Override
    public GeneralConcreteWorkSummary generateGeneralConcreteSummary(List<GeneralConcreteWorksResult> results) {
        Map<Category, ConcreteWorkMaterials> materialPerCategoryMap = new HashMap<>();
        ConcreteWorkMaterials total = new ConcreteWorkMaterials();

        for (GeneralConcreteWorksResult result : results) {

            // compute totals per category
            if (materialPerCategoryMap.containsKey(result.getCategory())) {
                // get previous data and add the result to it.
                materialPerCategoryMap.put(result.getCategory(), addResult(materialPerCategoryMap.get(result.getCategory()), result));
            } else {
                // create a new map entry and set result to it.
                materialPerCategoryMap.put(result.getCategory(), addResult(new ConcreteWorkMaterials(), result));
            }

            // compute totals regardless of category
            total.cement += result.getCement();
            total.sand += result.getSand();
            total.threeFourthGravel += result.getThreeFourthGravel();
            total.g1Gravel += result.getG1Gravel();
        }

        // normalize results
        for (Category category : materialPerCategoryMap.keySet()){
            ConcreteWorkMaterials data = materialPerCategoryMap.get(category);

            data.category = category;
            data.quantity = roundHalfUp(data.quantity);
            data.cement = roundUpToNearestWholeNumber(data.cement);
            data.sand = truncateToTwoDecimal(data.sand,RoundingMode.CEILING);
            data.threeFourthGravel = truncateToTwoDecimal(data.threeFourthGravel,RoundingMode.CEILING);
            data.g1Gravel = truncateToTwoDecimal(data.g1Gravel,RoundingMode.CEILING);

            materialPerCategoryMap.put(category,data);
        }

        // normalize results
        total.cement = roundUpToNearestWholeNumber(total.cement);
        total.sand = roundUpToNearestWholeNumber(total.sand);
        total.threeFourthGravel = roundUpToNearestWholeNumber(total.threeFourthGravel);
        total.g1Gravel = roundUpToNearestWholeNumber(total.g1Gravel);

        return new GeneralConcreteWorkSummary<Category>(materialPerCategoryMap,total);
    }

    @Override
    public GeneralConcreteWorkSummary generateConcreteSummary(List<RectangularGeneralConcreteResult> results) {

        Map<String, ConcreteWorkMaterials> materialPerSection = new HashMap<>();
        ConcreteWorkMaterials total = new ConcreteWorkMaterials();

        for (RectangularGeneralConcreteResult result : results) {

            // compute totals per category
            String key = result.getMemberSectionProperty().getKey();
            if (materialPerSection.containsKey(key)) {
                // get previous data and add the result to it.
                ConcreteWorkMaterials concreteWorkMaterials = addResult(materialPerSection.get(key), result);
                concreteWorkMaterials.memberSectionProperty = result.getMemberSectionProperty();
                materialPerSection.put(key, concreteWorkMaterials);
            } else {
                // create a new map entry and set result to it.
                ConcreteWorkMaterials concreteWorkMaterials = new ConcreteWorkMaterials();
                concreteWorkMaterials.memberSectionProperty = result.getMemberSectionProperty();
                materialPerSection.put(key, addResult(concreteWorkMaterials, result));
            }

            // compute totals regardless of category
            total.cement += result.getCement();
            total.sand += result.getSand();
            total.threeFourthGravel += result.getThreeFourthGravel();
            total.g1Gravel += result.getG1Gravel();
        }

        // normalize results
        for (String key : materialPerSection.keySet()){
            ConcreteWorkMaterials data = materialPerSection.get(key);

            data.memberSectionProperty = materialPerSection.get(key).memberSectionProperty;
            data.quantity = roundHalfUp(data.quantity);
            data.cement = roundUpToNearestWholeNumber(data.cement);
            data.sand = truncateToTwoDecimal(data.sand,RoundingMode.CEILING);
            data.threeFourthGravel = truncateToTwoDecimal(data.threeFourthGravel,RoundingMode.CEILING);
            data.g1Gravel = truncateToTwoDecimal(data.g1Gravel,RoundingMode.CEILING);

            materialPerSection.put(data.memberSectionProperty.getKey(),data);
        }

        // normalize results
        total.cement = roundUpToNearestWholeNumber(total.cement);
        total.sand = roundUpToNearestWholeNumber(total.sand);
        total.threeFourthGravel = roundUpToNearestWholeNumber(total.threeFourthGravel);
        total.g1Gravel = roundUpToNearestWholeNumber(total.g1Gravel);

        return new GeneralConcreteWorkSummary<String>(materialPerSection,total);
    }

    private ConcreteWorkMaterials addResult(ConcreteWorkMaterials materials, GeneralConcreteWorksResult result) {
        materials.quantity += result.getVolume();
        materials.cement += result.getCement();
        materials.sand += result.getSand();
        materials.threeFourthGravel += result.getThreeFourthGravel();
        materials.g1Gravel += result.getG1Gravel();
        return materials;
    }




    @Override
    public GeneralConcreteWorksResult calculateByShape(ConcreteFactor concreteFactor, ConcreteWorksInput input, Shape shape) {
        this.factor = concreteFactor;
        double volume;

        switch (shape) {
            case CIRCLE:
                volume = computeCircleVolume(input);
                break;
            case RECTANGLE:
                volume = computeGeneralConcreteVolume(input);
                break;
            case TRAPEZOID:
                volume = computeTrapezoidalVolume(input);
                break;
            default:
                throw new RuntimeException("Shape = " + shape + " not supported");
        }

        return computeResult(volume, input);
    }

    @Override
    public RectangularGeneralConcreteResult calculateRectangularConcrete(ConcreteFactor concreteFactor, RectangularConcreteInput input) {
        this.factor = concreteFactor;

        double lClear = (input.getLengthCC() - input.getOffset()) * input.getPouringCut();
        double volume   = computeRectangularVolume(input, lClear);

        return computeResult(volume, lClear, input);
    }


    @Override
    public GeneralConcreteWorksResult calculateGeneralConcrete(ConcreteFactor concreteFactor, ConcreteWorksInput input) {
        this.factor = concreteFactor;

        double volume   = computeGeneralConcreteVolume(input);

        return computeResult(volume, input);
    }

    private double computeRectangularVolume(RectangularConcreteInput input, double lClear) {

        double sectionalArea = 0;

        if (input.getPropertyReference() != null){
            sectionalArea = input.getPropertyReference().getCrossSectionalArea();
        }
        return sectionalArea * lClear * input.getSets();
    }

    /**
     * Formula
     * pi x (diameter^2) x 1/4 x Length
     */
    private double computeCircleVolume(ConcreteWorksInput input) {

        double length = 0;
        if (input.getDimension() != null) {
            length = input.getDimension().getLength();
        }

        return ComputationConstants.PI * (Math.pow(input.getDiameter(), 2)) * 0.25 * length;
    }


    /**
     * Formula
     * length x width x thickness x numOfFooting
     */
    private double computeGeneralConcreteVolume(ConcreteWorksInput input) {
        double width = 0;
        double height = 0;
        if (input.getDimension() != null) {
            width = input.getDimension().getWidth();
            height = input.getDimension().getHeight();
        }

        return width * height * input.getThickness() * input.getSets();
    }

    /**
     * Formula
     * ((base1 + base2)/2) x Length x thickness x numOfFooting
     */
    private double computeTrapezoidalVolume(ConcreteWorksInput input) {
        double width = 0;
        double width2 = 0;
        double height = 0;
        if (input.getDimension() != null) {
            width = input.getDimension().getWidth();
            width2 = input.getDimension().getLx2();
            height = input.getDimension().getHeight();
        }
        return ((width + width2) / 2) * height * input.getThickness() * input.getSets();
    }

    private GeneralConcreteWorksResult computeResult(double concreteVolume, ConcreteWorksInput input) {
        GeneralConcreteWorksResult result = new GeneralConcreteWorksResult();

        if (input.getCategoryEnum() != null) {
            result.setCategory(input.getCategoryEnum());
        }

        result.setVolume(roundHalfUp(concreteVolume));

        double volume = concreteVolume * (1 + toDoublePercentage(input.getWastage()));

        if (factor != null) {
            result.setCement(roundHalfUp(volume * factor.getCement()));
            result.setSand(roundHalfUp(volume * factor.getSand()));

            if (input.getGravelSizeType() != null && input.getGravelSizeTypeEnum() != null) {
                if (input.getGravelSizeTypeEnum() == GravelSize.G1) {
                    result.setG1Gravel(roundHalfUp(volume * factor.getG1()));
                } else {
                    result.setThreeFourthGravel(roundHalfUp(volume * factor.getThreeFourths()));
                }
            }
        }

        return result;
    }

    private RectangularGeneralConcreteResult computeResult(double concreteVolume, double lClear, RectangularConcreteInput input) {
        RectangularGeneralConcreteResult result = new RectangularGeneralConcreteResult();

        if (input.getPropertyReference() != null) {
            result.setMemberSectionProperty(input.getPropertyReference());
        }

        result.setVolume(roundHalfUp(concreteVolume));
        result.setlClear(lClear);

        double volume = concreteVolume * (1 + toDoublePercentage(input.getWastage()));

        if (factor != null) {
            result.setCement(roundHalfUp(volume * factor.getCement()));
            result.setSand(roundHalfUp(volume * factor.getSand()));

            if (input.getGravelSizeType() != null && input.getGravelSizeTypeEnum() != null) {
                if (input.getGravelSizeTypeEnum() == GravelSize.G1) {
                    result.setG1Gravel(roundHalfUp(volume * factor.getG1()));
                } else {
                    result.setThreeFourthGravel(roundHalfUp(volume * factor.getThreeFourths()));
                }
            }
        }

        return result;
    }
}
