package com.android.estimateapp.cleanarchitecture.presentation.profile;

import android.graphics.Bitmap;

import com.android.estimateapp.cleanarchitecture.presentation.base.Presenter;

public interface ProfileContract {


    int FIELD_FIRST_NAME = 1;
    int FIELD_MIDDLE_NAME = 2;
    int FIELD_LAST_NAME = 3;

    interface View {

        void displayFieldRequiredError(int field);

        void displayInvalidInputError(int field);

        void displayProfilePhoto(String url);

        void removedError(int field);

        void populateNameFields(String fullName, String firstName, String middleName, String lastName);

        void displayEmail(String email);

        void displaySubscription(String subscription);

        void showNoConnectionError();

        void showUnhandledError();

    }

    interface UserActionListener extends Presenter<View> {

        void updateName(String firstName, String middleName, String lastName);


        /**
         * @param firstName updated First Name
         * @param middleName updated Middle Name
         * @param lastName updated Last Name
         * @return whether inputs are all valid
         */
        boolean validateNameInputs(String firstName, String middleName, String lastName);

        void uploadProfilePhoto(Bitmap bitmap);
    }
}
