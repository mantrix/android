package com.android.estimateapp.cleanarchitecture.data.entity.contract;

public interface AccountCredential {

    String getID();

    String getToken();
}
