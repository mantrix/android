package com.android.estimateapp.configuration.injection.module.welcome;


import com.android.estimateapp.cleanarchitecture.domain.usecase.validator.InputValidator;
import com.android.estimateapp.cleanarchitecture.domain.usecase.validator.InputValidatorUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.LoginPresenter;
import com.android.estimateapp.cleanarchitecture.presentation.welcome.contract.LoginContract;
import com.android.estimateapp.configuration.injection.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    @Provides
    @PerFragment
    LoginContract.UserActionListener providesLoginPresenter(LoginPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerFragment
    InputValidatorUseCase provideInputValidator(InputValidator inputValidator){
        return inputValidator;
    }

}
