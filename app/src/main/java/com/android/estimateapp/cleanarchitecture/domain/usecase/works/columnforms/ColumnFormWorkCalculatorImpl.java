package com.android.estimateapp.cleanarchitecture.domain.usecase.works.columnforms;

import com.android.estimateapp.cleanarchitecture.data.entity.factors.FormworkFactor;
import com.android.estimateapp.cleanarchitecture.data.entity.factors.RectangularConcreteSection;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnforms.ColumnFormInput;
import com.android.estimateapp.cleanarchitecture.data.entity.works.columnforms.ColumnFormOutput;

import javax.inject.Inject;

public class ColumnFormWorkCalculatorImpl implements ColumnFormCalculator {

    @Inject
    public ColumnFormWorkCalculatorImpl() {

    }

    @Override
    public ColumnFormOutput calculate(boolean autoCompute, ColumnFormInput input, FormworkFactor factor) {

        double hClear = input.gethCC() - input.getOffset1() - input.getOffset2();
        if (autoCompute){
            hClear = input.computeHCC();
        }

        RectangularConcreteSection property = input.getRectangularConcreteSection();

        // TODO: Verify output after new sample is provided
        double area = (((property.getYd() + property.getYz()) * 2) + .2) * hClear;

        double contactArea = ((property.getYd() + property.getYz()) * 2 ) * hClear;
        double plywood = area * factor.getEffectiveArea();

        // TODO: Add Frame Factor
        int frameFactor = 0;
        double woodFrame = (property.getYz() + frameFactor) + (property.getYd() + frameFactor);
        double nails = area * factor.getNail();

        ColumnFormOutput output = new ColumnFormOutput();
        output.setArea(area);
        output.setContactArea(contactArea);
        output.setWoodFrame(woodFrame);
        output.setPlywood(plywood);
        output.setNails(nails);

        return output;
    }

}
