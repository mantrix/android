package com.android.estimateapp.cleanarchitecture.presentation.cost.material.columnwallfooting;

import com.android.estimateapp.cleanarchitecture.domain.executors.contract.PostExecutionThread;
import com.android.estimateapp.cleanarchitecture.domain.executors.contract.ThreadExecutorProvider;
import com.android.estimateapp.cleanarchitecture.domain.repository.ProjectRepository;
import com.android.estimateapp.cleanarchitecture.domain.usecase.contract.AccountSubscriptionUseCase;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.chb.CHBLayingMaterialCostContract;
import com.android.estimateapp.cleanarchitecture.presentation.cost.material.chb.CHBLayingMaterialCostPresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CreateScopeInputBasePresenter;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.CHBSummaryPerLocData;
import com.android.estimateapp.cleanarchitecture.presentation.summary.model.RebarMaterialSummary;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ColumnAndWallFootingMaterialCostPresenter extends CreateScopeInputBasePresenter implements ColumnAndWallFootingMaterialCostContract.UserActionListener {

    private static final String TAG = CHBLayingMaterialCostPresenter.class.getSimpleName();

    private ColumnAndWallFootingMaterialCostContract.View view;

    private String projectId;
    private String scopeKey;
    private ProjectRepository projectRepository;

    private RebarMaterialSummary input;

    private String itemKey;

    @Inject
    public ColumnAndWallFootingMaterialCostPresenter(ProjectRepository projectRepository,
                                          AccountSubscriptionUseCase accountSubscriptionUseCase,
                                          ThreadExecutorProvider threadExecutorProvider,
                                          PostExecutionThread postExecutionThread) {
        super(accountSubscriptionUseCase, threadExecutorProvider, postExecutionThread);
        this.projectRepository = projectRepository;

    }

    @Override
    public void setView(ColumnAndWallFootingMaterialCostContract.View view) {
        this.view = view;
    }

    @Override
    public void setData(String projectId, String scopeKey) {
        this.projectId = projectId;
        this.scopeKey = scopeKey;
    }

    @Override
    public void start() {
        loadInput();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void save(RebarMaterialSummary materials) {
        this.input = materials;

        if (allowPaidExperience) {
            if (input != null) {
                if (itemKey != null) {
                    // UPDATE
                    saveEntry(projectRepository.updateMaterial(projectId, scopeKey, input));
                } else {
                    // CREATE
                    saveEntry(projectRepository.createMaterial(projectId, scopeKey, input));
                }
            }
        } else {
            /*
             * Saving not allowed for Free Users with no trial days remaining.
             */
            view.showSavingNotAllowedError();
        }
    }

    private void saveEntry(Observable<RebarMaterialSummary> inputObservable) {

        inputObservable
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(input -> {
                    this.input = input;
                    view.inputSaved();
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }


    private void loadInput() {
        projectRepository.getMaterial(projectId, scopeKey, RebarMaterialSummary.class)
                .subscribeOn(threadExecutorProvider.computationScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(materials -> {
                    itemKey = materials.getKey();
                    this.input = materials;

                    view.setInput(input);

                }, throwable -> {
                    throwable.printStackTrace();
                });

    }

}