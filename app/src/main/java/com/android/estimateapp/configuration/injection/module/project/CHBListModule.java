package com.android.estimateapp.configuration.injection.module.project;

import com.android.estimateapp.cleanarchitecture.presentation.project.CHB.list.CHBListPresenter;
import com.android.estimateapp.cleanarchitecture.presentation.project.scope.CHB.list.CHBListContract;
import com.android.estimateapp.configuration.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class CHBListModule {


    @Provides
    @PerActivity
    CHBListContract.UserActionListener provideScopeOfWorkPresenter(CHBListPresenter presenter) {
        return presenter;
    }
}
