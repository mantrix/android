package com.android.estimateapp.cleanarchitecture.data.entity.works.conretework;

import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Category;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.Grade;
import com.android.estimateapp.cleanarchitecture.data.entity.Enums.GravelSize;
import com.android.estimateapp.cleanarchitecture.data.entity.Model;
import com.android.estimateapp.cleanarchitecture.data.entity.works.Dimension;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class ConcreteWorksInput extends Model {

    String category;

    String description;

    Dimension dimension;

    double thickness;

    double sets;

    double wastage;

    double diameter;

    int mortarClass;

    String gravelSizeType;


    public ConcreteWorksInput() {

    }

    @Exclude
    public Category getCategoryEnum() {
        return Category.parseString(category);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    public double getSets() {
        return sets;
    }

    public void setSets(double sets) {
        this.sets = sets;
    }

    public double getWastage() {
        return wastage;
    }

    public void setWastage(double wastage) {
        this.wastage = wastage;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    @Exclude
    public Grade getConcreteClassEnum() {
        return Grade.parseInt(mortarClass);
    }

    public int getMortarClass() {
        return mortarClass;
    }

    public void setMortarClass(int mortarClass) {
        this.mortarClass = mortarClass;
    }

    @Exclude
    public GravelSize getGravelSizeTypeEnum() {
        return GravelSize.parseString(gravelSizeType);
    }

    public String getGravelSizeType() {
        return gravelSizeType;
    }

    public void setGravelSizeType(String gravelSizeType) {
        this.gravelSizeType = gravelSizeType;
    }
}
